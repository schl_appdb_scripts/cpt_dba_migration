  #!/bin/bash
#set -x

if [ $# -gt 1 ]; then
DB=$1
INSTANCE=$2
else
  printf "\nUsage: "
  printf "\n\n $0 DB INSTANCE\n\n"
  exit 1
fi


DB=$1
INSTANCE=$2
LOGPATH="/backups/logs"


#--- DATABASE MANAGER CONFIG ---#
db2 "update dbm cfg using DFT_MON_BUFPOOL ON" > $LOGPATH/dbmcfg.log
db2 "update dbm cfg using DFT_MON_LOCK ON"  >> $LOGPATH/dbmcfg.log
db2 "update dbm cfg using DFT_MON_SORT ON" >> $LOGPATH/dbmcfg.log
db2 "update dbm cfg using DFT_MON_STMT ON" >> $LOGPATH/dbmcfg.log
db2 "update dbm cfg using DFT_MON_TABLE ON" >> $LOGPATH/dbmcfg.log
db2 "update dbm cfg using DFT_MON_TIMESTAMP ON" >> $LOGPATH/dbmcfg.log
db2 "update dbm cfg using DFT_MON_UOW ON" >> $LOGPATH/dbmcfg.log

#--- DATABASE CONFIG --- #
db2 "update db cfg for $DB using LOGBUFSZ 8192"  > $LOGPATH/dbcfg.log
db2 "update db cfg for $DB using LOGFILSIZ 40960" >> $LOGPATH/dbcfg.log
db2 "update db cfg for $DB using LOGPRIMARY 20" >> $LOGPATH/dbcfg.log
db2 "update db cfg for $DB using LOGSECOND 40" >> $LOGPATH/dbcfg.log
db2 "update db cfg for $DB using AUTORESTART ON" >> $LOGPATH/dbcfg.log

db2 "update db cfg for $DB using DATABASE_MEMORY AUTOMATIC" >> $LOGPATH/dbcfg.log

#--- Instance Memory to 4G --- #
db2 "update dbm cfg using INSTANCE_MEMORY 2097152" >> $LOGPATH/dbmcfg.log


#--- ALTER BUFFERPOOL AUTOMATIC ---#

db2 connect to $DB

db2 "alter bufferpool BP32K01D size automatic" > $LOGPATH/buff_alter.log
db2 "alter bufferpool BP4K01D  size automatic" >> $LOGPATH/buff_alter.log
db2 "alter bufferpool BP4K01I size automatic"  >> $LOGPATH/buff_alter.log
db2 "alter bufferpool BP8K01D size automatic"  >> $LOGPATH/buff_alter.log

db2 -x "connect reset"
#--- TRACKMOD ---#

db2 "update db cfg for $DB using TRACKMOD YES" >> $LOGPATH/dbcfg.log


db2 -x "terminate"
#db2 -x "force applications all"
db2 -x "deactivate db $DB"


#--- DB CFG LOG PATH ---#
db2 "update db cfg for $DB using logarchmeth1 LOGRETAIN" >> $LOGPATH/dbcfg.log
#db2 "update db cfg for $DB using NEWLOGPATH /db2logs/$INSTANCE/$DB/" >> $LOGPATH/dbcfg.log
db2 "update db cfg for $DB using LOGARCHMETH1 DISK:/backups/$INSTANCE/$DB/" >> $LOGPATH/dbcfg.log
#--- BACKUP DB ---#

db2 -x "terminate"
db2 -x "force applications all"
db2 -x  "deactivate db $DB"
db2 "backup db $DB to /dev/null"
#db2 "connect reset"
#db2 "activate db $DB"

#--- INSTANCE BOUNCE ---#


#db2 -x "connect reset"
#db2 -x "terminate"
db2 -x "force applications all"
db2stop
db2start
db2 -x "activate db $DB"
db2 connect to $DB

