##########################
#STEPS TO SETUP DATABASE:#
##########################

PREREQUISTE:
-----------
1. The log files are placed at /backups/logs the directory needs to be in
place in the target server.
2. We need to have LOGPATHS in place ,if not create them
/db2logs/$INSTANCE/$DB/ 
DISK:/backups/$INSTANCE/$DB/


I. DATA EXTRACTION IN SOURCE DB:
-------------------------------
1.EXPORT data in the source(use the below queries for extracting)

db2 "SELECT chr(34)||rtrim(tabschema)||chr(34)||'.'
||chr(34)||rtrim(tabname)||chr(34)||' 'FROM SYSCAT.tables  WHERE   tabname NOT
IN (SELECT TABNAME FROM SYSCAT.COLUMNS WHERE GENERATED ='A' or IDENTITY='Y')
and  type ='T' and TABSCHEMA='BKFAIR01'   or tabschema ='UNPUBLISHED'   and
tabname not in (SELECT TABNAME FROM SYSCAT.COLUMNS WHERE GENERATED = 'A'  or
IDENTITY='Y'  )" > > tablist_db2move.sql
db2 "SELECT chr(34)||'BKFAIR01'||chr(34)||'.'
||chr(34)||rtrim(tabname)||chr(34)||' 'FROM SYSCAT.COLUMNS WHERE GENERATED =
'A'  or IDENTITY='Y' AND TABSCHEMA='BKFAIR01'" > tablist_db2move_exclude.sql

db2move bkfair01 export -tf tablist_db2move_exclude.sql
db2move bkfair01 export -tf tablist_db2move.sql	

tar -cvf db2move_exclude.tar * 
gzip db2move_exclude.tar 

tar -cvf db2move.tar *   
gzip db2move_exclude.tar

2.Execute the sequences.sql file (To get sequences values)

db2 -tvf sequences.sql

II.Drop Old Database: (If present)
---------------------------------

3.drop existing database

db2 drop db <DBNAME>


III.Database Build Phase
--------------------

4.create.database.sh  <./create.database.sh -d bkf01d>

5.db2 -tvf create_tables_PRODUCTION.sql

6.tab_count.sh <DBNAME> ( Compare it with Production/QA/STAGE database
objects)

7.db2 connect to <DBNAME>

8. db2 -tvf check_status.sql <check the status of tables/views/SP's/triggers>

9. Drop Triggers & SP's

droptrig_proc.sql (Execute this sql for dropping all triggers & SP's)

III.SETTING DB,DBM PARAMETERS & Bounce the database instance:
-------------------------------------------------------------

10. Execute the set_db_dbmcfg.sh script

IV.Data Load Phase :db2move
---------------------------
11.cd /backups/db2move

12.db2move <dbname> load -lo replace

13.sh LoadCheck.sh [O/PFILE] {To check the errors related to Load }

14.set_integrity_enforced.sh <DBNAME> {enforce check integrity}


V.Data Load Phase :db2move_exclude
---------------------------------

15.cd /backups/db2move_exclude

16.db2 -tvf LOAD.sql -z LOAD.out

17.sh LoadCheck.sh [O/PFILE] {To check the errors related to Load }

18.set_integrity_enforced.sh <DBNAME> {enforce check integrity}


VI. Objects(SEQ/TRIGGERS) creation:
-----------------------------------
db2 connect to <dbname>

19.Sequence values should be set again from --> [sequences.sql from STEP I]

20.Triggers and Stored procedures creation:

db2 -tvf create_triggerProcs_Production.sql


VII. Drop and re-create the unique index/Primary key for Generated tables:
--------------------------------------------------------------------------

21. drop_create_pk.sql  (execute the script)

22.set_integrity_enforced.sh (To remove tables from set integrity)

23.Empty the tables with Generated tables (/backups/db2move_exclude/LOAD.sql) present in this script

eg: load from /dev/null of del replace into BKFAIR01.VOLUNTEER_LOGIN_TRACKING nonrecoverable;
    load from /dev/null of del replace into BKFAIR01.FAIRDATE_LOGIN_TRACKING nonrecoverable;


24.Load the data for the  Generated tables , execute the /backups/db2move_exclude/LOAD.sql

25.set_integrity_enforced.sh (To remove tables from set integrity)


VIII. Check the objects staus & Count:
-------------------------------------

26.check_status.sql <check the status of tables/views/SP's/triggers>

27. Count of the tables:-

db2 -x "select 'select count(*) from ' || ltrim(rtrim(substr(TABSCHEMA,1,50)))
|| '.' || ltrim(rtrim(substr(TABNAME,1,50))) ||'  with ur; ' from
syscat.tables where tabschema in ('UNPUBLISHED','BKFAIR01') and type ='T'" >
tab_count.sql
db2 -tvf tab_count.sql

Match the count values obtainted from the above with the
[/backups/db2move/EXPORT.out & /backups/db2move_exclude/EXPORT.out] in the
target server.

IX. Rebinding Packages:
------------------------

28. Rebind the packages

db2rbind <dbname> -l bind.log

X.Creating Roles:
------------------
29. create_role.sh

XI.Granting access to roles:
----------------------------
30. grants_app_user.sh
31. grants_dev_read.sh
32. grants_dev_write.sh
33. grants_endc_usr.sh
34. grants_perf_usr.sh
35. grants_prod_read.sh
36. grants_qa_read.sh


37.  adding users to roles

User Name               User id         Environment     Role
----------------        --------        -----------     ----- 
Priya  Subramaniam      priyasub        QA              qa_read
Beth  Tracey            bethtra         QA              qa_read
Naresh Male             naresmal        Dev,QA          dev write ,QA-qa_read
Franck  Cabaret         franccab        Dev,Qa          dev write,qa_read
Ramesh Avala            ramesava        Qa              qa_read
Jyotsna Duvvuri                         dev             dev_read
Ravi Mikkilineni                        dev             dev_read
Srinivasa Bora                          dev             dev_read
Sanjay Attada                           dev,qa,prod     dev_read,qa_read,prod_read

job user dev            cptjobd         Dev             app_user
job user qa             cptjobq         Qa              app_user
job user uat            cptjobu         Uat             app_user
job user prod           cptjobp         Prod            app_user
reporting user          repcptp         prod            prod_read
endeca user             endcptd         dev             endc_usr
endeca_user             endcptq         qa              endc_usr
endeca_user             endcptp         prod            endc_usr

-----------------------------------------------------------------------------------------------------------------------------------------------------

38. IGNITE SET UP:

please refer to ignite directory for setting up monitoring when setting up the server for the first time ONLY,if data is being reloaded skip this step 

setting_up_ignite


39. RUNSTATS ON SCHEMA BKFAIR01:

db2 "REORGCHK update statistics on schema bkfair01"


40. AUTO- MAINTAINENCE:

auto_maintenance.sh    {Turning on Auto maintainence} 
auto_Maint_window.sh   {Setting Auto maintainence window}


41.Setting HADR:

cr_HA_cfg.sh (Script for genertaing Hadr paramets for primary & secondary) 

cr_TSA_cfg.sh (For TSA setup)


check the wiki for reference:
----------------------------
https://scholastic.jira.com/wiki/pages/editpage.action?pageId=87883902
