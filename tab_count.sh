
 ##########################################################################
 # Author.....: SAMPATH MURUGESAN
 # Name.......: tab_count_PROD.sh 
 # Description:
 # Created....: 2014-06-04
 # Updated....: 2014-07-03 by Sampath Murgesan
 # Updated....: 
 # Notes......: This Script checks for the check tab/view/seq/proc counts 
 # Usage......: sh [script] [DATBASE]
 # ...........:
 ##########################################################################

# DB="BKF01D"

 if [ $# -ne 1 ]; then
   echo "Script usage: sh [script] [DATABASE]"
       exit 1
           fi







#db2 connect to $DB
db2 connect to $1


echo "TABLE COUNT SCHEMA :BKFAIR01"
db2 -x "select count(*)tabname from syscat.tables where  tabschema ='BKFAIR01' and type='T'" 
echo "TABLE COUNT SCHEMA :UNPUBLISHED"
db2 -x "select count(*)tabname from syscat.tables where  tabschema ='UNPUBLISHED' and type='T'"

echo "VIEW COUNT SCHEMA :BKFAIR01"
db2 -x "select count(*)tabname from syscat.tables where  tabschema ='BKFAIR01' and type='V'"
echo "VIEW COUNT SCHEMA :UNPUBLISHED"
db2 -x "select count(*)tabname from syscat.tables where  tabschema ='UNPUBLISHED' and type='V'"

echo "TRIGGER COUNT SCHEMA :BKFAIR01"
db2 -x "select count(*)tabname from syscat.triggers where tabschema ='BKFAIR01'" 
echo "TRIGGER COUNT SCHEMA :UNPUBLISHED"
db2 -x "select count(*)tabname from syscat.triggers where tabschema ='UNPUBLISHED'" 	

 echo "PROCEDURE COUNT SCHEMA :BKFAIR01"
 db2 -x "select count(*)tabname from syscat.procedures  where procschema='BKFAIR01'" 
 echo "PROCEDURE COUNT SCHEMA :UNPUBLISHED"
db2 -x "select count(*)tabname from syscat.procedures  where procschema='UNPUBLISHED'"


echo "SEQUENCE COUNT SCHEMA :BKFAIR01"
db2 -x "select count(*)tabname from syscat.sequences where  seqschema='BKFAIR01'"
echo "SEQUENCE COUNT SCHEMA :UNPUBLISHED"
db2 -x "select count(*)tabname from syscat.sequences where  seqschema='UNPUBLISHED'"
