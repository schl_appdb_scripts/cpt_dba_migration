 #!/bin/ksh

LOGPATH="/backups/logs"
#DBName=BKF01D   #inputs

if [ $# -ne 1 ]; then
   echo "Script usage: sh [script] [DATABASE]"
   exit 
fi

echo "Checking and removing tables from set integrity pending state"
db2 connect to $1 > $LOGPATH/output.out

db2 -x "select 'SET INTEGRITY FOR '|| TABSCHEMA ||'.'||TABNAME || ' IMMEDIATE CHECKED;' from SYSCAT.TABLES where STATUS='C' and type='T'" > $LOGPATH/set_integrity.sql

tabcnt=$(wc -l < $LOGPATH/set_integrity.sql)
while [[ ${tabcnt} -gt 0 ]];
 do
        echo "************************************************************"
        echo "Number of tables in set integrity pending state : $tabcnt"
        echo "Setting integrity of tables in set integrity pending state"
        echo "************************************************************"
    db2 -tf $LOGPATH/set_integrity.sql >> $LOGPATH/output.out
        db2 "commit" >> $LOGPATH/output.out
    # look for more tables in check pending state
    db2 -x "select 'SET INTEGRITY FOR '|| TABSCHEMA ||'.'||TABNAME || ' IMMEDIATE CHECKED;' from SYSCAT.TABLES where STATUS='C' and type='T'" > $LOGPATH/set_integrity.sql
    tabcnt=$(wc -l < $LOGPATH/set_integrity.sql)
done
echo "No table in set integrity pending state"
