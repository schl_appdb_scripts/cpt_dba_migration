 ####################################################################
 # Author.....: SAMPATH MURUGESAN
 # Name.......: common_error.sh
 # Description:
 # Created....: 2014-06-09
 # Updated....: 2014-06-10 by Sampath Murgesan
 # Updated....:
 # Notes......: This Script checks for errors in file
 # Usage......: sh [script] [FILE] )
 # ...........:
 ####################################################################

  LOGPATH="/backups/logs"

  #set -x

  if [ $# -ne 1 ]; then
    echo "Script usage: sh [script] [FILE]"
      exit 1
      fi

      FILE=`echo $1 | tr '[a-z]' '[A-Z]' `

              echo `date`
	      #SQL=`grep -i "SQL3088N" load.sql.log`
	      #ERROR_LOG='error.log'

	       cat $1 | grep -n "SQL....N" > $LOGPATH/error.log
	        cat $1 | grep -n "SQL....C" >> $LOGPATH/error.log
		 cat $1 | grep -n "ADM....C" >> $LOGPATH/error.log
		  cat $1 | grep -n "ADM....E" >> $LOGPATH/error.log
		   cat $1 | grep -n "ADM....N" >> $LOGPATH/error.log
		    cat $1 | grep -n "ADM....W" >> $LOGPATH/error.log
		     cat $1 | grep -n "AMI....W" >> $LOGPATH/error.log
		      cat $1 | grep -n "CCA....I" >> $LOGPATH/error.log
		       cat $1 | grep -n "ASN....W" >> $LOGPATH/error.log
		        cat $1 | grep -n "ASN....E" >> $LOGPATH/error.log
			 cat $1 | grep -n "CIE....E" >> $LOGPATH/error.log
			  cat $1 | grep -n "CIE....I" >> $LOGPATH/error.log
			   cat $1 | grep -n "CLI....I" >> $LOGPATH/error.log
			    cat $1 | grep -n "CLI....E" >> $LOGPATH/error.log
			     cat $1 | grep -n "AUD....N" >> $LOGPATH/error.log
			      cat $1 | grep -n "CTE....N" >> $LOGPATH/error.log
			       cat $1 | grep -n "CTE....E" >> $LOGPATH/error.log
			        cat $1 | grep -n "GSC....N" >> $LOGPATH/error.log
				 cat $1 | grep -n "GSC....C" >> $LOGPATH/error.log
				  cat $1 | grep -n "DBT....E" >> $LOGPATH/error.log
				   cat $1 | grep -n "DBT....W" >> $LOGPATH/error.log
				    cat $1 | grep -n "DBT....N" >> $LOGPATH/error.log
				     cat $1 | grep -n "DQP....E" >> $LOGPATH/error.log
				      cat $1 | grep -n "EXP....W" >> $LOGPATH/error.log
				       cat $1 | grep -n "DB2....E" >> $LOGPATH/error.log
				        cat $1 | grep -n "DB2.....E" >> $LOGPATH/error.log
					 cat $1 | grep -n "DBA....E" >> $LOGPATH/error.log
					  cat $1 | grep -n "DBA....N" >> $LOGPATH/error.log
					   cat $1 | grep -n "DBI....E" >> $LOGPATH/error.log
					    cat $1 | grep -n "DBI....N" >> $LOGPATH/error.log
					     cat $1 | grep -n "DBI....I" >> $LOGPATH/error.log
					      cat $1 | grep -n "DBI.....E" >> $LOGPATH/error.log
					       cat $1 | grep -n "SPM....C" >> $LOGPATH/error.log
					        cat $1 | grep -n "SAT....N" >> $LOGPATH/error.log
						 cat $1 | grep -n "SAT....C" >> $LOGPATH/error.log
						  cat $1 | grep -n "MQL....I" >> $LOGPATH/error.log
						   cat $1 | grep -n "MQL....W" >> $LOGPATH/error.log
						    cat $1 | grep -n "MQL....E" >> $LOGPATH/error.log
						     cat $1 | grep -n "LIC....E" >> $LOGPATH/error.log
						      cat $1 | grep -n "LIC....N" >> $LOGPATH/error.log
						       cat $1 | grep -n "ICM.....N" >> $LOGPATH/error.log
						        cat $1 | grep -n "ICM....N" >> $LOGPATH/error.log



							ERROR_LOG=`cat $LOGPATH/error.log`

							#cat $ERROR_LOG

							if [ -z "$ERROR_LOG" ] ; then
							           MESG="There are no  errors found ,Please proceed with the next step"
								              echo $MESG
									                # echo `date`

											     else
											                MESG="There are  error messages recorded , Please check error.log and troubleshoot it"
													           echo $ERROR_LOG
														              echo $MESG

															               #  echo `date`

																             fi

