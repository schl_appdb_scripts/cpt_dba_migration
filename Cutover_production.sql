 
        #STEPS TO SETUP/LOAD DATABASE:#
        ###############################

        ###############
        SOURCE SERVER #: (scewridbp03.scholastic.net )
        ###############

        I. DATA EXTRACTION IN SOURCE DB:
        -------------------------------

        1.EXPORT data in the source(use the below queries for extracting)

        db2 "SELECT chr(34)||rtrim(tabschema)||chr(34)||'.'||chr(34)||rtrim(tabname)||chr(34)||' 'FROM SYSCAT.tables  WHERE   tabname NOT IN (SELECT TABNAME FROM SYSCAT .COLUMNS WHERE GENERATED ='A' or IDENTITY='Y') and  type ='T' and TABSCHEMA='BKFAIR01'   or tabschema ='UNPUBLISHED'   and tabname not in (SELECT TABNAME FROM SYSCAT.COLUMNS WHERE GENERATED = 'A'  or IDENTITY='Y'  )" >  tablist_db2move.sql
        db2 "SELECT chr(34)||'BKFAIR01'||chr(34)||'.'||chr(34)||rtrim(tabname)||chr(34)||' 'FROM SYSCAT.COLUMNS WHERE GENERATED = 'A'  or IDENTITY='Y' AND TABSCHEMA='BKFAIR01'" > tablist_db2move_exclude.sql

        2.db2move bkfair01 export -tf tablist_db2move_exclude.sql
          db2move bkfair01 export -tf tablist_db2move.sql

        tar -cvf db2move_exclude.tar *
        gzip db2move_exclude.tar

        tar -cvf db2move.tar *
        gzip db2move_exclude.tar


        3.Execute the sequences.sql file (To get sequences values)

        db2 -tvf sequences.sql

        cat RESTART_VALUES.out

        4.SCP the tar files and RESTART_VALUES.out to the target server




        ################
        TARGET SERVER :# ec-e1b-ldbp-001 10.36.14.141 (Primary)
        ################



        II. DROP ALL THE EXISTING triggers & procedures
        
        droptrig_proc.sql (Execute this sql for dropping all triggers & SP's)

        III. delete the data in all the tables

        select 'load from /dev/null of del replace into UNPUBLISHED.'||lower(tabname)||' nonrecoverable;' from syscat.tables where tabschema='UNPUBLISHED' and type='T';
        select 'load from /dev/null of del replace into BKFAIR01.'||lower(tabname)||' nonrecoverable;' from syscat.tables where tabschema='BKFAIR01' and type='T';



        IV.Data Load Phase :db2move
        ---------------------------
        5.cd /backups/db2move

        6.db2move <dbname> load -lo replace

        7.sh LoadCheck.sh [O/PFILE] {To check the errors related to Load }

        8.set_integrity_enforced.sh <DBNAME> {enforce check integrity}


        V.Data Load Phase :db2move_exclude
        ---------------------------------

        9..cd /backups/db2move_exclude

        10..db2 -tvf LOAD.sql -z LOAD.out

        11..sh LoadCheck.sh [O/PFILE] {To check the errors related to Load }

        12.set_integrity_enforced.sh <DBNAME> {enforce check integrity}
         
        VI. Drop and re-create the unique index/Primary key for Generated tables:
        --------------------------------------------------------------------------

	13. drop_create_pk.sql  (execute the script)

	14.set_integrity_enforced.sh (To remove tables from set integrity)

	15.Empty the tables with Generated tables (/backups/db2move_exclude/LOAD.sql) present in this script

	eg: load from /dev/null of del replace into BKFAIR01.VOLUNTEER_LOGIN_TRACKING nonrecoverable;
	    load from /dev/null of del replace into BKFAIR01.FAIRDATE_LOGIN_TRACKING nonrecoverable;


	16.Load the data for the  Generated tables , execute the /backups/db2move_exclude/LOAD.sql

	17.set_integrity_enforced.sh (To remove tables from set integrity) 

        VII. Objects(SEQ/TRIGGERS) creation:
        -----------------------------------
        db2 connect to <dbname>

        18.Sequence values should be set again from --> [sequences.sql from STEP I, execute the RESTART_VALUES.out file]

        19.Triggers and Stored procedures creation:


        db2 -tvf create_triggerProcs_Production.sql

        reset identity values

        db2 -tvf set_identity

        VIII. Check the objects staus & Count:
        -------------------------------------

        20.check_status.sql <check the status of tables/views/SP's/triggers>

        21. Count of the tables:-

        db2 -x "select 'select count(*) from ' || ltrim(rtrim(substr(TABSCHEMA,1,50)))|| '.' || ltrim(rtrim(substr(TABNAME,1,50))) ||'  with ur; ' from syscat.tables where tabschema in ('UNPUBLISHED','BKFAIR01') and type ='T'" > tab_count.sql
        db2 -tvf tab_count.sql

        Match the count values obtainted from the above with the
        [/backups/db2move/EXPORT.out & /backups/db2move_exclude/EXPORT.out] in the
        target server.
        
        Execute - /backups/sam/count.sql against BKF01P and compare

        IX. Rebinding Packages:
        ------------------------

        22.Rebind the packages

           db2rbind <dbname> -l bind.log


        23. RUNSTATS ON SCHEMA BKFAIR01:

        db2 "REORGCHK update statistics on schema bkfair01"

        X. Backup & HADR:
        -------------------

log in as root and run following command

service crond restart        

24. Perform on-line backup of db BKF01P

        25. Set up the HADR cfg for secondary server - ec-e1c-ldbp-002 10.36.114.51
 
