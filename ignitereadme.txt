SolarWinds Ignite 8.3.307 (and 8.3.407) Hot Fix

Hot Fix (AT83DB2) enables Ignite to monitor DB2 version 10.5.
The hot fix includes classes that need to be applied to the Ignite server,
as well as scripts to run on the monitored DB2 instance.

This hot fix is meant to accompany the instructions found in the following KB article:

      http://support.confio.com/kb/1693


Instructions to Apply Hot Fix
=============================

TO INSTALL:
===========

1. Unzip the patch file included in this hot fix (ignite_DB2_patch.zip) into the following 
   Ignite directory:

      <install dir>\iwc\tomcat\webapps\iwc\WEB-INF\classes 

      For example, on Windows, unzip to:

          c:\Program Files (x86)\Confio\Ignite PI\iwc\tomcat\webapps\iwc\WEB-INF\classes

      This will create the following directory (with sub-directories):

          c:\Program Files (x86)\Confio\Ignite PI\iwc\tomcat\webapps\iwc\WEB-INF\classes\com

2. Restart Ignite



UNINSTALL
=========

1. To uninstall ALL patches, shutdown Ignite and delete the following directory:

      <Ignite install dir>\iwc\tomcat\webapps\iwc\WEB-INF\classes\com

2. Restart Ignite
