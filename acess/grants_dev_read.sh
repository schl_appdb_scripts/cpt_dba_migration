  LOGPATH="/backups/logs"


 if [ $# -ne 1 ]; then
   echo "Script usage: sh [script] [DATABASE]"
       exit 1
           fi

           #db2 connect to $DB
           db2 connect to $1


            db2 -x "select 'GRANT SELECT ON TABLE  '|| rtrim(tabschema)||'.'||rtrim(tabname) ||' TO dev_read '  || ' ;'  from syscat.tables where tabschema ='BKFAIR01' or  tabschema ='UNPUBLISHED'" > $LOGPATH/grants_dev_read.sql

            db2 -x "grant explain on database to role dev_read"
            
            db2 -tvf $LOGPATH/grants_dev_read.sql > $LOGPATH/grants_dev_read.out
        echo "Check the O/P log /backups/logs/grants_dev_read.out"
