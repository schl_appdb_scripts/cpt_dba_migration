####################################################################
# Author.....: SAMPATH MURUGESAN
# Name.......: add_role_to_user.sh
# Description:
# Created....: 2014-06-09
# Updated....: 2014-07-30 by Sampath Murgesan
# Updated....:
# Notes......: This Script enables to add role to user
# Usage......: sh [script] [DATABASE] [USER]
# ...........:
####################################################################

LOGPATH="/backups/logs/"

# set -x

 if [ $# -ne 2 ]; then
   echo "Script usage: sh [script] [DATABASE] [USER] "
     exit 1
     fi

     FILE=`echo $1 | tr '[a-z]' '[A-Z]' `

     #echo "ENTER THE DATABASE"
     #read DATABASE

     db2 "connect to $1"

     #echo "ENTER THE USER"
     #read USER

     echo "ENTER THE ROLE and press [ENTER]:"
     db2 -x "select rolename from syscat.roles where ROLENAME not in ('SYSDEBUG','SYSTS_ADM','SYSTS_MGR','SYSTS_USR')" > $LOGPATH/role_user.log
     cat $LOGPATH/role_user.log
     read ROLE

     #db2 -x "select rolename from syscat.roles"
     #db2 -x "GRANT ROLE APP_CPT_USER TO USER prasagun"

     db2 -x "GRANT ROLE $ROLE TO USER $2" > $LOGPATH/role_user.log

      cat $LOGPATH/role_user.log

