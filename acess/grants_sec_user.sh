   LOGPATH="/backups/logs"


  if [ $# -ne 1 ]; then
    echo "Script usage: sh [script] [DATABASE]"
        exit 1
            fi


            #DB="BKF01D"
            #PATH="/backups/scripts/UNPUBLISHED/acess"

            #db2 connect to $DB
            db2 connect to $1


  db2 -x "grant dbadm WITH DATAACCESS WITH ACCESSCTRL on database to user sec_role" > $LOGPATH/grants_sec.sql
  db2 -x "grant secadm on database to role  sec_role" >>  $LOGPATH/grants_sec.sql

             # db2 -tvf seq_grants.sql

             db2 -tvf $LOGPATH/grants_sec.sql > $LOGPATH/grants_sec.out
            echo "Check the O/P log /backups/logs/grants_sec.sql"

