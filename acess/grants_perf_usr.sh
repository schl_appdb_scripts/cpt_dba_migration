  LOGPATH="/backups/logs"


 if [ $# -ne 1 ]; then
   echo "Script usage: sh [script] [DATABASE]"
       exit 1
           fi


           #db2 connect to $DB
           db2 connect to $1


            db2 -x "select 'GRANT SELECT ON TABLE  '|| rtrim(tabschema)||'.'||rtrim(tabname) ||' TO perf_usr '  || ' ;'  from syscat.tables where tabschema ='BKFAIR01' or  tabschema ='UNPUBLISHED'" > $LOGPATH/grants_perf_usr.sql


            db2 -tvf $LOGPATH/grants_perf_usr.sql > $LOGPATH/grants_perf_usr.out

            echo "Check the O/P log /backups/logs/grants_perf_usr.out"
