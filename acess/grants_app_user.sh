   LOGPATH="/backups/logs"


  if [ $# -ne 1 ]; then
    echo "Script usage: sh [script] [DATABASE]"
        exit 1
            fi


            #DB="BKF01D"
            #PATH="/backups/scripts/UNPUBLISHED/acess"

            #db2 connect to $DB
            db2 connect to $1


             db2 -x "select 'GRANT SELECT,insert,update,delete ON TABLE  '|| rtrim(tabschema)||'.'||rtrim(tabname) ||' TO role app_usr '  || ' ;'  from syscat.tables where tabschema ='BKFAIR01' or  tabschema ='UNPUBLISHED'" > $LOGPATH/grants.sql



             # db2 -tvf tab_grants.sql

             db2 -x "select 'grant EXECUTE ON PROCEDURE   '|| rtrim(PROCSCHEMA)||'.'||rtrim(PROCNAME) ||' TO role app_usr '  || ' ;'  from syscat.PROCEDUREs  where PROCSCHEMA ='BKFAIR01' or PROCSCHEMA ='UNPUBLISHED'" >> $LOGPATH/grants.sql

             # db2 -tvf proc_grants.sql

             db2 -x "select 'GRANT USAGE ON SEQUENCE  '|| rtrim(SEQSCHEMA)||'.'||rtrim(SEQNAME) ||' TO role app_usr '  || ' ;'  from syscat.SEQUENCES  where SEQSCHEMA ='BKFAIR01' or SEQSCHEMA ='UNPUBLISHED'" >> $LOGPATH/grants.sql


             db2 -x "select 'GRANT use of tablespace   '|| rtrim(TBSPACE) ||' TO role app_usr '  || ' ;'  from syscat.tablespaces" >> $LOGPATH/grants.sql


             # db2 -tvf seq_grants.sql

             db2 -tvf $LOGPATH/grants.sql > $LOGPATH/grants.out
            echo "Check the O/P log /backups/logs/grants.out"

