  LOGPATH="/backups/logs"


 if [ $# -ne 1 ]; then
   echo "Script usage: sh [script] [DATABASE]"
       exit 1
           fi


           #db2 connect to $DB
           db2 connect to $1


            db2 -x "select 'GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE  '|| rtrim(tabschema)||'.'||rtrim(tabname) ||' TO dev_write '  || ' ;'  from syscat.tables where tabschema ='BKFAIR01' or  tabschema ='UNPUBLISHED'" > $LOGPATH/grants_dev_write.sql


            db2 -tvf $LOGPATH/grants_dev_write.sql > $LOGPATH/grants_dev_write.out
            #cat  $LOGPATH/grants_dev_write.out
         echo "Check the O/P log /backups/logs/grants_dev_write.out"
