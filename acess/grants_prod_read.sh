 
  LOGPATH="/backups/logs"

  if [ $# -ne 1 ]; then
    echo "Script usage: sh [script] [DATABASE]"
        exit 1
            fi


            #PATH="/backups/scripts/UNPUBLISHED/acess"

            db2 connect to $1

            db2 -x "select 'GRANT SELECT ON TABLE  '|| rtrim(tabschema)||'.'||rtrim(tabname) ||' TO prod_read '  || ' ;'  from syscat.tables where tabschema ='BKFAIR01' or  tabschema ='UNPUBLISHED'" > $LOGPATH/grants_prod_read.sql


            db2 -tvf LOGPATH/grants_prod_read.sql > $LOGPATH/grants_prod_read.out

            db2 -x "grant explain on database to role prod_read"
            echo "Check the O/P log /backups/logs/grants_prod_read.out"
