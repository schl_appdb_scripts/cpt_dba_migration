 ####################################################################
 # Author.....: SAMPATH MURUGESAN
 # Name.......: auto_maintenance.sh
 # Description:
 # Created....: 2014-06-09
 # Updated....: 2014-06-10 by Sampath Murgesan
 # Updated....:
 # Notes......: This Script enables to turn ON/OFF Automatic maintenance
 # Usage......: sh [script] [Database]
 # ...........:
 ####################################################################

  #set -x

   LOGPATH="/backups/logs"


   if [ $# -ne 1 ]; then
     echo "Script usage: sh [script] [Database]"
       exit 1
       fi

       FILE=`echo $1 | tr '[a-z]' '[A-Z]' `

               echo `date`

	       #DB="BKF01D"
	       #db2 connect to $DB

	       db2 connect to $1

	       #db2 -x "update db cfg using AUTO_MAINT ON" > AUTO_MAINT_ON.log

	       db2 "get db cfg for $1 show detail" | grep -i AUTO_MAINT > $LOGPATH/AUTO_MAINT.log


	       CHECK=`grep -i "= ON" $LOGPATH/AUTO_MAINT.log`



	        if [ -z "$CHECK" ] ; then

		           db2 -x "update db cfg using AUTO_MAINT ON" > $LOGPATH/AUTO_MAINT_ON.log
			              db2 "get db cfg for $1 show detail" | grep -i AUTO_MAINT >> $LOGPATH/AUTO_MAINT_ON.log

				                 MESG="The AUTO_MAINT is set to ON"
						            echo $MESG
							               echo $MESG >> $LOGPATH/AUTO_MAINT_ON.log
								       #echo $MESG | mailx -s "$MESG" eCommerceDatabaseAdministrators@Scholastic.com,PGunasekera-consultant@Scholastic.com



								                  #echo `date`

										       else

										                  db2 -x "update db cfg using AUTO_MAINT OFF" > $LOGPATH/AUTO_MAINT_OFF.log
												             db2 "get db cfg for $1 show detail" | grep -i AUTO_MAINT >> $LOGPATH/AUTO_MAINT_OFF.log

													                MESG="The AUTO_MAINT is set to OFF"
															           echo $MESG
																              echo $MESG >> $LOGPATH/AUTO_MAINT_OFF.log


																	            fi
