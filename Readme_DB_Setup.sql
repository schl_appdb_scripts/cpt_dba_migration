  STEPS TO SETUP DATABASE:
   ========================

    I.Drop Old Database: (If present)
     ---------------------------------

      1.drop existing database

       db2 drop db <DBNAME>


        II.Database Build Phase
	 --------------------

	  2.create.database.sh  <./create.database.sh -d bkf01d>

	   3.create_tables_PRODUCTION.sql

	    4.tab_count_PROD.sh <DBNAME> ( Compare it with Production/QA/STAGE database objects)

	     5.db2 connect to <DBNAME>

	      6.check_table_status.sql <check the status of tables>


	       III.Bounce the database instance:
	        -----------------------------

		 7.db2 "force applications all"

		  8.db2stop
		   9.db2start

		    10.db2 activate db <dbname>
		     11.db2 connect to <dbname>

		      IV.Data Load Phase :db2move
		       -------------------------
		        12.cd /backups/db2move

			 13.db2move <dbname> load -lo replace

			  14.sh LoadCheck.sh [O/PFILE] {To check the errors related to Load }

			   15.set_integrity_enforced.sh <DBNAME> {enforce check integrity}


			    V.Data Load Phase :db2move_exclude
			     ---------------------------------

			      16.cd /backups/db2move_exclude

			       17.db2 -tvf LOAD.sql -z LOAD.out

			        18.sh LoadCheck.sh [O/PFILE] {To check the errors related to Load }

				 19.set_integrity_enforced.sh <DBNAME> {enforce check integrity}

				  20.check_table_status.sql <check the status of tables>


				   VI.Creating Roles:
				    ------------------
				     21.create_role.sh

				      VII.Granting access to roles:
				       ----------------------------
				        22.grants_app_cpt_user.sh
					 23.grants_dev_user.sh
					  24.grant_prod_sup_role.sh
					   25.grant_role_to_user.sh

					   VII. Sequence re-creation:
					   -------------------------

					   Source server (Prod/Qa/Stg):
					   -----------------------------
					   use the below command to get the sequence values:

					    db2look -d <database> -e | grep -i "CREATE SEQUENCE" > Sequence.log

                                           Target Server:
					   --------------

					    move the Sequence.log to the target server and execute the file --->  db2 -tvf Sequence.log -z Sequence.out

