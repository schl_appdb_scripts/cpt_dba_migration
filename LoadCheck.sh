 ##########################################################################
 # Author.....: SAMPATH MURUGESAN
 # Name.......: LoadCheck.sh
 # Description:
 # Created....: 2014-06-04
 # Updated....: 2014-06-19 by Sampath Murgesan
 # Updated....:
 # Notes......: This Script checks erros related to LOAD command in file
 # Usage......: sh [script] [File]
 # ...........:
 ##########################################################################

 #. /home/cpt01d/sqllib/db2profile
 #set -x

 LOGPATH="/backups/logs"


 if [ $# -ne 1 ]; then
   echo "Script usage: sh [script] [FILE]"
     exit 1
     fi



     FILE=`echo $1 | tr '[a-z]' '[A-Z]' `


           echo `date`
	   #       SQL=`grep -i "SQL3088N" load.sql.log`
	           LOAD=`grep -i "SQL3088N" $1`

		   LOAD=`grep -i "SQL3119W" $1`
		   LOAD=`grep -i "SQL1188N" $1`
		   LOAD=`grep -i "SQL0902C" $1`
		   LOAD=`grep -i "SQL0901N" $1`
		   LOAD=`grep -i "SQL0437W" $1`
		   LOAD=`grep -i "SQL3508" $1`
		   LOAD=`grep -i "SQL0181N" $1`
		   LOAD=`grep -i "SQL0901N" $1`
		   LOAD=`grep -i "SQL2044" $1`
		   LOAD=`grep -i "SQL1224" $1`
		   LOAD=`grep -i "SQL0902C" $1`
		   LOAD=`grep -i "SQL3508N" $1`
		   LOAD=`grep -i "SQL3508" $1`
		   LOAD=`grep -i "SQL3168W" $1`
		   LOAD=`grep -i "SQL0954C" $1`
		   LOAD=`grep -i "SQL2044 " $1`
		   LOAD=`grep -i "SQL0901 " $1`
		   LOAD=`grep -i "SQL0973 " $1`
		   LOAD=`grep -i "SQL3005N" $1`
		   LOAD=`grep -i "SQL1218N" $1`
		   LOAD=`grep -i "SQL3604N" $1`
		   LOAD=`grep -i "SQL1042C" $1`
		   LOAD=`grep -i "SQL0902C" $1`
		   LOAD=`grep -i "SQL1007N" $1`
		   LOAD=`grep -i "SQL0452N" $1`
		   LOAD=`grep -i "SQL0902C" $1`
		   LOAD=`grep -i "SQL3012C" $1`
		   LOAD=`grep -i "SQL2032N" $1`
		   LOAD=`grep -i "SQL2044N" $1`
		   LOAD=`grep -i "SQL0954C" $1`
		   LOAD=`grep -i "SQL3088N" $1`

		   if [ -z "$LOAD" ] ; then
		              MESG="There are no generic errors found in LOAD,but please check for "0" rows related errors in Loaderror1.log "
			                 echo $MESG
					           # echo `date`

						        else
							           MESG="There are  error messages recorded in LOAD, Please check and troubleshoot it"
								              echo $MESG
									                 echo $LOAD

											          #  echo `date`

												        fi

													#cat $1 | grep -i Number of rows read         = 0
													cat $1  | grep -i "Number of rows read         = 0" > $LOGPATH/Loaderror1.log
													#cat LOAD.out | grep -i "Rows read:          0"
													cat $1 | grep -i "Rows read:          0" >> $LOGPATH/Loaderror1.log

													Load_Error=`cat $LOGPATH/Loaderror1.log`


													if [ -z "$Load_Error" ] ; then
													           MESG="There are no  errors related to "0" rows in LOAD,Please proceed with the next step"
														              echo $MESG
															                # echo `date`

																	     else
																	                MESG="There are  error messages recorded to "0" rows  LOAD, Please check and verify with source table count"
																			           echo $MESG
																				              echo $Load_Error

																					               #  echo `date`

																						             fi

