set current schema ignmon@
create or replace function appl_status(p_agent_id bigint)
  returns int
  language sql
  reads sql data
  deterministic
  begin atomic
  declare v_appl_stat int default null;
  declare v_appl_status varchar(32) default null;

  set (v_appl_status) =
  (select appl_status
  from table(sysproc.snap_get_appl_info(null,-1))
  where agent_id=p_agent_id
  );

  if v_appl_status = 'INIT' then
    set v_appl_stat = 0; 
  elseif v_appl_status = 'CONNECTPEND' then
    set v_appl_stat = 1;
  elseif v_appl_status = 'CONNECTED' then
    set v_appl_stat = 2;
  elseif v_appl_status = 'UOWEXEC' then
    set v_appl_stat = 3;
  elseif v_appl_status = 'UOWWAIT' then
    set v_appl_stat = 4;
  elseif v_appl_status = 'LOCKWAIT' then
    set v_appl_stat = 5;
  elseif v_appl_status = 'COMMIT_ACT' then
    set v_appl_stat = 6;
  elseif v_appl_status = 'ROLLBACK_ACT' then
    set v_appl_stat = 7;
  elseif v_appl_status = 'RECOMP' then
    set v_appl_stat = 8;
  elseif v_appl_status = 'COMP' then
    set v_appl_stat = 9;
  elseif v_appl_status = 'INTR' then
    set v_appl_stat = 10;
  elseif v_appl_status = 'DISCONNECTPEND' then
    set v_appl_stat = 11;
  elseif v_appl_status = 'TPREP' then
    set v_appl_stat = 12;
  elseif v_appl_status = 'THCOMT' then
    set v_appl_stat = 13;
  elseif v_appl_status = 'THABRT' then
    set v_appl_stat = 14;
  elseif v_appl_status = 'TEND' then
    set v_appl_stat = 15;
  elseif v_appl_status = 'CREATE_DB' then
    set v_appl_stat = 16;
  elseif v_appl_status = 'RESTART' then
    set v_appl_stat = 17;
  elseif v_appl_status = 'RESTORE' then 
    set v_appl_stat = 18;
  elseif v_appl_status = 'BACKUP' then
    set v_appl_stat = 19;
  elseif v_appl_status = 'LOAD' then
    set v_appl_stat = 20;
  elseif v_appl_status = 'UNLOAD' then
    set v_appl_stat = 21;
  elseif v_appl_status = 'IOERROR_WAIT' then
    set v_appl_stat = 22;
  elseif v_appl_status = 'QUIESCE_TABLESPACE' then
    set v_appl_stat = 23;
  elseif v_appl_status = 'WAITFOR_REMOTE' then
    set v_appl_stat = 24;
  elseif v_appl_status = 'REMOTE_RQST' then
    set v_appl_stat = 25;
  elseif v_appl_status = 'DECOUPLED' then
    set  v_appl_stat = 26;
  elseif v_appl_status = 'ROLLBACK_TO_SAVEPOINT' then
    set v_appl_stat = 27; 
  end if;
  
  return v_appl_stat; 
end
@

set current schema ignmon@
create or replace function stmt_operation(p_agent_id bigint, p_stmt_start_time timestamp)
  returns int
  language sql
  reads sql data
  deterministic
  begin atomic
  declare v_stmt_operation int default null;
  declare v_stmt_operation_desc varchar(32) default null;

  set (v_stmt_operation_desc) =
  (select stmt.stmt_operation
  from table(sysproc.snap_get_stmt('',-1)) as stmt
  where stmt.agent_id=p_agent_id
    and stmt.stmt_start = p_stmt_start_time 
  );
  if v_stmt_operation_desc = 'PREPARE' then
    set v_stmt_operation = 1;
  elseif v_stmt_operation_desc = 'EXECUTE' then
    set v_stmt_operation = 2; 
  elseif v_stmt_operation_desc = 'EXECUTE_IMMEDIATE' then
    set v_stmt_operation = 3;
  elseif v_stmt_operation_desc = 'OPEN' then
    set v_stmt_operation = 4;
  elseif v_stmt_operation_desc = 'FETCH' then
    set v_stmt_operation = 5;
  elseif v_stmt_operation_desc = 'CLOSE' then
    set v_stmt_operation = 6;
  elseif v_stmt_operation_desc = 'DESCRIBE' then
    set v_stmt_operation = 7;
   elseif v_stmt_operation_desc = 'STATIC_COMMIT' then
    set v_stmt_operation = 8;
  elseif v_stmt_operation_desc = 'STATIC_ROLLBACK' then
    set v_stmt_operation = 9;
  elseif v_stmt_operation_desc = 'FREE_LOCATOR' then
    set v_stmt_operation = 10;
  elseif v_stmt_operation_desc = 'PREP_COMMIT' then
    set v_stmt_operation = 11;
  elseif v_stmt_operation_desc = 'CALL' then
    set v_stmt_operation = 12;
  elseif v_stmt_operation_desc = 'SELECT' then
    set v_stmt_operation = 15;
  elseif v_stmt_operation_desc = 'PREP_OPEN' then
    set v_stmt_operation = 16;
  elseif v_stmt_operation_desc = 'PREP_EXEC' then
    set v_stmt_operation = 17;
  elseif v_stmt_operation_desc = 'COMPILE' then
    set v_stmt_operation = 18;
  elseif v_stmt_operation_desc = 'SET' then
    set v_stmt_operation = 19;
  elseif v_stmt_operation_desc = 'RUNSTATS' then
    set v_stmt_operation = 20;
  elseif v_stmt_operation_desc = 'REORG' then
    set v_stmt_operation = 21;
  elseif v_stmt_operation_desc = 'REBIND' then
    set v_stmt_operation = 22;
  elseif v_stmt_operation_desc = 'REDIST' then
    set v_stmt_operation = 23;
  elseif v_stmt_operation_desc = 'GETTA' then
    set v_stmt_operation = 24;
  elseif v_stmt_operation_desc = 'GETAA' then
    set v_stmt_operation = 25;
  elseif v_stmt_operation_desc = 'GETNEXTCHUNK' then
    set v_stmt_operation = 26;
  end if;

  return v_stmt_operation;
end
@

set current schema ignmon@
create or replace function stmt_type(p_agent_id bigint, p_stmt_start_time timestamp)
  returns int
  language sql
  reads sql data
  deterministic
  begin atomic
  declare v_stmt_type int default null;
  declare v_stmt_type_desc varchar(32) default null;

  set (v_stmt_type_desc) =
  (select stmt.stmt_type
  from table(sysproc.snap_get_stmt('',-1)) as stmt
  where stmt.agent_id=p_agent_id
    and stmt.stmt_start = p_stmt_start_time 
  );
  if v_stmt_type_desc = 'STATIC' then
    set v_stmt_type = 1;
  elseif v_stmt_type_desc = 'DYNAMIC' then
    set v_stmt_type = 2; 
  elseif v_stmt_type_desc = 'NON_STMT' then
    set v_stmt_type = 3;
  elseif v_stmt_type_desc = 'STMT_TYPE_UNKNOWN' then
    set v_stmt_type = 4;
  end if;

  return v_stmt_type;
end
@

set current schema ignmon@
create or replace function snap_get_db(database_name varchar(128) default NULL, partition_no integer default -1)
   returns table (db_name varchar(128))
   language sql
   reads sql data
   no external action
   deterministic
   begin atomic
     return 
       select distinct db_name 
	   from table(MON_GET_MEMORY_SET('DATABASE',database_name,partition_no));
   end
@

set current schema ignmon@
create or replace function env_get_sys_info()
   returns table (host_name varchar(255),
                  total_cpus bigint
                 )
   language sql
   reads sql data
   no external action
   deterministic
   begin atomic
     return
       select host_name,cpu_total 
	   from TABLE(SYSPROC.ENV_GET_SYSTEM_RESOURCES());
   end
@

set current schema ignmon@
create or replace function snapshot_appl_info(database_name varchar(128) default NULL, partition_no integer default -1)
   returns table (agent_id bigint,
                  appl_name varchar(128),
                  db_name varchar(128),
                  auth_id varchar(128),
                  appl_status bigint,
                  client_nname varchar(255),
                  codepage_id bigint,   
                  num_assoc_agents bigint,	
                  coord_partition_num	smallint,
                  authority_lvl varchar(128),	
                  CLIENT_PID bigint,
                  coord_agent_pid bigint, 
                  status_change_time timestamp,	
                  CLIENT_PLATFORM varchar(12),
                  client_protocol varchar(10),
				  country_code smallint,
				  appl_id varchar(128),
				  sequence_no varchar(4),
 				  CLIENT_PRDID varchar(128),
				  input_db_alias varchar(128),
				  client_db_alias varchar(128),
				  db_path varchar(1024),
				  EXECUTION_ID varchar(128),
				  corr_token varchar(128),
				  tpmon_client_userid varchar(255),
				  tpmon_client_wkstn varchar(255),
				  tpmon_client_app varchar(255),
				  tpmon_acc_str varchar(255)
                 )
   language sql
   reads sql data
   no external action
   deterministic
   begin atomic
     return 
       select con.application_handle,
         con.application_name,
         current_server,
         con.session_auth_id,
         ignmon.appl_status(application_handle) as appl_status,
         null as client_nname,	
         null as codepage_id,
		 con.NUM_ASSOC_AGENTS,
		 null as coord_partition_num,
		 null as authority_lvl,
		 con.CLIENT_PID,
		 null as coord_agent_pid,
		 null as status_change_time,
		 con.CLIENT_PLATFORM,
		 con.client_protocol,
		 null as country_code,
		 con.application_id,
		 null as sequence_no,
		 con.CLIENT_PRDID,
		 current_server as input_db_alias,
		 current_server as client_db_alias,
		 null as db_path,
		 con.EXECUTION_ID,
		 null as corr_token,
		 con.CLIENT_USERID,
		 con.CLIENT_WRKSTNNAME,
		 con.CLIENT_APPLNAME,
		 con.CLIENT_ACCTNG
       from TABLE(SYSPROC.MON_GET_CONNECTION(NULL,partition_no,NULL)) as con;
   end
@

set current schema ignmon@
create or replace function snapshot_statement(db_name varchar(128) default NULL, partition_no integer default -1)
   returns table (agent_id bigint,
                  creator varchar(128),
                  package_name varchar(128),
                  section_number bigint,
                  stmt_partition_number smallint,
                  stmt_type bigint,
                  stmt_operation bigint,
                  stmt_start timestamp,
                  stmt_stop timestamp, 
                  stmt_text clob(2M)                  
                 )
   language sql
   reads sql data
   no external action
   deterministic
   begin atomic
     return 
       select act.application_handle,
         act.package_schema,
         act.package_name,
         act.section_number,
         act.member as stmt_partition_number,
         ignmon.stmt_type(act.application_handle,act.local_start_time) as stmt_type,
         ignmon.stmt_operation(act.application_handle,act.local_start_time) as stmt_operation,
         act.local_start_time as stmt_start,
         null as stmt_stop,
         act.stmt_text  
       from table(sysproc.MON_GET_CONNECTION(NULL,partition_no,NULL)) as con join
	        TABLE(SYSPROC.MON_GET_ACTIVITY(NULL,partition_no)) as act 
	      	on act.application_handle =  con.application_handle;
   end
@

set current schema ignmon@
create or replace function snap_get_appl_info_v95(db_name varchar(128) default NULL, partition_no integer default -1)
   returns table (agent_id bigint,
                  is_system_appl smallint
                 )
   language sql
   reads sql data
   no external action
   deterministic
   begin atomic
     return 
       select application_handle,
         is_system_appl  
       from TABLE(SYSPROC.MON_GET_ACTIVITY(NULL,partition_no));
   end
@

set current schema ignmon@
create or replace function snapshot_dyn_sql(db_name varchar(128) default NULL, partition_no integer default -1)
    returns table (stmt_text clob(2M),
                   num_executions bigint,  
                   num_compilations bigint, 
                   rows_read bigint,
                   rows_written bigint, 
                   stmt_sorts bigint)
    language sql
    reads sql data
    no external action
    deterministic
    begin atomic
      return
      select stmt_text,
        num_executions,
        num_compilations,
        rows_read,
        rows_written,
        stmt_sorts
      from table(sysproc.SNAP_GET_DYN_SQL(db_name,partition_no));	  
    end
@   

set current schema ignmon@
create or replace function snapshot_bp(db_name varchar(128) default NULL, partition_no integer default -1)
	returns table(BP_NAME varchar(128),
				  POOL_DATA_L_READS bigint,
				  POOL_DATA_P_READS bigint,
				  POOL_DATA_WRITES bigint,
				  POOL_INDEX_L_READS bigint,
				  POOL_INDEX_P_READS bigint,
				  POOL_INDEX_WRITES bigint,
				  POOL_READ_TIME bigint,
				  POOL_WRITE_TIME bigint,
				  POOL_ASYNC_DATA_READS bigint,
				  POOL_ASYNC_DATA_WRITES bigint,
				  POOL_ASYNC_INDEX_WRITES bigint,
				  POOL_ASYNC_READ_TIME bigint,
				  POOL_ASYNC_WRITE_TIME bigint,
				  POOL_ASYNC_DATA_READ_REQS bigint,
				  DIRECT_READS bigint,
				  DIRECT_WRITES bigint,
				  DIRECT_READ_REQS bigint,
				  DIRECT_WRITE_REQS bigint,
				  DIRECT_READ_TIME bigint,
				  DIRECT_WRITE_TIME bigint,
				  POOL_ASYNC_INDEX_READS bigint,
				  UNREAD_PREFETCH_PAGES bigint,
				  FILES_CLOSED bigint
				)
   language sql
   reads sql data
   no external action
   deterministic
   begin atomic
     return 
		select BP_NAME,
               POOL_DATA_L_READS,
               POOL_DATA_P_READS,
               POOL_DATA_WRITES,
               POOL_INDEX_L_READS,
               POOL_INDEX_P_READS,
               POOL_INDEX_WRITES,
               POOL_READ_TIME,
               POOL_WRITE_TIME,
               POOL_ASYNC_DATA_READS,
               POOL_ASYNC_DATA_WRITES,
               POOL_ASYNC_INDEX_WRITES,
               POOL_ASYNC_READ_TIME,
               POOL_ASYNC_WRITE_TIME,
               POOL_ASYNC_DATA_READ_REQS,
               DIRECT_READS,
               DIRECT_WRITES,
               DIRECT_READ_REQS,
               DIRECT_WRITE_REQS,
               DIRECT_READ_TIME,
               DIRECT_WRITE_TIME,
               POOL_ASYNC_INDEX_READS,
               UNREAD_PREFETCH_PAGES,
               FILES_CLOSED
		from TABLE(MON_GET_BUFFERPOOL(NULL,partition_no)) as bp;
	end
@

set current schema ignmon@
create or replace function snapshot_lock(db_name varchar(128) default NULL, partition_no integer default -1)
	returns table(APPLICATION_HANDLE bigint,
                  MEMBER smallint,
                  LOCK_NAME varchar(32),
                  LOCK_OBJ_TYPE varchar(32),
                  LOCK_MODE_TYPE varchar(3),
                  LOCK_CURRENT_MODE varchar(3),
                  LOCK_STATUS char(1),
                  LOCK_ATTRIBUTES char(16),
                  LOCK_RELEASE_FLAGS char(16),
                  LOCK_RRIID bigint,
                  LOCK_COUNT bigint,
                  LOCK_HOLD_COUNT bigint,
                  TBSP_ID bigint,
                  TAB_FILE_ID bigint,
				  AGENT_ID bigint
				)
   language sql
   reads sql data
   no external action
   deterministic
   begin atomic
     return 
		select APPLICATION_HANDLE,
               MEMBER,
               LOCK_NAME,
               LOCK_OBJECT_TYPE as LOCK_OBJ_TYPE,
               LOCK_MODE as LOCK_MODE_TYPE,
               LOCK_CURRENT_MODE,
               LOCK_STATUS,
               LOCK_ATTRIBUTES,
               LOCK_RELEASE_FLAGS,
               LOCK_RRIID,
               LOCK_COUNT,
               LOCK_HOLD_COUNT,
               TBSP_ID,
               TAB_FILE_ID,
			   APPLICATION_HANDLE as AGENT_ID
		from TABLE(MON_GET_LOCKS(NULL,partition_no)) as locks;
	end
@

set current schema ignmon@
create or replace function snapshot_tbs_cfg(db_name varchar(128) default NULL, partition_no integer default -1)
	returns table(tablespace_name varchar(128),
                  total_pages bigint,
                  free_pages bigint,
                  tablespace_type smallint
				)
   language sql
   reads sql data
   no external action
   deterministic
   begin atomic
     return 
		select TBSP_NAME,
               TBSP_TOTAL_PAGES,
               TBSP_FREE_PAGES,
			   CASE TBSP_TYPE WHEN 'DMS' THEN 0 ELSE 1 END as TABLESPACE_TYPE
		from TABLE(MON_GET_TABLESPACE(NULL,partition_no)) as tablespaces;
	end
@

set current schema ignmon@
create or replace function snapshot_database(db_name varchar(128) default NULL, partition_no integer default -1)
	returns table(LOCKS_WAITING bigint,
				  APPLS_IN_DB2 bigint,
				  PKG_CACHE_LOOKUPS bigint,
				  PKG_CACHE_INSERTS bigint,
				  CAT_CACHE_LOOKUPS bigint,
				  CAT_CACHE_INSERTS bigint,
				  COMMIT_SQL_STMTS bigint,
				  ROLLBACK_SQL_STMTS bigint,
				  INT_COMMITS bigint,
				  INT_ROLLBACKS bigint
				)
   language sql
   reads sql data
   no external action
   deterministic
   begin atomic
     return 
		select
			NUM_LOCKS_WAITING as LOCKS_WAITING,
			APPLS_IN_DB2,
			PKG_CACHE_LOOKUPS,
			PKG_CACHE_INSERTS,
			CAT_CACHE_LOOKUPS,
			CAT_CACHE_INSERTS,
			TOTAL_APP_COMMITS as COMMIT_SQL_STMTS,
			TOTAL_APP_ROLLBACKS as ROLLBACK_SQL_STMTS,
			INT_COMMITS,
			INT_ROLLBACKS
		from TABLE(MON_GET_DATABASE(partition_no)) as db;
	end
@
