SETUP HADR and TSA (This must be DONE in the below order.) 

Step 1. Run the scripts to output HADR and TSA configuration.

	a. Run the cr_HA_cfg.sh 
	b. Run the cr_TSA_cfg.sh 
	
Step 2.  Apply the settings from Step 1 to primary and secondary.

Step 3.  Backup Primary Online 

Step 4.  Restore Secondary from Primary backup

Step 5.  db2 start hadr on database {DBNAME} as standby 

Step 6.  db2 start hadr on database {DBNAME} as primary 

Step 7.  Verify HADR State - check for PEER STATE

	a. db2pd -db {DBNAME} -hadr  

Step 7.  On Staging - Configure TSA    

	a. db2haicu -f {DBNAME}.db2ha_HADR.xml

Step 8.  On Primary - Configure TSA

	a. db2haicu -f {DBNAME}.db2ha_HADR.xml

Step 9.  Verify TSA - All resources but one should say ONLINE 

	a. lssam 
