 
---------------------------------
-- DDL statements for stored procedures
---------------------------------


SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE BKFAIR01.P_LU_HOMPAGE_EVENT ( ) 
  SPECIFIC BKFAIR01.P_LU_HOMPAGE_EVENT
  LANGUAGE SQL
  NOT DETERMINISTIC
  CALLED ON NULL INPUT
  MODIFIES SQL DATA
  INHERIT SPECIAL REGISTERS
  BEGIN 
         DECLARE V_EVENT_ID_COUNT          BIGINT; --
         DECLARE V_EVENT_ID            BIGINT;--
         DECLARE V_EVENT_PRODUCT_ID        VARCHAR(2);--
         DECLARE V_EVENT_START_DATE    DATE;--
         DECLARE V_EVENT_END_DATE          DATE;--
         DECLARE V_EVENT_NAME              VARCHAR(30);--

         DECLARE SQLSTATE CHAR(5); --
         DECLARE c_homepage_event CURSOR FOR 
         
         SELECT BIGINT(EVENT_ID),
                        EVENT_PRODUCT_ID,
                        DATE(substr(EVENT_START_DATE,1,4)||'-'||substr(EVENT_START_DATE,5,2)||'-'||substr(EVENT_START_DATE,7,2)),  
                        DATE(substr(EVENT_END_DATE,1,4)||'-'||substr(EVENT_END_DATE,5,2)||'-'||substr(EVENT_END_DATE,7,2)),
                        EVENT_NAME
                        FROM BKFAIR01.LU_HOMEPAGE_EVENT_LOAD 
                        WHERE EVENT_ID != ''  
                        AND EVENT_PRODUCT_ID != ''
                        AND EVENT_START_DATE != ''
                        AND EVENT_END_DATE != ''
                        AND EVENT_NAME != '' ;--

         OPEN c_homepage_event; --

         FETCH FROM c_homepage_event 
                   INTO   V_EVENT_ID,           V_EVENT_PRODUCT_ID,         V_EVENT_START_DATE,  
                                  V_EVENT_END_DATE,     V_EVENT_NAME;--
                                          
         WHILE (SQLSTATE = '00000') DO
              
          SELECT COUNT(EVENT_ID) INTO V_EVENT_ID_COUNT FROM BKFAIR01.LU_HOMEPAGE_EVENT 
                                WHERE EVENT_ID = V_EVENT_ID 
                            AND EVENT_PRODUCT_ID = V_EVENT_PRODUCT_ID;--
                                                        
          IF V_EVENT_ID_COUNT = 0 THEN   
                 
                 INSERT INTO BKFAIR01.LU_HOMEPAGE_EVENT 
                                         (EVENT_ID,                     EVENT_PRODUCT_ID,             EVENT_START_DATE,                 EVENT_END_DATE,            
                                         EVENT_NAME,                            CREATE_DATE,                      UPDATE_DATE)  

                 VALUES  (V_EVENT_ID,                   V_EVENT_PRODUCT_ID,           V_EVENT_START_DATE,               V_EVENT_END_DATE,                  
                          V_EVENT_NAME,                         CURRENT TIMESTAMP,                        CURRENT TIMESTAMP);  --

          ELSE   
                  UPDATE BKFAIR01.LU_HOMEPAGE_EVENT 
                                            SET EVENT_START_DATE   = V_EVENT_START_DATE,  
                                                        EVENT_END_DATE     = V_EVENT_END_DATE,  
                                                    EVENT_NAME             = V_EVENT_NAME,  
                                                UPDATE_DATE        = CURRENT TIMESTAMP 
                                                WHERE EVENT_ID             = V_EVENT_ID
                                                AND   EVENT_PRODUCT_ID = V_EVENT_PRODUCT_ID;--
          END IF;       --

         FETCH FROM c_homepage_event 
                   INTO   V_EVENT_ID,           V_EVENT_PRODUCT_ID,         V_EVENT_START_DATE,  
                                  V_EVENT_END_DATE,     V_EVENT_NAME;--

          
     END WHILE;                    --
         CLOSE c_homepage_event;    --
         COMMIT;    --
END;

SET CURRENT SCHEMA = "BKFRPRD ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE BKFAIR01.P_SHARE_IDEA ( ) 
  SPECIFIC BKFAIR01.P_SHARE_IDEA
  LANGUAGE SQL
  NOT DETERMINISTIC
  CALLED ON NULL INPUT
  MODIFIES SQL DATA
  INHERIT SPECIAL REGISTERS
  BEGIN 
         DECLARE V_IDEA_TOPIC_ID_COUNT   BIGINT;--
         DECLARE V_IDEA_TOPIC_ID                 INTEGER;--
         DECLARE V_IDEA_TOPIC                VARCHAR(30);--
         DECLARE V_IDEA_PRODUCT_ID        CHARACTER(2);--
         DECLARE V_IDEA_INCLUDE_FILE     CHARACTER(90);--

         DECLARE SQLSTATE CHAR(5); --

         DECLARE c_book_fair CURSOR FOR 
     SELECT INTEGER(IDEA_TOPIC_ID), 
                        IDEA_TOPIC,
                    IDEA_PRODUCT_ID, 
                        IDEA_INCLUDE_FILE       
         FROM BKFAIR01.LU_SHARE_IDEA_DATA_LOAD; --

         DELETE FROM BKFAIR01.LU_SHARE_IDEA;--

         OPEN c_book_fair; --
         FETCH FROM c_book_fair 
                   INTO       V_IDEA_TOPIC_ID,                             V_IDEA_TOPIC, V_IDEA_PRODUCT_ID, V_IDEA_INCLUDE_FILE ; --

         WHILE (SQLSTATE = '00000') DO     
          SELECT COUNT(IDEA_TOPIC_ID) INTO V_IDEA_TOPIC_ID_COUNT FROM BKFAIR01.LU_SHARE_IDEA
                                                WHERE IDEA_TOPIC_ID = V_IDEA_TOPIC_ID
                                                AND IDEA_PRODUCT_ID = V_IDEA_PRODUCT_ID; --
          IF V_IDEA_TOPIC_ID_COUNT = 0 THEN   
                 INSERT INTO BKFAIR01.LU_SHARE_IDEA 
                                 (IDEA_TOPIC_ID,                 IDEA_TOPIC,IDEA_PRODUCT_ID,IDEA_INCLUDE_FILE)
                 VALUES ( V_IDEA_TOPIC_ID,               V_IDEA_TOPIC, V_IDEA_PRODUCT_ID, V_IDEA_INCLUDE_FILE);  --
          ELSE   
                  UPDATE BKFAIR01.LU_SHARE_IDEA 
                                            SET IDEA_TOPIC         = V_IDEA_TOPIC,  
                                                        UPDATE_DATE                = CURRENT TIMESTAMP,
                                                        IDEA_PRODUCT_ID    = V_IDEA_PRODUCT_ID,
                                                        IDEA_INCLUDE_FILE  = V_IDEA_INCLUDE_FILE
                                                WHERE IDEA_TOPIC_ID    = V_IDEA_TOPIC_ID;--
          END IF;       --
         FETCH FROM c_book_fair 
                   INTO       V_IDEA_TOPIC_ID,                             V_IDEA_TOPIC, V_IDEA_PRODUCT_ID, V_IDEA_INCLUDE_FILE ;  --
     END WHILE;                    --
         CLOSE c_book_fair;    --
         COMMIT;    --
END;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE BKFAIR01.P_FUEL_SURCHARGE ( ) 
  SPECIFIC BKFAIR01.P_FUEL_SURCHARGE
  LANGUAGE SQL
  NOT DETERMINISTIC
  CALLED ON NULL INPUT
  MODIFIES SQL DATA
  INHERIT SPECIAL REGISTERS
  BEGIN
         DECLARE V_SURCHARGE_ID_COUNT                  BIGINT;  --
         DECLARE V_SURCHARGE_ID                        BIGINT;  --
         DECLARE V_SURCHARGE_START_DATE                DATE;--
         DECLARE V_SURCHARGE_END_DATE                  DATE;--
         DECLARE V_BRANCH_ID                           INTEGER;--
         DECLARE V_FAIR_TYPE                           VARCHAR(1);--
         DECLARE V_TOTAL_FAIR_SALES_FROM               BIGINT;--
         DECLARE V_TOTAL_FAIR_SALES_TO                 BIGINT;--
         DECLARE V_SURCHARGE                           INTEGER; --

         DECLARE SQLSTATE CHAR(5); --

         DECLARE c_book_fair CURSOR FOR 
     SELECT BIGINT(SURCHARGE_ID),

     DATE(substr(SURCHARGE_START_DATE,1,4)||'-'||substr(SURCHARGE_START_DATE,5,2)||'-'||substr(SURCHARGE_START_DATE,7,2)),
     DATE(substr(SURCHARGE_END_DATE,1,4)||'-'||substr(SURCHARGE_END_DATE,5,2)||'-'||substr(SURCHARGE_END_DATE,7,2)),
     INTEGER(BRANCH_ID),
     FAIR_TYPE,
     BIGINT(TOTAL_FAIR_SALES_FROM),
     BIGINT(TOTAL_FAIR_SALES_TO),
     INTEGER(SURCHARGE)                                            
                                                
         FROM BKFAIR01.FUEL_SURCHARGE_DATA_LOAD; --
         DELETE FROM BKFAIR01.FUEL_SURCHARGE; --
         OPEN c_book_fair; --
         FETCH FROM c_book_fair 
                   INTO   V_SURCHARGE_ID, V_SURCHARGE_START_DATE, V_SURCHARGE_END_DATE, V_BRANCH_ID, V_FAIR_TYPE, 
                          V_TOTAL_FAIR_SALES_FROM, V_TOTAL_FAIR_SALES_TO, V_SURCHARGE;--
         WHILE (SQLSTATE = '00000') DO     
          SELECT COUNT(SURCHARGE_ID) INTO V_SURCHARGE_ID_COUNT FROM BKFAIR01.FUEL_SURCHARGE
                                                WHERE SURCHARGE_ID  = V_SURCHARGE_ID;--
          IF V_SURCHARGE_ID_COUNT = 0 THEN   
                 INSERT INTO BKFAIR01.FUEL_SURCHARGE 
                                (SURCHARGE_ID, SURCHARGE_START_DATE, SURCHARGE_END_DATE, BRANCH_ID, FAIR_TYPE,
                                  TOTAL_FAIR_SALES_FROM, TOTAL_FAIR_SALES_TO, SURCHARGE)
                 VALUES (V_SURCHARGE_ID, V_SURCHARGE_START_DATE, V_SURCHARGE_END_DATE, V_BRANCH_ID,V_FAIR_TYPE,
                         V_TOTAL_FAIR_SALES_FROM, V_TOTAL_FAIR_SALES_TO, V_SURCHARGE);--
                           
          ELSE   
                  UPDATE BKFAIR01.FUEL_SURCHARGE 
                                            SET  SURCHARGE_ID    = V_SURCHARGE_ID,
                                                 SURCHARGE_START_DATE   = V_SURCHARGE_START_DATE,
                                                 SURCHARGE_END_DATE     = V_SURCHARGE_END_DATE,
                                                 BRANCH_ID              = V_BRANCH_ID,
                                                 FAIR_TYPE              = V_FAIR_TYPE,
                                                 TOTAL_FAIR_SALES_FROM  = V_TOTAL_FAIR_SALES_FROM,
                                                 TOTAL_FAIR_SALES_TO    = V_TOTAL_FAIR_SALES_TO,
                                                 SURCHARGE              = V_SURCHARGE
                                                     
                                                        
                                                WHERE SURCHARGE_ID = V_SURCHARGE_ID;--
          END IF;       --
         FETCH FROM c_book_fair
                    INTO V_SURCHARGE_ID, V_SURCHARGE_START_DATE, V_SURCHARGE_END_DATE, V_BRANCH_ID,V_FAIR_TYPE, 
                             V_TOTAL_FAIR_SALES_FROM, V_TOTAL_FAIR_SALES_TO, V_SURCHARGE;         --
                             
     END WHILE;                    --
         CLOSE c_book_fair;    --
         COMMIT;    --
END;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE "BKFAIR01"."P_COA" ( ) 
  SPECIFIC "BKFAIR01"."P_COA"
  LANGUAGE SQL
  NOT DETERMINISTIC
  CALLED ON NULL INPUT
  EXTERNAL ACTION
  OLD SAVEPOINT LEVEL
  MODIFIES SQL DATA
  INHERIT SPECIAL REGISTERS
  BEGIN 
     DECLARE V_COA_COUNT BIGINT; --
         DECLARE V_COA_FAIR_TYPE CHAR(1);--
         DECLARE V_COA_START_DATE DATE;--
         DECLARE V_COA_END_DATE DATE;--
         DECLARE V_COA_FILE_NAME VARCHAR(30);--

         DECLARE SQLSTATE CHAR(5); --

         DECLARE c_coa CURSOR FOR 
     SELECT CHAR(COA_FAIR_TYPE) , 
                        DATE(substr(COA_START_DATE,1,4)||'-'||substr(COA_START_DATE,5,2)||'-'||substr(COA_START_DATE,7,2)),  
                        DATE(substr(COA_END_DATE,1,4)||'-'||substr(COA_END_DATE,5,2)||'-'||substr(COA_END_DATE,7,2)),  
                        VARCHAR(COA_FILE_NAME)
         FROM BKFAIR01.LU_COA_LOAD; --

         OPEN c_coa; --
         FETCH FROM c_coa 
                   INTO       V_COA_FAIR_TYPE,  V_COA_START_DATE,       V_COA_END_DATE ,  V_COA_FILE_NAME       ;               --
                      
         WHILE (SQLSTATE = '00000') DO     
         
         SELECT COUNT(*) INTO V_COA_COUNT FROM BKFAIR01.LU_COA_FILE
                                                WHERE COA_FAIR_TYPE = V_COA_FAIR_TYPE AND COA_START_DATE=V_COA_START_DATE;--

     IF V_COA_COUNT = 0 THEN                                                                    
                      INSERT INTO bkfair01.LU_COA_FILE 
                      ( COA_FAIR_TYPE, COA_START_DATE, COA_END_DATE,COA_FILE_NAME, CREATE_DATE, UPDATE_DATE  )
              VALUES (V_COA_FAIR_TYPE, V_COA_START_DATE, V_COA_END_DATE,V_COA_FILE_NAME, CURRENT TIMESTAMP , CURRENT TIMESTAMP);--
        ELSE
                    UPDATE BKFAIR01.LU_COA_FILE
                             SET  UPDATE_DATE                   = CURRENT TIMESTAMP,
                                      COA_END_DATE        = V_COA_END_DATE,
                                      COA_FILE_NAME  = V_COA_FILE_NAME
                             WHERE COA_FAIR_TYPE = V_COA_FAIR_TYPE AND COA_START_DATE=V_COA_START_DATE; --
                        
         END IF;--
         FETCH FROM c_coa
                   INTO       V_COA_FAIR_TYPE,  V_COA_START_DATE,       V_COA_END_DATE ,  V_COA_FILE_NAME;                 --
     END WHILE;                    --
         CLOSE c_coa;    --
         COMMIT;    --
END;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE "BKFAIR01"."P_REWARDS_CHILD" ( ) 
  SPECIFIC "BKFAIR01"."P_REWARDS_CHILD"
  LANGUAGE SQL
  NOT DETERMINISTIC
  CALLED ON NULL INPUT
  EXTERNAL ACTION
  OLD SAVEPOINT LEVEL
  MODIFIES SQL DATA
  INHERIT SPECIAL REGISTERS
  BEGIN 
         DECLARE V_REWARD_CHILD_COUNT   BIGINT; --
         DECLARE V_REWARD_ID                    BIGINT;                                                  --
         DECLARE V_CHILD_SEQUENCE_NO    INTEGER;--
         DECLARE V_CHILD_DESCRIPTION    VARCHAR(256);--
         DECLARE V_CHILD_RESPONSE_TYPE  VARCHAR(1);--
         DECLARE V_REWARD_SUB_DESC_MANDATORY VARCHAR(1);--
         DECLARE SQLSTATE CHAR(5); --
         DECLARE c_book_fair CURSOR FOR 
     SELECT    BIGINT(REWARD_ID),                                                        
                           INTEGER(CHILD_SEQUENCE_NO),
                           CHILD_DESCRIPTION,                      
                           CHILD_RESPONSE_TYPE,
                           REWARD_SUB_DESC_MANDATORY
        FROM  BKFAIR01.CUSTOMER_REWARDS_CHILD_LOAD;--
        
         OPEN c_book_fair; --
         FETCH FROM c_book_fair
            INTO  V_REWARD_ID,             V_CHILD_SEQUENCE_NO,   V_CHILD_DESCRIPTION,           V_CHILD_RESPONSE_TYPE, V_REWARD_SUB_DESC_MANDATORY ;--
         WHILE (SQLSTATE = '00000') DO     
          SELECT COUNT(REWARD_ID) INTO V_REWARD_CHILD_COUNT FROM BKFAIR01.CUSTOMER_REWARDS_CHILD
                                                WHERE REWARD_ID = V_REWARD_ID AND CHILD_SEQUENCE_NO = V_CHILD_SEQUENCE_NO;--
          IF V_REWARD_CHILD_COUNT = 0 THEN   
                 INSERT INTO BKFAIR01.CUSTOMER_REWARDS_CHILD 
                                 (REWARD_ID,     CHILD_SEQUENCE_NO,     CHILD_DESCRIPTION,              CHILD_RESPONSE_TYPE ,  CREATE_DATE,     UPDATE_DATE, REWARD_SUB_DESC_MANDATORY )
                                 
                 VALUES ( V_REWARD_ID,   V_CHILD_SEQUENCE_NO,   V_CHILD_DESCRIPTION,    V_CHILD_RESPONSE_TYPE, CURRENT TIMESTAMP , CURRENT TIMESTAMP, V_REWARD_SUB_DESC_MANDATORY );  --
          ELSE   
                  UPDATE BKFAIR01.CUSTOMER_REWARDS_CHILD 
                                            SET CHILD_DESCRIPTION               = V_CHILD_DESCRIPTION,
                                                        CHILD_RESPONSE_TYPE         = V_CHILD_RESPONSE_TYPE,  
                                                        UPDATE_DATE                                     = CURRENT TIMESTAMP,
                                                        REWARD_SUB_DESC_MANDATORY = V_REWARD_SUB_DESC_MANDATORY
                                                WHERE REWARD_ID                         = V_REWARD_ID
                                                AND       CHILD_SEQUENCE_NO             = V_CHILD_SEQUENCE_NO;--
          END IF;       --
         FETCH FROM c_book_fair
            INTO  V_REWARD_ID,             V_CHILD_SEQUENCE_NO,   V_CHILD_DESCRIPTION,           V_CHILD_RESPONSE_TYPE, V_REWARD_SUB_DESC_MANDATORY ;--
     END WHILE;                    --
         CLOSE c_book_fair;    --
         COMMIT;    --
END;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE "BKFAIR01"."P_REWARDS_PARENT" ( ) 
  SPECIFIC "BKFAIR01"."P_REWARDS_PARENT"
  LANGUAGE SQL
  NOT DETERMINISTIC
  CALLED ON NULL INPUT
  EXTERNAL ACTION
  OLD SAVEPOINT LEVEL
  MODIFIES SQL DATA
  INHERIT SPECIAL REGISTERS
  BEGIN 
         DECLARE V_REWARD_PARENT_COUNT    BIGINT; --
         DECLARE V_REWARD_PARENT_ID      BIGINT;                                                         --
         DECLARE V_REWARD_ID             BIGINT;                                                         --
         DECLARE V_SEQUENCE_NO                   INTEGER;--
         DECLARE V_REGION_ID                     VARCHAR(2);--
         DECLARE V_PRODUCT_ID                    VARCHAR(2);--
         DECLARE V_DESCRIPTION                   VARCHAR(255);--
         DECLARE V_RESPONSE_TYPE                 VARCHAR(1);--
         DECLARE V_REWARD_START_DATE     DATE   ;--
         DECLARE V_REWARD_END_DATE               DATE;    --
         DECLARE V_VOUCHER_VALUE                 DECIMAL(5,2);--
         DECLARE SQLSTATE CHAR(5); --
         DECLARE c_book_fair CURSOR FOR 
     SELECT
            BIGINT(REWARD_ID),                                                   
                INTEGER(SEQUENCE_NO),
            REGION_ID,
                PRODUCT_ID,
                DESCRIPTION,
                RESPONSE_TYPE,
                (CASE WHEN REWARD_START_DATE != '' THEN DATE(substr(REWARD_START_DATE,1,4)||'-'||substr(REWARD_START_DATE,5,2)||'-'||substr(REWARD_START_DATE,7,2)) ELSE NULL END),  
                (CASE WHEN REWARD_END_DATE != '' THEN DATE(substr(REWARD_END_DATE,1,4)||'-'||substr(REWARD_END_DATE,5,2)||'-'||substr(REWARD_END_DATE,7,2)) ELSE NULL END),  
                (CASE WHEN VOUCHER_VALUE != '' THEN (decimal(VOUCHER_VALUE,7,2,'.')/100) ELSE 0.00 END)
        FROM  BKFAIR01.CUSTOMER_REWARDS_PARENT_LOAD;--
         OPEN c_book_fair; --
         FETCH FROM c_book_fair
            INTO  V_REWARD_ID,      V_SEQUENCE_NO,             V_REGION_ID,              V_PRODUCT_ID,             V_DESCRIPTION,
              V_RESPONSE_TYPE,  V_REWARD_START_DATE,   V_REWARD_END_DATE ,   V_VOUCHER_VALUE ;--
         WHILE (SQLSTATE = '00000') DO     

          SELECT COUNT(REWARD_PARENT_ID) INTO V_REWARD_PARENT_COUNT FROM BKFAIR01.CUSTOMER_REWARDS_PARENT
                                                WHERE REWARD_ID       = V_REWARD_ID 
                                                AND SEQUENCE_NO       = V_SEQUENCE_NO 
                                                AND REGION_ID         = V_REGION_ID 
                                                AND PRODUCT_ID        = V_PRODUCT_ID
                                                AND REWARD_START_DATE = V_REWARD_START_DATE;--
                                                
          IF V_REWARD_PARENT_COUNT = 0 THEN   
           SELECT NEXTVAL FOR BKFAIR01.SEQ_CUSTOMER_REWARDS_PARENT INTO V_REWARD_PARENT_ID FROM sysibm.sysdummy1;--

                 INSERT INTO BKFAIR01.CUSTOMER_REWARDS_PARENT 
                                 (REWARD_PARENT_ID,   REWARD_ID,                 SEQUENCE_NO,                    REGION_ID,              PRODUCT_ID,             DESCRIPTION,     
                      RESPONSE_TYPE,      REWARD_START_DATE,     REWARD_END_DATE ,       VOUCHER_VALUE,      CREATE_DATE,             UPDATE_DATE )
                                 
                 VALUES ( V_REWARD_PARENT_ID,  V_REWARD_ID,          V_SEQUENCE_NO,               V_REGION_ID,            V_PRODUCT_ID,          V_DESCRIPTION,    
                          V_RESPONSE_TYPE,     V_REWARD_START_DATE,   V_REWARD_END_DATE ,     V_VOUCHER_VALUE,    CURRENT TIMESTAMP , CURRENT TIMESTAMP );  --
          ELSE   
                  UPDATE BKFAIR01.CUSTOMER_REWARDS_PARENT 
                                            SET DESCRIPTION             = V_DESCRIPTION,  
                                                        RESPONSE_TYPE           = V_RESPONSE_TYPE,  
                                                    REWARD_START_DATE   = V_REWARD_START_DATE,  
                                                        REWARD_END_DATE         = V_REWARD_END_DATE,
                                                        VOUCHER_VALUE           = V_VOUCHER_VALUE , 
                                                        UPDATE_DATE                     = CURRENT TIMESTAMP
                                                WHERE REWARD_ID         = V_REWARD_ID
                                                AND       SEQUENCE_NO       = V_SEQUENCE_NO
                                                AND REGION_ID                   = V_REGION_ID 
                                                AND PRODUCT_ID                  = V_PRODUCT_ID
                                                AND REWARD_START_DATE = V_REWARD_START_DATE;--
          END IF;       --
         FETCH FROM c_book_fair
            INTO  V_REWARD_ID,      V_SEQUENCE_NO,             V_REGION_ID,              V_PRODUCT_ID,             V_DESCRIPTION,
              V_RESPONSE_TYPE,  V_REWARD_START_DATE,   V_REWARD_END_DATE ,   V_VOUCHER_VALUE ;--
     END WHILE;                    --
         CLOSE c_book_fair;    --
         COMMIT;    --
END;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_bkfair_ofe_stsupdt ( )
   SPECIFIC sp_bkfair_ofe_stsupdt
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   OLD SAVEPOINT LEVEL
   BEGIN
      -- Submitted to Active
      UPDATE bkfair01.online_shopping
      SET ofe_status = 'Active', 
          updated_date = CURRENT TIMESTAMP
      WHERE CURRENT DATE >= ofe_start_date 
        AND ofe_status = 'Submitted';--

      -- Active to Complete
      UPDATE bkfair01.online_shopping
      SET ofe_status = 'Complete', 
          updated_date = CURRENT TIMESTAMP
      WHERE ( ofe_end_date + 1 DAY ) <= CURRENT DATE 
        AND ofe_status = 'Active';--
      
      COMMIT;--
   END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_BKFAIR_OFE_STSUPDT"() IS 'Bkfair release 19, Oct. 2011; Runs only via the loading script bkfair_load_secondary_export.ksh; Sets OFE_STATUS to ACTIVE or COMPLETE following specific Business rules.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_profit_balance ( )
   SPECIFIC sp_profit_balance
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   OLD SAVEPOINT LEVEL
   BEGIN
      DECLARE v_school_id                BIGINT;--
      DECLARE v_reward_description       VARCHAR ( 30 );--
      DECLARE v_voucher_issue_date       DATE;--
      DECLARE v_voucher_type             VARCHAR ( 1 );--
      DECLARE v_voucher_expiration_date  DATE;--
      DECLARE v_voucher_amount           DECIMAL ( 10, 2 );--
      DECLARE v_book_profit              DECIMAL ( 10, 2 );--
      DECLARE sqlstate                   CHAR ( 5 );--

      DECLARE c_book_fair CURSOR WITH HOLD FOR
              SELECT BIGINT ( school_id ),
                     -- REWARD_DESCRIPTION,
                     ( CASE WHEN reward_description = '' 
                            THEN NULL 
                            ELSE reward_description 
                            END ),
                     ( CASE WHEN voucher_issue_date != '00000000' 
                            THEN DATE ( SUBSTR ( voucher_issue_date, 1, 4 ) || 
                                        '-' || SUBSTR ( voucher_issue_date, 5, 2 ) || 
                                        '-' || SUBSTR ( voucher_issue_date, 7, 2 ) ) 
                            ELSE NULL 
                            END ), 
                     ( CASE WHEN voucher_type = '' 
                            THEN NULL 
                            ELSE voucher_type 
                            END ), 
                     ( CASE WHEN voucher_expiration_date != '00000000' 
                            THEN DATE ( SUBSTR ( voucher_expiration_date, 1, 4 ) || 
                                        '-' || SUBSTR ( voucher_expiration_date, 5, 2 ) || 
                                        '-' || SUBSTR ( voucher_expiration_date, 7, 2 ) ) 
                            ELSE NULL 
                            END ),
                     ( CASE WHEN voucher_amount != '' 
                            THEN ( DECIMAL ( voucher_amount, 12, 2, '.' ) / 100 ) 
                            ELSE 0.00 
                            END ),
                     ( CASE WHEN book_profit != '' 
                          THEN ( DECIMAL ( book_profit, 12, 2, '.' ) / 100 ) 
                          ELSE 0.00 
                          END )
              FROM bkfair01.profit_balance_data_load
              WHERE voucher_type IN ('P', 'R', 'O', '') 
                AND INTEGER ( school_id ) IN ( SELECT id FROM bkfair01.school );--

      DELETE FROM bkfair01.profit_balance;--
      COMMIT;--
      OPEN c_book_fair;--

      FETCH FROM c_book_fair
      INTO v_school_id,    v_reward_description,      v_voucher_issue_date, 
           v_voucher_type, v_voucher_expiration_date, v_voucher_amount,
           v_book_profit;--

      WHILE ( sqlstate = '00000' ) DO
         INSERT INTO bkfair01.profit_balance (
                     school_id,    reward_description,        voucher_issue_date,
                     voucher_type, voucher_expiration_date,   voucher_amount,
                     book_profit,  create_date,               update_date )
                VALUES ( v_school_id,     v_reward_description,      v_voucher_issue_date,
                         v_voucher_type,  v_voucher_expiration_date, v_voucher_amount,
                         v_book_profit,   CURRENT TIMESTAMP,         CURRENT TIMESTAMP );--
         COMMIT;--
         FETCH FROM c_book_fair
         INTO v_school_id,    v_reward_description,      v_voucher_issue_date, 
              v_voucher_type, v_voucher_expiration_date, v_voucher_amount,
              v_book_profit;--
      END WHILE;--

      CLOSE c_book_fair;--

      COMMIT;--
   END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_PROFIT_BALANCE"() IS 'Bkfair release 19, Nov. 2011; Reinstated to replace an Export/Import replacing it in daily bookfair loading script.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.p_profit_rules ( )
   SPECIFIC p_profit_rules
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
BEGIN
      DECLARE v_profit_id                     BIGINT;--
      DECLARE v_fair_type                     VARCHAR(1);--
      DECLARE v_description                   VARCHAR(20);--
      DECLARE v_total_fair_sales_from         BIGINT;--
      DECLARE v_total_fair_sales_to           INTEGER;--
      DECLARE v_book_profit_percentage        INTEGER;--
      DECLARE v_irc_profit_percentage         INTEGER;--
      DECLARE v_cash_profit_percentage        INTEGER;--
      DECLARE v_profit_start_date             DATE;--
      DECLARE v_profit_end_date               DATE;--
      DECLARE v_max_bonus_percentage          INTEGER;--
      DECLARE v_max_bonus_increase_percentage INTEGER;--
      DECLARE v_booking_bonus_percentage      INTEGER;--
      DECLARE v_bogo_bonus_percentage         INTEGER;--
      DECLARE v_growth_percentage             INTEGER;--
      DECLARE v_second_bogo_bonus_percentage  INTEGER;--

      DECLARE sqlstate                        CHAR(5);--

      DECLARE c_book_fair CURSOR FOR
         SELECT BIGINT ( profit_id ),
                fair_type,
                description,
                BIGINT ( total_fair_sales_from ),
                INTEGER ( total_fair_sales_to ),
                INTEGER ( book_profit_percentage ),
                INTEGER ( irc_profit_percentage ),
                INTEGER ( cash_profit_percentage ),
                DATE ( SUBSTR ( profit_start_date, 1, 4 ) || '-' || 
                       SUBSTR ( profit_start_date, 5, 2 ) || '-' || 
                       SUBSTR ( profit_start_date, 7, 2 ) ),
                DATE ( SUBSTR ( profit_end_date, 1, 4 ) || '-' || 
                       SUBSTR ( profit_end_date, 5, 2 ) || '-' || 
                       SUBSTR ( profit_end_date, 7, 2 ) ),
                INTEGER ( max_bonus_percentage ),
                INTEGER ( max_bonus_increase_percentage ),
                INTEGER ( booking_bonus_percentage ),
                ( CASE WHEN bogo_bonus_percentage != '' 
                       THEN INTEGER ( bogo_bonus_percentage ) 
                       ELSE 0 END ),
                INTEGER ( growth_percentage ),
                INTEGER ( second_bogo_bonus_percentage )
          FROM bkfair01.profit_rules_data_load;--

      DELETE FROM bkfair01.profit_rules;--

      OPEN c_book_fair;--

      FETCH FROM c_book_fair
         INTO v_profit_id,                v_fair_type,                v_description, 
              v_total_fair_sales_from,    v_total_fair_sales_to,      v_book_profit_percentage, 
              v_irc_profit_percentage,    v_cash_profit_percentage,   v_profit_start_date, 
              v_profit_end_date,          v_max_bonus_percentage,     v_max_bonus_increase_percentage, 
              v_booking_bonus_percentage, v_bogo_bonus_percentage,    v_growth_percentage,
              v_second_bogo_bonus_percentage;--

      WHILE ( sqlstate = '00000' ) DO
         INSERT INTO bkfair01.profit_rules (
                     profit_id,                fair_type,              description,
                     total_fair_sales_from,    total_fair_sales_to,    book_profit_percentage,
                     irc_profit_percentage,    cash_profit_percentage, profit_start_date,
                     profit_end_date,          max_bonus_percentage,   max_bonus_increase_percentage,
                     booking_bonus_percentage, bogo_bonus_percentage,  growth_percentage,
                     second_bogo_bonus_percentage )
         VALUES ( v_profit_id,                v_fair_type,               v_description,
                  v_total_fair_sales_from,    v_total_fair_sales_to,     v_book_profit_percentage,
                  v_irc_profit_percentage,    v_cash_profit_percentage,  v_profit_start_date,
                  v_profit_end_date,          v_max_bonus_percentage,    v_max_bonus_increase_percentage,
                  v_booking_bonus_percentage, v_bogo_bonus_percentage,   v_growth_percentage,
                  v_second_bogo_bonus_percentage );--

         FETCH FROM c_book_fair
         INTO v_profit_id,                v_fair_type,              v_description, 
              v_total_fair_sales_from,    v_total_fair_sales_to,    v_book_profit_percentage, 
              v_irc_profit_percentage,    v_cash_profit_percentage, v_profit_start_date, 
              v_profit_end_date,          v_max_bonus_percentage,   v_max_bonus_increase_percentage, 
              v_booking_bonus_percentage, v_bogo_bonus_percentage,  v_growth_percentage,
              v_second_bogo_bonus_percentage;--
      END WHILE;--

      CLOSE c_book_fair;--

      COMMIT;--
   END;

COMMENT ON PROCEDURE "BKFAIR01"."P_PROFIT_RULES"() IS 'Modified for Release 23.5 in swimlane-1 without having release 23; Added handling of second_bogo_bonus_percentage.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.p_mx_fair_hist_prt()
   SPECIFIC p_mx_fair_hist_prt
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
      -- History: Release 23.5 (BTS)
      --          Add handling of new column "storia_sales"
   BEGIN
      DECLARE v_school_id_count         BIGINT;--
      DECLARE v_school_id               BIGINT;--
      DECLARE v_previous_fair_id        BIGINT;--
      DECLARE v_history_fair_date       DATE;--
      DECLARE v_history_fair_sales      DECIMAL(10, 2);--
      DECLARE v_history_fair_type       VARCHAR(1);--
      DECLARE v_online_fair_sales       DECIMAL(10, 2);--
      DECLARE v_storia_sales            DECIMAL(10, 2);--
      DECLARE sqlstate                  CHAR(5);--
  
      DECLARE c_mx_fair_hist CURSOR FOR
         SELECT BIGINT(school_id), 
                BIGINT(previous_fair_id), 
                ( CASE WHEN history_fair_date != '' 
                    THEN DATE( SUBSTR( history_fair_date, 1, 4 ) || '-' || 
                         SUBSTR( history_fair_date, 5, 2 ) || '-' || 
                         SUBSTR( history_fair_date, 7, 2 )) 
                    ELSE NULL 
                    END ), 
                ( CASE WHEN history_fair_sales != '' 
                    THEN (DECIMAL( history_fair_sales, 12, 2, '.' ) / 100 ) 
                    ELSE 0.00 
                    END ), 
                history_fair_type, 
                ( CASE WHEN online_fair_sales != '' 
                    THEN ( DECIMAL( online_fair_sales, 10, 2, '.' ) / 100 ) 
                    ELSE 0.00 
                    END ),
                ( CASE WHEN storia_sales != '' 
                    THEN ( DECIMAL( storia_sales, 10, 2, '.' ) / 100 ) 
                    ELSE 0.00 
                    END )
            FROM bkfair01.max_fair_history_prt_load
            WHERE INTEGER(school_id) IN 
               ( SELECT id FROM bkfair01.school );--
  
         -- Body starts here...
      DELETE FROM bkfair01.max_fair_history_prt;--
  
      COMMIT;--
  
      OPEN c_mx_fair_hist;--
  
      FETCH FROM c_mx_fair_hist
         INTO v_school_id, v_previous_fair_id, 
              v_history_fair_date, v_history_fair_sales, 
              v_history_fair_type, v_online_fair_sales,
              v_storia_sales;--
  
      WHILE (sqlstate = '00000') DO
         SELECT count(*) INTO v_school_id_count
            FROM bkfair01.max_fair_history_prt
            WHERE school_id = v_school_id 
              AND previous_fair_id = v_previous_fair_id;--
  
         IF v_school_id_count = 0 THEN
            INSERT INTO bkfair01.max_fair_history_prt( 
                        school_id, previous_fair_id, 
                        history_fair_date, history_fair_sales,
                        history_fair_type, online_fair_sales,
                        storia_sales)
          VALUES ( v_school_id, v_previous_fair_id, 
                   v_history_fair_date, v_history_fair_sales, 
                   v_history_fair_type, v_online_fair_sales, 
                   v_storia_sales );--
         ELSE
            UPDATE bkfair01.max_fair_history_prt
               SET school_id = v_school_id, 
                   previous_fair_id = v_previous_fair_id,
                   history_fair_date = v_history_fair_date, 
                   history_fair_sales = v_history_fair_sales,
                   history_fair_type = v_history_fair_type, 
                   online_fair_sales = v_online_fair_sales,
                   storia_sales = v_storia_sales
               WHERE  school_id = v_school_id 
                 AND previous_fair_id = v_previous_fair_id;--
         END IF;--
  
        FETCH FROM c_mx_fair_hist
        INTO v_school_id, v_previous_fair_id, 
             v_history_fair_date, v_history_fair_sales, 
             v_history_fair_type, v_online_fair_sales,
             v_storia_sales;--
      END WHILE;--
  
      CLOSE c_mx_fair_hist;--
  
      COMMIT;--
   END;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.p_mx_fair_hist_chl ( )
   SPECIFIC p_mx_fair_hist_chl
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
   BEGIN
         -- History: Rel. 23.1     Summer 2013
         --          Removal of the "where-clause" which was filtering 
         --          on earnings_type IN ('S', 'O', 'R').
         --          This mofificatio achieves better flexibility for
         --          future releases.
         --          The application currently selects specific values driven
         --          by Business Request(s).
      
      DECLARE v_previous_fair_id_count       BIGINT;--
      DECLARE v_previous_fair_id             BIGINT;--
      DECLARE v_history_sequence_no          INTEGER;--
      DECLARE v_history_fair_description     VARCHAR ( 30 );--
      DECLARE v_history_fair_value           DECIMAL ( 10, 2 );--
      DECLARE v_earnings_type                VARCHAR ( 1 );--
      DECLARE sqlstate                       CHAR ( 5 );--

      DECLARE
         c_mx_fair_chld CURSOR FOR
            SELECT BIGINT ( previous_fair_id ),
                   BIGINT ( history_sequence_no ),
                   history_fair_description,
                   ( CASE WHEN history_fair_value != '' 
                        THEN ( DECIMAL ( history_fair_value, 12, 2, '.' ) / 100 ) 
                        ELSE 0.00 
                        END ),
                   earnings_type
            FROM bkfair01.max_fair_history_chld_load;--

      DELETE FROM bkfair01.max_fair_history_chld;--

      OPEN c_mx_fair_chld;--

      FETCH FROM c_mx_fair_chld
         INTO v_previous_fair_id, 
              v_history_sequence_no, 
              v_history_fair_description, 
              v_history_fair_value, 
              v_earnings_type;--

      WHILE ( sqlstate = '00000' ) DO
         SELECT count ( * ) INTO v_previous_fair_id_count
            FROM bkfair01.max_fair_history_chld
            WHERE previous_fair_id = v_previous_fair_id 
              AND history_sequence_no = v_history_sequence_no;--

         IF v_previous_fair_id_count = 0 THEN
            INSERT INTO bkfair01.max_fair_history_chld (
                        previous_fair_id, 
                        history_sequence_no,
                        history_fair_description, 
                        history_fair_value,
                        earnings_type )
               VALUES ( v_previous_fair_id, 
                        v_history_sequence_no, 
                        v_history_fair_description, 
                        v_history_fair_value, 
                        v_earnings_type );--
         ELSE
            UPDATE bkfair01.max_fair_history_chld
               SET previous_fair_id = v_previous_fair_id, 
                   history_sequence_no = v_history_sequence_no, 
                   history_fair_description = v_history_fair_description, 
                   history_fair_value = v_history_fair_value, 
                   earnings_type = v_earnings_type
               WHERE previous_fair_id = v_previous_fair_id 
                 AND history_sequence_no = v_history_sequence_no;--
         END IF;--

         FETCH FROM c_mx_fair_chld
            INTO v_previous_fair_id, 
                 v_history_sequence_no, 
                 v_history_fair_description, 
                 v_history_fair_value, 
                 v_earnings_type;--
      END WHILE;--

      CLOSE c_mx_fair_chld;--

      COMMIT;--
   END;

COMMENT ON PROCEDURE "BKFAIR01"."P_MX_FAIR_HIST_CHL"() IS 'Bkfair updated release 23.1 in Summer 2013 Multiple schools; Removed the filtering on EARNINGS_TYPE. Loads data from MAX_FAIR_HISTORY_CHLD_LOAD into MAX_FAIR_HISTORY_CHLD.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_reset_online_homepage_special_programs (
                 IN  pv_online_homepage_column_in VARCHAR(40),
                 OUT pi_bkfair01_update_count_out  INTEGER,
                 OUT pi_unpublished_update_count_out INTEGER )
  SPECIFIC sp_reset_online_homepage_special_programs
  LANGUAGE SQL
  NOT DETERMINISTIC
  EXTERNAL ACTION
  MODIFIES SQL DATA
  CALLED ON NULL INPUT
  INHERIT SPECIAL REGISTERS
  OLD SAVEPOINT LEVEL
BEGIN
         -- This procedure has been released as part of QC 1543 ( July 2013 ).
         -- It must be used to update on a yearly basis, 
         -- to update all fairs globally disregarding the 
         -- value in HOMEPAGE_URL.URL_STATUS to prevent any
         -- data issues as encountered beforehand.
         -- The table ONLINE_HOMEPAGE in both schemas will be updated
         -- for column SPECIAL_PROGRAM_INCLUDE... (one at a time) which
         -- column name must be passed as a parameter.
     
     DECLARE v_bkfair01_update_count         INTEGER DEFAULT 0;--
     DECLARE v_unpublished_update_count      INTEGER DEFAULT 0;--
     DECLARE v_bkfair01_rows                 INTEGER DEFAULT 0;--
     DECLARE v_bkfair01_rows_temp            INTEGER DEFAULT 0;--
     DECLARE v_unpublished_rows              INTEGER DEFAULT 0;--
     DECLARE v_unpublished_rows_temp         INTEGER DEFAULT 0;--
     
     DECLARE SQLSTATE                        CHAR (5) DEFAULT '00000';--
     DECLARE v_column_name                   VARCHAR(40);--
     DECLARE v_special_program_desc          VARCHAR(400);--
     DECLARE v_fair_id                       BIGINT;--
     DECLARE v_product_id                    VARCHAR(2);--
     
     DECLARE v_dyn_cursor                    VARCHAR(1000);--
     DECLARE v_dyn_updt_unpub                VARCHAR(1000);--
     DECLARE v_dyn_updt_bkfair01             VARCHAR(1000);--
     
     DECLARE v_cur_statement                 STATEMENT;--
     DECLARE v_updt_unpub_stat               STATEMENT;--
     DECLARE v_updt_bkfair01_stat            STATEMENT;--
     
          -- Cursor against UNPUBLISHED.ONLINE_HOMEPAGE...
     DECLARE c_unpublished_homepage CURSOR WITH HOLD FOR v_cur_statement;--

        -- BODY STARTS HERE:
          
        -- Assign the procedure IN parameter...
     SET v_column_name = LOWER(pv_online_homepage_column_in);--

        -- Dynamic SQL for "c_unpublished_homepage" cursor...
     SET v_dyn_cursor = 'SELECT oh.fair_id, ' ||
                        'f.product_id ' ||
                        'FROM unpublished.online_homepage oh, ' ||
                              'bkfair01.fair f ' ||
                        'WHERE oh.' || v_column_name || ' IS NOT NULL ' ||
                          'AND oh.' || v_column_name || ' != '''' ' ||
                          'AND oh.fair_id = f.id';--

         -- Dynamic SQL for the Update statement...
         
      SET v_dyn_updt_unpub = 'UPDATE unpublished.online_homepage oh ' ||
                             'SET ' || v_column_name || ' = ( ' ||
                                  ' SELECT sp.' || v_column_name ||
                                     ' FROM bkfair01.lu_special_programs sp ' ||
                                     ' WHERE sp.product_id = ? ) ' ||
                             'WHERE oh.fair_id = ? ';--
      
      SET v_dyn_updt_bkfair01 = 'UPDATE bkfair01.online_homepage oh ' ||
                                'SET ' || v_column_name || ' = ( ' ||
                                     ' SELECT sp.' || v_column_name ||
                                        ' FROM bkfair01.lu_special_programs sp ' ||
                                        ' WHERE sp.product_id = ? ) ' ||
                                'WHERE oh.fair_id = ? ';--
      
      PREPARE v_updt_unpub_stat FROM v_dyn_updt_unpub;--
      
      PREPARE v_updt_bkfair01_stat FROM v_dyn_updt_bkfair01;--
      
      PREPARE v_cur_statement FROM v_dyn_cursor;--
      
      OPEN c_unpublished_homepage;--
      FETCH c_unpublished_homepage INTO v_fair_id, v_product_id;--
      
      WHILE (SQLSTATE = '00000') DO
            -- Evaluate which column from ONLINE_HOMEPAGE needs to be updated...

         EXECUTE v_updt_unpub_stat USING v_product_id, v_fair_id;--
         GET DIAGNOSTICS v_unpublished_rows_temp = ROW_COUNT;--
         SET v_unpublished_rows = v_unpublished_rows + v_unpublished_rows_temp;--
                  
         EXECUTE v_updt_bkfair01_stat USING v_product_id, v_fair_id;--
         GET DIAGNOSTICS v_bkfair01_rows_temp = ROW_COUNT;--
         SET v_bkfair01_rows = v_bkfair01_rows + v_bkfair01_rows_temp;--
         
         COMMIT;--
         FETCH c_unpublished_homepage INTO v_fair_id, v_product_id;--
      END WHILE;--

      CLOSE c_unpublished_homepage;--
      
      COMMIT;--
         --
         -- Set OUT paramaters...
         --
      SET pi_bkfair01_update_count_out = v_bkfair01_rows;--
      SET pi_unpublished_update_count_out = v_unpublished_rows;--
   END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_RESET_ONLINE_HOMEPAGE_SPECIAL_PROGRAMS"(VARCHAR(),INTEGER,INTEGER) IS 'July 2013; Will update globally the SPECIAL_PROGRAM_INCLUDE... columns from tables ONLINE_HOMEPAGE in both schemas based on a submitted parameter indicating which one of the column to be updated.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_bkfair_finanform
  SPECIFIC sp_bkfair_finanform
  LANGUAGE SQL
  NOT DETERMINISTIC
  EXTERNAL ACTION
  MODIFIES SQL DATA
  CALLED ON NULL INPUT
  INHERIT SPECIAL REGISTERS
  OLD SAVEPOINT LEVEL
BEGIN
       -- History: Rel. 23     Summer 2013
       --          1- All references to the table DISTRICT have been removed
       --             since that table has been dropped in that release.
       --          2- Include a handler for missing FAIR.ID
       --          3- Addition of feedback on insertion/updates.
       --          
    DECLARE v_fair_id                        BIGINT;--
    DECLARE v_tax_detail_count               BIGINT;--
    DECLARE v_tax_detail_tax_rate            INTEGER;--
    DECLARE v_tax_detail_exempt_salesallow   SMALLINT;--
    DECLARE v_tax_detail_customer_may_payt   SMALLINT;--
    DECLARE v_fair_financial_info_count      BIGINT;--
    DECLARE v_fair_financial_info_scdbooki   SMALLINT;--
    DECLARE v_fair_financial_info_incr_rev   DECIMAL(10, 2);--
    DECLARE v_bogo_bonus                     SMALLINT;--

    -- declaration2 IRC_PERCENTAGE_INCREASE_RULE local variables...
    DECLARE v_irc_percentage_rule_count      BIGINT;--
    DECLARE v_percentage_id                  BIGINT;--
    DECLARE v_irc_voucher_profit             INTEGER;--
    DECLARE v_percentage_start_date          DATE;--
    DECLARE v_percentage_end_date            DATE;--
    DECLARE v_percentage_increase_from       DECIMAL(10, 2);--
    DECLARE v_percentage_increase_to         DECIMAL(10, 2);--

    -- declaration 3
    DECLARE v_irc_threshold_bonus_count      BIGINT;--
    DECLARE v_multiplier_id                  BIGINT;--
    DECLARE v_irc_voucher_profit_multiplier  INTEGER;--
    DECLARE v_multiplier_start_date          DATE;--
    DECLARE v_multiplier_end_date            DATE;--
    DECLARE v_total_fair_sales_from          DECIMAL(10, 2);--
    DECLARE v_total_fair_sales_to            DECIMAL(10, 2);--

    -- House-keeping Counts...
       -- ...fk_fair_ct tracks the number of records that 
       --    could not be inserted as the parent key did not exist.
    DECLARE vi_fair_financial_insert_ct      INTEGER DEFAULT 0;--
    DECLARE vi_fair_financial_updt_ct        INTEGER DEFAULT 0;--
    DECLARE vi_fair_taxdetail_insert_ct      INTEGER DEFAULT 0;--
    DECLARE vi_fair_taxdetail_updt_ct        INTEGER DEFAULT 0;--
    
    -- Params Declarations...
    DECLARE sqlstate                         CHAR(5);--
    
    -- Cursor Declarations:
    -- cursor 1 declare cursor for IRC_PERCENTAGE_INCREASE_RULE import data...
    DECLARE c_irc_percentage CURSOR WITH HOLD FOR 
         SELECT BIGINT(percentage_id),
                INTEGER(irc_voucher_profit),
                DATE(SUBSTR(percentage_start_date, 1, 4) || 
                     '-' || SUBSTR(percentage_start_date, 5, 2) || 
                     '-' || SUBSTR(percentage_start_date, 7, 2)),
                DATE(SUBSTR(percentage_end_date, 1, 4) || 
                     '-' || SUBSTR(percentage_end_date, 5, 2) || 
                     '-' || SUBSTR(percentage_end_date, 7, 2)),
                (CASE WHEN percentage_increase_from != ''
                      THEN (DECIMAL(percentage_increase_from, 12, 2, '.') / 100) 
                      ELSE 0.00 
                      END ),
                (CASE WHEN percentage_increase_to != '' 
                      THEN (DECIMAL(percentage_increase_to, 12, 2, '.') / 100) 
                      ELSE 0.00 
                      END )
         FROM   bkfair01.irc_percentage_increase_rule_data_load;--

    -- cursor 2 declare cursor for IRC_THRESHOLD_BONUS_RULES_DATA_LOAD import data...
    DECLARE c_irc_threshold_bo CURSOR WITH HOLD FOR 
         SELECT BIGINT(multiplier_id),
                INTEGER(irc_voucher_profit_multiplier),
                DATE(SUBSTR(multiplier_start_date, 1, 4) || 
                     '-' || SUBSTR(multiplier_start_date, 5, 2) || 
                     '-' || SUBSTR(multiplier_start_date, 7, 2) ),
                DATE(SUBSTR(multiplier_end_date, 1, 4) || 
                     '-' || SUBSTR(multiplier_end_date, 5, 2) || 
                     '-' || SUBSTR(multiplier_end_date, 7, 2) ),
                (CASE WHEN total_fair_sales_from != '' 
                      THEN (DECIMAL(total_fair_sales_from, 12, 2, '.') / 100 ) 
                      ELSE 0.00 
                      END ),
                (CASE WHEN total_fair_sales_to != '' 
                      THEN (DECIMAL(total_fair_sales_to, 12, 2, '.') / 100 ) 
                      ELSE 0.00 
                      END )
         FROM   bkfair01.irc_threshold_bonus_rules_data_load;--

    -- cursor 3 declare cursor for Financial Forms import data
    DECLARE c_book_fair CURSOR WITH HOLD FOR
        SELECT BIGINT(fair_id),
               INTEGER(tax_detail_tax_rate),
               (CASE WHEN tax_detail_exempt_salesallowed = 'Y' 
                     THEN 1
                     ELSE 0 
                     END ),
               (CASE WHEN tax_detail_customer_may_paytax = 'Y' 
                     THEN 1 
                     ELSE 0 
                     END ),
               (CASE WHEN fair_financial_info_scdbooking = 'Y' 
                     THEN 1 
                     ELSE 0 END),
               (CASE WHEN fair_financial_info_incr_reven != '' 
                     THEN (DECIMAL(fair_financial_info_incr_reven, 12, 2, '.') / 100) 
                     ELSE 0.00 
                     END ),
               (CASE WHEN bogo_bonus = 'Y' 
                     THEN 1 
                     ELSE 0 
                     END )
        FROM   bkfair01.book_fair_data_load;--

   -- Procedure Body:
    -- cursor 1 irc_percentage_increase_rule data import starts here
    OPEN c_irc_percentage;--

    FETCH FROM c_irc_percentage
    INTO v_percentage_id, v_irc_voucher_profit, 
         v_percentage_start_date, v_percentage_end_date, 
         v_percentage_increase_from, v_percentage_increase_to;--

    WHILE (sqlstate = '00000') DO
       SELECT COUNT(percentage_id) INTO v_irc_percentage_rule_count
          FROM   bkfair01.irc_percentage_increase_rule
          WHERE  percentage_id = v_percentage_id;--

       IF v_irc_percentage_rule_count = 0 THEN
          INSERT INTO bkfair01.irc_percentage_increase_rule(
                      percentage_id, irc_voucher_profit,
                      percentage_start_date, percentage_end_date, 
                      percentage_increase_from, percentage_increase_to,
                      create_date, update_date )
          VALUES (v_percentage_id, v_irc_voucher_profit, 
                  v_percentage_start_date, v_percentage_end_date,
                  v_percentage_increase_from, v_percentage_increase_to, 
                  CURRENT TIMESTAMP, CURRENT TIMESTAMP);--
       ELSE
          UPDATE bkfair01.irc_percentage_increase_rule
             SET irc_voucher_profit = v_irc_voucher_profit, 
                 percentage_start_date = v_percentage_start_date,
                 percentage_end_date = v_percentage_end_date, 
                 percentage_increase_from = v_percentage_increase_from,
                 percentage_increase_to = v_percentage_increase_to, 
                 update_date = CURRENT TIMESTAMP
             WHERE  percentage_id = v_percentage_id;--
       END IF;--

      COMMIT;--
      FETCH FROM c_irc_percentage
         INTO v_percentage_id, v_irc_voucher_profit, 
              v_percentage_start_date, v_percentage_end_date, 
              v_percentage_increase_from, v_percentage_increase_to;--
    END WHILE;--

    CLOSE c_irc_percentage;--

    -- cursor 1 irc_percentage_increase_rule data import ends here

    -- cursor 2 IRC_THRESHOLD_BONUS_RULES data import starts here
    OPEN c_irc_threshold_bo;--

    FETCH FROM c_irc_threshold_bo
       INTO v_multiplier_id, v_irc_voucher_profit_multiplier, 
            v_multiplier_start_date, v_multiplier_end_date, 
            v_total_fair_sales_from, v_total_fair_sales_to;--

    WHILE (sqlstate = '00000') DO
       SELECT COUNT(multiplier_id) INTO v_irc_threshold_bonus_count
          FROM   bkfair01.irc_threshold_bonus_rules
          WHERE  multiplier_id = v_multiplier_id;--

      IF v_irc_threshold_bonus_count = 0 THEN
         INSERT INTO bkfair01.irc_threshold_bonus_rules(
            multiplier_id, irc_voucher_profit_multiplier, 
            multiplier_start_date, multiplier_end_date, 
            total_fair_sales_from, total_fair_sales_to,
            create_date, update_date         )
         VALUES (v_multiplier_id, v_irc_voucher_profit_multiplier, 
                 v_multiplier_start_date, v_multiplier_end_date,
                 v_total_fair_sales_from, v_total_fair_sales_to, 
                 CURRENT TIMESTAMP, CURRENT TIMESTAMP );--
      ELSE
         UPDATE bkfair01.irc_threshold_bonus_rules
            SET irc_voucher_profit_multiplier = v_irc_voucher_profit_multiplier, 
                multiplier_start_date = v_multiplier_start_date,
                multiplier_end_date = v_multiplier_end_date, 
                total_fair_sales_from = v_total_fair_sales_from,
                total_fair_sales_to = v_total_fair_sales_to, 
                update_date = CURRENT TIMESTAMP
            WHERE  multiplier_id = v_multiplier_id;--
      END IF;--

      COMMIT;--
      FETCH FROM c_irc_threshold_bo
         INTO v_multiplier_id, v_irc_voucher_profit_multiplier, 
              v_multiplier_start_date, v_multiplier_end_date,
              v_total_fair_sales_from, v_total_fair_sales_to;--
    END WHILE;--

    CLOSE c_irc_threshold_bo;--
       -- cursor 2 irc_threshold_bonus_rules data import ends here

    -- cursor 3 Financial Forms cursor data data import starts here
    OPEN c_book_fair;--

    FETCH FROM c_book_fair
    INTO v_fair_id, v_tax_detail_tax_rate, 
         v_tax_detail_exempt_salesallow, v_tax_detail_customer_may_payt,
         v_fair_financial_info_scdbooki, v_fair_financial_info_incr_rev, 
         v_bogo_bonus;--

    WHILE (sqlstate = '00000') DO
      SELECT COUNT(fair_id) INTO v_fair_financial_info_count
          FROM bkfair01.fair_financial_info
          WHERE fair_id = v_fair_id;--

      IF v_fair_financial_info_count = 0 THEN
         INSERT INTO bkfair01.fair_financial_info(
                     fair_id, second_booking, 
                     incresing_revenue_prior, bogo_bonus )
            VALUES (v_fair_id, v_fair_financial_info_scdbooki, 
                    v_fair_financial_info_incr_rev, v_bogo_bonus );--
         SET vi_fair_financial_insert_ct = vi_fair_financial_insert_ct + 1;--
         
      ELSE
         UPDATE bkfair01.fair_financial_info
            SET second_booking = v_fair_financial_info_scdbooki, 
                incresing_revenue_prior = v_fair_financial_info_incr_rev,
                bogo_bonus = v_bogo_bonus
            WHERE fair_id = v_fair_id;--
         
         SET vi_fair_financial_updt_ct = vi_fair_financial_updt_ct + 1;--
      END IF;--

      SELECT COUNT(fair_id) INTO v_tax_detail_count
         FROM bkfair01.tax_detail
         WHERE  fair_id = v_fair_id;--

      IF v_tax_detail_count = 0 THEN
         INSERT INTO bkfair01.tax_detail(
                     fair_id, tax_rate, 
                     exempt_sales_allowed, customer_may_pay_tax )
            VALUES (v_fair_id, v_tax_detail_tax_rate, 
                    v_tax_detail_exempt_salesallow, v_tax_detail_customer_may_payt );--
         SET vi_fair_taxdetail_insert_ct = vi_fair_taxdetail_insert_ct + 1;--
      ELSE
        UPDATE bkfair01.tax_detail
           SET tax_rate = v_tax_detail_tax_rate, 
               exempt_sales_allowed = v_tax_detail_exempt_salesallow,
               customer_may_pay_tax = v_tax_detail_customer_may_payt
           WHERE  fair_id = v_fair_id;--
        SET vi_fair_taxdetail_updt_ct = vi_fair_taxdetail_updt_ct + 1;--
      END IF;--
      
      COMMIT;--
      
      FETCH FROM c_book_fair
      INTO v_fair_id, v_tax_detail_tax_rate, 
           v_tax_detail_exempt_salesallow, v_tax_detail_customer_may_payt,
           v_fair_financial_info_scdbooki, v_fair_financial_info_incr_rev, 
           v_bogo_bonus;--
    END WHILE;--

    CLOSE c_book_fair;--

   -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        insertion_count, update_count, 
        procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'tax_detail', 
               vi_fair_financial_insert_ct, vi_fair_taxdetail_updt_ct, 
               'sp_bkfair_finanform' );--

      INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        insertion_count, update_count, 
        procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'fair_financial_info', 
               vi_fair_taxdetail_insert_ct, vi_fair_financial_updt_ct, 
               'sp_bkfair_finanform' );--
    -- cursor 3 Financial Forms cursor data data import ends here

    COMMIT;--
  END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_BKFAIR_FINANFORM"() IS 'Bkfair release 23 - Remove all reference to table DISTRICT; Load data from ...LOAD tables and used in loading script bkfair_cis_load.ksh.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_disassociate_url ( )
   SPECIFIC sp_disassociate_url
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
BEGIN
      -- History: 07/22/2013
      --          Release 23 - Real Time Messaging
      --          Remove references to table "fair_startend_date
      --          as that object was dropped during that release.
      
   DECLARE v_fair_id                     BIGINT;--
   DECLARE v_fair_physical_enddate       DATE;--
   DECLARE v_fair_actual_enddate         DATE;--
   DECLARE SQLSTATE                      CHAR (5);--
   DECLARE v_fair_physical_enddate_ofe   DATE;--

      -- Logic for disassociate the URL from a Fair based 
      --    on fair physical end date
      -- Reduce the set of data being brought back by cursor 
      -- using appropriate date range.
      
   DECLARE c_homepg_actual_ds CURSOR WITH HOLD FOR 
           SELECT a.id,
                  a.pickup_date,
                  CASE WHEN (b.ofe_end_date > a.pickup_date) 
                       THEN b.ofe_end_date
                       ELSE a.pickup_date
                       END CASE
           FROM bkfair01.fair a
                LEFT OUTER JOIN bkfair01.online_shopping b
                  ON a.id = b.fair_id
           WHERE a.pickup_date >= CURRENT DATE - 30 DAYS
             AND a.pickup_date <= CURRENT DATE - 1 DAY
           UNION
           SELECT a.id,
                  a.pickup_date,
                  CASE WHEN (b.ofe_end_date > a.pickup_date) 
                       THEN b.ofe_end_date
                       ELSE a.pickup_date
                       END CASE
           FROM bkfair01.fair a
                LEFT OUTER JOIN bkfair01.online_shopping b
                  ON a.id = b.fair_id
           WHERE b.ofe_end_date >= CURRENT DATE - 30 DAYS
             AND b.ofe_end_date <= CURRENT DATE - 1 DAY;--
   
      -- Logic for disassociate the URL from a Fair based 
      --    on fair actual login end date
      -- Reduce the set of data being brought back by 
      -- cursor using appropriate date range.
      -- 07/22/2013 Release 23:
      --   Replace table "fair_startend_date" with "fair"
      --   and adjust with the ( pickup_date + 29 ) days rule.
      
   DECLARE c_homepage_web_dis CURSOR WITH HOLD FOR 
           SELECT hwb.fair_id,
                  ( f.pickup_date + 29 DAYS )
           FROM bkfair01.fair          f,
                bkfair01.homepage_url  hwb
           WHERE f.id = hwb.fair_id
             AND ( f.pickup_date + 29 DAYS ) BETWEEN 
                 ( CURRENT DATE - 760 DAYS ) AND ( CURRENT DATE - 700 DAYS );--

      -- Prevent Fairs from being automatically published when
      --    the Fair.pickup_date had rendered the fair expired.
   
   DELETE FROM bkfair01.new_fairs
          WHERE id IN ( SELECT id FROM bkfair01.fair
                        WHERE pickup_date < CURRENT DATE );--
   COMMIT;--

   -- Fair physical login end date logic starts here
   OPEN c_homepg_actual_ds;--

   FETCH FROM c_homepg_actual_ds
      INTO v_fair_id, 
           v_fair_physical_enddate, 
           v_fair_physical_enddate_ofe;--

   WHILE (SQLSTATE = '00000') DO
         -- When the current date becomes equal to one day after 
         --    the physical fair eligibility end date
         -- Release # 21: Business decided to expire fairs just the following
         --               day following its end date (Sept 2012)
         --               Previous code: > ( v_fair_physical_enddate_ofe + 5 DAYS )
      IF (CURRENT DATE) > v_fair_physical_enddate_ofe THEN
         UPDATE bkfair01.homepage_url
            SET published_status = 'EXPIRED'
            WHERE fair_id = v_fair_id;--
      END IF;--

         -- When the current date becomes older than seven day after 
         --   the fair physical end date, change the url_status
         -- Oct. 03, 2011: Modification of following IF statement:
         --                The goal of that statement is to take whichever
         --                date is greater
         -- Nov. 03, 2011: between fair.fair_end_date and 
         --                online_shopping.ofe_end_date.
         --                the value before this modificatio was: 
         --                "v_fair_physical_enddate"
         
      IF (CURRENT DATE) > (v_fair_physical_enddate_ofe + 7 DAYS) THEN
         UPDATE bkfair01.homepage_url
            SET url_status = 'NOTAVAILABLE'
            WHERE fair_id = v_fair_id;--
      END IF;--
      
      COMMIT;--
      FETCH FROM c_homepg_actual_ds
        INTO v_fair_id,
             v_fair_physical_enddate,
             v_fair_physical_enddate_ofe;--
   END WHILE;--

   CLOSE c_homepg_actual_ds;--

   -- Fair physical login end date logic ends here


   -- Fair actual login end date logic starts here
   OPEN c_homepage_web_dis;--

   FETCH FROM c_homepage_web_dis
      INTO v_fair_id, 
           v_fair_actual_enddate;--

   WHILE (SQLSTATE = '00000') DO
      -- Start Allow the URL to be available to all schools if:
      -- The URL has not been re-used by the School Account for two years from the login eligibility end date
      -- where the URL was last used
      IF ( DAYS (CURRENT DATE) - days (v_fair_actual_enddate) ) > 730 THEN
         UPDATE bkfair01.homepage_url
            SET url_status = 'AVAILABLE'
            WHERE fair_id = v_fair_id;--
      END IF;--
      
      COMMIT;--
      FETCH FROM c_homepage_web_dis
         INTO v_fair_id, 
              v_fair_actual_enddate;--
   END WHILE;--

   CLOSE c_homepage_web_dis;--

   COMMIT;--
   END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_DISASSOCIATE_URL"() IS 'Bkfair release 19, Oct. 2011; Runs only via the loading script bkfair_load.sh; Sets the HOMEPAGE_URL.PUBLISHED_STATUS following specific Business rules; Release 23: Remove references to FAIR_STARTEND_DATE table.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_qc1797_online_shopping (
   OUT pi_sqlcode_out   INTEGER,
   OUT pc_sqlstate_out  CHARACTER(5),
   OUT pi_new_ofe       INTEGER )
   SPECIFIC sp_qc1797_online_shopping
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
BEGIN
      -- Global variables...
   DECLARE v_create_date                            TIMESTAMP;--
   DECLARE v_update_date                            TIMESTAMP;--
   
      -- Data declaration linked to OFE record creation... 

   DECLARE v_fair_id                                BIGINT;--
   DECLARE v_fair_start_date                        DATE;--
   DECLARE v_fair_end_date                          DATE;--
   DECLARE v_ofe_start_date                         DATE;--
   DECLARE v_ofe_end_date                           DATE;--
   DECLARE v_ofe_status                             VARCHAR(25);--
   DECLARE v_ofe_ok                                 CHAR(1) DEFAULT 'N';--
   DECLARE v_new_ofe_temp_ct                        INTEGER DEFAULT 0;--
   DECLARE v_new_ofe_ct                             INTEGER DEFAULT 0;--
   
      -- Condition Declarations...
   DECLARE v_return_code                            INTEGER DEFAULT 0;--
   DECLARE pi_sqlcode_out                           INTEGER DEFAULT 0;--
   DECLARE pc_sqlstate_out                          CHAR(5) DEFAULT '00000';--
   DECLARE pv_message_out                           VARCHAR(40);--
   DECLARE sqlstate                                 CHAR(5) DEFAULT '00000';--
   DECLARE sqlcode                                  INTEGER DEFAULT 0;--

      -- Cursor collecting all fairs for which ONLINE_SHOPPING
      -- records need to be created and only when these fairs
      -- are eligible and pickup_date is greater than today's date.
      
   DECLARE c_new_ofe CURSOR WITH HOLD FOR
      SELECT f.id,
             f.delivery_date,
             f.pickup_date
      FROM bkfair01.homepage_url       hu,
           bkfair01.fair               f
      WHERE hu.published_status = 'PUBLISHED'
        AND hu.fair_id = f.id
        AND f.pickup_date >= CURRENT DATE
        AND f.fair_type NOT IN ( 'B', 'G' )
        AND f.id NOT IN ( SELECT fair_id 
                            FROM bkfair01.online_shopping );--

         -- Handler declarations...
--      DECLARE CONTINUE HANDLER FOR NOT FOUND
--         VALUES (SQLCODE, SQLSTATE)
--            INTO pi_sqlcode_out, pc_sqlstate_out;--
         
         -- ------------------------------
         -- PROCEDURE BODY STARTS HERE...
         -- ------------------------------
      
      SET ( v_create_date,
            v_update_date ) = ( SELECT CURRENT TIMESTAMP, CURRENT TIMESTAMP
                                FROM sysibm.sysdummy1 );--
      
         -- ---------------------------------------------------------------------
         -- REGARDING FAIR WITHOUT OFE RECORD.
         -- INSERTION INTO BOOKFAIR01.ONLINE_SHOPPING
         -- ---------------------------------------------------------------------
      OPEN c_new_ofe;--

      FETCH FROM c_new_ofe
            INTO v_fair_id, v_fair_start_date, v_fair_end_date;--

      WHILE ( sqlstate = '00000' ) DO

            -- Check the dates using Business rules to set up 
            -- the ONLINE_SHOPPING record accordingly.

         CASE 
            WHEN v_fair_start_date >= ( CURRENT DATE + 6 DAYS ) THEN
               SET v_ofe_start_date = ( v_fair_start_date - 5 DAYS );--
               SET v_ofe_end_date = ( v_ofe_start_date + 20 DAYS );--
               SET v_ofe_status = 'Submitted';--
               SET v_ofe_ok = 'Y';--
            WHEN v_fair_start_date = ( CURRENT DATE + 5 DAYS ) THEN
               SET v_ofe_start_date = ( v_fair_start_date - 5 DAYS );--
               SET v_ofe_end_date = ( v_ofe_start_date + 20 DAYS );--
               SET v_ofe_status = 'Active';--
               SET v_ofe_ok = 'Y';--
            WHEN v_fair_start_date = ( CURRENT DATE + 4 DAYS ) THEN
               SET v_ofe_start_date = ( v_fair_start_date - 4 DAYS );--
               SET v_ofe_end_date = ( v_ofe_start_date + 20 DAYS );--
               SET v_ofe_status = 'Active';--
               SET v_ofe_ok = 'Y';--
            WHEN v_fair_start_date = ( CURRENT DATE + 3 DAYS ) THEN
               SET v_ofe_start_date = ( v_fair_start_date - 3 DAYS );--
               SET v_ofe_end_date = ( v_ofe_start_date + 20 DAYS );--
               SET v_ofe_status = 'Active';--
               SET v_ofe_ok = 'Y';--
            WHEN v_fair_start_date = ( CURRENT DATE + 2 DAYS ) THEN
               SET v_ofe_start_date = ( v_fair_start_date - 2 DAYS );--
               SET v_ofe_end_date = ( v_ofe_start_date + 20 DAYS );--
               SET v_ofe_status = 'Active';--
               SET v_ofe_ok = 'Y';--
            WHEN v_fair_start_date = ( CURRENT DATE + 1 DAY ) THEN
               SET v_ofe_start_date = ( v_fair_start_date - 1 DAY );--
               SET v_ofe_end_date = ( v_ofe_start_date + 20 DAYS );--
               SET v_ofe_status = 'Active';--
               SET v_ofe_ok = 'Y';--
            WHEN v_fair_start_date = CURRENT DATE  THEN
               SET v_ofe_start_date = v_fair_start_date;--
               SET v_ofe_end_date = ( v_ofe_start_date + 20 DAYS );--
               SET v_ofe_status = 'Active';--
               SET v_ofe_ok = 'Y';--
            WHEN ( v_fair_start_date < CURRENT DATE ) AND
                 ( CURRENT DATE <= v_fair_end_date ) THEN 
               SET v_ofe_start_date = CURRENT DATE;--
               SET v_ofe_end_date = ( v_ofe_start_date + 20 DAYS );--
               SET v_ofe_status = 'Active';--
               SET v_ofe_ok = 'Y';--
         END CASE;--
         
            -- Only insert ofe record when OKed...
         IF v_ofe_ok = 'Y' THEN
            INSERT INTO bkfair01.online_shopping
                      ( fair_id, ofe_start_date, ofe_end_date,
                        create_date, updated_date, ofe_status )
                   VALUES ( v_fair_id, v_ofe_start_date, v_ofe_end_date, 
                            CURRENT TIMESTAMP, CURRENT TIMESTAMP, v_ofe_status );--
            GET DIAGNOSTICS v_new_ofe_temp_ct = ROW_COUNT;--
               -- Count feedback update...
            SET v_new_ofe_ct = ( v_new_ofe_ct + v_new_ofe_temp_ct );--
            COMMIT;--
         END IF;--
         
            -- Reset v_ofe_ok
         SET v_ofe_ok = 'N';--
         FETCH FROM c_new_ofe
            INTO v_fair_id, v_fair_start_date, v_fair_end_date;--
      END WHILE;--

      CLOSE c_new_ofe;--

         -- Insert feedback for number of online_shopping.fair_id processed.
      INSERT INTO bkfair01.fair_load_feedback ( 
                  table_name,
                  insertion_count, 
                  procedure_name ) 
             VALUES ( 'bkfair01.online_shopping', 
                      v_new_ofe_ct, 
                      'bkfair01.sp_qc1797_online_shopping' );--
      COMMIT;--
      
      EXITNOW:
          SET pi_sqlcode_out = v_return_code;--
          SET pc_sqlstate_out = sqlstate;--
          SET pi_new_ofe = v_new_ofe_ct;--
   END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_QC1797_ONLINE_SHOPPING"(INTEGER,CHAR(),INTEGER) IS 'QC 1797: This procedure creates missing ONLINE_SHOPPING records occuring since release 23.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_bk_cancel_fair (
                 IN pb_fair_id     BIGINT,
                 IN pc_test_data   CHARACTER(1) )
  SPECIFIC sp_bk_cancel_fair
  LANGUAGE SQL
  NOT DETERMINISTIC
  EXTERNAL ACTION
  MODIFIES SQL DATA
  CALLED ON NULL INPUT
  INHERIT SPECIAL REGISTERS
  OLD SAVEPOINT LEVEL
BEGIN
       -- History: Rel. 23     Summer 2013
       --             1- Adapted to Real Time Messages modifiacations as 
       --                several tables were added and some removed.
       --          Rel. 24     Winter 2013-2014
       --             1- Allow ALL Test Fairs 
       --                [ TT, TM, BE, BM, BP, BB, TB, MB, ME, MM ]
       --                to be deleted without any restriction as opposed
       --                to just [ TT, TM ].
       --             2- Add New tables to be deleted from when a fair 
       --                is cancelled.
       --          
   DECLARE vb_fair_id                    BIGINT;--
   DECLARE vs_fair_id_count              SMALLINT DEFAULT 0;--
   DECLARE vc_test_data                  CHAR(1) DEFAULT 'N';--
   DECLARE sqlstate                      CHAR(5) DEFAULT '00000';--
   DECLARE pc_sqlstate_out               CHAR(5) DEFAULT '00000';--

      -----------------------------
      -- Processing starts here...
      -----------------------------
      -- Assign the procedure IN parameter...
   SET vb_fair_id = pb_fair_id;--
   SET vc_test_data = UPPER(pc_test_data);--

        -- We must check if the fair to be deleted is a Test fair. 
        -- We have a Test Fair when pc_test_data = 'Y'

   IF ( vc_test_data = 'Y' ) THEN
      SELECT COUNT(*) INTO vs_fair_id_count
         FROM bkfair01.financial_section_status fi,
              bkfair01.fair fa
         WHERE fi.fair_id = vb_fair_id
           AND fi.fair_id = fa.id
           AND fa.product_id IN 
             ( 'TT', 'TM', 'BE', 'BM', 'BP', 'BB', 'TB', 'MB', 'ME', 'MM' );--
      COMMIT;--
      
      IF ( vs_fair_id_count > 0 ) THEN
         DELETE FROM bkfair01.financial_section_status
            WHERE fair_id = vb_fair_id;--
      END IF;--
   END IF;--
   
   COMMIT;--
   
      -- Reset vs_fair_id_count
   SET vs_fair_id_count = 0;--
   
      -- Find out if purchase were applied against that Fair.
   SELECT COUNT(*) INTO vs_fair_id_count
      FROM bkfair01.financial_section_status
      WHERE fair_id = vb_fair_id;--

      --   If there were no purchase, then proceed with the fair deletion...
   IF vs_fair_id_count = 0 THEN
      DELETE FROM bkfair01.cc_sales
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.financial_saved_urls
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.financial_section_status
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.po_contact
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.po_sales
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.profit_selection
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.profit_summary
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.profit_taken
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.profit_to_split
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.sales_summary
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.sales_tax_collected
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.sales_tax_exempt_details
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.sales_tax_exempt_status
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.sales_tax_exempt_transactions
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.sales_tax_remittance
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.cash_and_checks_sale
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.fair_cplist
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.fair_financial_info
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.fairdate_login_tracking
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.myteam
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.not_correct_email_forms
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.previous_fairs
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.previous_fairs
         WHERE previous_fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.sales_consultant_email_forms
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.tax_detail
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.chairperson_email_invitation
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.homepage_event
         WHERE fair_id = vb_fair_id;--
      DELETE FROM unpublished.homepage_event
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.homepage_schedule
         WHERE fair_id = vb_fair_id;--
      DELETE FROM unpublished.homepage_schedule
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.homepage_url
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.online_homepage
         WHERE fair_id = vb_fair_id;--
      DELETE FROM unpublished.online_homepage
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.online_shopping
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.audit_ofe
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.homepage_principal_activities
         WHERE fair_id = vb_fair_id;--

         -- Added in Release 23
      DELETE FROM bkfair01.workshop_rewards
         WHERE fair_id = vb_fair_id;--

      DELETE FROM bkfair01.volunteer_login_tracking
         WHERE fair_id = vb_fair_id;--

      DELETE FROM bkfair01.volunteer_forms
         WHERE fair_id = vb_fair_id;--

      DELETE FROM bkfair01.homepage_customer_emails
         WHERE fair_id = vb_fair_id;--

      DELETE FROM bkfair01.recipient_position
         WHERE fair_id = vb_fair_id;--

      DELETE FROM bkfair01.fair_cis
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.fair_date_changed
         WHERE fair_id = vb_fair_id;--

         -- Added in Release 24
      DELETE FROM bkfair01.fair_planner_status
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.fair_planner_goals
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.fair_planner_programs
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.fair_planner_events
         WHERE fair_id = vb_fair_id;--
      
      DELETE FROM bkfair01.fair
         WHERE id = vb_fair_id;--
      
      DELETE FROM bkfair01.new_fairs
         WHERE id = vb_fair_id;--
      
      COMMIT;--
   END IF;--
 END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_BK_CANCEL_FAIR"(BIGINT,CHAR()) IS 'Bkfair updated release 23 in Summer 2013 Real Time; Using a Fair ID as its unique input parameter, this procedure deletes all records from all tables only if no purchase occured against that Fair. updgraded with new objects for Release 24';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.p_bkfair_data_load ( )
   SPECIFIC p_bkfair_data_load
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
BEGIN
       -- History: Rel. 23     Summer 2013
       --          1- Adapted to Real Time Messages implemted for that release 
       --             wich sees a lot of columns being removed from the file
       --             PROD_CIS_CPW.
       --          
       --          Rel. 23.0.1 OFE Real Time Sept. 2013
       --          1- Removed all code relating to the recreation of 
       --             OFE records (online_shopping) since OFE data is now
       --             populated via Real Time
       --          
   DECLARE v_fair_count                      BIGINT; --
   DECLARE v_fair_id                         BIGINT; --
   DECLARE v_fair_gray_state                 VARCHAR(1); --
   DECLARE v_payment_rcvd                    VARCHAR(1); --
   DECLARE v_trk_delivery_date               DATE; --
   DECLARE v_trk_pickup_date                 DATE; --
   DECLARE v_prefair_description             VARCHAR(30); --
   DECLARE v_prefair_value                   DECIMAL(10,2); --
   DECLARE v_attended_workshop_description   VARCHAR(35); --
   DECLARE v_attended_workshop_value         DECIMAL(10,2); --
   DECLARE v_schedule_date                   DATE;--
   DECLARE sqlstate                          CHAR(5); --
      -- Release 23.5 (BTS) without Release 23
   DECLARE v_second_bogo_booking             CHAR(1);--
   
      -- Cursor to navigate all records to be processed one by one
      -- and which have been loaded from CPW into "book_fair_data_load".
   DECLARE c_book_fair CURSOR WITH HOLD FOR  
           SELECT BIGINT(fair_id), 
                  fair_gray_state, 
                  payment_rcvd,   
                  trk_delivery_date,
                  trk_pickup_date,
                  prefair_description,
                  (CASE WHEN prefair_value != '' 
                        THEN (DECIMAL(prefair_value,10,2,'.')/100) 
                        ELSE 0.00 
                        END ), 
                  attended_workshop_description, 
                  (CASE WHEN attended_workshop_value != '' 
                        THEN (DECIMAL(attended_workshop_value,10,2,'.')/100) 
                        ELSE 0.00 
                        END ),
                  UPPER(second_bogo_booking)
              FROM bkfair01.book_fair_data_load ; --

   OPEN c_book_fair;  --

   FETCH FROM c_book_fair  
         INTO v_fair_id, 
              v_fair_gray_state, v_payment_rcvd, 
              v_trk_delivery_date, v_trk_pickup_date, 
              v_prefair_description, v_prefair_value, 
              v_attended_workshop_description, 
              v_attended_workshop_value, 
              v_second_bogo_booking ; --

   WHILE (sqlstate = '00000') DO  

         -- Check if FAIR exists...

      SELECT COUNT(fair_id) INTO v_fair_count 
         FROM bkfair01.fair_cis 
         WHERE fair_id = v_fair_id; --

         -- Handle the fair record (insert or update)...
      -- # 01-A
      IF v_fair_count = 0 THEN 
         INSERT INTO bkfair01.fair_cis ( 
                     fair_id, 
                     create_date, 
                     gray_state, payment_rcvd, 
                     trk_delivery_date, trk_pickup_date, 
                     prefair_description, prefair_value, 
                     attended_workshop_description, attended_workshop_value, 
                     second_bogo_booking ) 
            VALUES ( v_fair_id, 
                     CURRENT TIMESTAMP, 
                     v_fair_gray_state, v_payment_rcvd, 
                     v_trk_delivery_date, v_trk_pickup_date,
                     v_prefair_description, v_prefair_value, 
                     v_attended_workshop_description, v_attended_workshop_value, 
                     v_second_bogo_booking ); --
      -- # 01-B
         -- a fair_cis record exists...
      ELSE 
         UPDATE bkfair01.fair_cis
            SET gray_state = v_fair_gray_state, 
                payment_rcvd = v_payment_rcvd, 
                trk_delivery_date = v_trk_delivery_date, 
                trk_pickup_date = v_trk_pickup_date ,
                prefair_description = v_prefair_description,
                prefair_value = v_prefair_value , 
                attended_workshop_description = v_attended_workshop_description,
                attended_workshop_value = v_attended_workshop_value,
                update_date = CURRENT TIMESTAMP,
                second_bogo_booking = v_second_bogo_booking
            WHERE fair_id = v_fair_id; --
         
      END IF;   -- # 01-A-B

      COMMIT; --

      FETCH FROM c_book_fair  
         INTO v_fair_id, 
              v_fair_gray_state, v_payment_rcvd, 
              v_trk_delivery_date, v_trk_pickup_date, 
              v_prefair_description, v_prefair_value, 
              v_attended_workshop_description, 
              v_attended_workshop_value, 
              v_second_bogo_booking ; --
   END WHILE; --
   CLOSE c_book_fair; --
   COMMIT; --
END;

COMMENT ON PROCEDURE "BKFAIR01"."P_BKFAIR_DATA_LOAD"() IS 'Release 23, 23.1, 23.5 in Summer 2013 Real Time; Reviewed procedure to adapt to the reduced data set columns sent by CIS following the Real Time messaging implementation; Loads all CIS related table data.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.p_bkfair_err_load ( )
   SPECIFIC p_bkfair_err_load
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
BEGIN
         -- History: Rel. 23.5     Summer 2013
         --          1- Reviewed entirely to accomodate to the new structure of 
         --             the PROD_CPW switch to PROD_CIS_CPW following the
         --             implementation of Real Time messages.
         --          2- Addition of check on OFE_SALES data.
         --          3- Addition of feedback on insertion/updates.
         --
         --          Rel. 23.0.1  Sept. 2013
         --          1- Removed the error checking on column "ofe_sales"
         --             as that data is now populated via Real Time Messaging.
         --             For the moment, CIS will keep delivering data for that
         --             column which data will not be used.
         --          2- For info, the procedure was modified using
         --             original code from rel. 23...sql_7.
         --
         --          Rel. 24 Winter 2013-2014
         --          1- Remove the processing for "ofe_sales"

   -- Declarations
   DECLARE vi_ins_updt_del_ct                INTEGER DEFAULT 0;--
      
   DELETE FROM bkfair01.book_fair_error_data;--
   COMMIT;--

   INSERT INTO bkfair01.book_fair_error_data (
          fair_id, 
          tax_detail_tax_rate, 
          tax_detail_exempt_salesallowed, 
          tax_detail_customer_may_paytax, 
          fair_financial_info_scdbooking, 
          fair_financial_info_incr_reven,         
          fair_gray_state, 
          payment_rcvd, 
          attended_workshop_description, 
          attended_workshop_value, 
--          ofe_sales,
          second_bogo_booking )
   SELECT fair_id, 
          tax_detail_tax_rate, 
          tax_detail_exempt_salesallowed, 
          tax_detail_customer_may_paytax, 
          fair_financial_info_scdbooking, 
          fair_financial_info_incr_reven,
          fair_gray_state,
          payment_rcvd, 
          attended_workshop_description, 
          attended_workshop_value, 
--          ofe_sales,
          second_bogo_booking
        FROM bkfair01.book_fair_data_load
        WHERE TRIM( fair_id ) = '' 
          OR TRIM( fair_financial_info_scdbooking ) NOT IN ( 'Y', 'N' ) 
          OR TRIM( tax_detail_tax_rate ) = ''
          OR TRIM( tax_detail_exempt_salesallowed ) NOT IN ( 'Y', 'N' ) 
          OR TRIM( tax_detail_customer_may_paytax ) NOT IN ( 'Y', 'N' ) 
          OR TRIM (fair_gray_state) NOT IN ( 'Y', 'N' ) 
          OR TRIM ( payment_rcvd ) NOT IN ( 'Y', 'N' ) 
          OR UCASE( attended_workshop_value ) 
             <> LCASE ( attended_workshop_value ) 
          OR UPPER(second_bogo_booking) NOT IN ( 'Y', 'N' );--

   GET DIAGNOSTICS vi_ins_updt_del_ct = ROW_COUNT;--
   
   IF vi_ins_updt_del_ct > 0 THEN
         -- Data load feedback data entry...
      INSERT INTO bkfair01.fair_load_feedback
         ( create_date, table_name, 
           insertion_count, procedure_name )
         VALUES ( CURRENT TIMESTAMP, 'book_fair_data_load', 
                  vi_ins_updt_del_ct, 'p_bkfair_err_load:data_errors' );--
      
      SET vi_ins_updt_del_ct = 0;--
   END IF;--

   COMMIT;--
   
      -- Check for any fair_id not yet inserted in table FAIR;--
      -- If FAIR.ID is missing we prevent the CIS fair record from being loaded.
   INSERT INTO bkfair01.book_fair_error_data (
          fair_id, 
          tax_detail_tax_rate, 
          tax_detail_exempt_salesallowed, 
          tax_detail_customer_may_paytax, 
          fair_financial_info_scdbooking, 
          fair_financial_info_incr_reven,         
          fair_gray_state, 
          payment_rcvd, 
          attended_workshop_description, 
          attended_workshop_value,
          missing_fair_record,
          second_bogo_booking )
   SELECT fair_id, 
          tax_detail_tax_rate, 
          tax_detail_exempt_salesallowed, 
          tax_detail_customer_may_paytax, 
          fair_financial_info_scdbooking, 
          fair_financial_info_incr_reven,
          fair_gray_state,
          payment_rcvd, 
          attended_workshop_description, 
          attended_workshop_value, 
          'Y',
          second_bogo_booking
        FROM bkfair01.book_fair_data_load
        WHERE BIGINT(fair_id) NOT IN
           ( SELECT id FROM bkfair01.fair );--
   
   GET DIAGNOSTICS vi_ins_updt_del_ct = ROW_COUNT;--
   
   IF vi_ins_updt_del_ct > 0 THEN
         -- Data load feedback data entry...
      INSERT INTO bkfair01.fair_load_feedback
         ( create_date, table_name, 
           missing_parent_count, procedure_name )
         VALUES ( CURRENT TIMESTAMP, 'book_fair_data_load', 
                  vi_ins_updt_del_ct, 'p_bkfair_err_load:missing_parent_fair' );--
      
      SET vi_ins_updt_del_ct = 0;--
   END IF;--

   COMMIT;--
   
      -- Sept. 26, 2012; added this statement to delete blank fair_id entered mistakenly 
      --    into the table book_fair_data_load
   DELETE FROM bkfair01.book_fair_data_load
      WHERE fair_id IS NULL;--
   COMMIT;--

      -- Not to load the error data into book fair database, delete 
      --   the error data from the book_fair_data_load table

   DELETE FROM bkfair01.book_fair_data_load
      WHERE fair_id IN ( 
            SELECT fair_id FROM bkfair01.book_fair_error_data );--
   COMMIT;--

      -- To avoid foreign key violation errors on school_id column 
      --    on bkfair01.profit_balance table implemented on 02-05-2007

   DELETE FROM bkfair01.profit_balance_data_load
      WHERE BIGINT(school_id) IN
           ( SELECT BIGINT(school_id) FROM bkfair01.profit_balance_data_load
             EXCEPT
             SELECT id FROM bkfair01.school);--
   COMMIT;--
END;

COMMENT ON PROCEDURE "BKFAIR01"."P_BKFAIR_ERR_LOAD"() IS 'Rel 23; Adaptation to Real Time Message implementation in removing all Real Time column no longer imported via PROD_CIS_CPW FILE. Added checking of OFE_SALES new column from CPW file';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_planner_admin_publish (
    OUT pov_feedback          VARCHAR(6),
    OUT pov_current_table     VARCHAR(60),
    OUT poi_sqlcode           INTEGER,
    OUT poc_sqlstate          CHARACTER(6) )
  SPECIFIC sp_planner_admin_publish
  LANGUAGE SQL
  NOT DETERMINISTIC
  EXTERNAL ACTION
  MODIFIES SQL DATA
  CALLED ON NULL INPUT
  INHERIT SPECIAL REGISTERS
  OLD SAVEPOINT LEVEL
BEGIN
       -- History: Rel. 24   Winter 2013
       --          Version : V-01
       --          1- Created to publish all Planner Administration data
       --             from schema UNPUBLISHED to schema BKFAIR01
       --             The tables being updated or inserted into via MERGE
       --             commands are in the very order:
       --               - planner_goals;--
       --               - planner_categories;--
       --               - planner_programs;--
       --               - planner_program_tasks;--
       --               - planner_program_products;--
       --               - planner_tasks;--
       --               - planner_task_products;--
       --               - planner_task_headings
       --          2- This procedure is called from the application when the 
       --             Planner Administrator clicks on button "publish now"
       --             located on HTML page PAT100.
       --          3- The new value "publish_date" is kept in the unique row
       --             "unpublished/bkfair01.planner_goals.publish_date".
       --          4- All publishing actions, will see the update_date in all
       --             bkfair01 relevant tables be updated with 
       --             the vt_new_publish_date;--
       --             All new records in bkfair01 schema will also have both
       --             their create_date and update_date setup with
       --             the vt_new_publish_date.
       --          5- It was decided to put the COMMIT at the very end of the
       --             task (instead of after every single statement) to allow
       --             a full rollback for every statement has all that data
       --             depends on each other. In addition, the small volume
       --             of the data allows largely for us to do so.
       --          6- 01/03/2014:
       --             Remove the category_id from the where-clause when 
       --             publishing "planner_programs" and add "category_id" as
       --             in the update section of the merge command at the 
       --             "planner_programs" table level.
       -- Rel. 24.5  Winter 2013-2014 - V-01
       --          1- Add columns processing to 3 tables per his release:
       --             1-1 PLANNER_PROGRAM_PRODUCTS
       --             1-2 PLANNER_PROGRAM_TASKS
       --             1-3 PLANNER_TASKS
       
   DECLARE vi_bkfair01_goal_count  INTEGER;--
   DECLARE vi_percentage_1         INTEGER;--
   DECLARE vi_percentage_2         INTEGER;--
   DECLARE vi_percentage_3         INTEGER;--
   DECLARE vt_goal_update_date     TIMESTAMP;--
   DECLARE vt_goal_publish_date    TIMESTAMP;--
   DECLARE vi_affected_row_ct      INTEGER DEFAULT 0;  -- Tracks insert/update counts
   DECLARE vt_new_publish_date     TIMESTAMP;--
   DECLARE sqlstate                CHAR(5) DEFAULT '00000';--
   DECLARE sqlcode                 INTEGER DEFAULT 0;--
   DECLARE pc_sqlstate_out         CHAR(5) DEFAULT '00000';--
      -- Declare Handlers
   DECLARE EXIT HANDLER FOR SQLEXCEPTION
      SELECT sqlcode, sqlstate 
         INTO poi_sqlcode, poc_sqlstate
         FROM sysibm.sysdummy1;--
      ROLLBACK;--
      SET pov_feedback = 'FALSE';--

      --------------------------------------
      -- Processing starts here
      -- Assign the new publish date...
      --------------------------------------
   SET vt_new_publish_date = CURRENT TIMESTAMP;--
   
   
      ------------------------------------------------------
      -- FIRST, address PLANNER_GOALS table...
      -- Note_1: for merge to occur, we compare the "update_date" 
      --         of all table records against the unique record column 
      --         value BKFAIR01.planner_goals.publish_date
      --         "vt_goal_publish_date" variable.
      -- Note_2: the table planner_goals is the exception
      --         for not using the merge command since there
      --         is only one row for ever in that table and no
      --         ID column exists.
      ------------------------------------------------------
   
   SELECT percentage_1, percentage_2, percentage_3,
          update_date, publish_date
      INTO vi_percentage_1, vi_percentage_2, vi_percentage_3,
           vt_goal_update_date, vt_goal_publish_date
      FROM unpublished.planner_goals;--
   
   SELECT COALESCE(publish_date, '1900-01-01-01.00.00')
      INTO vt_goal_publish_date
      FROM bkfair01.planner_goals;--
   
   -- # 01-A
   SET pov_current_table = '1_PLANNER_GOALS';--
   IF ( vt_goal_update_date  > vt_goal_publish_date ) THEN
      UPDATE bkfair01.planner_goals 
         SET percentage_1 = vi_percentage_1,
             percentage_2 = vi_percentage_2,
             percentage_3 = vi_percentage_3,
             update_date = vt_new_publish_date,
             publish_date = vt_new_publish_date;--
   END IF;   -- # 01

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--

      -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_goals', 
               vi_affected_row_ct, 'sp_planner_admin_publish:update' );--
      -- Reset count...
   SET vi_affected_row_ct = 0;--

      -- Also, we need to set the publish date no matter what to map it to
      --  the unpublished schema table as a result of the publish action.
   UPDATE bkfair01.planner_goals 
      SET publish_date = vt_new_publish_date;--

      -------------------------------------------------------------
      -- SECOND, address PLANNER_CATEGORIES table...
      -------------------------------------------------------------
   SET pov_current_table = '2_PLANNER_CATEGORIES';--
   MERGE INTO bkfair01.planner_categories  t1  
      USING ( SELECT id, title, 
                     description, sequence, 
                     active, active_date, 
                     inactive_date  
                FROM unpublished.planner_categories
                WHERE update_date > vt_goal_publish_date )  t2  
      ON ( t1.id = t2.id )   
      WHEN MATCHED THEN  
           UPDATE SET  
           ( t1.title, t1.description, 
             t1.sequence, t1.update_date ) =  
           ( t2.title, t2.description, 
             t2.sequence, vt_new_publish_date )  
      WHEN NOT MATCHED THEN 
         INSERT ( id, title, 
                  description, sequence,
                  active, create_date, 
                  update_date, active_date,
                  inactive_date )
            VALUES ( t2.id, t2.title, 
                     t2.description, t2.sequence, 
                     t2.active, vt_new_publish_date,
                     vt_new_publish_date, t2.active_date, 
                     t2.inactive_date );--

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--

      -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_categories', 
               vi_affected_row_ct, 'sp_planner_admin_publish:merge' );--

      -- Reset count...
   SET vi_affected_row_ct = 0;--


      -------------------------------------------------------------
      -- THIRD, address PLANNER_PROGRAMS table...
      -------------------------------------------------------------
   SET pov_current_table = '3_PLANNER_PROGRAMS';--
   MERGE INTO bkfair01.planner_programs  t1  
      USING ( SELECT id, category_id, 
                     title, active, 
                     active_date, description, 
                     file_name, inactive_date  
                FROM unpublished.planner_programs
                WHERE update_date > vt_goal_publish_date )  t2  
      ON ( t1.id = t2.id )
      WHEN MATCHED THEN  
           UPDATE SET  
           ( t1.category_id, t1.title, t1.description, 
             t1.file_name, t1.update_date ) =  
           ( t2.category_id, t2.title, t2.description, 
             t2.file_name, vt_new_publish_date )  
      WHEN NOT MATCHED THEN 
         INSERT ( id, category_id, 
                  title, active, 
                  create_date, update_date, 
                  active_date, description, 
                  file_name, inactive_date )
            VALUES ( t2.id, t2.category_id, 
                     t2.title, t2.active, 
                     vt_new_publish_date, vt_new_publish_date, 
                     t2.active_date, t2.description, 
                     t2.file_name, t2.inactive_date );--

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--

      -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_programs', 
               vi_affected_row_ct, 'sp_planner_admin_publish:merge' );--

      -- Reset count...
   SET vi_affected_row_ct = 0;--


      -------------------------------------------------------------
      -- FOURTH, address PLANNER_PROGRAM_TASKS table...
      -------------------------------------------------------------
   SET pov_current_table = '4_PLANNER_PROGRAM_TASKS';--
   MERGE INTO bkfair01.planner_program_tasks  t1  
      USING ( SELECT id, program_id, 
                     title, description, 
                     active, active_date, 
                     no_of_days, date_flag, 
                     inactive_date, partial_edit_ckbox 
                FROM unpublished.planner_program_tasks
                WHERE update_date > vt_goal_publish_date )  t2  
      ON ( t1.id = t2.id )
        AND ( t1.program_id = t2.program_id ) 
      WHEN MATCHED THEN  
           UPDATE SET  
           ( t1.title, t1.description, 
             t1.no_of_days, t1.date_flag, 
             t1.update_date, t1.partial_edit_ckbox ) =  
           ( t2.title, t2.description, 
             t2.no_of_days, t2.date_flag, 
             vt_new_publish_date, t2.partial_edit_ckbox )  
      WHEN NOT MATCHED THEN 
         INSERT ( id, program_id, 
                  title, description, 
                  active, create_date, 
                  update_date, active_date, 
                  no_of_days, date_flag, 
                  inactive_date, partial_edit_ckbox )
            VALUES ( t2.id, t2.program_id, 
                     t2.title, t2.description, 
                     t2.active, vt_new_publish_date, 
                     vt_new_publish_date, t2.active_date, 
                     t2.no_of_days, t2.date_flag, 
                     t2.inactive_date, t2.partial_edit_ckbox );--

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--

      -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_program_tasks', 
               vi_affected_row_ct, 'sp_planner_admin_publish:merge' );--

      -- Reset count...
   SET vi_affected_row_ct = 0;--


      -------------------------------------------------------------
      -- FIFTH A, address PLANNER_PROGRAM_PRODUCTS table...
      -------------------------------------------------------------
   SET pov_current_table = '5A_PLANNER_PROGRAM_PRODUCTS';--
   MERGE INTO bkfair01.planner_program_products  t1  
      USING ( SELECT program_id, group_id, category_id, 
                     sequence, product_listing, 
                     recommended_flag, display_scheduler_flag, 
                     no_of_volunteers, duration,
                     participants, books_per_participant, 
                     benefits_statement
                FROM unpublished.planner_program_products
                WHERE update_date > vt_goal_publish_date )  t2  
      ON ( t1.program_id = t2.program_id )
        AND ( t1.group_id = t2.group_id ) 
      WHEN MATCHED THEN  
           UPDATE SET  
           ( t1.category_id, t1.sequence, t1.update_date, 
             t1.product_listing, t1.recommended_flag, 
             t1.display_scheduler_flag, 
             t1.no_of_volunteers, t1.duration, 
             t1.participants, t1.books_per_participant, 
             t1.benefits_statement ) =  
           ( t2.category_id, t2.sequence, vt_new_publish_date, 
             t2.product_listing, t2.recommended_flag, 
             t2.display_scheduler_flag, 
             t2.no_of_volunteers, t2.duration, 
             t2.participants, t2.books_per_participant, 
             t2.benefits_statement )  
      WHEN NOT MATCHED THEN 
         INSERT ( program_id, group_id, 
                  category_id, sequence, create_date, 
                  update_date, product_listing, 
                  recommended_flag, display_scheduler_flag, 
                  no_of_volunteers, duration, 
                  participants, books_per_participant, 
                  benefits_statement )
            VALUES ( t2.program_id, t2.group_id, 
                     t2.category_id, t2.sequence, vt_new_publish_date, 
                     vt_new_publish_date, t2.product_listing, 
                     t2.recommended_flag, t2.display_scheduler_flag, 
                     t2.no_of_volunteers, t2.duration, 
                     t2.participants, t2.books_per_participant, 
                     t2.benefits_statement ) ;--

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--

      -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_program_products', 
               vi_affected_row_ct, 'sp_planner_admin_publish:merge' );--

      -- Reset count...
   SET vi_affected_row_ct = 0;--

      -------------------------------------------------------------
      -- FIFTH B, address PLANNER_PROGRAM_PRODUCTS table...
      --   This section addresses the SPECIFIC DELETION when needed
      --   of all the BKFAIR01.PLANNER_PROGAM_PRODUCTS records for
      --   which no records exist in UNPUBLISHED.PLANNER_PROGRAM_PRODUCTS.
      --   This scenario can occur when the Administrator unchecks
      --   all product IDs under a set of Product ID Grouping which
      --   had not been the case for the previous "publishing"
      --   action.
      -------------------------------------------------------------
   SET pov_current_table = '5B_PLANNER_PROGRAM_PRODUCTS';--
   DELETE FROM bkfair01.planner_program_products
      WHERE ( program_id, group_id ) NOT IN 
         ( SELECT program_id, group_id
              FROM unpublished.planner_program_products );--

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--

         -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_program_products', 
               vi_affected_row_ct, 'sp_planner_admin_publish:delete' );--

      -- Reset count...
   SET vi_affected_row_ct = 0;--


      -------------------------------------------------------------
      -- SIXTH, address PLANNER_TASKS table...
      -------------------------------------------------------------
   SET pov_current_table = '6_PLANNER_TASKS';--
   MERGE INTO bkfair01.planner_tasks  t1  
      USING ( SELECT id, 
                     title, description, 
                     active, active_date, 
                     no_of_days, date_flag, 
                     inactive_date, partial_edit_ckbox  
                FROM unpublished.planner_tasks
                WHERE update_date > vt_goal_publish_date )  t2  
      ON ( t1.id = t2.id )  
      WHEN MATCHED THEN  
           UPDATE SET  
           ( t1.title, t1.description, 
             t1.update_date, t1.no_of_days, 
             t1.date_flag, t1.partial_edit_ckbox ) =  
           ( t2.title, t2.description, 
             vt_new_publish_date, t2.no_of_days, 
             t2.date_flag, t2.partial_edit_ckbox  )  
      WHEN NOT MATCHED THEN 
         INSERT ( id, 
                  title, description, 
                  active, create_date, 
                  update_date, active_date, 
                  no_of_days, date_flag, 
                  inactive_date, partial_edit_ckbox )
            VALUES ( t2.id, 
                     t2.title, t2.description, 
                     t2.active, vt_new_publish_date, 
                     vt_new_publish_date, t2.active_date, 
                     t2.no_of_days, t2.date_flag, 
                     t2.inactive_date, t2.partial_edit_ckbox );--

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--

      -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_tasks', 
               vi_affected_row_ct, 'sp_planner_admin_publish:merge' );--

      -- Reset count...
   SET vi_affected_row_ct = 0;--


      -------------------------------------------------------------
      -- SEVENTH A, address PLANNER_TASK_PRODUCTS table...
      -------------------------------------------------------------
   SET pov_current_table = '7A_PLANNER_TASK_PRODUCTS';--
   MERGE INTO bkfair01.planner_task_products  t1  
      USING ( SELECT task_id, group_id, 
                     product_listing  
                FROM unpublished.planner_task_products
                WHERE update_date > vt_goal_publish_date )  t2  
      ON ( t1.task_id = t2.task_id )
        AND ( t1.group_id = t2.group_id ) 
      WHEN MATCHED THEN  
           UPDATE SET  
           ( t1.update_date, 
             t1.product_listing ) =  
           ( vt_new_publish_date, 
             t2.product_listing )  
      WHEN NOT MATCHED THEN 
         INSERT ( task_id, 
                  group_id, create_date, 
                  update_date, product_listing )
            VALUES ( t2.task_id, 
                     t2.group_id, vt_new_publish_date, 
                     vt_new_publish_date, t2.product_listing ) ;--

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--
         
      -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_task_products', 
               vi_affected_row_ct, 'sp_planner_admin_publish:merge' );--

      -- Reset count...
   SET vi_affected_row_ct = 0;--

      -------------------------------------------------------------
      -- SEVENTH B, address PLANNER_TASK_PRODUCTS table...
      --   This section addresses the SPECIFIC DELETION when needed
      --   of all the BKFAIR01.PLANNER_TASK_PRODUCTS records for
      --   which no records exist in UNPUBLISHED.PLANNER_TASK_PRODUCTS.
      --   This scenario can occur when the Administrator unchecks
      --   all product IDs under a set of Product ID Grouping which
      --   had not been the case for the previous "publishing"
      --   action.
      -------------------------------------------------------------
   SET pov_current_table = '7B_PLANNER_TASK_PRODUCTS';--
   DELETE FROM bkfair01.planner_task_products
      WHERE ( task_id, group_id ) NOT IN 
         ( SELECT task_id, group_id
              FROM unpublished.planner_task_products );--

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--

         -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_task_products', 
               vi_affected_row_ct, 'sp_planner_admin_publish:delete' );--

      -- Reset count...
   SET vi_affected_row_ct = 0;--


      -------------------------------------------------------------
      -- EIGTH, address PLANNER_TASK_HEADINGS table...
      -------------------------------------------------------------
   SET pov_current_table = '8_PLANNER_TASK_HEADINGS';--
   MERGE INTO bkfair01.planner_task_headings  t1  
      USING ( SELECT id, sequence, 
                     title, date_flag, 
                     from_days, to_days 
                FROM unpublished.planner_task_headings
                WHERE update_date > vt_goal_publish_date )  t2  
      ON ( t1.id = t2.id )   
      WHEN MATCHED THEN  
           UPDATE SET  
           ( t1.sequence, t1.update_date, 
             t1.title, t1.date_flag, 
             t1.from_days, t1.to_days ) =  
           ( t2.sequence, vt_new_publish_date, 
             t2.title, t2.date_flag, 
             t2.from_days, t2.to_days )  
      WHEN NOT MATCHED THEN 
         INSERT ( id, sequence, 
                  create_date, update_date, 
                  title, date_flag, 
                  from_days, to_days )
            VALUES ( t2.id, t2.sequence, 
                     vt_new_publish_date, vt_new_publish_date, 
                     t2.title, t2.date_flag, 
                     t2.from_days, t2.to_days );--

   GET DIAGNOSTICS vi_affected_row_ct = ROW_COUNT;--

      -- Data load feedback data entry...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, 
        update_count, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 'bkfair01.planner_task_headings', 
               vi_affected_row_ct, 'sp_planner_admin_publish:merge' );--

      -- Reset count...
   SET vi_affected_row_ct = 0;--

      -- Time to set the publish_date to today's date.
      -- kept in unpublished.planner_goals.publish_date.
      -- There's only one row in that table at all times.
   UPDATE unpublished.planner_goals 
      SET publish_date = vt_new_publish_date;--

      -- Data logging of new publish date just in case it's needed...
   INSERT INTO bkfair01.fair_load_feedback
      ( create_date, table_name, procedure_name )
      VALUES ( CURRENT TIMESTAMP, 
               'bkfair01.planner_goals:new publish_date', 
               ('sp_planner_admn_publish:' || CHAR(vt_new_publish_date )));--
   
      -- Time to commit the whole Publishing action as a whole.
      -- Doing so as a whole, allows for a full rollback in case
      -- a problem arise;  In addition, the volume of data allows it 
      -- to a large extent and at hardly no cost to the database engine.
   COMMIT;--
      -- Set the output parameter...
   SET pov_feedback = 'TRUE';--
   END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_PLANNER_ADMIN_PUBLISH"(VARCHAR(),VARCHAR(),INTEGER,CHAR()) IS 'Bkfair release 24/24.5 - Publish all Administration Planner Tables from schema UNPUBLISHED to schema BKFAIR01; This publishing occurs by either updating or inserting the data via several MERGE statements.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_rel_24_data_population (
    IN piv_table_name                   VARCHAR(40),
    OUT poi_update_count_out            INTEGER,
    OUT poi_insert_count_out            INTEGER,
    OUT pc_sqlstate_out                 CHAR(5),
    OUT pi_sqlcode_out                  INTEGER,
    OUT pv_statement_out                VARCHAR(20) )
   SPECIFIC sp_rel_24_data_population
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
BEGIN
      -- Purpose:
      -- This procedure is part of Release 24 and used only
      -- towards that release specific data population for  
      -- additional columns in the table FAIR and a new 
      -- table FAIR_PLANNER_GOALS.
      -- The parameter table_name must be in upper-case!
   
   DECLARE v_table_name            VARCHAR(40);--
   DECLARE num_rows                INTEGER DEFAULT 0;--
   DECLARE tot_updated_rows        INTEGER DEFAULT 0;--
   DECLARE tot_inserted_rows       INTEGER DEFAULT 0;--
   DECLARE vb_fair_id              BIGINT;--
   DECLARE vc_class                CHAR(1);--
   DECLARE vd_enrollment           DEC(10);--
   DECLARE SQLSTATE                CHAR(5) DEFAULT '00000';--
   DECLARE SQLCODE                 INTEGER DEFAULT 0;--
      -- Cursors...
   -- 01 To populate FAIR new columns...
   DECLARE fair_cur CURSOR WITH HOLD FOR
      SELECT r.fair_id, r.fair_class, r.fair_enrollment
         FROM bkfair01.rel_24_ken_fair_class_enrollment_data r,
              bkfair01.fair f
         WHERE r.fair_id = f.id;--

   -- 02 To populate FAIR_PLANNER_GOALS new table...
   DECLARE goal_cur CURSOR WITH HOLD FOR
      SELECT oh.fair_id
         FROM bkfair01.online_homepage oh,
              bkfair01.fair f
         WHERE f.pickup_date > ( CURRENT DATE - 1 YEAR )
           AND f.id = oh.fair_id
           AND oh.goal_amount > 0;--
             
   -- Release 24.5
   -- 03-A
   -- To populate UNPUBLISHED.HOMEPAGE_EVENT.PLANNER_CALENDAR_CKBOX...
   DECLARE unpub_hpevent_cur CURSOR WITH HOLD FOR
      SELECT hpe.fair_id
         FROM unpublished.homepage_event hpe,
              bkfair01.fair f
         WHERE f.pickup_date >= ( CURRENT DATE - 1 MONTH )
           AND f.id = hpe.fair_id
           AND hpe.event_id != 29;--
   
   -- 03-B
   -- To populate BKFAIR01.HOMEPAGE_EVENT.PLANNER_CALENDAR_CKBOX...
   DECLARE bkfair01_hpevent_cur CURSOR WITH HOLD FOR
      SELECT hpe.fair_id
         FROM bkfair01.homepage_event hpe,
              bkfair01.fair f
         WHERE f.pickup_date >= ( CURRENT DATE - 1 MONTH )
           AND f.id = hpe.fair_id
           AND hpe.event_id != 29;--
   
      -- Handlers
   DECLARE EXIT HANDLER FOR SQLEXCEPTION
      SELECT SQLSTATE, SQLCODE
         INTO pc_sqlstate_out, pi_sqlcode_out
         FROM sysibm.sysdummy1;--
      SET poi_update_count_out = tot_updated_rows;--
      SET poi_insert_count_out = tot_inserted_rows;--
      
      -- Initialization...
   -- 01
   VALUES (SQLSTATE, SQLCODE)
         INTO pc_sqlstate_out, pi_sqlcode_out;--
   -- 02
   SET v_table_name = piv_table_name;--
   
   IF ( v_table_name = 'FAIR' ) THEN
      OPEN fair_cur;--
      FETCH FROM fair_cur INTO vb_fair_id, vc_class, vd_enrollment;--
      
      WHILE ( SQLSTATE = '00000' ) DO
         UPDATE bkfair01.fair 
               SET fair_class = vc_class, 
                   fair_enrollment = vd_enrollment 
               WHERE id = vb_fair_id; --
   
         GET DIAGNOSTICS num_rows = ROW_COUNT;--
         SET tot_updated_rows = ( tot_updated_rows + num_rows );--
         FETCH FROM fair_cur INTO vb_fair_id, vc_class, vd_enrollment;--
      END WHILE;--
   
      CLOSE fair_cur;--
      INSERT INTO bkfair01.fair_load_feedback 
                 ( create_date, 
                   table_name, 
                   update_count, 
                   procedure_name ) 
         VALUES ( CURRENT TIMESTAMP, 
                  'fc.fair', 
                  tot_updated_rows, 
                  'sp_rel_24_data_population.FAIR' );--
      COMMIT;--
      
   ELSEIF ( v_table_name = 'FAIR_PLANNER_GOALS' ) THEN
      OPEN goal_cur;--
      FETCH FROM goal_cur INTO vb_fair_id;--
      
      WHILE ( SQLSTATE = '00000' ) DO
         INSERT INTO bkfair01.fair_planner_goals 
            ( fair_id, percentage_flag, 
              where_initial_goal_setup, where_current_goal_setup )
         VALUES ( vb_fair_id, 'EYO', 'HOMEPAGE', 'HOMEPAGE' ); --
   
         GET DIAGNOSTICS num_rows = ROW_COUNT;--
         SET tot_inserted_rows = ( tot_inserted_rows + num_rows );--
         FETCH FROM goal_cur INTO vb_fair_id;--
      END WHILE;--
   
      CLOSE goal_cur;--
      INSERT INTO bkfair01.fair_load_feedback 
                 ( create_date, 
                   table_name, 
                   insertion_count, 
                   procedure_name ) 
         VALUES ( CURRENT TIMESTAMP, 
                  'fc.fair', 
                  tot_inserted_rows, 
                  'sp_rel_24_data_population.planner_goals' );--
      COMMIT;--
   
   ELSEIF ( v_table_name = 'HOMEPAGE_EVENT' ) THEN
         -- 3-A Process updating unpublished_homepage_event...
      OPEN unpub_hpevent_cur;--
      FETCH FROM unpub_hpevent_cur INTO vb_fair_id;--
      
      WHILE ( SQLSTATE = '00000' ) DO
         UPDATE unpublished.homepage_event 
            SET planner_calendar_ckbox = 'Y' 
               WHERE fair_id = vb_fair_id; --
   
         GET DIAGNOSTICS num_rows = ROW_COUNT;--
         SET tot_updated_rows = ( tot_updated_rows + num_rows );--
         FETCH FROM unpub_hpevent_cur INTO vb_fair_id;--
      END WHILE;--
   
      CLOSE unpub_hpevent_cur;--
      INSERT INTO bkfair01.fair_load_feedback 
                 ( create_date, 
                   table_name, 
                   update_count, 
                   procedure_name ) 
         VALUES ( CURRENT TIMESTAMP, 
                  'unpub.homepage_event', 
                  tot_updated_rows, 
                  'sp_rel_24_data_population.unpub_homepage_event' );--
      COMMIT;--

         -- 3-B Process updating bkfair01_homepage_event...
         -- First reset the counts
      SET num_rows = 0;--
      SET tot_updated_rows = 0;--
      
      OPEN bkfair01_hpevent_cur;--
      FETCH FROM bkfair01_hpevent_cur INTO vb_fair_id;--
      
      WHILE ( SQLSTATE = '00000' ) DO
         UPDATE bkfair01.homepage_event 
            SET planner_calendar_ckbox = 'Y' 
               WHERE fair_id = vb_fair_id; --
   
         GET DIAGNOSTICS num_rows = ROW_COUNT;--
         SET tot_updated_rows = ( tot_updated_rows + num_rows );--
         FETCH FROM bkfair01_hpevent_cur INTO vb_fair_id;--
      END WHILE;--
   
      CLOSE bkfair01_hpevent_cur;--
      INSERT INTO bkfair01.fair_load_feedback 
                 ( create_date, 
                   table_name, 
                   update_count, 
                   procedure_name ) 
         VALUES ( CURRENT TIMESTAMP, 
                  'bkfair01.homepage_event', 
                  tot_updated_rows, 
                  'sp_rel_24_data_population.bkfair01_homepage_event' );--
      COMMIT;--
      
   ELSE
      SET pv_statement_out = 'Wrong Table';--

   END IF;--
         --
         -- Set OUT paramaters...
         --
   SET poi_update_count_out = tot_updated_rows;--
   SET poi_insert_count_out = tot_inserted_rows;--
   END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_REL_24_DATA_POPULATION"(VARCHAR(),INTEGER,INTEGER,CHAR(),INTEGER,VARCHAR()) IS 'Release 24: and only Release 24 oriented stored procedure which is used to populate data into new columns and new table.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_previous_fairs (
                 IN  piv_run_type  VARCHAR(15),
                 OUT poi_del_count INTEGER,
                 OUT poi_ins_count INTEGER,
                 OUT poi_rec_count INTEGER,
                 OUT poc_sqlstate  CHAR(5), 
                 OUT poi_sqlcode   INTEGER )
   SPECIFIC sp_previous_fairs
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
BEGIN
      -- History: Rel. 23     Summer 2013
      --          1- All references to the table DISTRICT have been removed
      --             since that table has been dropped in that release.
      -- 
      --          Rel. 24.5 around Spring 2014
      --          1- Modify the procedure in parametizing it as well as 
      --             preventing large and unnecessary data shuffling reducing
      --             running the procedure from 1 hour to a couple of minutes.
      -- 
      -- Variables declaration for Fair table
   DECLARE v_fair_id                 BIGINT;--
   DECLARE v_school_id               BIGINT;--
   DECLARE v_delivery_date           DATE;          -- Fair Start date
   DECLARE v_pickup_date             DATE;          -- Fair End date
   DECLARE v_previous_fair_id1       BIGINT;--
   DECLARE v_previous_start_date1    DATE;--
   DECLARE v_previous_start_date2    DATE;--
   DECLARE v_previous_start_date3    DATE;--
   DECLARE viv_run_type              VARCHAR(15);--
   DECLARE vi_delete_ct              INTEGER DEFAULT 0;--
   DECLARE voi_tot_delete_ct         INTEGER DEFAULT 0;--
   DECLARE vi_insert_ct              INTEGER DEFAULT 0;--
   DECLARE voi_tot_insert_ct         INTEGER DEFAULT 0;--
   DECLARE voi_tot_record_ct         INTEGER DEFAULT 0;--

   DECLARE sqlstate                  CHAR (5) DEFAULT '00000';--
   DECLARE sqlcode                   INTEGER DEFAULT 0;--
   
   -- Cursor Declarations...
   -- # 01: Cursor to use the data provided by CIS only...
   DECLARE c_bkfair_data_load CURSOR WITH HOLD FOR
      SELECT f.id, 
             f.school_id, 
             f.delivery_date
         FROM bkfair01.book_fair_data_load bf,
              bkfair01.fair f
         WHERE BIGINT(bf.fair_id) = f.id;--
   
   -- # 02: Cursor to use the complete Fair table...
   DECLARE c_book_fair CURSOR WITH HOLD FOR
      SELECT f.id, 
             f.school_id, 
             f.delivery_date
         FROM bkfair01.fair f;--
   
   -- Handlers
   DECLARE EXIT HANDLER FOR SQLEXCEPTION
      SELECT sqlstate, sqlcode
         INTO poc_sqlstate, poi_sqlcode
         FROM sysibm.sysdummy1;--
      SET poi_del_count = voi_tot_delete_ct;--
      SET poi_ins_count = voi_tot_insert_ct;--
      SET poi_rec_count = voi_tot_record_ct;--
      
      -- Initialization...
   VALUES (sqlstate, sqlcode)
         INTO poc_sqlstate, poi_sqlcode;--
   SET viv_run_type = piv_run_type;--
   
   
   -- ---------------------------------------------------------
   -- # 01:A PREVIOUS_FAIRS DELTA DATA CREATION BASED ON CIS
   --        WHICH ALLOWS FOR MUCH LESS DATA SHUFFLING...
   -- ---------------------------------------------------------
   
   IF ( viv_run_type = 'DAILY_LOAD' ) THEN
         -- First, delete all fair records common to both "book_fair_data_load"
         --   and "previous_fairs" prior to recreate them:
      DELETE FROM bkfair01.previous_fairs
         WHERE fair_id IN ( SELECT BIGINT(fair_id) 
                               FROM bkfair01.book_fair_data_load );--
      
         -- Track the deletions...
      GET DIAGNOSTICS vi_delete_ct = ROW_COUNT;--
      SET voi_tot_delete_ct = ( voi_tot_delete_ct + vi_delete_ct );--
      COMMIT;--
      
      -- This section will delete/insert the PREVIOUS_FAIRS table data
      -- with the data found in BOOK_FAIR_DATA_LOAD table matching the 
      -- SCHOOL_ID using table data from MYTEAM.fair_id/FAIR.id/school_id.
      
      OPEN c_bkfair_data_load;--
      FETCH FROM c_bkfair_data_load
        INTO v_fair_id, 
             v_school_id, 
             v_delivery_date;--
   
      WHILE (sqlstate = '00000' ) DO
         SET voi_tot_record_ct = voi_tot_record_ct + 1;--
         SET v_previous_start_date1 = NULL;--
         SET v_previous_start_date2 = NULL;--
         SET v_previous_start_date3 = NULL;--
   
         -- To get the 1st Previous fair_id's detail 
         --   & it should exist on myteam table
         SELECT MAX (delivery_date) 
            INTO v_previous_start_date1
            FROM bkfair01.fair f, 
                 bkfair01.myteam mt
            WHERE f.id = mt.fair_id
              AND f.school_id = v_school_id
              AND f.pickup_date < v_delivery_date;--
            
         -- # 01-A:01
         IF v_previous_start_date1 IS NOT NULL THEN
               -- If it finds the 1st previous fair_id 
               --   then insert otherwise don't insert
            INSERT INTO bkfair01.previous_fairs (
                        fair_id,
                        school_id,
                        previous_fair_id,
                        previous_fair_start_date )
               SELECT DISTINCT v_fair_id,
                      v_school_id,
                      f.id,
                      f.delivery_date
                  FROM bkfair01.fair   f, 
                       bkfair01.myteam mt
                  WHERE f.id = mt.fair_id
                    AND f.school_id = v_school_id
                    AND f.delivery_date = v_previous_start_date1;--
            
               -- Track the insertions...
            GET DIAGNOSTICS vi_insert_ct = ROW_COUNT;--
            SET voi_tot_insert_ct = ( voi_tot_insert_ct + vi_insert_ct );--
            COMMIT;--
            
               -- To get the 2nd Previous fair_id's detail 
               --  & it should exist on myteam table
            SELECT MAX (delivery_date)
              INTO v_previous_start_date2
              FROM bkfair01.fair   f, 
                   bkfair01.myteam mt
             WHERE f.id = mt.fair_id
               AND f.school_id = v_school_id
               AND f.pickup_date < v_previous_start_date1;--
            
            -- # 01-A:01:01
            IF v_previous_start_date2 IS NOT NULL THEN
                  -- If it find the 2nd previous fair_id 
                  --    then insert otherwise don't insert
               INSERT INTO bkfair01.previous_fairs ( 
                           fair_id,
                           school_id,
                           previous_fair_id,
                           previous_fair_start_date )
                  SELECT DISTINCT v_fair_id,
                         v_school_id,
                         f.id,
                         f.delivery_date
                     FROM bkfair01.fair f, 
                          bkfair01.myteam mt
                     WHERE f.id = mt.fair_id
                       AND f.school_id = v_school_id
                       AND f.delivery_date = v_previous_start_date2;--
               
                  -- Track the insertions...
               GET DIAGNOSTICS vi_insert_ct = ROW_COUNT;--
               SET voi_tot_insert_ct = ( voi_tot_insert_ct + vi_insert_ct );--
               COMMIT;--
               
                  -- To get the 3rd Previous fair_id
               SELECT MAX (delivery_date)
                 INTO v_previous_start_date3
                 FROM bkfair01.fair f, 
                      bkfair01.myteam mt
                 WHERE f.id = mt.fair_id
                   AND f.school_id = v_school_id
                   AND f.pickup_date < v_previous_start_date2;--
               
               -- # 01-A:01:01:01
               IF v_previous_start_date3 IS NOT NULL THEN
               
                  -- If it finds the 3rd previous fair_id then 
                  --   insert otherwise don't insert
                  INSERT INTO bkfair01.previous_fairs (
                              fair_id,
                              school_id,
                              previous_fair_id,
                              previous_fair_start_date)
                     SELECT DISTINCT v_fair_id,
                            v_school_id,
                            f.id,
                            f.delivery_date
                       FROM bkfair01.fair   f, 
                            bkfair01.myteam mt
                       WHERE f.id = mt.fair_id
                         AND f.school_id = v_school_id
                         AND f.delivery_date = v_previous_start_date3;--
                  
                     -- Track the insertions...
                  GET DIAGNOSTICS vi_insert_ct = ROW_COUNT;--
                  SET voi_tot_insert_ct = ( voi_tot_insert_ct + vi_insert_ct );--
                  COMMIT;--
                  
               END IF;   -- 01-A:01:01:01
            END IF;      -- 01-A:01:01
         END IF;    -- 01-A:01
         
         FETCH FROM c_bkfair_data_load
            INTO v_fair_id, 
                 v_school_id, 
                 v_delivery_date;--
      END WHILE;--
      
      CLOSE c_bkfair_data_load;--
      COMMIT;--
      
         -- Provide Feedback at the End of the Job...
      SET poi_del_count = voi_tot_delete_ct;--
      SET poi_ins_count = voi_tot_insert_ct;--
      SET poi_rec_count = voi_tot_record_ct;--
      
   -- -----------------------------------------------------
   -- # 01:B   FULL DELETION AND TABLE REBUILDING SECTION...
   -- -----------------------------------------------------
   ELSEIF ( viv_run_type = 'FULL_RELOAD' ) THEN
      -- This section will reload from scratch the PREVIOUS_FAIRS table with 
      -- using a cursor over the entire data from the table FAIR which will 
      -- take some 40 minutes to run (original section).
      
            -- Delete all the records.
      DELETE FROM bkfair01.previous_fairs;--
      
         -- Track the deletions...
      GET DIAGNOSTICS vi_delete_ct = ROW_COUNT;--
      SET voi_tot_delete_ct = ( voi_tot_delete_ct + vi_delete_ct );--
      COMMIT;--
   
      OPEN c_book_fair;--
   
      FETCH FROM c_book_fair
        INTO v_fair_id, v_school_id, v_delivery_date; --
   
      WHILE (sqlstate = '00000' ) DO
         SET voi_tot_record_ct = voi_tot_record_ct + 1;--
         SET v_previous_start_date1 = NULL;--
         SET v_previous_start_date2 = NULL;--
         SET v_previous_start_date3 = NULL;--
   
         -- To get the 1st Previous fair_id's detail 
         --   & it should exist on myteam table
         SELECT MAX (delivery_date) 
            INTO v_previous_start_date1
            FROM bkfair01.fair f, 
                 bkfair01.myteam mt
            WHERE f.id = mt.fair_id
              AND f.school_id = v_school_id
              AND f.pickup_date < v_delivery_date;--
         
         -- # 01-B:01
         IF v_previous_start_date1 IS NOT NULL THEN
         
               -- If it find the 1st previous fair_id 
               --   then insert otherwise don't insert
            INSERT INTO bkfair01.previous_fairs (
                        fair_id,
                        school_id,
                        previous_fair_id,
                        previous_fair_start_date )
               SELECT DISTINCT v_fair_id,
                      v_school_id,
                      f.id,
                      f.delivery_date
                  FROM bkfair01.fair   f, 
                       bkfair01.myteam mt
                  WHERE f.id = mt.fair_id
                    AND f.school_id = v_school_id
                    AND f.delivery_date = v_previous_start_date1;--
            
               -- Track the insertions...
            GET DIAGNOSTICS vi_insert_ct = ROW_COUNT;--
            SET voi_tot_insert_ct = ( voi_tot_insert_ct + vi_insert_ct );--
            COMMIT;--
            
               -- To get the 2nd Previous fair_id's detail 
               --   & it should exist on myteam table
            
            SELECT MAX (delivery_date)
              INTO v_previous_start_date2
              FROM bkfair01.fair   f, 
                   bkfair01.myteam mt
             WHERE f.id = mt.fair_id
               AND f.school_id = v_school_id
               AND f.pickup_date < v_previous_start_date1;--
            
            -- # 01-B:01:01
            IF v_previous_start_date2 IS NOT NULL THEN
            
                  -- If it find the 2nd previous fair_id 
                  --    then insert otherwise don't insert
               INSERT INTO bkfair01.previous_fairs ( 
                           fair_id,
                           school_id,
                           previous_fair_id,
                           previous_fair_start_date )
                  SELECT DISTINCT v_fair_id,
                         v_school_id,
                         f.id,
                         f.delivery_date
                     FROM bkfair01.fair f, 
                          bkfair01.myteam mt
                     WHERE f.id = mt.fair_id
                       AND f.school_id = v_school_id
                       AND f.delivery_date = v_previous_start_date2;--
               
                  -- Track the insertions...
               GET DIAGNOSTICS vi_insert_ct = ROW_COUNT;--
               SET voi_tot_insert_ct = ( voi_tot_insert_ct + vi_insert_ct );--
               COMMIT;--
               
                  -- To get the 3rd Previous fair_id
               SELECT MAX (delivery_date)
                 INTO v_previous_start_date3
                 FROM bkfair01.fair f, 
                      bkfair01.myteam mt
                 WHERE f.id = mt.fair_id
                   AND f.school_id = v_school_id
                   AND f.pickup_date < v_previous_start_date2;--
               
               -- # 01-B:01:01:01
               IF v_previous_start_date3 IS NOT NULL THEN
               
                     -- If it finds the 3rd previous fair_id 
                     --   then insert otherwise don't insert
                  INSERT INTO bkfair01.previous_fairs (
                              fair_id,
                              school_id,
                              previous_fair_id,
                              previous_fair_start_date)
                     SELECT DISTINCT v_fair_id,
                            v_school_id,
                            f.id,
                            f.delivery_date
                       FROM bkfair01.fair   f, 
                            bkfair01.myteam mt
                       WHERE f.id = mt.fair_id
                         AND f.school_id = v_school_id
                         AND f.delivery_date = v_previous_start_date3;--
                  
                     -- Track the insertions...
                  GET DIAGNOSTICS vi_insert_ct = ROW_COUNT;--
                  SET voi_tot_insert_ct = ( voi_tot_insert_ct + vi_insert_ct );--
                  COMMIT;--
                  
               END IF;   -- # 01-B:01:01:01
            END IF;   -- # 01-B:01:01
         END IF;   -- # 01-B:01
   
         FETCH FROM c_book_fair
           INTO v_fair_id, 
                v_school_id, 
                v_delivery_date;--
      END WHILE;--
   
      CLOSE c_book_fair;--
      COMMIT;--
      
         -- Provide Feedback at the End of the Job...
      SET poi_del_count = voi_tot_delete_ct;--
      SET poi_ins_count = voi_tot_insert_ct;--
      SET poi_rec_count = voi_tot_record_ct;--
   END IF;   -- # 01:A-B
END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_PREVIOUS_FAIRS"(VARCHAR(),INTEGER,INTEGER,INTEGER,CHAR(),INTEGER) IS 'Release 24.5 in Spring 2014: Populates PREVIOUS_FAIRS using the daily data set provided by CIS and reduce unnecessary large data shuffling.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE PROCEDURE bkfair01.sp_qc1981_refresh_task_descriptions (
                 OUT poc_sqlstate  CHAR(5), 
                 OUT poi_sqlcode   INTEGER )
   SPECIFIC sp_qc1981_refresh_task_descriptions
   LANGUAGE SQL
   NOT DETERMINISTIC
   EXTERNAL ACTION
   MODIFIES SQL DATA
   CALLED ON NULL INPUT
   INHERIT SPECIAL REGISTERS
   OLD SAVEPOINT LEVEL
BEGIN
      -- History: Designed temporarily to address QC # 1981     Spring 2014
      --          Updates column "description" from table "fair_planner_events"
      --          using the "description" using respectively:
      --             - planner_program_tasks.id
      --             - planner_tasks.id
      --          As a reminder, "id" is created from the same sequence making
      --          each table ID mutually exclusive.
   
      -- Variables declaration for Fair table
   DECLARE v_task_id                 INTEGER;--
   DECLARE v_description             VARCHAR(460);--
   DECLARE vi_update_ct              INTEGER DEFAULT 0;--

   DECLARE sqlstate                  CHAR (5) DEFAULT '00000';--
   DECLARE sqlcode                   INTEGER DEFAULT 0;--
   
   -- Cursor Declarations...
   -- # 01: Cursor to get planner_program_tasks.IDs...
   DECLARE c_planner_program_task CURSOR WITH HOLD FOR
      SELECT id, description
         FROM bkfair01.planner_program_tasks;--
   
   -- # 02: Cursor to get planner_tasks.IDs...
   DECLARE c_planner_task CURSOR WITH HOLD FOR
      SELECT id, description
         FROM bkfair01.planner_tasks;--
   
   -- Handlers
   DECLARE EXIT HANDLER FOR SQLEXCEPTION
      SELECT sqlstate, sqlcode
         INTO poc_sqlstate, poi_sqlcode
         FROM sysibm.sysdummy1;--
      
      -- Initialization...
   VALUES (sqlstate, sqlcode)
         INTO poc_sqlstate, poi_sqlcode;--
   
      -- -------------------------------------------------------------
      -- # 01: Set "Fair_planner_event.description" using as many
      --       Update-batches as we have "Planner_program_tasks.ID"s
      --       to limit the size of each update statement.
      -- -------------------------------------------------------------
   
   OPEN c_planner_program_task;--
   FETCH FROM c_planner_program_task
     INTO v_task_id, 
          v_description;--

   WHILE (sqlstate = '00000' ) DO
      UPDATE bkfair01.fair_planner_events
         SET description = v_description
         WHERE task_id = v_task_id;--
         
         -- Track the updates...
      GET DIAGNOSTICS vi_update_ct = ROW_COUNT;--
         -- Log the update...
      INSERT INTO bkfair01.fair_load_feedback
         ( create_date, 
           table_name, 
           update_count, 
           procedure_name )
         VALUES ( CURRENT TIMESTAMP, 
                  'bkfair01.fair_planner_events', 
                  vi_update_ct, 
                  'sp_qc1981_refresh...PROGRAM_TASK:Task_id: ' || TRIM(CHAR(v_task_id)) );--
      COMMIT;--
      
         -- Reset Variables...
      SET v_task_id = NULL;--
      SET v_description = NULL;--
      SET vi_update_ct = 0;--
      
      FETCH FROM c_planner_program_task
            INTO v_task_id, 
                 v_description;--
   END WHILE;--
   
   CLOSE c_planner_program_task;--
   COMMIT;--
   
      -- Reset Variables...
   SET v_task_id = NULL;--
   SET v_description = NULL;--
   SET vi_update_ct = 0;--
   VALUES (sqlstate, sqlcode)
         INTO poc_sqlstate, poi_sqlcode;--
   
   
      -- -------------------------------------------------------------
      -- # 02: Set "Fair_planner_event.description" using as many
      --       Update-batches as we have "Planner_tasks.ID"s
      --       to limit the size of each update statement.
      -- -------------------------------------------------------------
   
   OPEN c_planner_task;--
   FETCH FROM c_planner_task
     INTO v_task_id, 
          v_description;--

   WHILE (sqlstate = '00000' ) DO
      UPDATE bkfair01.fair_planner_events
         SET description = v_description
         WHERE task_id = v_task_id;--
         
         -- Track the updates...
      GET DIAGNOSTICS vi_update_ct = ROW_COUNT;--
         -- Log the update...
      INSERT INTO bkfair01.fair_load_feedback
         ( create_date, 
           table_name, 
           update_count, 
           procedure_name )
         VALUES ( CURRENT TIMESTAMP, 
                  'bkfair01.fair_planner_events', 
                  vi_update_ct, 
                  'sp_qc1981_refresh...TASK:Task_id: ' || TRIM(CHAR(v_task_id)) );--
      COMMIT;--
      
         -- Reset Variables...
      SET v_task_id = NULL;--
      SET v_description = NULL;--
      SET vi_update_ct = 0;--
      
      FETCH FROM c_planner_task
            INTO v_task_id, 
                 v_description;--
   END WHILE;--
   
   CLOSE c_planner_task;--
   COMMIT;--
   
END;

COMMENT ON PROCEDURE "BKFAIR01"."SP_QC1981_REFRESH_TASK_DESCRIPTIONS"(CHAR(),INTEGER) IS 'Release 24.5 in Spring 2014: Update all FAIR_PLANNER_EVENTS.DESCRIPTION per the request from QC # 1981 with the data from Planner Admin tables PLANNER_PROGRAM_TASKS/PLANNER_TASKS.DESCRIPTION.';

-------------------------------
-- DDL Statements for Triggers
-------------------------------

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE TRIGGER BKFAIR01.TR_FAIR_AIR
  AFTER INSERT
  ON "BKFAIR01"."FAIR"
  REFERENCING 
    NEW AS N
  FOR EACH ROW
BEGIN ATOMIC
      INSERT INTO bkfair01.new_fairs ( id ) VALUES ( N.id ) ;--
   END;

COMMENT ON TRIGGER "BKFAIR01"."TR_FAIR_AIR" IS 'Bkfair release 19, Oct. 2011; This trigger creates a record into the table NEW_FAIRS AFTER a new FAIR record is created via the data load script bkfair_load.sh';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","BKFAIR01";
CREATE TRIGGER BKFAIR01.TR_FAIR_CPLIST_BUR 
  NO CASCADE BEFORE 
  UPDATE 
  ON BKFAIR01.FAIR_CPLIST 
  REFERENCING 
    OLD AS O 
    NEW AS N 
  FOR EACH ROW 
  MODE DB2SQL 
  BEGIN ATOMIC 
  IF N.INFO_STATUS = 'Accepted' THEN 
     SET N.info_status_date = CURRENT TIMESTAMP; --
  END IF; --
END;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE TRIGGER bkfair01.tr_fairdate_login_tracking_air
  AFTER INSERT
  ON bkfair01.fairdate_login_tracking
  REFERENCING 
    NEW AS N
  FOR EACH ROW
BEGIN ATOMIC
      DECLARE v_new_fair_id_count     SMALLINT;--
      
      SET v_new_fair_id_count = ( SELECT COUNT(*) 
                                     FROM bkfair01.new_fairs 
                                     WHERE id = N.fair_id );--
      
      IF ( v_new_fair_id_count = 1 ) THEN 
         INSERT INTO bkfair01.online_homepage (
                     fair_id,                    contact_person_ckbox,        contact_email_ckbox, 
                     contact_phone_number_ckbox, payment_check_ckbox,         payment_cc_ckbox, 
                     payment_cash_ckbox,         volunteer_recruitment_ckbox, book_fair_goal_ckbox, 
                     special_program_ckbox,      special_program_ckbox2,      special_program_ckbox3, 
                     special_program_ckbox4,     ofe_feature_ckbox,           create_date, 
                     update_date,                selected_goal,               fair_start_date, 
                     fair_end_date,              goal_amount,                 actual_amount, 
                     school_state,               school_postalcode,           fair_location, 
                     contact_phone_number,
                     school_city,                contact_first_name,          contact_last_name, 
                     school_name,                contact_email,               school_address1 )
              SELECT fair_id,                    contact_person_ckbox,        contact_email_ckbox, 
                     contact_phone_number_ckbox, payment_check_ckbox,         payment_cc_ckbox, 
                     payment_cash_ckbox,         volunteer_recruitment_ckbox, book_fair_goal_ckbox, 
                     special_program_ckbox,      special_program_ckbox2,      special_program_ckbox3, 
                     special_program_ckbox4,     ofe_feature_ckbox,           create_date, 
                     update_date,                selected_goal,               fair_start_date, 
                     fair_end_date,              goal_amount,                 actual_amount, 
                     school_state,               school_postalcode,           fair_location, 
                     contact_phone_number,
                     school_city,                contact_first_name,          contact_last_name, 
                     school_name,                contact_email,               school_address1 
              FROM bkfair01.v_new_published_online_homepage_record
              WHERE fair_id = N.fair_id;--
         INSERT INTO unpublished.online_homepage (
                     fair_id,                    contact_person_ckbox,        contact_email_ckbox, 
                     contact_phone_number_ckbox, payment_check_ckbox,         payment_cc_ckbox, 
                     payment_cash_ckbox,         volunteer_recruitment_ckbox, book_fair_goal_ckbox, 
                     special_program_ckbox,      special_program_ckbox2,      special_program_ckbox3, 
                     special_program_ckbox4,     ofe_feature_ckbox,           create_date, 
                     update_date,                selected_goal,               fair_start_date, 
                     fair_end_date,              goal_amount,                 actual_amount, 
                     school_state,               school_postalcode,           fair_location, 
                     contact_phone_number,
                     school_city,                contact_first_name,          contact_last_name, 
                     school_name,                contact_email,               school_address1 )
              SELECT fair_id,                    contact_person_ckbox,        contact_email_ckbox, 
                     contact_phone_number_ckbox, payment_check_ckbox,         payment_cc_ckbox, 
                     payment_cash_ckbox,         volunteer_recruitment_ckbox, book_fair_goal_ckbox, 
                     special_program_ckbox,      special_program_ckbox2,      special_program_ckbox3, 
                     special_program_ckbox4,     ofe_feature_ckbox,           create_date, 
                     update_date,                selected_goal,               fair_start_date, 
                     fair_end_date,              goal_amount,                 actual_amount, 
                     school_state,               school_postalcode,           fair_location, 
                     contact_phone_number,
                     school_city,                contact_first_name,          contact_last_name, 
                     school_name,                contact_email,               school_address1 
              FROM bkfair01.v_new_published_online_homepage_record
              WHERE fair_id = N.fair_id;--
      END IF;--
  END;

COMMENT ON TRIGGER "BKFAIR01"."TR_FAIRDATE_LOGIN_TRACKING_AIR" IS 'Bkfair release 19, Oct. 2011; This AFTER trigger creates a record into FAIRDATE_LOGIN_TRACKING upon the first login action of a chairperson for a given fair. In effect the fair is automatically PUBLISHED.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE TRIGGER bkfair01.tr_fairdate_login_tracking_air2
  AFTER INSERT
  ON bkfair01.fairdate_login_tracking
  REFERENCING 
    NEW AS N
  FOR EACH ROW
BEGIN ATOMIC
      DECLARE v_new_fair_id_count       SMALLINT;--
      DECLARE v_fair_id                 BIGINT;--
      DECLARE v_fair_start_date         DATE;--
      DECLARE v_fair_end_date           DATE;--
      DECLARE v_schedule_date           DATE;--
      DECLARE v_ofe_start_date          DATE;--
      DECLARE v_ofe_end_date            DATE;--
      DECLARE v_ofe_status              VARCHAR(25);--
      DECLARE v_ofe_ok                  CHAR(1) DEFAULT 'N';--
      
         -- Get the fair_id if it still exists...
      SET v_new_fair_id_count = ( SELECT COUNT(*) 
                                  FROM bkfair01.new_fairs 
                                  WHERE id = N.fair_id );--

         -- Check if the chairperson logs for the first time...
      IF ( v_new_fair_id_count = 1 ) THEN    -- # 01-A
            
            -- ------------------------------------------------------------
            -- SECTION #1 ... HOMEPAGE_SCHEDULE record creation automation:
            -- ------------------------------------------------------------
            
            -- get the fair physical dates...
         SET ( v_fair_id,
               v_fair_start_date,
               v_fair_end_date ) = ( SELECT f.id,
                                           f.delivery_date,
                                           f.pickup_date
                                       FROM bkfair01.fair  f
                                       WHERE f.id = N.fair_id ); --
         
              -- Set the first schedule date...
         SET v_schedule_date = v_fair_start_date;--
         
              -- Loop through each day of the physical fair and 
              --    create as many records in both schema tables
              --    bkfair01/unpublished.homepage_schedule...
              
         WHILE ( v_schedule_date <= v_fair_end_date ) DO
               -- First in schema BKFAIR01...
            INSERT INTO bkfair01.homepage_schedule
                      ( fair_id, schedule_date, schedule_date_ckbox,
                        create_date, update_date )
                   VALUES ( v_fair_id, v_schedule_date, 'Y', 
                            CURRENT TIMESTAMP, CURRENT TIMESTAMP );--
           
               -- Second in schema UNPUBLISHED...
            INSERT INTO unpublished.homepage_schedule
                      ( fair_id, schedule_date, schedule_date_ckbox,
                        create_date, update_date )
                   VALUES ( v_fair_id, v_schedule_date, 'Y', 
                            CURRENT TIMESTAMP, CURRENT TIMESTAMP );--
                  -- Get the following day...
            SET v_schedule_date = ( v_schedule_date + 1 DAY );--
         END WHILE;--
            -- Now we can delete the new_fairs record as the full
            --   loop of the homepage publishing automation is accomplished.

            -- Time to acknowledge that the fair got published based on first login into it...
         DELETE FROM bkfair01.new_fairs WHERE id = N.fair_id;--
      END IF;     -- # 01-A
   END;

COMMENT ON TRIGGER "BKFAIR01"."TR_FAIRDATE_LOGIN_TRACKING_AIR2" IS 'Trigger creating as many records into HOMEPAGE_SCHEDULE as their are days contained within a physical fair. IMPORTANT: this trigger must absolutely be created AFTER the trigger TR_FAIRDATE_LOGIN_TRACKING_AIR+ONLINE_SHOPPING record-creation automation';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE TRIGGER bkfair01.tr_fairplanworkshop_bur
   NO CASCADE BEFORE UPDATE
   ON bkfair01.fair_planner_workshops
   REFERENCING 
    OLD AS O
    NEW AS N
   FOR EACH ROW
   BEGIN ATOMIC
      SET N.update_date = CURRENT TIMESTAMP;--
END;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","BKFAIR01";
CREATE TRIGGER BKFAIR01.TR_FINANCIAL_SECT 
  NO CASCADE BEFORE 
  UPDATE 
  ON BKFAIR01.FINANCIAL_SECTION_STATUS 
  REFERENCING 
    OLD AS O 
    NEW AS N 
  FOR EACH ROW 
  MODE DB2SQL 
  BEGIN ATOMIC 
  SET N.update_date = CURRENT TIMESTAMP; --
END;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE TRIGGER bkfair01.tr_homepage_url_bur
  NO CASCADE BEFORE UPDATE
  ON bkfair01.homepage_url
  REFERENCING 
    OLD AS O
    NEW AS N
  FOR EACH ROW
  BEGIN ATOMIC
    SET N.update_date = CURRENT TIMESTAMP;--
  END;

COMMENT ON TRIGGER "BKFAIR01"."TR_HOMEPAGE_URL_BUR" IS 'Bkfair release 19, Oct. 2011; This BEFORE UPDATE trigger updates HOMEPAGE_URL.UPDATE_DATE.';

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE TRIGGER bkfair01.tr_online_homepage_air
  AFTER INSERT
  ON bkfair01.online_homepage
  REFERENCING 
    NEW AS N
  FOR EACH ROW
BEGIN ATOMIC
      DECLARE v_published_status       VARCHAR(25) DEFAULT NULL;--
      DECLARE v_url_status             VARCHAR(25) DEFAULT NULL;--
      DECLARE v_web_url                VARCHAR(35);--
      DECLARE v_school_id              BIGINT;--
      DECLARE v_school_root_url        VARCHAR(200);--
      DECLARE v_final_web_url          VARCHAR(35);--
      DECLARE v_url_found_count        INTEGER;--
      DECLARE v_url_found_count2       INTEGER;--
      DECLARE v_url_seed               INTEGER DEFAULT 0;--
      DECLARE v_notavailable_ct        INTEGER DEFAULT 0;--
      DECLARE v_available_ct           INTEGER DEFAULT 0;--
      DECLARE v_fair_finder_ckbox      CHAR(1) DEFAULT 'Y';--
      DECLARE v_product_id             VARCHAR(2);--
      DECLARE v_latitude_longitude     VARCHAR(50);--
      
         -- Create the root for the new URL...
         -- About 10 special characters found in the school name
         -- are being filtered out and removed.
         
      SET ( v_school_id,
            v_product_id,
            v_school_root_url,
            v_latitude_longitude ) = ( SELECT t1.school_id, 
                                              t1.product_id,
                                              LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE
                                                   (REPLACE(REPLACE(REPLACE(REPLACE(REPLACE 
                                                   (REPLACE(REPLACE
                                                   (t2.name, ' ', '' ), '&', ''), 
                                                     ',', ''), '*', ''), '/', '' ),
                                                     '@', '' ), '.', '' ), '(', '' ),
                                                     ')', '' ), '-', '' ), '#', '' ), '''', '') ), 
                                              t3.str_latlon 
                                FROM bkfair01.fair    t1
                                     JOIN bkfair01.school  t2
                                       ON t1.school_id = t2.id 
                                     LEFT OUTER JOIN bkfair01.lu_zipcodes t3
                                       ON t2.postalcode = t3.zipcode
                                WHERE t1.id = N.fair_id ); --
      
         -- Reduce the length of the new URL if it's greater than 30
      IF ( LENGTH( v_school_root_url ) > 30 ) THEN
         SET v_school_root_url = SUBSTR( v_school_root_url, 1, 30 );--
      END IF;--
               
      
      SET ( v_published_status,
            v_url_status, 
            v_web_url ) = ( SELECT published_status, url_status, web_url 
                            FROM bkfair01.homepage_url 
                            WHERE fair_id IN 
                               ( SELECT MAX(fair_id) 
                                 FROM bkfair01.homepage_url
                                 WHERE school_id = v_school_id
                                   AND url_status IN ( 'NOTAVAILABLE', 'AVAILABLE' )
                                   AND web_url IS NOT NULL ) ); --
           
           -- Evaluate the different scenarios involving combinations
           -- of values for both published_status, url_status, and the
           -- existence/absence of web_url...
            
      IF ( v_url_status = 'NOTAVAILABLE' ) THEN            -- IF # 01-A or FIRST SCENARIO
               -- The first and most recent record would always be  
               -- and in a 'NOTAVAILABLE' state ( most current scenario ) 
               -- for the current school since the fair would be more recent than 'AVAILABLE' ones
               
               -- Mainly the case...
               -- But we need to further check if that URL is not being used...
            SET v_url_found_count = ( SELECT COUNT(*) 
                                      FROM bkfair01.homepage_url 
                                      WHERE web_url = v_web_url 
                                        AND url_status = 'RESERVED' ); --
            
            IF ( v_url_found_count = 0 ) THEN   -- IF # 05-A
                  -- This URL is not being activated by any fair.
                  -- We can then use it.
               SET v_final_web_url = v_web_url;--
               
            ELSE                                -- IF # 05-B
                  -- This URL was found to be used by a fair.
                  -- We then need to create a new one...
                  
                  -- This first check must ensure that the School_root_url is not
                  -- being used (RESERVED state) disrespecting any school_id...
               SET v_url_found_count = ( SELECT COUNT(*) 
                                         FROM bkfair01.homepage_url 
                                         WHERE web_url = v_school_root_url 
                                            AND url_status = 'RESERVED' ); --
               
               SET v_url_found_count2 = ( SELECT COUNT(*) 
                                          FROM bkfair01.homepage_url 
                                          WHERE school_id != v_school_id
                                            AND web_url = v_school_root_url 
                                            AND url_status = 'NOTAVAILABLE' ); --
                     
                  -- Evaluates if the original school root is used as URL...
               IF ( v_url_found_count = 0 ) AND 
                  ( v_url_found_count2 = 0 ) THEN   -- IF # 06-A
                  SET v_final_web_url = v_school_root_url;--
               
               ELSE                                -- IF # 06-B
                     -- The URL has unfortunately been used by another school.
                     -- We must then create a new one and check its validity in a loop.
                     -- For new school web site, we do use a first number of 1++ when 
                     -- the web site is found to be used.
                  WHILE ( v_url_found_count != 0 )  OR 
                        (  v_url_found_count2 != 0 ) DO
                     
                     SET v_url_seed = ( v_url_seed + 1 );--
                     SET v_final_web_url = TRIM(( v_school_root_url || CHAR(v_url_seed) ));--
                     SET v_url_found_count = ( SELECT COUNT(*) 
                                               FROM bkfair01.homepage_url 
                                               WHERE web_url = v_final_web_url 
                                                 AND url_status = 'RESERVED' ); --
                     
                     SET v_url_found_count2 = ( SELECT COUNT(*) 
                                                FROM bkfair01.homepage_url 
                                                WHERE school_id != v_school_id
                                                  AND web_url = v_final_web_url 
                                                  AND url_status = 'NOTAVAILABLE' ); --
                  END WHILE;--
               END IF;                             -- ENDIF # 06-A-B    
                  
                  
                  
            END IF;                             -- ENDIF # 05-A-B
         
         -- When the web_url has become AVAILABLE, 
         -- we must first check if it has been used by another school
         -- if it is not being used, then use it again, 
         -- else set a new URL and check for its existence by other schools...
         
      ELSEIF ( v_url_status = 'AVAILABLE' ) THEN      -- IF # 01-B or SECOND SCENARIO
      
            -- Before setting the newly created URL, we must still check if it's not being used.
            -- if it is, set a new URL and check for its usage as well 
            -- with the extra check of having a not null URL due to old data.
         
               -- case where web_url has a seemingly valid value.
               -- We still check if that WEB_URL is being used for any school...
               
         SET v_url_found_count = ( SELECT COUNT(*) FROM bkfair01.homepage_url 
                                   WHERE web_url = v_web_url 
                                     AND url_status IN ('RESERVED', 'NOTAVAILABLE') ); --
               
         IF ( v_url_found_count = 0 ) THEN      -- IF # 02-A
            
               -- This URL had not been re-used by another school
               -- we can therefore re-use it for the current school.
            SET v_final_web_url = v_web_url;--
            
         ELSE                                   -- IF # 02-B
               -- The URL has unfortunately been used by another school in between.
               -- We must then create a new one and check its validity in a loop 
               --   starting by using the SCHOOL NAME as the new URL root...
            
                  -- This first check must ensure that the School_root_url is not
                  -- being used (RESERVED state) disrespecting any school_id...
               SET v_url_found_count = ( SELECT COUNT(*) 
                                         FROM bkfair01.homepage_url 
                                         WHERE web_url = v_school_root_url 
                                            AND url_status = 'RESERVED' ); --
               
               SET v_url_found_count2 = ( SELECT COUNT(*) 
                                          FROM bkfair01.homepage_url 
                                          WHERE school_id != v_school_id
                                            AND web_url = v_school_root_url 
                                            AND url_status = 'NOTAVAILABLE' ); --
                     
                  -- Evaluates if the original school root is used as URL...
               IF ( v_url_found_count = 0 ) AND 
                  ( v_url_found_count2 = 0 ) THEN   -- IF # 03-A
                  
                  SET v_final_web_url = v_school_root_url;--
               
               ELSE                                -- IF # 03-B
                     -- The URL has unfortunately been used by another school.
                     -- We must then create a new one and check its validity in a loop.
                     -- For new school web site, we do use a first number of 1++ when 
                     -- the web site is found to be used.
                  WHILE ( v_url_found_count != 0 )  OR 
                        (  v_url_found_count2 != 0 ) DO
                     
                     SET v_url_seed = ( v_url_seed + 1 );--
                     SET v_final_web_url = TRIM(( v_school_root_url || CHAR(v_url_seed) ));--
                     SET v_url_found_count = ( SELECT COUNT(*) 
                                               FROM bkfair01.homepage_url 
                                               WHERE web_url = v_final_web_url 
                                                 AND url_status = 'RESERVED' ); --
                     
                     SET v_url_found_count2 = ( SELECT COUNT(*) 
                                                FROM bkfair01.homepage_url 
                                                WHERE school_id != v_school_id
                                                  AND web_url = v_final_web_url 
                                                  AND url_status = 'NOTAVAILABLE' ); --
                  END WHILE;--
               END IF;                             -- ENDIF # 03-A-B          
            END IF;                                -- ENDIF # 02-A-B
         
      ELSE                       -- IF # 01-C or THIRD SCENARIO (New School)
      -- This case targets the situation when a school is creating its website for the first time.
      -- Therefore no values were found at all in HOMEPAGE_URL.
      -- However we still need to check for possibility of the new URL to have been already used.
            
            -- 1- Find out if the abbreviated school name is being used as web_url...
         SET v_url_found_count = ( SELECT COUNT(*) 
                                   FROM bkfair01.homepage_url 
                                   WHERE web_url = v_school_root_url 
                                     AND url_status = 'RESERVED' );--
         
         SET v_url_found_count2 = ( SELECT COUNT(*) 
                                    FROM bkfair01.homepage_url 
                                    WHERE school_id != v_school_id
                                      AND web_url = v_school_root_url 
                                      AND url_status = 'NOTAVAILABLE' ); --
         
         IF ( v_url_found_count = 0  AND 
              v_url_found_count2 = 0 ) THEN            -- IF # 04-A
               
               -- This URL had not been used by another school
               -- we can therefore use it for the current school.
               -- And its length has already been checked.
            SET v_final_web_url = v_school_root_url;--
                  
         ELSE                                         -- IF # 04-B
               -- The URL has unfortunately been used by another school.
               -- We must then create a new one and check its validity in a loop.
               -- For new school web site, we do use a first number of 1++ when 
               -- the web site is found to be used.
            
            WHILE (( v_url_found_count != 0 ) OR 
                  ( v_url_found_count2 != 0 ) ) DO
               
               SET v_url_seed = ( v_url_seed + 1 );--
               SET v_final_web_url = TRIM(( v_school_root_url || CHAR(v_url_seed) ));--
               SET v_url_found_count = ( SELECT COUNT(*) 
                                         FROM bkfair01.homepage_url 
                                         WHERE web_url = v_final_web_url 
                                           AND url_status = 'RESERVED'  ); --
               
               SET v_url_found_count2 = ( SELECT COUNT(*) 
                                          FROM bkfair01.homepage_url 
                                          WHERE school_id != v_school_id
                                            AND web_url = v_final_web_url 
                                            AND url_status = 'NOTAVAILABLE' ); --
            END WHILE;--
         END IF;                                      -- ENDIF # 04-A-B
      END IF;                                         -- IF # 01-A-B-C
 
         -- When creating a Test Fair, set the fair_finder_ckbox to N...
      
      IF ( v_product_id IN ( 'TT', 'TM', 'BE', 'BM', 'BP', 
                             'TB', 'BB', 'ME', 'MM', 'MB' )) THEN
           SET v_fair_finder_ckbox = 'N';--
      END IF;--
      
         -- Finally, create the HOMEPAGE_URL record...
      INSERT INTO bkfair01.homepage_url ( 
                  fair_id, 
                  school_id, 
                  fair_finder_ckbox,
                  published_date, 
                  published_status,
                  url_status, 
                  web_url,
                  str_latlon ) 
         VALUES ( N.fair_id, 
                  v_school_id, 
                  v_fair_finder_ckbox,
                  CURRENT TIMESTAMP,
                  'PUBLISHED',
                  'RESERVED',
                  v_final_web_url,
                  v_latitude_longitude );--
   END;

COMMENT ON TRIGGER "BKFAIR01"."TR_ONLINE_HOMEPAGE_AIR" IS 'Release 19-9; This trigger creates a record into the table HOMEPAGE_URL AFTER a new ONLINE_HOMEPAGE record is created. In effect, it creates the URL page access for a given fair; adds latlon data; Release 23: Adjust URL to just 30 char in length.';








