   ####################################################################################
   # Author.....: SAMPATH MURUGESAN
   # Name.......: auto_Maint_window.sh
   # Description:
   # Created....: 2014-06-11
   # Updated....:
   # Updated....:
   # Notes......: This Script deals with changing of Automatic Maintenance window
   # Usage......: sh [script] [START_TIME:ex 01:00:00] [DURATION:ex 02]
   # ...........:
   ####################################################################################

   #set -x

   #export PATH="/usr/local/bin:/usr/bin:/bin"
   #
   #. /home/cpt01d/sqllib/db2profile

    LOGPATH="/backups/logs"



    ChkDB2Fail()
    {
    #
    if [ $1 -gt 0 ]
    then
      echo "---------------------------------------------------- " | tee -a $LOGPATH/automaint_set_policyfile.log
        echo "ERROR:  db2 command Failed.  Exiting Program "  | tee -a $LOG
	  echo "---------------------------------------------------- " | tee -a $LOGPATH/automaint_set_policyfile.log
	    exit 5
	     fi
	     }
	     #--------------------------------------------------------------------------

	     TIME=$HOME/sqllib/tmp/STIME.sql
	     TIME2=$HOME/sqllib/tmp/STIME2.sql

	     DATABASE=$1
	     START_TIME=$2
	     DURATION=$3
	     HPATH=$HOME/sqllib/tmp/
	     #NPATH=/home/cpt01d/sqllib/tmp/BKF01D

	     if [ $# -eq 3 ]; then
	        echo ""
		 else
		   echo "Usage: "
		     echo "$0 [DATBASE] [START_TIME:ex 01:00:00] [DURATION:ex 02] "
		       exit 1

		       fi

		       db2 connect to $1

		       cd $HPATH

		       db2 "call sysproc.automaint_get_policyfile('MAINTENANCE_WINDOW','MaintenanceWindow.xml');" > $LOGPATH/automaint_get_policyfile.log
		       db2 commit
		       db2 "call sysproc.automaint_get_policyfile('AUTO_RUNSTATS','Runstats.xml');" >> $LOGPATH/automaint_get_policyfile.log
		       db2 commit
		       db2 "call sysproc.automaint_get_policyfile('AUTO_REORG','Reorg.xml');" >> $LOGPATH/automaint_get_policyfile.log
		       db2 commit


		       #echo  "1,6d"  > sed.opt
		       #echo  "s/startTime=/startTime="$START_TIME"/g"  > sed.opt
		       #echo  "/startTime/i\\"  > sed.opt
		       #echo  "startTime='$START_TIME'" >> sed.opt
		       #echo  "s/duration=/duration="$DURATION"/g"  >> sed.opt
		       #echo  "/duration/i\\" >> sed.opt
		       #echo  "duration='$DURATION'" >> sed.opt


		       #sed -f sed.opt MaintenanceWindow.xml > MaintenanceWindow_new.xml
		       sed -n '6p' MaintenanceWindow.xml | cut -c 47-54 > $TIME
		       sed -n '6p' MaintenanceWindow.xml | cut -c 67-68 > $TIME2

		       T1=`cat $TIME`
		       T2=`cat $TIME2`
		       echo  "s/$T1/$START_TIME/g"  > $LOGPATH/sed.opt
		       echo  "s/$T2/$DURATION/g"  >> $LOGPATH/sed.opt

		       sed -f $LOGPATH/sed.opt MaintenanceWindow.xml > MaintenanceWindow_new.xml


		       chmod 775 MaintenanceWindow_new.xml
		      # chmod 775 MaintenanceWindow.xml

		       db2 "call sysproc.automaint_set_policyfile('MAINTENANCE_WINDOW','MaintenanceWindow_new.xml')" > $LOGPATH/automaint_set_policyfile.log
		       cat $LOGPATH/automaint_set_policyfile.log

		       echo "Check db2diag.log to check the changes have taken effect"
                       tail -10 $HOME/sqllib/db2dump/db2diag.log

