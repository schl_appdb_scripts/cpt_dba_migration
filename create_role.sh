#DB="BKF01D"


if [ $# -ne 1 ]; then
  echo "Script usage: sh [script] [DATABASE]"
    exit 1
    fi


#db2 connect to $DB
db2 connect to $1


db2 -x "create role  app_usr"
db2 -x " create role  dev_read"     
db2 -x " create role  dev_write"     
db2 -x "create role  qa_read"      
db2 -x "create role  perf_usr"    
db2 -x "create role  prod_read"    
db2 -x "create role  endc_usr"    
db2 -x "create role  sec_role"    


