#!/bin/sh 
##############
# Author.....: Renford McDonald
# Name.......: create.database.sh
# Description:
# Created....: 2011-06-05
# Updated....: 2011-08-03 by Renford McDonald
# Updated....: 2012-03-06 by Renford McDonald - Enhancements
# Notes......:
# ...........:
##############
# Set some defaults
script_dir=$(dirname ${0}) # default path for scripts
[[ -z ${ostype} ]] && ostype=$( uname | tr a-z A-Z )
[[ ${ostype} == 'LINUX' ]] && echo_option=' -e ' || echo_option=''

#########################
# Adjustable variables: Change to suit the individual environment (db2d, db2t, db2p...)
#########################

#########################

helpUsage() {
   echo "Usage: $0 -h |  -d <Database> "
}

envCheck(){
   if [[ -z ${DB2INSTANCE} ]]; then
      echo "DB2 database environment/instance not loaded!"
      helpUsage
      exit 1
   fi
}

envCheck              # Check DB2 environment

OPTIND=1
while getopts "d:h" option
do
   case $option in
      h) helpUsage $0
         exit 1 ;;
      d) database=${OPTARG};;
      *) echo "Unexpected input, try again!"
         exit 1;;
   esac
   # echo "Option ${option}: ${OPTARG}"
done

if [[ -z ${database} ]]; then
   helpUsage
   exit 1
fi
database=$( echo ${database} | tr A-Z a-z )
DATABASE=$( echo ${database} | tr a-z A-Z )

tablespaces='tablespaces.lst'
tablespaces="${script_dir}/${DB2INSTANCE}/${database}/tablespaces.lst"
if [[ ! -f  ${tablespaces} ]]; then
   echo "Tablespace configuration file '${tablespaces}' not found!"
   exit 1
fi

# set the database storage path base directory
if [[ ${ostype} == 'AIX' ]] && [[ ${DB2INSTANCE} == 'db2p' ]]; then # MAXP Production
   db2data="/db01/${DB2INSTANCE}/${DB2INSTANCE}"
   db2home="/db01/${DB2INSTANCE}"
elif [[ ${ostype} == 'AIX' ]]; then # MAXD, MAXT etc
   db2data="/db02/${DB2INSTANCE}/${DB2INSTANCE}"
   db2home="/db02/${DB2INSTANCE}"
else
   db2data="/db2data/${DB2INSTANCE}"
   db2home="/db2home/${DB2INSTANCE}"
fi

# Create storage path directories; if more paths are needed, add more number to the 'for' loop below
#for seq_val in 01 02 03 04 05 06
for seq_val in 1 2 3 4
do
   if [[ -z ${onPath} ]]; then
      #onPath="${db2data}/data${seq_val}"
      onPath="/db2data${seq_val}/${DB2INSTANCE}"
   else
      #onPath="${onPath},${db2data}/data${seq_val}"
      onPath="${onPath},/db2data${seq_val}/${DB2INSTANCE}"
   fi
done
#echo ${onPath}
#onPath="/db2data1/${DB2INSTANCE}"
#onPath="/db2data${seq_val}/${DB2INSTANCE}"
#echo $(echo ${onPath} | sed 's/,/ /g')
#exit

# Remove the commas and create the required directories for the container storage paths
mkdir -p $(echo ${onPath} | sed 's/,/ /g')
mkdir -p ${db2home}
#echo $(echo ${onPath} | sed 's/,/ /g')
#exit 0
# Verify that all the required directories were created and is accessible
dbDirList="${db2home} $(echo ${onPath} | sed 's/,/ /g')"
for dbDir in ${dbDirList}
do
   if [[ ! -d ${dbDir} ]]; then
      echo "Directory '${dbDir}' does not exist, check permission!"
      exit 1
   fi
done
unset dbDirList dbDir

#db2clp_cmd=$( db2 -vx "CREATE DATABASE ${DATABASE} AUTOMATIC STORAGE YES  ON ${onPath} DBPATH ON ${db2home} USING CODESET UTF-8 TERRITORY US COLLATE USING SYSTEM_819_US PAGESIZE 4096 RESTRICTIVE" )
db2clp_cmd=$( db2 -vx "CREATE DATABASE ${DATABASE} AUTOMATIC STORAGE YES  ON ${onPath} DBPATH ON ${db2home} USING CODESET UTF-8 TERRITORY US COLLATE USING SYSTEM_819_US PAGESIZE 4096 DFT_EXTENT_SZ 16" )
exitCode=$?
sql_code=${db2clp_cmd%% *}     # Get the SQL code from the message by removing text to the right of SQL####N
echo "$(date '+%Y-%m-%d-%H.%M.%S'): ${db2clp_cmd}"
if [[ ${exitCode} -ne 0 ]]; then
   echo "$(date '+%Y-%m-%d-%H.%M.%S'): ${exitCode} Database '${DATABASE}' creation failed!."
   # exit ${exitCode}
fi

db2 -x "CONNECT TO ${DATABASE}"
#db2 -x "update db cfg for ${DATABASE} using DECFLT_ROUNDING ROUND_HALF_UP SELF_TUNING_MEM ON DATABASE_MEMORY 131072 immediate"
db2 -x "update db cfg for ${DATABASE} using DECFLT_ROUNDING ROUND_HALF_UP SELF_TUNING_MEM ON DATABASE_MEMORY 98304 immediate"
db2 -x "update db cfg for ${DATABASE} using LOCKTIMEOUT 55 LOGSECOND 16 LOGFILSIZ 1024 immediate"

db2 -x "AUTOCONFIGURE USING mem_percent 2 Workload_type mixed num_stmts 32 admin_priority both num_local_apps 32 num_remote_apps 64 isolation CS APPLY DB ONLY"

db2 -x "CONNECT RESET"
db2 -x "CONNECT TO ${DATABASE}"
db2 -x "CREATE BUFFERPOOL BP4K01D      SIZE  2048 PAGESIZE  4 K"
db2 -x "CREATE BUFFERPOOL BP4K01I      SIZE  2048 PAGESIZE  4 K"
db2 -x "CREATE BUFFERPOOL BP8K01D      SIZE  4096 PAGESIZE  8 K"
db2 -x "CREATE BUFFERPOOL BP32K01D     SIZE  2048 PAGESIZE 32 K"
db2 -x "CREATE BUFFERPOOL STEMP4K01D   SIZE   256 PAGESIZE  4 K"
db2 -x "CREATE BUFFERPOOL STEMP8K01D   SIZE   256 PAGESIZE  8 K"
db2 -x "CREATE BUFFERPOOL STEMP32K01D  SIZE   256 PAGESIZE 32 K"
db2 -x "ALTER  BUFFERPOOL IBMDEFAULTBP IMMEDIATE SIZE 2048"

# Permission error; fenced user needs to be a member of the sysadm group 
# Looks like AUTOCONFIGURE maybe calling a stored procedure internally (needs more research)
#db2 -x "AUTOCONFIGURE USING
#mem_percent 2
#Workload_type mixed
#num_stmts 25
#admin_priority both
#num_local_apps 128
#num_remote_apps 128
#isolation CS
#APPLY DB AND DBM"

db2 -x "CONNECT RESET"
db2 -x "TERMINATE"

db2 -x "CONNECT TO ${DATABASE}"

db2 -x "DROP TABLESPACE USERSPACE1"
for tablespace in $( egrep -v '^$|^ .|^#' ${tablespaces} )
do
   if [[ -z ${tablespace} ]]; then
      continue
   fi
   if [[ ${tablespace} == 'TOOLS' ]]; then
      # Only create a single table space for the tools catalog
      db2 -x "CREATE       TABLESPACE ${tablespace}32K01D  PAGESIZE 32K MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL BP32K01D"
      db2 -x "ALTER TABLESPACE ${tablespace}32K01D NO FILE SYSTEM CACHING DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE 32M"
      db2 -x "COMMENT ON TABLESPACE ${tablespace}32K01D IS 'DB2 Tools Catalog - Task Scheduler'"
      continue
   fi
   if [[ ${tablespace} == 'SDBA' ]]; then
      db2 -x "CREATE TABLESPACE ${tablespace}4K1  PAGESIZE  4K MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL BP4K01D"
      db2 -x "CREATE TABLESPACE ${tablespace}32K1 PAGESIZE 32K MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL BP32K01D"
      db2 -x "ALTER  TABLESPACE ${tablespace}4K1  NO FILE SYSTEM CACHING DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE 32M"
      db2 -x "ALTER  TABLESPACE ${tablespace}32K1 NO FILE SYSTEM CACHING DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE 32M"
      continue
   fi

   db2 -x "CREATE TABLESPACE ${tablespace}4K01D  PAGESIZE  4K MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL BP4K01D"
   db2 -x "CREATE TABLESPACE ${tablespace}4K01I  PAGESIZE  4K MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL BP4K01I"
   db2 -x "CREATE LARGE TABLESPACE "BKF4K02I" IN DATABASE PARTITION GROUP IBMDEFAULTGROUP          PAGESIZE 4096 MANAGED BY AUTOMATIC STORAGE          USING STOGROUP "IBMSTOGROUP"          AUTORESIZE YES          INITIALSIZE 32 M          INCREASESIZE 32 M          MAXSIZE NONE          EXTENTSIZE 32          PREFETCHSIZE AUTOMATIC          BUFFERPOOL "BP4K01I"          DATA TAG INHERIT          OVERHEAD 8.500000          TRANSFERRATE 0.040000          NO FILE SYSTEM CACHING          DROPPED TABLE RECOVERY ON"

db2 -x "CREATE LARGE TABLESPACE "BKF4K02D" IN DATABASE PARTITION GROUP IBMDEFAULTGROUP          PAGESIZE 4096 MANAGED BY AUTOMATIC STORAGE          USING STOGROUP "IBMSTOGROUP"          AUTORESIZE YES          INITIALSIZE 32 M          INCREASESIZE 32 M          MAXSIZE NONE          EXTENTSIZE 32          PREFETCHSIZE AUTOMATIC          BUFFERPOOL "BP4K01D"          DATA TAG INHERIT          OVERHEAD 8.500000          TRANSFERRATE 0.040000          NO FILE SYSTEM CACHING          DROPPED TABLE RECOVERY ON"

db2 -x  "CREATE LARGE TABLESPACE "BKF8K01D" IN DATABASE PARTITION GROUP IBMDEFAULTGROUP          PAGESIZE 8192 MANAGED BY AUTOMATIC STORAGE          USING STOGROUP "IBMSTOGROUP"          AUTORESIZE YES          INITIALSIZE 32 M          INCREASESIZE 32 M          MAXSIZE NONE          EXTENTSIZE 32          PREFETCHSIZE AUTOMATIC          BUFFERPOOL "BP8K01D"          DATA TAG INHERIT          OVERHEAD 8.500000          TRANSFERRATE 0.040000          NO FILE SYSTEM CACHING          DROPPED TABLE RECOVERY ON" 
   db2 -x "CREATE TABLESPACE ${tablespace}32K01D PAGESIZE 32K MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL BP32K01D"
   db2 -x "CREATE TABLESPACE ${tablespace}32K01L PAGESIZE 32K MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL BP32K01D"
   
   #-- Disable file system caching (double buffering); let DB2 bufferpools handle data caching
   db2 -x "ALTER TABLESPACE ${tablespace}4K01D   NO FILE SYSTEM CACHING DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE 32M"
   db2 -x "ALTER TABLESPACE ${tablespace}4K01I   NO FILE SYSTEM CACHING DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE 32M"
   db2 -x "ALTER TABLESPACE ${tablespace}32K01D  NO FILE SYSTEM CACHING DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE 32M"
   db2 -x "ALTER TABLESPACE ${tablespace}32K01L  NO FILE SYSTEM CACHING DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE 32M"
done

for pageSize in 4 8 32
do
   # System Temporary Tablespaces: Sorts, Reorgs etc.
   db2 -x "CREATE  SYSTEM TEMPORARY  TABLESPACE STEMP${pageSize}K01D PAGESIZE ${pageSize}K  MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL  STEMP${pageSize}K01D"
   # User Temporary Tablespaces: Global Temporary Tables etc.
   db2 -x "CREATE  USER   TEMPORARY  TABLESPACE UTEMP${pageSize}K01D PAGESIZE ${pageSize}K  MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL  STEMP${pageSize}K01D"
done
# At all times a database has to have at least one temporary tablespace
# Drop default system temporary tablespace after proper ones have been created
db2 -x "DROP TABLESPACE TEMPSPACE1"

#db2 -x "CREATE  SYSTEM TEMPORARY  TABLESPACE SYSTOOLTMPSPACE PAGESIZE  4K  MANAGED BY AUTOMATIC STORAGE EXTENTSIZE 32 OVERHEAD 8.5 TRANSFERRATE 0.04 BUFFERPOOL  STEMP4K01D"

# When a database is created with RESTRICTIVE only SYSADM and DBADM can connect by default
# All database users can connect if they're in the DB2USER user group
db2 -x "GRANT USAGE ON WORKLOAD SYSDEFAULTUSERWORKLOAD TO GROUP DB2USER"
db2 -x "GRANT CONNECT ON DATABASE TO GROUP DB2USER"
#db2 -x "GRANT CONNECT ON DATABASE TO user s_CLAPI"

# Finer grain workload control in the future.
#db2 -x "GRANT USAGE ON WORKLOAD SYSDEFAULTUSERWORKLOAD TO GROUP S_CON"
#db2 -x "GRANT USAGE ON WORKLOAD SYSDEFAULTUSERWORKLOAD TO GROUP S_APPD"
#db2 -x "GRANT DBADM ON DATABASE TO USER MR5650"
#db2 -x "GRANT DBADM ON DATABASE TO USER S_BFEM"
#db2 -x "GRANT DBADM ON DATABASE TO USER BFEM"

# Create the SYSTOOLS schema (Storage Manager) and objects in the SYSTOOLSPACE tablespace
db2 -x "CALL DROP_STORAGEMGMT_TABLES (1)"
db2 -x "CALL CREATE_STORAGEMGMT_TABLES ('SYSTOOLSPACE') "

# Call Reorg Check stored procedure for SYSIBM.SYSTABLES; uses the SYSTOOLTMPSPACE tablespace 
db2 -x "CALL SYSPROC.REORGCHK_TB_STATS('T','SYSIBM.SYSTABLES')"

db2 -x "CALL GET_DBSIZE_INFO(?, ?, ?, -1)"

# The next 2 table spaces are required by Storage Manager(CC).
db2 -x "ALTER TABLESPACE SYSTOOLSPACE NO FILE SYSTEM CACHING DROPPED TABLE RECOVERY ON AUTORESIZE YES INCREASESIZE 5 PERCENT"
db2 -x "COMMENT ON TABLESPACE SYSTOOLSPACE IS 'DB2 Administration tools/routines - historical and configuration data'"
db2 -x "ALTER TABLESPACE SYSTOOLSTMPSPACE BUFFERPOOL STEMP4K01D"

db2 -x "REORGCHK UPDATE STATISTICS ON SCHEMA SYSIBM" >/dev/null
db2 -x "REORGCHK UPDATE STATISTICS ON SCHEMA DBTOOLS" >/dev/null
db2 -x "REORGCHK UPDATE STATISTICS ON SCHEMA SYSTOOLS" >/dev/null

#db2 -x "update db cfg for ${DATABASE} using APPLHEAPSZ 2048 UTIL_HEAP_SZ 8192 PCKCACHESZ 2048 immediate"
#db2 -x "update db cfg for ${DATABASE} using LOCKTIMEOUT 55 LOGSECOND 16 immediate"

# Enable auto maintenance.
db2 -x "update db cfg for ${DATABASE} using AUTO_MAINT ON AUTO_TBL_MAINT ON AUTO_RUNSTATS ON AUTO_STMT_STATS ON"
db2 -x "update db cfg for ${DATABASE} using AUTO_TBL_MAINT ON AUTO_RUNSTATS ON AUTO_STMT_STATS ON AUTO_REORG ON"
db2 -x "update db cfg for ${DATABASE} using AUTORESTART ON AUTO_REVAL DEFERRED_FORCE immediate"

##############Backup will be required##############
#db2 -x "update db cfg for ${DATABASE} using TRACKMOD ON LOGARCHMETH1 TSM:DB2_API immediate" 
#db2 -x "update db cfg for ${DATABASE} using TRACKMOD ON LOGARCHMETH1 TSM:DB2_API immediate" 
db2 -x "connect reset"
db2 -x "terminate"
db2 "backup db ${DATABASE} to /dev/null"
db2 "activate db ${DATABASE}"
db2 "connect reset"
