#!/bin/bash



BASE=`pwd`
SOURCE=$BASE/db2ha_sample_HADR.xml
DOMAIN="scholastic.net"
INTERFACE=`ifconfig | head -1| awk '{print $1}'`

if [ $# -gt 4 ]; then
  DBNAME=$1
  INSTANCE=$2
  HAPRIMARYHOST=$3
  HASECONDARYHOST=$4
  QUORUMIP=$5
  OUT=$BASE/$DBNAME.db2ha_HADR.xml
else
  printf "\nUsage: "
  printf "\n\n $0 DBNAME INSTANCE HA_PRIMARY_HOST HA_SECONDARY_HOST QUORUM \n"
  printf "\n  - HOSTNAMES must be a FQDN.\n"
  printf "\n  - QUORUM must be IP address.\n\n"
  exit 1
fi

nslookup $HAPRIMARYHOST  &>/dev/null
   if [ $? -ne 0 ]; then
	printf "\n * ERROR: $HAPRIMARYHOST could not properly resolve IP.  Please check DNS/network configuration\n\n"
        exit
   else
	LOCALIP=`nslookup $HAPRIMARYHOST| tail -2| awk '{print $2}'`
   fi	 

nslookup $HASECONDARYHOST &>/dev/null
   if [ $? -ne 0 ]; then
       printf "\n * ERROR: $HASECONDARYHOST could not properly resolve IP.  Please check DNS/network configuration\n\n"
       exit
   else
        REMOTEIP=`nslookup $HASECONDARYHOST| tail -2| awk '{print $2}'`
   fi

ping -c 2 $QUORUMIP &>/dev/null
   if [ $? -ne 0 ]; then
       printf "\n * ERROR: QUORUM $QUORUMIP unkown host. \n\n"
       exit
   fi		


sed    "s/QUORUMIP/$QUORUMIP/g" $SOURCE > $OUT
sed -i "s/HAPRIMARYHOST/$HAPRIMARYHOST/g"       $OUT
sed -i "s/HASECONDARYHOST/$HASECONDARYHOST/g"       $OUT
sed -i "s/LOCALIP/$LOCALIP/g"   $OUT
sed -i "s/REMOTEIP/$REMOTEIP/g" $OUT
sed -i "s/INSTANCE/$INSTANCE/g" $OUT
sed -i "s/DBNAME/$DBNAME/g"		$OUT
sed -i "s/eth0/$INTERFACE/g"	$OUT

if [ -s $OUT ] ; then 
  printf "\n File Created:  $OUT\n\n" 
fi
