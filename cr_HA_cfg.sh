#!/bin/bash

if [ $# -gt 7 ]; then
DB=$1
LHOST=$2
LOCAL_HADR_PORT=$3
LOCAL_PORT=$4
RHOST=$5
REMOTE_HADR_PORT=$6
REMOTE_PORT=$7
INSTANCE=$8
else
  printf "\nUsage: "
  printf "\n\n $0 DB LHOST LOCAL_HADR_PORT LOCAL_PORT RHOST REMOTE_HADR_PORT REMOTE_PORT INSTANCE\n\n"
  exit 1
fi


#BASE=/opt/schl/hadr/
BASE=`pwd`
DB=$1
LHOST=$2
LOCAL_HADR_PORT=$3
LOCAL_PORT=$4
RHOST=$5
REMOTE_HADR_PORT=$6
REMOTE_PORT=$7
INSTANCE=$8
OUTDIR=$BASE

PCFG=$OUTDIR/$DB.primary.hadr.cfg
SCFG=$OUTDIR/$DB.standby.hadr.cfg 

#--Primary Config
echo "db2 \"update db cfg for $DB using HADR_LOCAL_HOST $LHOST\"" > $PCFG 
echo "db2 \"update db cfg for $DB using HADR_LOCAL_SVC $LOCAL_HADR_PORT\""  >> $PCFG
echo "db2 \"update db cfg for $DB using HADR_REMOTE_HOST $RHOST\"" >> $PCFG 
echo "db2 \"update db cfg for $DB using HADR_REMOTE_SVC $REMOTE_HADR_PORT\"" >> $PCFG  
echo "db2 \"update db cfg for $DB using HADR_REMOTE_INST $INSTANCE\"" >> $PCFG
echo "db2 \"update alternate server for database $DB using hostname $RHOST port $REMOTE_PORT\"" >> $PCFG  
#-- Instance Memory limited to 4G 
echo "db2 \"update dbm cfg using INSTANCE_MEMORY 2097152\"" >> $PCFG

awk -v var="$DB" '{print "db2 \"update db cfg for " var " using "$1" "$2 "\""}' $BASE/db.cfg >> $PCFG
awk -v var="$DB" '{print "db2 \"update db cfg for " var " using "$1" "$2 "\""}' $BASE/db.cfg.hadr >> $PCFG
awk '{print "db2set "$1"="$2}' $BASE/db.registry.hadr >> $PCFG

#--Standy Config
echo "db2 \"update db cfg for $DB using HADR_LOCAL_HOST $RHOST\"" > $SCFG
echo "db2 \"update db cfg for $DB using HADR_LOCAL_SVC $REMOTE_HADR_PORT\""  >> $SCFG
echo "db2 \"update db cfg for $DB using HADR_REMOTE_HOST $LHOST\"" >> $SCFG
echo "db2 \"update db cfg for $DB using HADR_REMOTE_SVC $LOCAL_HADR_PORT\"" >> $SCFG
echo "db2 \"update db cfg for $DB using HADR_REMOTE_INST $INSTANCE\"" >> $SCFG
echo "db2 \"update alternate server for database $DB using hostname $LHOST port $LOCAL_PORT\"" >> $SCFG  
#-- Instance Memory limited to 4G 
echo "db2 \"update dbm cfg using INSTANCE_MEMORY 1048576\"" >> $SCFG

awk -v var="$DB" '{print "db2 \"update db cfg for " var " using "$1" "$2 "\""}' $BASE/db.cfg >> $SCFG
awk -v var="$DB" '{print "db2 \"update db cfg for " var " using "$1" "$2 "\""}' $BASE/db.cfg.hadr >> $SCFG
awk '{print "db2set "$1"="$2}' $BASE/db.registry.hadr >> $SCFG

if [ -s $SCFG ] ; then
  printf "\n File Created:  $SCFG\n\n"
fi

if [ -s $PCFG ] ; then
  printf "\n File Created:  $PCFG\n\n"
fi

