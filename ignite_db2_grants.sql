SET SCHEMA = ignmon;
GRANT CREATEIN, ALTERIN, DROPIN ON SCHEMA ignmon TO USER ignmon;
GRANT DBADM WITHOUT DATAACCESS WITHOUT ACCESSCTRL, CREATETAB, BINDADD, CONNECT, IMPLICIT_SCHEMA, EXPLAIN, DATAACCESS, ACCESSCTRL ON DATABASE TO USER ignmon;

GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.ENV_GET_SYSTEM_RESOURCES TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.MON_GET_ACTIVITY TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.MON_GET_BUFFERPOOL TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.MON_GET_CONNECTION TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.MON_GET_DATABASE TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.MON_GET_LOCKS TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.MON_GET_MEMORY_SET TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.MON_GET_TABLESPACE TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.SNAP_GET_APPL_INFO TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.SNAP_GET_DYN_SQL TO USER ignmon;
GRANT EXECUTE ON SPECIFIC FUNCTION SYSPROC.SNAP_GET_STMT TO USER ignmon;