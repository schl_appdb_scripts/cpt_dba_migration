 

---------------------------------
-- DDL Statements for Sequences
---------------------------------


CREATE SEQUENCE "BKFAIR01"."SEQ_ADDRESS_BOOK" AS INTEGER
        MINVALUE 1 MAXVALUE 2147483647
        START WITH 1000 INCREMENT BY 1
        NO CACHE NO CYCLE NO ORDER;

ALTER SEQUENCE "BKFAIR01"."SEQ_ADDRESS_BOOK" RESTART WITH 344646;

CREATE SEQUENCE "BKFAIR01"."SEQ_ASSET_ID" AS INTEGER
        MINVALUE 100 MAXVALUE 2147483647
        START WITH 100 INCREMENT BY 1
        NO CACHE NO CYCLE NO ORDER;

ALTER SEQUENCE "BKFAIR01"."SEQ_ASSET_ID" RESTART WITH 8020;

CREATE SEQUENCE "BKFAIR01"."SEQ_CHAIRPERSON" AS INTEGER
        MINVALUE 1 MAXVALUE 2147483647
        START WITH 6789 INCREMENT BY 1
        NO CACHE NO CYCLE NO ORDER;

ALTER SEQUENCE "BKFAIR01"."SEQ_CHAIRPERSON" RESTART WITH 406318;

CREATE SEQUENCE "BKFAIR01"."SEQ_CUSTOMER_REWARDS_PARENT" AS INTEGER
        MINVALUE 1 MAXVALUE 2147483647
        START WITH 1000 INCREMENT BY 1
        NO CACHE NO CYCLE NO ORDER;

ALTER SEQUENCE "BKFAIR01"."SEQ_CUSTOMER_REWARDS_PARENT" RESTART WITH 2891;

CREATE SEQUENCE "BKFAIR01"."SEQ_DISTRIBUTION_LIST" AS INTEGER
        MINVALUE 1 MAXVALUE 2147483647
        START WITH 1000 INCREMENT BY 1
        NO CACHE NO CYCLE NO ORDER;

ALTER SEQUENCE "BKFAIR01"."SEQ_DISTRIBUTION_LIST" RESTART WITH 5020;

CREATE SEQUENCE "BKFAIR01"."SEQ_FILES" AS INTEGER
        MINVALUE 1 MAXVALUE 2147483647
        START WITH 1000 INCREMENT BY 1
        NO CACHE NO CYCLE NO ORDER;

ALTER SEQUENCE "BKFAIR01"."SEQ_FILES" RESTART WITH 12500;

CREATE SEQUENCE "UNPUBLISHED"."SEQ_EVENT_TASK" AS INTEGER
        MINVALUE 5000 MAXVALUE 2147483647
        START WITH 5000 INCREMENT BY 1
        CACHE 100 NO CYCLE NO ORDER;

ALTER SEQUENCE "UNPUBLISHED"."SEQ_EVENT_TASK" RESTART WITH 12099;

CREATE SEQUENCE "UNPUBLISHED"."SEQ_PLANNER_CATEGORY" AS INTEGER
        MINVALUE 1000 MAXVALUE 2147483647
        START WITH 1000 INCREMENT BY 10
        NO CACHE NO CYCLE NO ORDER;

CREATE SEQUENCE "UNPUBLISHED"."SEQ_PLANNER_PROGRAM" AS INTEGER
        MINVALUE 10000 MAXVALUE 2147483647
        START WITH 10000 INCREMENT BY 10
        NO CACHE NO CYCLE NO ORDER;

CREATE SEQUENCE "UNPUBLISHED"."SEQ_TASK_HEADING" AS INTEGER
        MINVALUE 10 MAXVALUE 2147483647
        START WITH 10 INCREMENT BY 1
        NO CACHE NO CYCLE NO ORDER;


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CHAIRPERSON"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CHAIRPERSON"  (
                  "ID" BIGINT NOT NULL , 
                  "FIRST_NAME" VARCHAR(40) NOT NULL , 
                  "LAST_NAME" VARCHAR(40) NOT NULL , 
                  "EMAIL" VARCHAR(200) NOT NULL , 
                  "PHONE_NUMBER" VARCHAR(40) , 
                  "SPSID" VARCHAR(20) , 
                  "SPSID_REGISTRATION_DATE" TIMESTAMP , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."CHAIRPERSON" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."CHAIRPERSON" IS 'TO store the Chair Persons informations.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON"."CREATE_DATE" IS 'Bkfair Release 21, Nov. 2012; tracks the timestamp of the record creation';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON"."EMAIL" IS 'To store the chair person email.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON"."FIRST_NAME" IS 'To store the chair person first name.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON"."ID" IS 'Its a primary key field this column values been generated from the sequence seq_chairperson.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON"."LAST_NAME" IS 'To store the chair person last name.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON"."SPSID" IS 'Bkfair release 21, Nov. 2012; capture the chairperson spsid the first time that chairperson logs in successfully through SPS in Bkfair.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON"."SPSID_REGISTRATION_DATE" IS 'Bkfair release 21, Nov. 2012;Tracks the registration date of SPSID in the Chairperson Toolkit.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON"."UPDATE_DATE" IS 'Bkfair Release 21, Nov. 2012; tracks the timestamp of the updating of the record.';


-- DDL Statements for indexes on Table "BKFAIR01"."CHAIRPERSON"

CREATE INDEX "BKFAIR01"."X_CHAIRPRSN__EMAIL" ON "BKFAIR01"."CHAIRPERSON" 
                ("EMAIL" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."CHAIRPERSON"

CREATE INDEX "BKFAIR01"."X_CHAIRPRSN_SPSID" ON "BKFAIR01"."CHAIRPERSON" 
                ("SPSID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."CHAIRPERSON"

CREATE INDEX "BKFAIR01"."X2CHAIRPRSN_EMAIL" ON "BKFAIR01"."CHAIRPERSON" 
                ("ID" ASC,
                 "EMAIL" ASC)
                DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."CHAIRPERSON"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_CHAIRPRSN_ID" ON "BKFAIR01"."CHAIRPERSON" 
                ("ID" ASC)
                PCTFREE 10 CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CHAIRPERSON"

ALTER TABLE "BKFAIR01"."CHAIRPERSON" 
        ADD CONSTRAINT "XPKC_CHAIRPRSN_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SCHOOL"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SCHOOL"  (
                  "ID" BIGINT NOT NULL , 
                  "NAME" VARCHAR(200) NOT NULL , 
                  "ADDRESS1" VARCHAR(255) NOT NULL , 
                  "ADDRESS2" VARCHAR(255) , 
                  "CITY" VARCHAR(50) , 
                  "STATE" VARCHAR(10) , 
                  "POSTALCODE" VARCHAR(10) , 
                  "SCHOOL_UCN" DECIMAL(15,0) , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP )   
                  IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."SCHOOL" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."SCHOOL" IS 'TO store the school informations for book fair project.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL"."ADDRESS1" IS 'To store the School address line1.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL"."ADDRESS2" IS 'To store the school address line2.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL"."CITY" IS 'To store the school city name.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL"."ID" IS 'Its a primary key column the values been generated from sequence SEQ_SCHOOL.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL"."NAME" IS 'To store the School Name.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL"."POSTALCODE" IS 'To store the school zip code (postal code).';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL"."SCHOOL_UCN" IS 'Release 23.1 BTS; Tracks the UCN value for a given school by CRM.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL"."STATE" IS 'To store the school state name.';


-- DDL Statements for indexes on Table "BKFAIR01"."SCHOOL"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SCHOOL_ID" ON "BKFAIR01"."SCHOOL" 
                ("ID" ASC)
                CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SCHOOL"

ALTER TABLE "BKFAIR01"."SCHOOL" 
        ADD CONSTRAINT "XPKC_SCHOOL_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_CPLIST"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_CPLIST"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CHAIRPERSON_ID" BIGINT NOT NULL , 
                  "LOGIN_COUNT" INTEGER NOT NULL WITH DEFAULT 0 , 
                  "INFO_STATUS" VARCHAR(25) NOT NULL WITH DEFAULT 'Not Yet Accepted' , 
                  "INFO_STATUS_DATE" TIMESTAMP , 
                  "COA_STATUS" VARCHAR(25) NOT NULL WITH DEFAULT 'Not Yet Accepted' , 
                  "COA_STATUS_DATE" TIMESTAMP , 
                  "NEW_FEA_STATUS" VARCHAR(25) NOT NULL WITH DEFAULT 'Not Yet Visited' , 
                  "NEW_FEA_DATE" TIMESTAMP , 
                  "COA_FILE_ID" BIGINT , 
                  "COA_FILE_UPDATE_DATE" TIMESTAMP , 
                  "OFE_STATUS" VARCHAR(25) NOT NULL WITH DEFAULT 'Not Yet Accepted' , 
                  "OFE_STATUS_DATE" TIMESTAMP , 
                  "DATES_MOVED" CHAR(1) , 
                  "PRODUCT_CHANGE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."FAIR_CPLIST" PCTFREE 10;

COMMENT ON COLUMN "BKFAIR01"."FAIR_CPLIST"."COA_STATUS" IS 'To store the chairperson Certificates of Agreement (COA) Possible values are Accepted / Not Yet Accepted / Rejected.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_CPLIST"."DATES_MOVED" IS 'When set to Y, allows a splash page to be displayed; indicating that the Fair dates have been modified.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_CPLIST"."PRODUCT_CHANGE_DATE" IS 'Release 24; Tracks the timestamp when the product ID for this fair has been changed.';


-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_CPLIST"

ALTER TABLE "BKFAIR01"."FAIR_CPLIST" 
        ADD CONSTRAINT "XPKC_FAIRCPLIST_FC" PRIMARY KEY
                ("FAIR_ID",
                 "CHAIRPERSON_ID");



-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_CPLIST"

CREATE INDEX "BKFAIR01"."XFK_FAIR_CPLST_CID" ON "BKFAIR01"."FAIR_CPLIST" 
                ("CHAIRPERSON_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_CPLIST"

CREATE INDEX "BKFAIR01"."XFK_FAIR_CPLST_FID" ON "BKFAIR01"."FAIR_CPLIST" 
                ("FAIR_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."ADDRESS_BOOK"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."ADDRESS_BOOK"  (
                  "ID" BIGINT NOT NULL , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "FIRST_NAME" VARCHAR(50) NOT NULL , 
                  "LAST_NAME" VARCHAR(50) NOT NULL , 
                  "EMAIL" VARCHAR(200) , 
                  "HOME_PHONE" VARCHAR(20) , 
                  "WORK_PHONE" VARCHAR(20) , 
                  "MOBILE_PHONE" VARCHAR(20) , 
                  "ADDRESS1" VARCHAR(50) , 
                  "ADDRESS2" VARCHAR(50) , 
                  "CITY" VARCHAR(40) , 
                  "STATE" VARCHAR(20) , 
                  "ZIP" VARCHAR(20) , 
                  "NOTES" VARCHAR(500) , 
                  "TYPE" VARCHAR(30) , 
                  "DELETED_FLAG" VARCHAR(1) NOT NULL WITH DEFAULT 'N' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP , 
                  "SPSID" VARCHAR(20) , 
                  "SPSID_REGISTRATION_DATE" TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."ADDRESS_BOOK" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."ADDRESS_BOOK" IS 'To store the Address Book information';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."ADDRESS1" IS 'The address book address1.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."ADDRESS2" IS 'Address line 2.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."CITY" IS 'To store the city name.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."CREATE_DATE" IS 'Bkfair Release 21, Dec. 2012; tracks the timestamp of the record creation';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."EMAIL" IS 'To store the address book email.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."FIRST_NAME" IS 'To store the first name.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."HOME_PHONE" IS 'To store Home phone number.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."ID" IS 'Its a primary key column the values been generated from sequence SEQ_ADDRESS_BOOK.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."LAST_NAME" IS 'To store the last name.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."MOBILE_PHONE" IS 'To store the mobile phone number.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."NOTES" IS '.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."SPSID" IS 'Bkfair release 21, Dec. 2012; capture the Volunteer spsid the first time that Contact logs in successfully through SPS in Bkfair.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."SPSID_REGISTRATION_DATE" IS 'Bkfair release 21, Dec. 2012;Tracks the registration date of SPSID in the Chairperson Toolkit.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."STATE" IS 'To store the state name.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."TYPE" IS '.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."UPDATE_DATE" IS 'Bkfair Release 21, Dec. 2012; tracks the timestamp of the updating of the record.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."WORK_PHONE" IS 'To store the Work phone number.';

COMMENT ON COLUMN "BKFAIR01"."ADDRESS_BOOK"."ZIP" IS 'To store the zip code.';


-- DDL Statements for indexes on Table "BKFAIR01"."ADDRESS_BOOK"

CREATE INDEX "BKFAIR01"."X_ADDRESSBOOK_EMAIL" ON "BKFAIR01"."ADDRESS_BOOK" 
                ("EMAIL" ASC,
                 "ID" ASC,
                 "SPSID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."ADDRESS_BOOK"

CREATE INDEX "BKFAIR01"."X_ADDRESSBOOK_EMAILSPSID" ON "BKFAIR01"."ADDRESS_BOOK" 
                ("EMAIL" ASC,
                 "SPSID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."ADDRESS_BOOK"

CREATE INDEX "BKFAIR01"."X_ADDRESSBOOK_SPSID" ON "BKFAIR01"."ADDRESS_BOOK" 
                ("SPSID" ASC,
                 "ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."ADDRESS_BOOK"

CREATE INDEX "BKFAIR01"."XFK_ADDRESS_BOOK_D" ON "BKFAIR01"."ADDRESS_BOOK" 
                ("ID" ASC,
                 "SCHOOL_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."ADDRESS_BOOK"

CREATE INDEX "BKFAIR01"."XFK_ADDRSBOOK_SID" ON "BKFAIR01"."ADDRESS_BOOK" 
                ("SCHOOL_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."ADDRESS_BOOK"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_ADDRSBOOK_ID" ON "BKFAIR01"."ADDRESS_BOOK" 
                ("ID" ASC)
                INCLUDE ("EMAIL" ,
                 "SCHOOL_ID" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."ADDRESS_BOOK"

ALTER TABLE "BKFAIR01"."ADDRESS_BOOK" 
        ADD CONSTRAINT "XPKC_ADDRSBOOK_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."MYTEAM"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."MYTEAM"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CONTACT_ID" BIGINT NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."MYTEAM" IS 'To store the myteam information';

COMMENT ON COLUMN "BKFAIR01"."MYTEAM"."CONTACT_ID" IS 'Its a foreign key column referencing Address_book table ID column.';

COMMENT ON COLUMN "BKFAIR01"."MYTEAM"."CREATE_DATE" IS 'Bkfair Release 21, Dec. 2012; tracks the timestamp of the record creation';

COMMENT ON COLUMN "BKFAIR01"."MYTEAM"."FAIR_ID" IS 'Its a foreign key column referencing fair table';


-- DDL Statements for indexes on Table "BKFAIR01"."MYTEAM"

CREATE INDEX "BKFAIR01"."XFK_MYTEAM_CID" ON "BKFAIR01"."MYTEAM" 
                ("CONTACT_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."MYTEAM"

CREATE INDEX "BKFAIR01"."XFK_MYTEAM_FAIR_ID" ON "BKFAIR01"."MYTEAM" 
                ("FAIR_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."MYTEAM"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_MYTEAM_FCID" ON "BKFAIR01"."MYTEAM" 
                ("FAIR_ID" ASC,
                 "CONTACT_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."MYTEAM"

ALTER TABLE "BKFAIR01"."MYTEAM" 
        ADD CONSTRAINT "XPK_MYTEAM_FCID" PRIMARY KEY
                ("FAIR_ID",
                 "CONTACT_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."DISTRIBUTION_LIST"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."DISTRIBUTION_LIST"  (
                  "ID" BIGINT NOT NULL , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "TITLE" VARCHAR(50) NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."DISTRIBUTION_LIST" IS 'To store the distribution_list information';

COMMENT ON COLUMN "BKFAIR01"."DISTRIBUTION_LIST"."ID" IS 'Its a primary key column and values been generated from sequecne SEQ_DISTRIBUTION_LIST.';

COMMENT ON COLUMN "BKFAIR01"."DISTRIBUTION_LIST"."SCHOOL_ID" IS 'To store the school_id.';

COMMENT ON COLUMN "BKFAIR01"."DISTRIBUTION_LIST"."TITLE" IS 'To store the title name.';


-- DDL Statements for indexes on Table "BKFAIR01"."DISTRIBUTION_LIST"

CREATE INDEX "BKFAIR01"."XFK_DISBTLIST_SID" ON "BKFAIR01"."DISTRIBUTION_LIST" 
                ("SCHOOL_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."DISTRIBUTION_LIST"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_DISBTLLIST_ID" ON "BKFAIR01"."DISTRIBUTION_LIST" 
                ("ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."DISTRIBUTION_LIST"

ALTER TABLE "BKFAIR01"."DISTRIBUTION_LIST" 
        ADD CONSTRAINT "XPKC_DISBTLLIST_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS"  (
                  "DISTRIBUTION_ID" BIGINT NOT NULL , 
                  "CONTACT_ID" BIGINT NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS" IS 'To store the distribution_list_contacts information';

COMMENT ON COLUMN "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS"."CONTACT_ID" IS 'The contact_id and distribution_id are composite primay key.';

COMMENT ON COLUMN "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS"."DISTRIBUTION_ID" IS 'The distribution_id and contact_id are composite primay key.';


-- DDL Statements for indexes on Table "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS"

CREATE INDEX "BKFAIR01"."XFK_DISTLISTC_CID" ON "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS" 
                ("CONTACT_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS"

CREATE INDEX "BKFAIR01"."XFK_DISTLISTC_DI" ON "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS" 
                ("DISTRIBUTION_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_DISTLTC_DI_CI" ON "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS" 
                ("DISTRIBUTION_ID" ASC,
                 "CONTACT_ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS"

ALTER TABLE "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS" 
        ADD CONSTRAINT "XPKC_DISTLTC_DI_CI" PRIMARY KEY
                ("DISTRIBUTION_ID",
                 "CONTACT_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."DISTRIBUTION_LIST_EMAILS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."DISTRIBUTION_LIST_EMAILS"  (
                  "DISTRIBUTION_ID" BIGINT NOT NULL , 
                  "EMAIL" VARCHAR(200) NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."DISTRIBUTION_LIST_EMAILS" IS 'To store the distribution_list_emails information';

COMMENT ON COLUMN "BKFAIR01"."DISTRIBUTION_LIST_EMAILS"."DISTRIBUTION_ID" IS '.';

COMMENT ON COLUMN "BKFAIR01"."DISTRIBUTION_LIST_EMAILS"."EMAIL" IS 'To store the email id.';


-- DDL Statements for indexes on Table "BKFAIR01"."DISTRIBUTION_LIST_EMAILS"

CREATE INDEX "BKFAIR01"."XFK_DISTLISTEM_DI" ON "BKFAIR01"."DISTRIBUTION_LIST_EMAILS" 
                ("DISTRIBUTION_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."DISTRIBUTION_LIST_EMAILS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_DISTLTE_DI_EM" ON "BKFAIR01"."DISTRIBUTION_LIST_EMAILS" 
                ("DISTRIBUTION_ID" ASC,
                 "EMAIL" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."DISTRIBUTION_LIST_EMAILS"

ALTER TABLE "BKFAIR01"."DISTRIBUTION_LIST_EMAILS" 
        ADD CONSTRAINT "XPKC_DISTLTE_DI_EM" PRIMARY KEY
                ("DISTRIBUTION_ID",
                 "EMAIL");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FILES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FILES"  (
                  "ID" BIGINT NOT NULL , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "CATEGORY_NAME" VARCHAR(254) , 
                  "TITLE" VARCHAR(254) , 
                  "FILE_SIZE" INTEGER , 
                  "FILE_EXTENSION" VARCHAR(25) , 
                  "CREATION_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."FILES" IS 'To store the files information';

COMMENT ON COLUMN "BKFAIR01"."FILES"."CATEGORY_NAME" IS 'To store the school_id and its a foreign key column referencing school table.';

COMMENT ON COLUMN "BKFAIR01"."FILES"."CREATION_DATE" IS 'To store the category name.';

COMMENT ON COLUMN "BKFAIR01"."FILES"."FILE_EXTENSION" IS 'To store the category name.';

COMMENT ON COLUMN "BKFAIR01"."FILES"."FILE_SIZE" IS 'To store the file name.';

COMMENT ON COLUMN "BKFAIR01"."FILES"."ID" IS 'Its a primay key column the values been generated from sequence SEQ_FILES.';

COMMENT ON COLUMN "BKFAIR01"."FILES"."SCHOOL_ID" IS 'To store the school_id and its a foreign key column referencing school table.';

COMMENT ON COLUMN "BKFAIR01"."FILES"."TITLE" IS 'To store the file name.';


-- DDL Statements for indexes on Table "BKFAIR01"."FILES"

CREATE INDEX "BKFAIR01"."XFK_FILES_IDSCHID" ON "BKFAIR01"."FILES" 
                ("ID" ASC,
                 "SCHOOL_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FILES"

CREATE INDEX "BKFAIR01"."XFK_FILES_SCHOOLID" ON "BKFAIR01"."FILES" 
                ("SCHOOL_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FILES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FILES_ID" ON "BKFAIR01"."FILES" 
                ("ID" ASC)
                PCTFREE 10 CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FILES"

ALTER TABLE "BKFAIR01"."FILES" 
        ADD CONSTRAINT "XPKC_FILES_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PREVIOUS_FAIRS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PREVIOUS_FAIRS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "PREVIOUS_FAIR_ID" BIGINT NOT NULL , 
                  "PREVIOUS_FAIR_START_DATE" DATE )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."PREVIOUS_FAIRS" IS 'TO store the Book Fair formations for book fair project.';

COMMENT ON COLUMN "BKFAIR01"."PREVIOUS_FAIRS"."FAIR_ID" IS 'Its a primary key column the values been getting from IT and the fair id should be unique.';

COMMENT ON COLUMN "BKFAIR01"."PREVIOUS_FAIRS"."PREVIOUS_FAIR_START_DATE" IS 'To store the fair delivery_date.';

COMMENT ON COLUMN "BKFAIR01"."PREVIOUS_FAIRS"."SCHOOL_ID" IS 'To store the school ID.';


-- DDL Statements for indexes on Table "BKFAIR01"."PREVIOUS_FAIRS"

CREATE INDEX "BKFAIR01"."X_PREVIOUS_FAS_PFI" ON "BKFAIR01"."PREVIOUS_FAIRS" 
                ("PREVIOUS_FAIR_ID" ASC)
                DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."PREVIOUS_FAIRS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_PREVIOUS_FSP" ON "BKFAIR01"."PREVIOUS_FAIRS" 
                ("FAIR_ID" ASC,
                 "SCHOOL_ID" ASC,
                 "PREVIOUS_FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PREVIOUS_FAIRS"

ALTER TABLE "BKFAIR01"."PREVIOUS_FAIRS" 
        ADD CONSTRAINT "XPKC_PREVIOUS_FSP" PRIMARY KEY
                ("FAIR_ID",
                 "SCHOOL_ID",
                 "PREVIOUS_FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CASH_AND_CHECKS_SALE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CASH_AND_CHECKS_SALE"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TOTAL_AMOUNT" DECIMAL(8,2) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."CASH_AND_CHECKS_SALE" IS 'Users cash and checks amount and this table would be a master table for all financial tables.';

COMMENT ON COLUMN "BKFAIR01"."CASH_AND_CHECKS_SALE"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on fair table.';

COMMENT ON COLUMN "BKFAIR01"."CASH_AND_CHECKS_SALE"."TOTAL_AMOUNT" IS 'Cash and checks sales total amount.';


-- DDL Statements for indexes on Table "BKFAIR01"."CASH_AND_CHECKS_SALE"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_CASH_CHK_FID" ON "BKFAIR01"."CASH_AND_CHECKS_SALE" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CASH_AND_CHECKS_SALE"

ALTER TABLE "BKFAIR01"."CASH_AND_CHECKS_SALE" 
        ADD CONSTRAINT "XPKC_CASH_CHK_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FINANCIAL_SECTION_STATUS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FINANCIAL_SECTION_STATUS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "STARTED_ON" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP , 
                  "CURRENT_SECTION" VARCHAR(25) , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP , 
                  "EMAIL_COUNT" INTEGER , 
                  "INVOICE_SCENARIO" INTEGER , 
                  "INVOICE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."FINANCIAL_SECTION_STATUS" IS 'Users currect financial section like (SALES/PROFIT/REWARDS/COMPLATE/CONFIRM).';

COMMENT ON COLUMN "BKFAIR01"."FINANCIAL_SECTION_STATUS"."CURRENT_SECTION" IS 'Web page current section status like Sales/Profit/Rewards/Complete/Confirm.';

COMMENT ON COLUMN "BKFAIR01"."FINANCIAL_SECTION_STATUS"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."FINANCIAL_SECTION_STATUS"."INVOICE_DATE" IS 'The date will be captured when the Invoice was Genarated for the first time by Chairperson and this date displays in all genarated Invoice Pages for that Fair.';

COMMENT ON COLUMN "BKFAIR01"."FINANCIAL_SECTION_STATUS"."INVOICE_SCENARIO" IS 'Invoice Scenario will be calculated based on Total_Sales_Tax, Sales_Tax_Due, PO_Order, and Tax_Remittance and for FAIR_TYPE U it would be 9,10,11,12,13,14,15,16 and rest of the FAIR_TYPE it would be 1,2,3,4,5,6,7,8.';

COMMENT ON COLUMN "BKFAIR01"."FINANCIAL_SECTION_STATUS"."STARTED_ON" IS 'Stores date and time for when was this record was created.';


-- DDL Statements for indexes on Table "BKFAIR01"."FINANCIAL_SECTION_STATUS"

CREATE INDEX "BKFAIR01"."IDX_FF_UPDDATE" ON "BKFAIR01"."FINANCIAL_SECTION_STATUS" 
                ("CURRENT_SECTION" ASC,
                 "UPDATE_DATE" ASC,
                 "FAIR_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FINANCIAL_SECTION_STATUS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FINCL_SEC_FID" ON "BKFAIR01"."FINANCIAL_SECTION_STATUS" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FINANCIAL_SECTION_STATUS"

ALTER TABLE "BKFAIR01"."FINANCIAL_SECTION_STATUS" 
        ADD CONSTRAINT "XPKC_FINCL_SEC_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CC_SALES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CC_SALES"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TOTAL_AMOUNT" DECIMAL(8,2) , 
                  "NUM_OF_TRANSACTIONS" SMALLINT )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."CC_SALES" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."CC_SALES"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."CC_SALES"."NUM_OF_TRANSACTIONS" IS 'Total number of transactions.';

COMMENT ON COLUMN "BKFAIR01"."CC_SALES"."TOTAL_AMOUNT" IS 'Credit Cards total sales amount.';


-- DDL Statements for indexes on Table "BKFAIR01"."CC_SALES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_CC_SALES_FID" ON "BKFAIR01"."CC_SALES" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CC_SALES"

ALTER TABLE "BKFAIR01"."CC_SALES" 
        ADD CONSTRAINT "XPKC_CC_SALES_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PO_CONTACT"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PO_CONTACT"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "BILL_TO_NAME" VARCHAR(30) , 
                  "DISTRICT_NAME" VARCHAR(30) , 
                  "ADDRESS" VARCHAR(30) , 
                  "CITY" VARCHAR(20) , 
                  "STATE" VARCHAR(2) , 
                  "ZIPCODE" VARCHAR(9) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."PO_CONTACT" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."PO_CONTACT" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."PO_CONTACT"."ADDRESS" IS 'Purchase Order contact Address.';

COMMENT ON COLUMN "BKFAIR01"."PO_CONTACT"."BILL_TO_NAME" IS 'Purchase Order contact Billing Name.';

COMMENT ON COLUMN "BKFAIR01"."PO_CONTACT"."CITY" IS 'Purchase Order contact City Name.';

COMMENT ON COLUMN "BKFAIR01"."PO_CONTACT"."DISTRICT_NAME" IS 'Purchase Order contact District Name.';

COMMENT ON COLUMN "BKFAIR01"."PO_CONTACT"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."PO_CONTACT"."STATE" IS 'Purchase Order contact State Name.';

COMMENT ON COLUMN "BKFAIR01"."PO_CONTACT"."ZIPCODE" IS 'Purchase Order contact Zip code.';


-- DDL Statements for indexes on Table "BKFAIR01"."PO_CONTACT"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_PO_CONTAC_FID" ON "BKFAIR01"."PO_CONTACT" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PO_CONTACT"

ALTER TABLE "BKFAIR01"."PO_CONTACT" 
        ADD CONSTRAINT "XPKC_PO_CONTAC_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PO_SALES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PO_SALES"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "PO_1_NUMBER" VARCHAR(25) , 
                  "PO_1_AMOUNT" DECIMAL(8,2) , 
                  "PO_2_NUMBER" VARCHAR(25) , 
                  "PO_2_AMOUNT" DECIMAL(8,2) , 
                  "PO_3_NUMBER" VARCHAR(25) , 
                  "PO_3_AMOUNT" DECIMAL(8,2) , 
                  "PO_4_NUMBER" VARCHAR(25) , 
                  "PO_4_AMOUNT" DECIMAL(8,2) , 
                  "PO_5_NUMBER" VARCHAR(25) , 
                  "PO_5_AMOUNT" DECIMAL(8,2) , 
                  "PO_6_NUMBER" VARCHAR(25) , 
                  "PO_6_AMOUNT" DECIMAL(8,2) , 
                  "PO_7_NUMBER" VARCHAR(25) , 
                  "PO_7_AMOUNT" DECIMAL(8,2) , 
                  "PO_8_NUMBER" VARCHAR(25) , 
                  "PO_8_AMOUNT" DECIMAL(8,2) , 
                  "PO_9_NUMBER" VARCHAR(25) , 
                  "PO_9_AMOUNT" DECIMAL(8,2) , 
                  "PO_10_NUMBER" VARCHAR(25) , 
                  "PO_10_AMOUNT" DECIMAL(8,2) , 
                  "PO_11_NUMBER" VARCHAR(25) , 
                  "PO_11_AMOUNT" DECIMAL(8,2) , 
                  "PO_12_NUMBER" VARCHAR(25) , 
                  "PO_12_AMOUNT" DECIMAL(8,2) , 
                  "TOTAL_PO_COLLECTED" DECIMAL(10,2) , 
                  "PO_1_TYPE" VARCHAR(50) , 
                  "PO_2_TYPE" VARCHAR(50) , 
                  "PO_3_TYPE" VARCHAR(50) , 
                  "PO_4_TYPE" VARCHAR(50) , 
                  "PO_5_TYPE" VARCHAR(50) , 
                  "PO_6_TYPE" VARCHAR(50) , 
                  "PO_7_TYPE" VARCHAR(50) , 
                  "PO_8_TYPE" VARCHAR(50) , 
                  "PO_9_TYPE" VARCHAR(50) , 
                  "PO_10_TYPE" VARCHAR(50) , 
                  "PO_11_TYPE" VARCHAR(50) , 
                  "PO_12_TYPE" VARCHAR(50) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."PO_SALES" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."PO_SALES"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."PO_SALES"."PO_1_AMOUNT" IS 'Purchase Order Amount 1.';

COMMENT ON COLUMN "BKFAIR01"."PO_SALES"."PO_1_NUMBER" IS 'Purchase Order Number 1.';


-- DDL Statements for indexes on Table "BKFAIR01"."PO_SALES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_PO_SALES_FID" ON "BKFAIR01"."PO_SALES" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PO_SALES"

ALTER TABLE "BKFAIR01"."PO_SALES" 
        ADD CONSTRAINT "XPKC_PO_SALES_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SALES_TAX_COLLECTED"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SALES_TAX_COLLECTED"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TAX_COLLECTED" SMALLINT )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."SALES_TAX_COLLECTED" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_COLLECTED"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_COLLECTED"."TAX_COLLECTED" IS 'Tax Collected (Yes/No) 1 means yes 0 means no.';


-- DDL Statements for indexes on Table "BKFAIR01"."SALES_TAX_COLLECTED"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SALES_TAXC_FI" ON "BKFAIR01"."SALES_TAX_COLLECTED" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SALES_TAX_COLLECTED"

ALTER TABLE "BKFAIR01"."SALES_TAX_COLLECTED" 
        ADD CONSTRAINT "XPKC_SALES_TAXC_FI" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TAX_EXEMPTED" SMALLINT , 
                  "TAX_EXEMPT_ID" VARCHAR(20) , 
                  "TAX_EXEMPT_TRANSACTIONS" DECIMAL(10,2) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"   ; 

ALTER TABLE "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"."TAX_EXEMPT_ID" IS 'Tax Excempt ID number.';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"."TAX_EXEMPT_TRANSACTIONS" IS 'Tax Excempt amount.';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"."TAX_EXEMPTED" IS 'Sales Tax Exempt 1 means Yes or 0 means No.';


-- DDL Statements for indexes on Table "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"

CREATE UNIQUE INDEX "BKFAIR01"."IDX_SALESTAX_EXEMPTID" ON "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS" 
                ("FAIR_ID" ASC)
                INCLUDE ("TAX_EXEMPT_ID" )
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SALESTAXE_FID" ON "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"

ALTER TABLE "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS" 
        ADD CONSTRAINT "XPKC_SALESTAXE_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SALES_TAX_EXEMPT_STATUS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SALES_TAX_EXEMPT_STATUS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TAX_STATUS" VARCHAR(30) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"   ; 

COMMENT ON TABLE "BKFAIR01"."SALES_TAX_EXEMPT_STATUS" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_EXEMPT_STATUS"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_EXEMPT_STATUS"."TAX_STATUS" IS 'Cash and checks sales total amount.';


-- DDL Statements for indexes on Table "BKFAIR01"."SALES_TAX_EXEMPT_STATUS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SALES_TXE_FI" ON "BKFAIR01"."SALES_TAX_EXEMPT_STATUS" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SALES_TAX_EXEMPT_STATUS"

ALTER TABLE "BKFAIR01"."SALES_TAX_EXEMPT_STATUS" 
        ADD CONSTRAINT "XPKC_SALES_TXE_FI" PRIMARY KEY
                ("FAIR_ID");




-- DDL Statements for indexes on Table "BKFAIR01"."SALES_TAX_EXEMPT_STATUS"

CREATE UNIQUE INDEX "BOOKFAIR01"."IDX_SALES_TAXSTAT" ON "BKFAIR01"."SALES_TAX_EXEMPT_STATUS" 
                ("FAIR_ID" ASC)
                INCLUDE ("TAX_STATUS" )
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TAX_EXEMPT_ID" VARCHAR(20) , 
                  "TAX_EXEMPT_TRANSACTIONS" DECIMAL(10,2) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS"."TAX_EXEMPT_ID" IS 'Sales Tax Exempt ID number.';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS"."TAX_EXEMPT_TRANSACTIONS" IS 'Sales Tax Exempt Amount.';


-- DDL Statements for indexes on Table "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SALESTAXD_FID" ON "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS"

ALTER TABLE "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS" 
        ADD CONSTRAINT "XPKC_SALESTAXD_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SALES_TAX_REMITTANCE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SALES_TAX_REMITTANCE"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "REMITTANCE_BY_SCHOLASTIC" SMALLINT )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."SALES_TAX_REMITTANCE" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_REMITTANCE"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."SALES_TAX_REMITTANCE"."REMITTANCE_BY_SCHOLASTIC" IS 'Remittance Collected By Scholastic (Yes/No) 1 means yes 0 means no.';


-- DDL Statements for indexes on Table "BKFAIR01"."SALES_TAX_REMITTANCE"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SALES_TXR_FI" ON "BKFAIR01"."SALES_TAX_REMITTANCE" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SALES_TAX_REMITTANCE"

ALTER TABLE "BKFAIR01"."SALES_TAX_REMITTANCE" 
        ADD CONSTRAINT "XPKC_SALES_TXR_FI" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SALES_SUMMARY"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SALES_SUMMARY"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TOTAL_COLLECTED" DECIMAL(10,2) , 
                  "TAX_EXEMPT_SALES" DECIMAL(10,2) , 
                  "TAXABLE_SALES_LESS_TAX" DECIMAL(10,2) , 
                  "SALES_TAX" DECIMAL(10,2) , 
                  "SALES_TAX_DUE" DECIMAL(10,2) , 
                  "TOTAL_FAIR_SALES" DECIMAL(10,2) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"   ; 

COMMENT ON TABLE "BKFAIR01"."SALES_SUMMARY" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."SALES_SUMMARY"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."SALES_SUMMARY"."SALES_TAX" IS 'Sales tax amount.';

COMMENT ON COLUMN "BKFAIR01"."SALES_SUMMARY"."SALES_TAX_DUE" IS 'Total fair sales amount.';

COMMENT ON COLUMN "BKFAIR01"."SALES_SUMMARY"."TAX_EXEMPT_SALES" IS 'Sales taxable transaction amount.';

COMMENT ON COLUMN "BKFAIR01"."SALES_SUMMARY"."TOTAL_COLLECTED" IS 'Sales Total collected amount.';

COMMENT ON COLUMN "BKFAIR01"."SALES_SUMMARY"."TOTAL_FAIR_SALES" IS 'Total fair sales amount.';


-- DDL Statements for indexes on Table "BKFAIR01"."SALES_SUMMARY"

CREATE UNIQUE INDEX "BKFAIR01"."IDX_SALESSUM_TOTALSALES" ON "BKFAIR01"."SALES_SUMMARY" 
                ("FAIR_ID" ASC)
                INCLUDE ("TOTAL_FAIR_SALES" )
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."SALES_SUMMARY"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SALES_SUM_FID" ON "BKFAIR01"."SALES_SUMMARY" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SALES_SUMMARY"

ALTER TABLE "BKFAIR01"."SALES_SUMMARY" 
        ADD CONSTRAINT "XPKC_SALES_SUM_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_FINANCIAL_INFO"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_FINANCIAL_INFO"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SECOND_BOOKING" SMALLINT , 
                  "INCRESING_REVENUE_PRIOR" DECIMAL(10,2) , 
                  "BOGO_BONUS" SMALLINT )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."FAIR_FINANCIAL_INFO" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."FAIR_FINANCIAL_INFO"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on fair table.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_FINANCIAL_INFO"."INCRESING_REVENUE_PRIOR" IS 'Increasing Revenue Prior.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_FINANCIAL_INFO"."SECOND_BOOKING" IS 'Second Booking.';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_FINANCIAL_INFO"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FAIR_FNCL_FID" ON "BKFAIR01"."FAIR_FINANCIAL_INFO" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_FINANCIAL_INFO"

ALTER TABLE "BKFAIR01"."FAIR_FINANCIAL_INFO" 
        ADD CONSTRAINT "XPKC_FAIR_FNCL_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."TAX_DETAIL"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."TAX_DETAIL"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TAX_RATE" INTEGER , 
                  "EXEMPT_SALES_ALLOWED" SMALLINT , 
                  "CUSTOMER_MAY_PAY_TAX" SMALLINT , 
                  "INITIAL_TAX_RATE" INTEGER WITH DEFAULT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."TAX_DETAIL" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."TAX_DETAIL"."CUSTOMER_MAY_PAY_TAX" IS 'Customer May Pay Tax --> Y(Yes)/N(No).';

COMMENT ON COLUMN "BKFAIR01"."TAX_DETAIL"."EXEMPT_SALES_ALLOWED" IS 'Exempt Sales Allowed.';

COMMENT ON COLUMN "BKFAIR01"."TAX_DETAIL"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on fair table.';

COMMENT ON COLUMN "BKFAIR01"."TAX_DETAIL"."INITIAL_TAX_RATE" IS 'Release 18: This column became obsolete as of May 2011. The value of this column used to be updated via Procedure p_bkfair_finanform.';

COMMENT ON COLUMN "BKFAIR01"."TAX_DETAIL"."TAX_RATE" IS 'To store the Tax Rate for Finanical Forms on going data load this value may get change with t';


-- DDL Statements for indexes on Table "BKFAIR01"."TAX_DETAIL"

CREATE UNIQUE INDEX "BKFAIR01"."IDX_TAXDET_INIT" ON "BKFAIR01"."TAX_DETAIL" 
                ("FAIR_ID" ASC)
                INCLUDE ("INITIAL_TAX_RATE" )
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."TAX_DETAIL"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_TAX_DETAI_FID" ON "BKFAIR01"."TAX_DETAIL" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."TAX_DETAIL"

ALTER TABLE "BKFAIR01"."TAX_DETAIL" 
        ADD CONSTRAINT "XPKC_TAX_DETAI_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."REGION"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."REGION"  (
                  "REGION_ID" VARCHAR(2) NOT NULL , 
                  "REGION_NAME" VARCHAR(25) , 
                  "NAME" VARCHAR(200) , 
                  "ADDRESS1" VARCHAR(255) , 
                  "CITY" VARCHAR(50) , 
                  "STATE" VARCHAR(10) , 
                  "ZIPCODE" VARCHAR(10) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."REGION" IS 'TO store the Schools Region informations for book fair project.';

COMMENT ON COLUMN "BKFAIR01"."REGION"."REGION_ID" IS 'Its a primary key column and region_id should be unique.';

COMMENT ON COLUMN "BKFAIR01"."REGION"."REGION_NAME" IS 'To store the Region Name.';


-- DDL Statements for indexes on Table "BKFAIR01"."REGION"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_REGION_RID" ON "BKFAIR01"."REGION" 
                ("REGION_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."REGION"

ALTER TABLE "BKFAIR01"."REGION" 
        ADD CONSTRAINT "XPKC_REGION_RID" PRIMARY KEY
                ("REGION_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PROFIT_TAKEN"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PROFIT_TAKEN"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "BOOK_PROFIT_TAKEN" DECIMAL(10,2) , 
                  "BOOK_BONUS_TAKEN" DECIMAL(10,2) , 
                  "PRE_BOOK_PROFIT_TAKEN" DECIMAL(10,2) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."PROFIT_TAKEN" IS 'This table is to store the profit taken before the fair ends.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_TAKEN"."BOOK_BONUS_TAKEN" IS 'Bonus book profit taken by the use before the fair completed.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_TAKEN"."BOOK_PROFIT_TAKEN" IS 'Book profit taken by the use before the fair completed.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_TAKEN"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_TAKEN"."PRE_BOOK_PROFIT_TAKEN" IS 'Book Profit Taken from Fair Using Prev Bale. Stored in CPT DB and reported to CSI in gen export file.';


-- DDL Statements for indexes on Table "BKFAIR01"."PROFIT_TAKEN"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_PROFIT_TA_FID" ON "BKFAIR01"."PROFIT_TAKEN" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PROFIT_TAKEN"

ALTER TABLE "BKFAIR01"."PROFIT_TAKEN" 
        ADD CONSTRAINT "XPKC_PROFIT_TA_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PROFIT_SELECTION"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PROFIT_SELECTION"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "PROFIT_SELECTION_OPTION" VARCHAR(5) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."PROFIT_SELECTION" IS 'This table is to store the profit selection option that user did choose.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SELECTION"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SELECTION"."PROFIT_SELECTION_OPTION" IS 'User.s option how he want to take/split the profit (allowed values: BOOKS, IRC, CASH, SPLIT).';


-- DDL Statements for indexes on Table "BKFAIR01"."PROFIT_SELECTION"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_PROFIT_SE_FID" ON "BKFAIR01"."PROFIT_SELECTION" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PROFIT_SELECTION"

ALTER TABLE "BKFAIR01"."PROFIT_SELECTION" 
        ADD CONSTRAINT "XPKC_PROFIT_SE_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PROFIT_TO_SPLIT"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PROFIT_TO_SPLIT"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TOTAL_AMOUNT_BOOKS" DECIMAL(10,2) , 
                  "TOTAL_AMOUNT_IRC" DECIMAL(10,2) , 
                  "TOTAL_AMOUNT_CASH" DECIMAL(10,2) , 
                  "IND_LOCK" VARCHAR(1) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."PROFIT_TO_SPLIT" IS 'This table is to store the profit information that is been split as per the user request.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_TO_SPLIT"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_TO_SPLIT"."IND_LOCK" IS 'While calculating profit split, which one user want to give more preference (lock) of the above totals (allowed values: B . Book Lock, I . IRC Lock, C . Cash Lock, U - Unlock).';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_TO_SPLIT"."TOTAL_AMOUNT_BOOKS" IS 'From the profit how much amount he want to use towards books.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_TO_SPLIT"."TOTAL_AMOUNT_CASH" IS 'From the profit how much amount he want to use towards cash.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_TO_SPLIT"."TOTAL_AMOUNT_IRC" IS 'From the profit how much amount he want to use towards IRC.';


-- DDL Statements for indexes on Table "BKFAIR01"."PROFIT_TO_SPLIT"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_PROFIT_T0_FID" ON "BKFAIR01"."PROFIT_TO_SPLIT" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PROFIT_TO_SPLIT"

ALTER TABLE "BKFAIR01"."PROFIT_TO_SPLIT" 
        ADD CONSTRAINT "XPKC_PROFIT_TO_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PROFIT_SUMMARY"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PROFIT_SUMMARY"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TOTAL_BOOK_PROFIT_TAKEN" DECIMAL(10,2) , 
                  "TOTAL_BOOK_PROFIT_BAL" DECIMAL(10,2) , 
                  "TOTAL_IRC_VOCHURE" DECIMAL(10,2) , 
                  "TOTAL_CASH_PROFIT" DECIMAL(10,2) , 
                  "TOTAL_BOOK_BONUS_TAKEN" DECIMAL(10,2) , 
                  "TOTAL_BOOK_BONUS_BAL" DECIMAL(10,2) , 
                  "MAX_BONUS" DECIMAL(10,2) , 
                  "MAX_BONUS_INCENTIVE" DECIMAL(10,2) , 
                  "BOGO_BONUS" DECIMAL(10,2) , 
                  "SECOND_BOGO_BONUS" DECIMAL(10,2) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."PROFIT_SUMMARY" IS 'This table is to store the profit summary information that is been derived as the user.s selected option.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SUMMARY"."FAIR_ID" IS 'It''s a Primary Key column and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SUMMARY"."SECOND_BOGO_BONUS" IS 'Release 23.5 BTS; Application computes the dollar amount generated by the new column second_bogo_bonus_percentage in table PROFIT_RULES.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SUMMARY"."TOTAL_BOOK_BONUS_BAL" IS 'Total amount of bonus taken from the profit.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SUMMARY"."TOTAL_BOOK_BONUS_TAKEN" IS 'The book bonus already taken before fair ends.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SUMMARY"."TOTAL_BOOK_PROFIT_BAL" IS 'Total amount of books taken from the profit.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SUMMARY"."TOTAL_BOOK_PROFIT_TAKEN" IS 'The book profit already taken before fair ends.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SUMMARY"."TOTAL_CASH_PROFIT" IS 'Total amount of cash taken from the profit.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_SUMMARY"."TOTAL_IRC_VOCHURE" IS 'Total amount IRC vouchers taken from the profit.';


-- DDL Statements for indexes on Table "BKFAIR01"."PROFIT_SUMMARY"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_PROFIT_SU_FID" ON "BKFAIR01"."PROFIT_SUMMARY" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PROFIT_SUMMARY"

ALTER TABLE "BKFAIR01"."PROFIT_SUMMARY" 
        ADD CONSTRAINT "XPKC_PROFIT_SU_FID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FINANCIAL_SAVED_URLS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FINANCIAL_SAVED_URLS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "URL_DISPLAY_SEQ" SMALLINT NOT NULL , 
                  "URL" VARCHAR(250) NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."FINANCIAL_SAVED_URLS" IS 'Keep history of users visited urls.';

COMMENT ON COLUMN "BKFAIR01"."FINANCIAL_SAVED_URLS"."FAIR_ID" IS 'It''s a Composite Primary Key and it references the fair_id on cash_and_checks_sale table.';

COMMENT ON COLUMN "BKFAIR01"."FINANCIAL_SAVED_URLS"."URL" IS 'Web page URL.';

COMMENT ON COLUMN "BKFAIR01"."FINANCIAL_SAVED_URLS"."URL_DISPLAY_SEQ" IS 'It''s a Composite Primary Key and Web url display sequence number.';


-- DDL Statements for indexes on Table "BKFAIR01"."FINANCIAL_SAVED_URLS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FINCL_SAV_FU" ON "BKFAIR01"."FINANCIAL_SAVED_URLS" 
                ("FAIR_ID" ASC,
                 "URL" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FINANCIAL_SAVED_URLS"

ALTER TABLE "BKFAIR01"."FINANCIAL_SAVED_URLS" 
        ADD CONSTRAINT "XPKC_FINCL_SAV_FU" PRIMARY KEY
                ("FAIR_ID",
                 "URL");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SC_TOOLKIT_ASSISTANCE" VARCHAR(1) , 
                  "SC_SCHEDULE_BOOK_FAIR" VARCHAR(1) , 
                  "SC_BOOK_FAIR_MONTH" VARCHAR(8) , 
                  "SC_BOOK_FAIR_YEAR" INTEGER , 
                  "SC_CHAIPERSON_INFORMATION" VARCHAR(1) , 
                  "SC_FAIR_DATES" VARCHAR(1) , 
                  "SC_OTHER" VARCHAR(1) , 
                  "SC_FIRST_NAME" VARCHAR(25) , 
                  "SC_LAST_NAME" VARCHAR(25) , 
                  "SC_EMAIL" VARCHAR(75) , 
                  "SC_PHONE_NUMBER" VARCHAR(40) , 
                  "SC_TIME_TO_CALL" VARCHAR(10) , 
                  "SC_CONTACT_BY_EMAIL" VARCHAR(1) WITH DEFAULT 'N' , 
                  "SC_CONTACT_BY_PHONE" VARCHAR(1) WITH DEFAULT 'N' , 
                  "SC_CONTACT_BY_EITHER" VARCHAR(1) WITH DEFAULT 'N' , 
                  "SC_COMMENTS" VARCHAR(500) , 
                  "SC_SUBMIT_DATE_TIME" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "SALES_TYPE" VARCHAR(3) , 
                  "FORMNAME" VARCHAR(50) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" IS 'To store the Contact your Sales consultact Email Form details.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."FAIR_ID" IS 'Chairperson login fair_id.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_BOOK_FAIR_MONTH" IS 'Chairperson Desired Time: Select Month Name from the Drop down list box on the Email Form.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_BOOK_FAIR_YEAR" IS 'Chairperson Desired Time: Select Year from the Drop down list box on the Email Form.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_CHAIPERSON_INFORMATION" IS 'Chairperson Information(Y/N).';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_COMMENTS" IS 'Chairperson comments.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_CONTACT_BY_EITHER" IS 'Contact by phone/Email Y/N.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_CONTACT_BY_EMAIL" IS 'Contact by Email Y/N.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_CONTACT_BY_PHONE" IS 'Contact by phone Y/N.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_EMAIL" IS 'Chairperson Email address.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_FAIR_DATES" IS 'Chairperson Fair Dates (Y/N).';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_FIRST_NAME" IS 'Chairperson First Name.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_LAST_NAME" IS 'Chairperson Last Name.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_OTHER" IS 'Chairperson Other comments Y/N.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_PHONE_NUMBER" IS 'Chairperson  Phone Number.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_SCHEDULE_BOOK_FAIR" IS 'Chairperson would like to schedule a scholastic book fair(Y/N).';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_SUBMIT_DATE_TIME" IS 'Record created time Default current timetamp.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_TIME_TO_CALL" IS 'Best time to call the Chairperson.';

COMMENT ON COLUMN "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"."SC_TOOLKIT_ASSISTANCE" IS 'Chairperson required assistance for using the Online Toolkit(Y/N).';






-- DDL Statements for indexes on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

CREATE INDEX "BKFAIR01"."X_SALES_CONSULT_EF" ON "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

CREATE INDEX "BKFAIR01"."X_SALES_CONSULT_SD" ON "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
                ("SC_SUBMIT_DATE_TIME" ASC)
                DISALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"  (
                  "CS_TITLE" VARCHAR(9) , 
                  "CS_FIRST_NAME" VARCHAR(10) , 
                  "CS_LAST_NAME" VARCHAR(16) , 
                  "CS_POSITION" VARCHAR(30) , 
                  "CS_SCHOOL_NAME" VARCHAR(30) , 
                  "CS_CITY" VARCHAR(30) , 
                  "CS_STATE" VARCHAR(20) , 
                  "CS_POSTALCODE" VARCHAR(9) , 
                  "CS_COUNTRY" VARCHAR(30) , 
                  "CS_PHONE_NUMBER" VARCHAR(30) , 
                  "CS_TIME_TO_CALL" VARCHAR(30) , 
                  "CS_TIME_ZONE" VARCHAR(13) , 
                  "CS_EMAIL" VARCHAR(75) , 
                  "CS_FAIR_START_DATE" VARCHAR(10) , 
                  "CS_FAIR_END_DATE" VARCHAR(10) , 
                  "CS_FAMILY_DATE" VARCHAR(30) , 
                  "CS_VOLUNTEERS_NO" VARCHAR(3) , 
                  "CS_COMMENTS" VARCHAR(500) , 
                  "CS_SUBMIT_DATE_TIME" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS" IS 'To store the Contact Book Fair Cutomer Service Email Form details.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_CITY" IS 'Chairperson City.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_COMMENTS" IS 'Chairperson Comments.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_COUNTRY" IS 'Chairperson Country Name.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_EMAIL" IS 'Chairperson email address.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_FAIR_END_DATE" IS 'Fair End Date.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_FAIR_START_DATE" IS 'Fair Start Date.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_FAMILY_DATE" IS 'Family Event Date.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_FIRST_NAME" IS 'Chairperson First Name.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_LAST_NAME" IS 'Chairperson Last Name.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_PHONE_NUMBER" IS 'Chairperson contact Phone Number.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_POSITION" IS 'Chariperson Position.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_POSTALCODE" IS 'Chairperson Zip code.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_SCHOOL_NAME" IS 'Chairperson School Name.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_STATE" IS 'Chairperson Stat.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_SUBMIT_DATE_TIME" IS 'Record created date default current time stamp.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_TIME_TO_CALL" IS 'Best time to call the Chairperson.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_TIME_ZONE" IS 'Best time to call Time Zone.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_TITLE" IS 'Chairperson Title.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"."CS_VOLUNTEERS_NO" IS 'Number of Volunteers.';






-- DDL Statements for indexes on Table "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS"

CREATE INDEX "BKFAIR01"."X_CUSTOMER_SERV_SD" ON "BKFAIR01"."CUSTOMER_SERVICE_EMAIL_FORMS" 
                ("CS_SUBMIT_DATE_TIME" ASC)
                DISALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "NC_CHAIRPERSON_INFORMATION" VARCHAR(1) , 
                  "NC_SCHOOL_INFORMATION" VARCHAR(1) , 
                  "NC_FAIR_DATES" VARCHAR(1) , 
                  "NC_OTHER" VARCHAR(1) , 
                  "NC_FIRST_NAME" VARCHAR(30) , 
                  "NC_LAST_NAME" VARCHAR(30) , 
                  "NC_EMAIL" VARCHAR(75) , 
                  "NC_PHONE" VARCHAR(40) , 
                  "NC_TIME_TO_CALL" VARCHAR(10) , 
                  "NC_CONTACT_BY_EMAIL" VARCHAR(1) WITH DEFAULT 'N' , 
                  "NC_CONTACT_BY_PHONE" VARCHAR(1) WITH DEFAULT 'N' , 
                  "NC_CONTACT_BY_EITHER" VARCHAR(1) WITH DEFAULT 'N' , 
                  "NC_COMMENTS" VARCHAR(500) , 
                  "NC_SUBMIT_DATE_TIME" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "NC_SCHOOL_ADDRESS1" VARCHAR(30) , 
                  "NC_SCHOOL_ADDRESS2" VARCHAR(30) , 
                  "NC_SCHOOL_CITY" VARCHAR(20) , 
                  "NC_SCHOOL_STATE" VARCHAR(2) , 
                  "NC_SCHOOL_ZIP" VARCHAR(5) , 
                  "NC_FAIR_START_DATE" DATE , 
                  "NC_FAIR_END_DATE" DATE , 
                  "NC_NOT_SCHEDULED" VARCHAR(1) , 
                  "CREATE_TIMESTAMP" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" IS 'To store the Not Corrent Email Form details.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."FAIR_ID" IS 'Chairperson login fair_id.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_CHAIRPERSON_INFORMATION" IS 'Contact the chairperson to update Information Y/N.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_COMMENTS" IS 'Chairperson comments.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_CONTACT_BY_EITHER" IS 'Contact the Chairperson via Email/Phone Y/N.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_CONTACT_BY_EMAIL" IS 'Contact the Chairperson via Email Y/N.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_CONTACT_BY_PHONE" IS 'Contact the Chairperson via Phone Y/N.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_EMAIL" IS 'Chairperson Email.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_FAIR_DATES" IS 'Contact the chairperson to update Fair Dates Y/N.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_FAIR_END_DATE" IS 'Fair End Date.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_FAIR_START_DATE" IS 'Fair Start Date.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_FIRST_NAME" IS 'Chairperson First Name.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_LAST_NAME" IS 'Chairperson Last Name.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_NOT_SCHEDULED" IS 'Is it Scheduled Y / N means Yes/No.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_OTHER" IS 'Contact the Chairperson to Other Option Y/N If it''s Y see the Comments.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_PHONE" IS 'Chairperson Phone.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_SCHOOL_ADDRESS1" IS 'School Address.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_SCHOOL_ADDRESS2" IS 'School Address line2.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_SCHOOL_CITY" IS 'School City Name.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_SCHOOL_INFORMATION" IS 'Contact the chairperson to update School Information Y/N.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_SCHOOL_STATE" IS 'School State Name.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_SCHOOL_ZIP" IS 'School Zip cdoe.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_SUBMIT_DATE_TIME" IS 'Record created date default current time stamp.';

COMMENT ON COLUMN "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"."NC_TIME_TO_CALL" IS 'Best time to call the Chairperson.';






-- DDL Statements for indexes on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

CREATE INDEX "BKFAIR01"."X_NOT_CORRECTEF_CREATE_TMSTP" ON "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
                ("CREATE_TIMESTAMP" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

CREATE INDEX "BKFAIR01"."X_NOT_CORRECTEF_FI" ON "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
                ("FAIR_ID" ASC)
                PCTFREE 10 CLUSTER DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

CREATE INDEX "BKFAIR01"."X_NOT_CORRECTEF_SD" ON "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
                ("NC_SUBMIT_DATE_TIME" ASC)
                DISALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CUSTOMER_REWARDS_PARENT"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CUSTOMER_REWARDS_PARENT"  (
                  "REWARD_PARENT_ID" BIGINT NOT NULL , 
                  "REWARD_ID" BIGINT NOT NULL , 
                  "SEQUENCE_NO" INTEGER NOT NULL , 
                  "REGION_ID" VARCHAR(2) NOT NULL , 
                  "PRODUCT_ID" VARCHAR(2) NOT NULL , 
                  "REWARD_START_DATE" DATE NOT NULL , 
                  "DESCRIPTION" VARCHAR(256) NOT NULL , 
                  "RESPONSE_TYPE" VARCHAR(1) NOT NULL , 
                  "REWARD_END_DATE" DATE NOT NULL , 
                  "VOUCHER_VALUE" DECIMAL(5,2) NOT NULL WITH DEFAULT 0.00 , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."CUSTOMER_REWARDS_PARENT" IS 'TO store the Customer Rewards Parent informations.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."CREATE_DATE" IS 'Record create date by default current timestamp.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."DESCRIPTION" IS 'Rewards Description.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."PRODUCT_ID" IS 'To store the Product_id.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."REGION_ID" IS 'To store the Region_id.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."RESPONSE_TYPE" IS 'Reward Response Type and possible values are Y(Yes), and N(No).';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."REWARD_END_DATE" IS 'Rewards End date.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."REWARD_ID" IS 'To Store the Reward Id.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."REWARD_PARENT_ID" IS 'it''s a Primary Key and Generated from SQL_CUSTOMER_REWARDS_PARENT sequence number.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."REWARD_START_DATE" IS 'Rewards Start date.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."SEQUENCE_NO" IS 'Reward display Sequence number.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."UPDATE_DATE" IS 'Record last modified date.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT"."VOUCHER_VALUE" IS 'Rewards Voucher Value.';


-- DDL Statements for indexes on Table "BKFAIR01"."CUSTOMER_REWARDS_PARENT"

CREATE UNIQUE INDEX "BKFAIR01"."X_CUSTOMER_RP_R" ON "BKFAIR01"."CUSTOMER_REWARDS_PARENT" 
                ("REWARD_ID" ASC,
                 "SEQUENCE_NO" ASC,
                 "REGION_ID" ASC,
                 "PRODUCT_ID" ASC,
                 "REWARD_START_DATE" ASC)
                DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."CUSTOMER_REWARDS_PARENT"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_CUSTOMER_RP_R" ON "BKFAIR01"."CUSTOMER_REWARDS_PARENT" 
                ("REWARD_PARENT_ID" ASC)
                DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CUSTOMER_REWARDS_PARENT"

ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_PARENT" 
        ADD CONSTRAINT "XPKC_CUSTOMER_RP_R" PRIMARY KEY
                ("REWARD_PARENT_ID");



-- DDL Statements for unique constraints on Table "BKFAIR01"."CUSTOMER_REWARDS_PARENT"


ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_PARENT" 
        ADD CONSTRAINT "XPKC_CUSTOMER_UNC" UNIQUE
                ("REWARD_ID",
                 "REGION_ID",
                 "PRODUCT_ID",
                 "REWARD_START_DATE");

------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CUSTOMER_REWARDS_CHILD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CUSTOMER_REWARDS_CHILD"  (
                  "REWARD_ID" BIGINT NOT NULL , 
                  "CHILD_SEQUENCE_NO" INTEGER NOT NULL , 
                  "CHILD_DESCRIPTION" VARCHAR(256) NOT NULL , 
                  "CHILD_RESPONSE_TYPE" VARCHAR(1) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "REWARD_SUB_DESC_MANDATORY" CHAR(1) NOT NULL WITH DEFAULT 'N' )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."CUSTOMER_REWARDS_CHILD" IS 'TO store the Customer Rewards Child informations.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD"."CHILD_DESCRIPTION" IS 'Rewards child record description.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD"."CHILD_RESPONSE_TYPE" IS 'Child Reward Response Type and possible values are 1. [Y/N -- Yes/No flag width 1 char], 2. [D (Dollar Amount) Decimal(8,2) numeric], 3. [C mean Count Numeric(3)], and 4. T means Text VARCHAR(35)].';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD"."CHILD_SEQUENCE_NO" IS 'Customer Rewards Child record Sequence Number and it''s composite Primay Key.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD"."CREATE_DATE" IS 'Record create date by default current timestamp.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD"."REWARD_ID" IS 'Reward_id and it''s composite Primary Key.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD"."UPDATE_DATE" IS 'Record last modified date.';


-- DDL Statements for indexes on Table "BKFAIR01"."CUSTOMER_REWARDS_CHILD"

CREATE UNIQUE INDEX "BKFAIR01"."X_CUSTOMER_RC_RC" ON "BKFAIR01"."CUSTOMER_REWARDS_CHILD" 
                ("REWARD_ID" ASC,
                 "CHILD_SEQUENCE_NO" ASC)
                DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CUSTOMER_REWARDS_CHILD"

ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_CHILD" 
        ADD CONSTRAINT "X_CUSTOMER_RC_RC" PRIMARY KEY
                ("REWARD_ID",
                 "CHILD_SEQUENCE_NO");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"  (
                  "REWARD_PARENT_ID" BIGINT NOT NULL , 
                  "FAIR_ID" BIGINT NOT NULL , 
                  "REWARD_ID" BIGINT NOT NULL , 
                  "SEQUENCE_NO" INTEGER NOT NULL , 
                  "CHILD_SEQUENCE_NO" INTEGER NOT NULL , 
                  "RESPONSE_TYPE" VARCHAR(1) NOT NULL , 
                  "RESPONSE_VALUE" VARCHAR(7) , 
                  "VOUCHER_VALUE" DECIMAL(5,2) WITH DEFAULT 0 , 
                  "CHILD_RESPONSE_TYPE" VARCHAR(1) , 
                  "CHILD_RESPONSE_VALUE" VARCHAR(60) , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE" IS 'TO store the Customer Rewards Response informations for Parent and Child rewards.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"."CHILD_RESPONSE_TYPE" IS 'Reward Response Type and possible values are 1. [Y/N -- Yes/No flag width 1 char], 2. [D (Dollar Amount) Decimal(8,2) numeric], 3. [C mean Count Numeric(3)], and 4. T means Text VARCHAR(35)].';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"."CHILD_RESPONSE_VALUE" IS 'CHILD reward customer Response Value.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"."CHILD_SEQUENCE_NO" IS 'Reward Child Sequence Number.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"."CREATE_DATE" IS 'Record create date by default current timestamp.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"."RESPONSE_TYPE" IS 'Reward Response Type and possible values are Y(Yes), and N(No).';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"."REWARD_ID" IS 'Reward Id and it''s composite Primary Key.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"."REWARD_PARENT_ID" IS 'Reward Parent Id and it''s composite Primary Key.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"."SEQUENCE_NO" IS 'Reward display Sequence number and it''s composite Primay key.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"."UPDATE_DATE" IS 'Record last modified date.';


-- DDL Statements for indexes on Table "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"

CREATE INDEX "BKFAIR01"."X_CUSTOMER_REWARDS_RSPSE_UPDATE_DATE" ON "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE" 
                ("UPDATE_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"

CREATE UNIQUE INDEX "BKFAIR01"."X_CUSTOMER_RRR_FRS" ON "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE" 
                ("REWARD_PARENT_ID" ASC,
                 "FAIR_ID" ASC,
                 "REWARD_ID" ASC,
                 "SEQUENCE_NO" ASC,
                 "CHILD_SEQUENCE_NO" ASC)
                DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"

ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE" 
        ADD CONSTRAINT "XPKC_CUSTOMER_RC_R" PRIMARY KEY
                ("REWARD_PARENT_ID",
                 "FAIR_ID",
                 "REWARD_ID",
                 "SEQUENCE_NO",
                 "CHILD_SEQUENCE_NO");




-- DDL Statements for indexes on Table "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"

CREATE INDEX "BKFAIR01"."X2_CUSTOMER_RRR" ON "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE" 
                ("FAIR_ID" ASC)
                PCTFREE 10 DISALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"  (
                  "REWARD_ID" VARCHAR(6) , 
                  "REGION_ID" VARCHAR(2) , 
                  "PRODUCT_ID" VARCHAR(2) , 
                  "DESCRIPTION" VARCHAR(256) , 
                  "SEQUENCE_NO" VARCHAR(2) , 
                  "RESPONSE_TYPE" VARCHAR(1) , 
                  "REWARD_START_DATE" VARCHAR(8) , 
                  "REWARD_END_DATE" VARCHAR(8) , 
                  "VOUCHER_VALUE" VARCHAR(5) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD" IS 'TO store the Customer Rewards Parent informations.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"."DESCRIPTION" IS 'Rewards Description.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"."PRODUCT_ID" IS 'To store the Product_id.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"."REGION_ID" IS 'To store the Region_id.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"."RESPONSE_TYPE" IS 'Reward Response Type and possible values are Y(Yes), and N(No).';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"."REWARD_END_DATE" IS 'Rewards End date.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"."REWARD_ID" IS 'Reward Id and it''s composite Primary Key.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"."REWARD_START_DATE" IS 'Rewards Start date.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_PARENT_LOAD"."SEQUENCE_NO" IS 'Reward display Sequence number and it''s composite Primay key.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CUSTOMER_REWARDS_CHILD_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CUSTOMER_REWARDS_CHILD_LOAD"  (
                  "REWARD_ID" VARCHAR(6) , 
                  "CHILD_DESCRIPTION" VARCHAR(256) , 
                  "CHILD_SEQUENCE_NO" VARCHAR(2) , 
                  "CHILD_RESPONSE_TYPE" VARCHAR(1) , 
                  "REWARD_SUB_DESC_MANDATORY" CHAR(1) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."CUSTOMER_REWARDS_CHILD_LOAD" IS 'TO store the Customer Rewards Child informations.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD_LOAD"."CHILD_DESCRIPTION" IS 'Rewards child description.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD_LOAD"."CHILD_RESPONSE_TYPE" IS 'Reward Response Type and possible values are 1. [Y/N -- Yes/No flag width 1 char], 2. [D (Dollar Amount) Decimal(8,2) numeric], 3. [C mean Count Numeric(3)], and 4. T means Text VARCHAR(35)].';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD_LOAD"."CHILD_SEQUENCE_NO" IS 'Rewards display Child Sequence number.';

COMMENT ON COLUMN "BKFAIR01"."CUSTOMER_REWARDS_CHILD_LOAD"."REWARD_ID" IS 'Reward_id and it''s composite Unique Index.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CHAIRPERSON_EMAIL" VARCHAR(200) NOT NULL , 
                  "EMAIL_TYPE" VARCHAR(10) NOT NULL , 
                  "EMAIL_CLICK_THROUGH" VARCHAR(1) NOT NULL WITH DEFAULT 'N' , 
                  "EMAIL_DELIVERED" VARCHAR(1) NOT NULL WITH DEFAULT 'N' , 
                  "EMAIL_SENT_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION" IS 'TO store the Chairperson Email invitations informations.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"."CHAIRPERSON_EMAIL" IS 'To store the Chairperson''s Email.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"."EMAIL_CLICK_THROUGH" IS 'it specifies that the chair person entered the tool kit by clicking on link which was on email invitation.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"."EMAIL_DELIVERED" IS 'The email invitation was delivered successfully to the Chairperson.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"."EMAIL_SENT_DATE" IS 'The record created date.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"."EMAIL_TYPE" IS 'Email_type is generated baced on product_id values for the Fair_id and passible email_types are e1 to e8.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"."FAIR_ID" IS 'To store the Fair_id and It''s a Primary key references the Fair table (ID).';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"."UPDATE_DATE" IS 'The record last modified date.';


-- DDL Statements for indexes on Table "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"

CREATE INDEX "BKFAIR01"."XPKC_CHAIRPRN_EIFD" ON "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION" 
                ("FAIR_ID" ASC)
                DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_CHAIRPRN_EIFT" ON "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION" 
                ("FAIR_ID" ASC,
                 "EMAIL_TYPE" ASC)
                DISALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"

ALTER TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION" 
        ADD CONSTRAINT "XPKC_CHAIRPRN_EIFT" PRIMARY KEY
                ("FAIR_ID",
                 "EMAIL_TYPE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."HOMEPAGE_SCHEDULE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."HOMEPAGE_SCHEDULE"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SCHEDULE_DATE" DATE NOT NULL , 
                  "SCHEDULE_HOURS" VARCHAR(30) , 
                  "SCHEDULE_DATE_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "START_TIME" TIME , 
                  "END_TIME" TIME )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."HOMEPAGE_SCHEDULE" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."HOMEPAGE_SCHEDULE" IS 'TO store the fair Home Page schedule informations.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_SCHEDULE"."CREATE_DATE" IS 'The record created date.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_SCHEDULE"."END_TIME" IS 'Release 19.8; Replaces column data SCHEDULE_HOURS (varcbar), and tracks the global ending time of the fair schedule for a given day as a true time data.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_SCHEDULE"."FAIR_ID" IS 'It''s primary key and it''s foreign key references homepage.fair_id.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_SCHEDULE"."SCHEDULE_DATE" IS 'To store the Fair scheduled date.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_SCHEDULE"."SCHEDULE_DATE_CKBOX" IS 'To store the Fair scheduled Date Check box value Y(Yes) N(No).';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_SCHEDULE"."SCHEDULE_HOURS" IS 'To store the Fair scheduled Hours.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_SCHEDULE"."START_TIME" IS 'Release 19.8; Replaces column data SCHEDULE_HOURS (varchar), and tracks the global starting time of the fair schedule for a given day as a true time data.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_SCHEDULE"."UPDATE_DATE" IS 'The record last modified date.';


-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_SCHEDULE"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_HOMEPGE_SCHED" ON "BKFAIR01"."HOMEPAGE_SCHEDULE" 
                ("FAIR_ID" ASC,
                 "SCHEDULE_DATE" ASC)
                PCTFREE 10 CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."HOMEPAGE_SCHEDULE"

ALTER TABLE "BKFAIR01"."HOMEPAGE_SCHEDULE" 
        ADD CONSTRAINT "XPKC_HOMEPGE_SCHED" PRIMARY KEY
                ("FAIR_ID",
                 "SCHEDULE_DATE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."HOMEPAGE_EVENT"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."HOMEPAGE_EVENT"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SCHEDULE_DATE" DATE NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "EVENT_NAME" VARCHAR(50) , 
                  "EVENT_TIME" VARCHAR(20) , 
                  "EVENT_ID" BIGINT , 
                  "START_TIME" TIME WITH DEFAULT '8:00' , 
                  "END_TIME" TIME WITH DEFAULT '8:30' , 
                  "UPDATE_DATE" TIMESTAMP , 
                  "DISPLAY_SEQ_NO" SMALLINT , 
                  "PLANNER_CALENDAR_CKBOX" CHAR(1) WITH DEFAULT 'Y' , 
                  "COMPLETED_FLAG" CHAR(1) WITH DEFAULT 'N' )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."HOMEPAGE_EVENT" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."HOMEPAGE_EVENT" IS 'TO store the fair Home Page event informations.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."COMPLETED_FLAG" IS 'Rel. 24.5: When set to Y, indicates that this auto-added task was completed; It is set to N when it is not.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."CREATE_DATE" IS 'The record created date.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."DISPLAY_SEQ_NO" IS 'Release 21, October 2012; Tracks the order sequence by which the events will be displayed.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."END_TIME" IS 'Release 19.8; Replaces column data EVENT_TIME (varcbar), and tracks the ending time of a given event as a true time data.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."EVENT_NAME" IS 'Release 19.8; This column no longer track a non standard event name. it now tracks an event description.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."EVENT_TIME" IS 'To store the Fair Event time.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."FAIR_ID" IS 'To store the fair_id.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."PLANNER_CALENDAR_CKBOX" IS 'Rel. 24.5: Allow by default all homepage event records to be displayed except for event ID # 29';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."SCHEDULE_DATE" IS 'To store the fair schedule date.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_EVENT"."START_TIME" IS 'Release 19.8; Replaces column data EVENT_TIME (varchar), and tracks the starting time of a given event as a true time data.';


-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_EVENT"

CREATE INDEX "BKFAIR01"."XPKC_FAIR_HMPGE_EI" ON "BKFAIR01"."HOMEPAGE_EVENT" 
                ("FAIR_ID" ASC,
                 "SCHEDULE_DATE" ASC,
                 "CREATE_DATE" ASC)
                PCTFREE 10 CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."HOMEPAGE_EVENT"

ALTER TABLE "BKFAIR01"."HOMEPAGE_EVENT" 
        ADD CONSTRAINT "XPKC_FAIR_HMPGE_EI" PRIMARY KEY
                ("FAIR_ID",
                 "SCHEDULE_DATE",
                 "CREATE_DATE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"  (
                  "PERCENTAGE_ID" BIGINT NOT NULL , 
                  "IRC_VOUCHER_PROFIT" INTEGER NOT NULL , 
                  "PERCENTAGE_START_DATE" DATE NOT NULL , 
                  "PERCENTAGE_END_DATE" DATE NOT NULL , 
                  "PERCENTAGE_INCREASE_FROM" DECIMAL(10,2) NOT NULL , 
                  "PERCENTAGE_INCREASE_TO" DECIMAL(10,2) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"; 

COMMENT ON TABLE "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE" IS 'TO store the IRS Percentage Increase Rule.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"."CREATE_DATE" IS 'Record create date by default current timestamp.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"."IRC_VOUCHER_PROFIT" IS 'Voucher Profit Amount.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"."PERCENTAGE_END_DATE" IS 'To store the percentange increase End date.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"."PERCENTAGE_ID" IS 'It''s a Primary key uniquely generated from book Fair IT.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"."PERCENTAGE_INCREASE_FROM" IS 'Percentage Increase start range based on IRC voucher Profit.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"."PERCENTAGE_INCREASE_TO" IS 'Percentage Increase End range based on IRC voucher Profit.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"."PERCENTAGE_START_DATE" IS 'To store the percentage incresase Start date.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"."UPDATE_DATE" IS 'Record last modified date.';


-- DDL Statements for primary key on Table "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE"

ALTER TABLE "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE" 
        ADD CONSTRAINT "XPKC_IRC_PERCENTGE" PRIMARY KEY
                ("PERCENTAGE_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE_DATA_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE_DATA_LOAD"  (
                  "PERCENTAGE_ID" VARCHAR(25) , 
                  "IRC_VOUCHER_PROFIT" VARCHAR(25) , 
                  "PERCENTAGE_START_DATE" VARCHAR(20) , 
                  "PERCENTAGE_END_DATE" VARCHAR(20) , 
                  "PERCENTAGE_INCREASE_FROM" VARCHAR(10) , 
                  "PERCENTAGE_INCREASE_TO" VARCHAR(10) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE_DATA_LOAD" IS 'TO store the IRS Percentage Increase Rule and it''s a import table.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE_DATA_LOAD"."IRC_VOUCHER_PROFIT" IS 'Voucher Profit Amount.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE_DATA_LOAD"."PERCENTAGE_END_DATE" IS 'To store the percentange increase End date.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE_DATA_LOAD"."PERCENTAGE_ID" IS 'It''s a Primary key uniquely generated from book Fair IT.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE_DATA_LOAD"."PERCENTAGE_INCREASE_FROM" IS 'Percentage Increase start range based on IRC voucher Profit.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE_DATA_LOAD"."PERCENTAGE_INCREASE_TO" IS 'Percentage Increase End range based on IRC voucher Profit.';

COMMENT ON COLUMN "BKFAIR01"."IRC_PERCENTAGE_INCREASE_RULE_DATA_LOAD"."PERCENTAGE_START_DATE" IS 'To store the percentage incresase Start date.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"  (
                  "MULTIPLIER_ID" BIGINT NOT NULL , 
                  "IRC_VOUCHER_PROFIT_MULTIPLIER" INTEGER NOT NULL , 
                  "MULTIPLIER_START_DATE" DATE NOT NULL , 
                  "MULTIPLIER_END_DATE" DATE NOT NULL , 
                  "TOTAL_FAIR_SALES_FROM" DECIMAL(10,2) NOT NULL , 
                  "TOTAL_FAIR_SALES_TO" DECIMAL(10,2) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES" IS 'TO store the IRS Threshold Bonus rules.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"."CREATE_DATE" IS 'Record create date by default current timestamp.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"."IRC_VOUCHER_PROFIT_MULTIPLIER" IS 'Bonus points awarded based on the Fair Sales amount the number of times multiplier.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"."MULTIPLIER_END_DATE" IS 'To store the multiplier End date range.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"."MULTIPLIER_ID" IS 'It''s a Primary key uniquely generated from book Fair IT.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"."MULTIPLIER_START_DATE" IS 'To store the multiplier Start date range.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"."TOTAL_FAIR_SALES_FROM" IS 'Fair Sales range starting amount.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"."TOTAL_FAIR_SALES_TO" IS 'Fair Sales range ending amount.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"."UPDATE_DATE" IS 'Record last modified date.';


-- DDL Statements for primary key on Table "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES"

ALTER TABLE "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES" 
        ADD CONSTRAINT "XPKC_IRC_THRESHOLD" PRIMARY KEY
                ("MULTIPLIER_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES_DATA_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES_DATA_LOAD"  (
                  "MULTIPLIER_ID" VARCHAR(25) , 
                  "IRC_VOUCHER_PROFIT_MULTIPLIER" VARCHAR(25) , 
                  "MULTIPLIER_START_DATE" VARCHAR(20) , 
                  "MULTIPLIER_END_DATE" VARCHAR(20) , 
                  "TOTAL_FAIR_SALES_FROM" VARCHAR(25) , 
                  "TOTAL_FAIR_SALES_TO" VARCHAR(25) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES_DATA_LOAD" IS 'TO store the IRS Threshold Bonus rules and it''s a import table.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES_DATA_LOAD"."IRC_VOUCHER_PROFIT_MULTIPLIER" IS 'Bonus points awarded based on the Fair Sales amount the number of times multiplier.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES_DATA_LOAD"."MULTIPLIER_END_DATE" IS 'To store the multiplier End date range.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES_DATA_LOAD"."MULTIPLIER_ID" IS 'It''s a Primary key uniquely generated from book Fair IT.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES_DATA_LOAD"."MULTIPLIER_START_DATE" IS 'To store the multiplier Start date range.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES_DATA_LOAD"."TOTAL_FAIR_SALES_FROM" IS 'Fair Sales range starting amount.';

COMMENT ON COLUMN "BKFAIR01"."IRC_THRESHOLD_BONUS_RULES_DATA_LOAD"."TOTAL_FAIR_SALES_TO" IS 'Fair Sales range ending amount.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_RECIPIENT_POSITION"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_RECIPIENT_POSITION"  (
                  "RECIPIENT_POSITION_ID" BIGINT NOT NULL , 
                  "RECIPIENT_POSITION_DESC" VARCHAR(50) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."LU_RECIPIENT_POSITION" IS 'TO store receipient_postions id and description.';

COMMENT ON COLUMN "BKFAIR01"."LU_RECIPIENT_POSITION"."RECIPIENT_POSITION_DESC" IS 'Receipient description.';

COMMENT ON COLUMN "BKFAIR01"."LU_RECIPIENT_POSITION"."RECIPIENT_POSITION_ID" IS 'RECEIPTINT_ID and Its a primary key column.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_RECIPIENT_POSITION"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_LU_RECIPT_POS" ON "BKFAIR01"."LU_RECIPIENT_POSITION" 
                ("RECIPIENT_POSITION_ID" ASC)
                PCTFREE 10 CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_RECIPIENT_POSITION"

ALTER TABLE "BKFAIR01"."LU_RECIPIENT_POSITION" 
        ADD CONSTRAINT "XPKC_LU_RECIPT_POS" PRIMARY KEY
                ("RECIPIENT_POSITION_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_HOMEPAGE_EVENT"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_HOMEPAGE_EVENT"  (
                  "EVENT_ID" BIGINT NOT NULL , 
                  "EVENT_PRODUCT_ID" VARCHAR(2) NOT NULL , 
                  "EVENT_START_DATE" DATE NOT NULL , 
                  "EVENT_END_DATE" DATE NOT NULL , 
                  "EVENT_NAME" VARCHAR(30) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."LU_HOMEPAGE_EVENT" IS 'TO store the homepage events data feed from book fair IT.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_EVENT"."CREATE_DATE" IS 'The record created date.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_EVENT"."EVENT_END_DATE" IS 'Event End Date.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_EVENT"."EVENT_ID" IS 'Event_id is composite primary key with Event_product_id.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_EVENT"."EVENT_NAME" IS 'Event Name.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_EVENT"."EVENT_PRODUCT_ID" IS 'Event_product_id is composite primary key with Event_id.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_EVENT"."EVENT_START_DATE" IS 'Event Start Date.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_EVENT"."UPDATE_DATE" IS 'The record last modified date.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_HOMEPAGE_EVENT"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_LU_HOPAGE_EVT" ON "BKFAIR01"."LU_HOMEPAGE_EVENT" 
                ("EVENT_ID" ASC,
                 "EVENT_PRODUCT_ID" ASC)
                PCTFREE 10 CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_HOMEPAGE_EVENT"

ALTER TABLE "BKFAIR01"."LU_HOMEPAGE_EVENT" 
        ADD CONSTRAINT "XPKC_LU_HOPAGE_EVT" PRIMARY KEY
                ("EVENT_ID",
                 "EVENT_PRODUCT_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_HOMEPAGE_EVENT_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_HOMEPAGE_EVENT_LOAD"  (
                  "EVENT_ID" VARCHAR(25) , 
                  "EVENT_PRODUCT_ID" VARCHAR(2) , 
                  "EVENT_START_DATE" VARCHAR(20) , 
                  "EVENT_END_DATE" VARCHAR(20) , 
                  "EVENT_NAME" VARCHAR(30) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."LU_HOMEPAGE_EVENT_LOAD" IS 'Data will be imported from book fair IT and this data will be inserted into lu_homepage_event table via stored procedure.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_SHARE_IDEA_DATA_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_SHARE_IDEA_DATA_LOAD"  (
                  "IDEA_TOPIC_ID" VARCHAR(10) , 
                  "IDEA_TOPIC" VARCHAR(30) , 
                  "IDEA_PRODUCT_ID" VARCHAR(2) , 
                  "IDEA_INCLUDE_FILE" VARCHAR(90) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."LU_SHARE_IDEA_DATA_LOAD" IS 'It''s a temp load table to insert data into the lookup table lu_share_idea via a stored procedure.';

COMMENT ON COLUMN "BKFAIR01"."LU_SHARE_IDEA_DATA_LOAD"."IDEA_TOPIC" IS 'Idea Topic text.';

COMMENT ON COLUMN "BKFAIR01"."LU_SHARE_IDEA_DATA_LOAD"."IDEA_TOPIC_ID" IS 'Idea_Topic_id and it'' primary key.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_SHARE_IDEA"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_SHARE_IDEA"  (
                  "IDEA_TOPIC_ID" INTEGER NOT NULL , 
                  "IDEA_TOPIC" VARCHAR(30) , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "IDEA_PRODUCT_ID" CHAR(2) NOT NULL , 
                  "IDEA_INCLUDE_FILE" CHAR(90) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."LU_SHARE_IDEA" IS 'TO store the share idea topic information. This table data has been populated by the Book fair IT data feed PROD_IDEA_CPW on production database';

COMMENT ON COLUMN "BKFAIR01"."LU_SHARE_IDEA"."CREATE_DATE" IS 'The record created date.';

COMMENT ON COLUMN "BKFAIR01"."LU_SHARE_IDEA"."IDEA_TOPIC" IS 'Idea Topic text.';

COMMENT ON COLUMN "BKFAIR01"."LU_SHARE_IDEA"."IDEA_TOPIC_ID" IS 'Idea_Topic_id and it'' primary key.';

COMMENT ON COLUMN "BKFAIR01"."LU_SHARE_IDEA"."UPDATE_DATE" IS 'The record last modified date.';


-- DDL Statements for primary key on Table "BKFAIR01"."LU_SHARE_IDEA"

ALTER TABLE "BKFAIR01"."LU_SHARE_IDEA" 
        ADD CONSTRAINT "XPKC_LU_SHARE_IDEA" PRIMARY KEY
                ("IDEA_TOPIC_ID",
                 "IDEA_PRODUCT_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FUEL_SURCHARGE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FUEL_SURCHARGE"  (
                  "SURCHARGE_ID" INTEGER NOT NULL , 
                  "SURCHARGE_START_DATE" DATE , 
                  "SURCHARGE_END_DATE" DATE , 
                  "BRANCH_ID" INTEGER , 
                  "FAIR_TYPE" VARCHAR(1) NOT NULL , 
                  "TOTAL_FAIR_SALES_FROM" BIGINT , 
                  "TOTAL_FAIR_SALES_TO" BIGINT , 
                  "SURCHARGE" INTEGER , 
                  "LAST_UPDATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."FUEL_SURCHARGE" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."FUEL_SURCHARGE"."FAIR_TYPE" IS '.';


-- DDL Statements for primary key on Table "BKFAIR01"."FUEL_SURCHARGE"

ALTER TABLE "BKFAIR01"."FUEL_SURCHARGE" 
        ADD CONSTRAINT "XPKC_FUEL_SURC_FS" PRIMARY KEY
                ("SURCHARGE_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FUEL_SURCHARGE_DATA_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FUEL_SURCHARGE_DATA_LOAD"  (
                  "SURCHARGE_ID" VARCHAR(25) , 
                  "SURCHARGE_START_DATE" VARCHAR(20) , 
                  "SURCHARGE_END_DATE" VARCHAR(20) , 
                  "BRANCH_ID" VARCHAR(20) , 
                  "FAIR_TYPE" VARCHAR(1) , 
                  "TOTAL_FAIR_SALES_FROM" VARCHAR(25) , 
                  "TOTAL_FAIR_SALES_TO" VARCHAR(25) , 
                  "SURCHARGE" VARCHAR(25) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."FUEL_SURCHARGE_DATA_LOAD" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."FUEL_SURCHARGE_DATA_LOAD"."FAIR_TYPE" IS '.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PROFIT_RULES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PROFIT_RULES"  (
                  "PROFIT_ID" BIGINT NOT NULL , 
                  "FAIR_TYPE" VARCHAR(1) NOT NULL , 
                  "DESCRIPTION" VARCHAR(20) , 
                  "TOTAL_FAIR_SALES_FROM" BIGINT , 
                  "TOTAL_FAIR_SALES_TO" INTEGER , 
                  "BOOK_PROFIT_PERCENTAGE" INTEGER , 
                  "IRC_PROFIT_PERCENTAGE" INTEGER , 
                  "CASH_PROFIT_PERCENTAGE" INTEGER , 
                  "PROFIT_START_DATE" DATE , 
                  "PROFIT_END_DATE" DATE , 
                  "MAX_BONUS_PERCENTAGE" INTEGER WITH DEFAULT 0 , 
                  "MAX_BONUS_INCREASE_PERCENTAGE" INTEGER WITH DEFAULT 0 , 
                  "BOOKING_BONUS_PERCENTAGE" INTEGER WITH DEFAULT 0 , 
                  "BOGO_BONUS_PERCENTAGE" INTEGER WITH DEFAULT 0 , 
                  "GROWTH_PERCENTAGE" SMALLINT , 
                  "SECOND_BOGO_BONUS_PERCENTAGE" INTEGER )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."PROFIT_RULES" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_RULES"."FAIR_TYPE" IS '.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_RULES"."SECOND_BOGO_BONUS_PERCENTAGE" IS 'Release 23.5 BTS; Percentage of second allowed BOGO.';


-- DDL Statements for primary key on Table "BKFAIR01"."PROFIT_RULES"

ALTER TABLE "BKFAIR01"."PROFIT_RULES" 
        ADD CONSTRAINT "XPKC_PROF_RUL_PR" PRIMARY KEY
                ("PROFIT_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PROFIT_RULES_DATA_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PROFIT_RULES_DATA_LOAD"  (
                  "PROFIT_ID" VARCHAR(25) , 
                  "FAIR_TYPE" VARCHAR(1) , 
                  "DESCRIPTION" VARCHAR(20) , 
                  "TOTAL_FAIR_SALES_FROM" VARCHAR(25) , 
                  "TOTAL_FAIR_SALES_TO" VARCHAR(25) , 
                  "BOOK_PROFIT_PERCENTAGE" VARCHAR(25) , 
                  "IRC_PROFIT_PERCENTAGE" VARCHAR(25) , 
                  "CASH_PROFIT_PERCENTAGE" VARCHAR(25) , 
                  "PROFIT_START_DATE" VARCHAR(20) , 
                  "PROFIT_END_DATE" VARCHAR(20) , 
                  "MAX_BONUS_PERCENTAGE" VARCHAR(25) , 
                  "MAX_BONUS_INCREASE_PERCENTAGE" VARCHAR(25) , 
                  "BOOKING_BONUS_PERCENTAGE" VARCHAR(25) , 
                  "BOGO_BONUS_PERCENTAGE" VARCHAR(25) , 
                  "GROWTH_PERCENTAGE" VARCHAR(6) , 
                  "SECOND_BOGO_BONUS_PERCENTAGE" VARCHAR(25) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."PROFIT_RULES_DATA_LOAD" IS ' .';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_RULES_DATA_LOAD"."FAIR_TYPE" IS '.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_RULES_DATA_LOAD"."SECOND_BOGO_BONUS_PERCENTAGE" IS 'Release 23.5 BTS; Percentage of second allowed BOGO.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."MYOB"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."MYOB"  (
                  "ID" BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (  
                    START WITH +1000  
                    INCREMENT BY +1  
                    MINVALUE +1000  
                    MAXVALUE +9223372036854775807  
                    NO CYCLE  
                    CACHE 20  
                    NO ORDER ) , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "XMLSTRING" VARCHAR(4000) , 
                  "SAVENAME" VARCHAR(150) , 
                  "DESCRIPTION" VARCHAR(150) , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF8K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON COLUMN "BKFAIR01"."MYOB"."CREATE_DATE" IS 'The create date and it defaults to the current timestamp.';

COMMENT ON COLUMN "BKFAIR01"."MYOB"."DESCRIPTION" IS 'Stores the description.';

COMMENT ON COLUMN "BKFAIR01"."MYOB"."ID" IS 'It''s  Primary Key column and is sequentially being generated starting at number 1000.';

COMMENT ON COLUMN "BKFAIR01"."MYOB"."SAVENAME" IS 'Holds the name saved.';

COMMENT ON COLUMN "BKFAIR01"."MYOB"."SCHOOL_ID" IS 'Holds the school id and is a foreing key to the id in the school table.';

COMMENT ON COLUMN "BKFAIR01"."MYOB"."UPDATE_DATE" IS 'The update date and it defaults to the current timestamp.';

COMMENT ON COLUMN "BKFAIR01"."MYOB"."XMLSTRING" IS 'Holds the xml string in an 8K pagesize.';


-- DDL Statements for primary key on Table "BKFAIR01"."MYOB"

ALTER TABLE "BKFAIR01"."MYOB" 
        ADD CONSTRAINT "XPKC_MYOB" PRIMARY KEY
                ("ID");



-- DDL Statements for indexes on Table "BKFAIR01"."MYOB"

CREATE INDEX "BKFAIR01"."X_MYOB_SCHOOL_ID" ON "BKFAIR01"."MYOB" 
                ("SCHOOL_ID" ASC)
                ALLOW REVERSE SCANS;
ALTER TABLE "BKFAIR01"."MYOB" ALTER COLUMN "ID" RESTART WITH 52819;

------------------------------------------------
-- DDL Statements for table "BKFAIR01"."MAX_FAIR_HISTORY_PRT"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."MAX_FAIR_HISTORY_PRT"  (
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "PREVIOUS_FAIR_ID" BIGINT NOT NULL , 
                  "HISTORY_FAIR_DATE" DATE NOT NULL , 
                  "HISTORY_FAIR_SALES" DECIMAL(10,2) NOT NULL , 
                  "HISTORY_FAIR_TYPE" VARCHAR(1) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "ONLINE_FAIR_SALES" DECIMAL(10,2) , 
                  "STORIA_SALES" DECIMAL(10,2) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_PRT"."HISTORY_FAIR_DATE" IS 'Holds the history fair date.';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_PRT"."HISTORY_FAIR_SALES" IS 'Holds the fair sales.';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_PRT"."HISTORY_FAIR_TYPE" IS 'ckeck for C  B  P X R U';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_PRT"."PREVIOUS_FAIR_ID" IS 'It''s part Primary Key columns';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_PRT"."SCHOOL_ID" IS 'It''s part Primary Key columns';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_PRT"."STORIA_SALES" IS 'Release 23.5 BTS; Dollar amount collected from Storia sales stores.';


-- DDL Statements for primary key on Table "BKFAIR01"."MAX_FAIR_HISTORY_PRT"

ALTER TABLE "BKFAIR01"."MAX_FAIR_HISTORY_PRT" 
        ADD CONSTRAINT "XPKC_MAX_FR_HI_PRT" PRIMARY KEY
                ("SCHOOL_ID",
                 "PREVIOUS_FAIR_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."MAX_FAIR_HISTORY_PRT_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."MAX_FAIR_HISTORY_PRT_LOAD"  (
                  "SCHOOL_ID" VARCHAR(50) , 
                  "PREVIOUS_FAIR_ID" VARCHAR(50) , 
                  "HISTORY_FAIR_DATE" VARCHAR(20) , 
                  "HISTORY_FAIR_SALES" VARCHAR(20) , 
                  "HISTORY_FAIR_TYPE" VARCHAR(1) , 
                  "ONLINE_FAIR_SALES" VARCHAR(20) , 
                  "STORIA_SALES" VARCHAR(20) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"; 

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_PRT_LOAD"."STORIA_SALES" IS 'Release 23.5 BTS; Dollar amount collected from Storia sales stores.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."MAX_FAIR_HISTORY_CHLD_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."MAX_FAIR_HISTORY_CHLD_LOAD"  (
                  "PREVIOUS_FAIR_ID" VARCHAR(50) , 
                  "HISTORY_SEQUENCE_NO" VARCHAR(20) , 
                  "HISTORY_FAIR_DESCRIPTION" VARCHAR(30) , 
                  "HISTORY_FAIR_VALUE" VARCHAR(50) , 
                  "EARNINGS_TYPE" VARCHAR(1) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_CHLD_LOAD"."HISTORY_FAIR_DESCRIPTION" IS 'Holds the history fair description.';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_CHLD_LOAD"."HISTORY_FAIR_VALUE" IS 'Holds the history fair value';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_CHLD_LOAD"."HISTORY_SEQUENCE_NO" IS 'It''s part Primary Key columns';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_CHLD_LOAD"."PREVIOUS_FAIR_ID" IS 'It''s part Primary Key columns';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PROFIT_BALANCE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PROFIT_BALANCE"  (
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "REWARD_DESCRIPTION" VARCHAR(30) , 
                  "VOUCHER_ISSUE_DATE" DATE , 
                  "VOUCHER_TYPE" VARCHAR(1) , 
                  "VOUCHER_EXPIRATION_DATE" DATE , 
                  "VOUCHER_AMOUNT" DECIMAL(10,2) WITH DEFAULT 0.00 , 
                  "BOOK_PROFIT" DECIMAL(10,2) WITH DEFAULT 0.00 , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."PROFIT_BALANCE" IS 'TO store the Profit Balance informations.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE"."BOOK_PROFIT" IS 'Book Profit Amount.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE"."REWARD_DESCRIPTION" IS 'the description';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE"."SCHOOL_ID" IS 'It''s School_Id.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE"."VOUCHER_AMOUNT" IS 'Voucher Amount.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE"."VOUCHER_EXPIRATION_DATE" IS 'Voucher Expiration Date.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE"."VOUCHER_ISSUE_DATE" IS 'Voucher Issue Date.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE"."VOUCHER_TYPE" IS 'Voucher Type and possible values are (P (Profit), R (Reward), O (Other) ,and NULL).';






-- DDL Statements for indexes on Table "BKFAIR01"."PROFIT_BALANCE"

CREATE INDEX "BKFAIR01"."IDX_REWARDDESC" ON "BKFAIR01"."PROFIT_BALANCE" 
                ("REWARD_DESCRIPTION" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."PROFIT_BALANCE"

CREATE INDEX "BKFAIR01"."IDX_SCH_ID" ON "BKFAIR01"."PROFIT_BALANCE" 
                ("SCHOOL_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD"  (
                  "SCHOOL_ID" VARCHAR(25) , 
                  "REWARD_DESCRIPTION" VARCHAR(30) , 
                  "VOUCHER_ISSUE_DATE" VARCHAR(20) , 
                  "VOUCHER_TYPE" VARCHAR(1) , 
                  "VOUCHER_EXPIRATION_DATE" VARCHAR(20) , 
                  "VOUCHER_AMOUNT" VARCHAR(11) , 
                  "BOOK_PROFIT" VARCHAR(11) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"; 

COMMENT ON TABLE "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD" IS 'TO store the Profit Balance informations.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD"."BOOK_PROFIT" IS 'Book Profit Amount.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD"."REWARD_DESCRIPTION" IS 'Description.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD"."SCHOOL_ID" IS 'It''s School_Id.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD"."VOUCHER_AMOUNT" IS 'Voucher Amount.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD"."VOUCHER_EXPIRATION_DATE" IS 'Voucher Expiration Date.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD"."VOUCHER_ISSUE_DATE" IS 'Voucher Issue Date.';

COMMENT ON COLUMN "BKFAIR01"."PROFIT_BALANCE_DATA_LOAD"."VOUCHER_TYPE" IS 'Voucher Type and possible values are (P (Profit), R (Reward), O (Other) ,and NULL).';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_COA_FILE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_COA_FILE"  (
                  "COA_ID" BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (  
                    START WITH +1  
                    INCREMENT BY +1  
                    MINVALUE +1  
                    MAXVALUE +9223372036854775807  
                    NO CYCLE  
                    CACHE 20  
                    NO ORDER ) , 
                  "COA_FAIR_TYPE" VARCHAR(1) NOT NULL , 
                  "COA_START_DATE" DATE NOT NULL , 
                  "COA_END_DATE" DATE NOT NULL , 
                  "COA_FILE_NAME" VARCHAR(30) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_COA_FILE"

ALTER TABLE "BKFAIR01"."LU_COA_FILE" 
        ADD CONSTRAINT "XPKC_LU_COA_PR" PRIMARY KEY
                ("COA_FAIR_TYPE",
                 "COA_START_DATE");


ALTER TABLE "BKFAIR01"."LU_COA_FILE" ALTER COLUMN "COA_ID" RESTART WITH 120;

------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_COA_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_COA_LOAD"  (
                  "COA_FAIR_TYPE" VARCHAR(1) , 
                  "COA_START_DATE" VARCHAR(30) , 
                  "COA_END_DATE" VARCHAR(30) , 
                  "COA_FILE_NAME" VARCHAR(30) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_PROGRAMS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_PROGRAMS"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_PROGRAMS"

ALTER TABLE "BKFAIR01"."LU_PROGRAMS" 
        ADD CONSTRAINT "PK_LU_PROGRAM" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_AD_PROMOTE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_AD_PROMOTE"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_AD_PROMOTE"

ALTER TABLE "BKFAIR01"."LU_AD_PROMOTE" 
        ADD CONSTRAINT "PK_LU_AD_PROMO" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_VOLN_STU_CREW"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_VOLN_STU_CREW"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_VOLN_STU_CREW"

ALTER TABLE "BKFAIR01"."LU_VOLN_STU_CREW" 
        ADD CONSTRAINT "PK_LU_VSTU_CREW" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_FORMS_TOOLS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_FORMS_TOOLS"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_FORMS_TOOLS"

ALTER TABLE "BKFAIR01"."LU_FORMS_TOOLS" 
        ADD CONSTRAINT "PK_LU_FTOOLS" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_THEMES_DECOR"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_THEMES_DECOR"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_THEMES_DECOR"

ALTER TABLE "BKFAIR01"."LU_THEMES_DECOR" 
        ADD CONSTRAINT "PK_LU_THEMEDECOR" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_CONTESTS_ACTIVITIES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_CONTESTS_ACTIVITIES"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_CONTESTS_ACTIVITIES"

ALTER TABLE "BKFAIR01"."LU_CONTESTS_ACTIVITIES" 
        ADD CONSTRAINT "PK_LU_CONTESTS" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_TYPE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_TYPE"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_TYPE"

ALTER TABLE "BKFAIR01"."LU_TYPE" 
        ADD CONSTRAINT "PK_LU_TYPE" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_LANGUAGE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_LANGUAGE"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_LANGUAGE"

ALTER TABLE "BKFAIR01"."LU_LANGUAGE" 
        ADD CONSTRAINT "PK_LU_LANG" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_GRADE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_GRADE"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_GRADE"

ALTER TABLE "BKFAIR01"."LU_GRADE" 
        ADD CONSTRAINT "PK_LU_GRADE" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_STYLE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_STYLE"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_STYLE"

ALTER TABLE "BKFAIR01"."LU_STYLE" 
        ADD CONSTRAINT "PK_LU_STYLE" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_FORMAT"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_FORMAT"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."LU_FORMAT"

ALTER TABLE "BKFAIR01"."LU_FORMAT" 
        ADD CONSTRAINT "PK_LU_FORMAT" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."BRANCH"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."BRANCH"  (
                  "ID" INTEGER NOT NULL , 
                  "DESC" VARCHAR(255) , 
                  "REGION_ID" VARCHAR(2) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."BRANCH"

ALTER TABLE "BKFAIR01"."BRANCH" 
        ADD CONSTRAINT "PK_BRANCH" PRIMARY KEY
                ("ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."USERS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."USERS"  (
                  "USER_ID" VARCHAR(255) NOT NULL , 
                  "PASSWORD" VARCHAR(255) , 
                  "FNAME" VARCHAR(255) , 
                  "LNAME" VARCHAR(255) , 
                  "EMAIL" VARCHAR(255) , 
                  "ROLE" VARCHAR(255) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."USERS"

ALTER TABLE "BKFAIR01"."USERS" 
        ADD CONSTRAINT "PK_USERS_ID" PRIMARY KEY
                ("USER_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."ASSET_AUDITTRAIL"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."ASSET_AUDITTRAIL"  (
                  "ASSET_ID" INTEGER NOT NULL , 
                  "USER_ID" VARCHAR(255) , 
                  "OPERATION" VARCHAR(255) , 
                  "OP_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."ASSET"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."ASSET"  (
                  "ASSET_ID" INTEGER NOT NULL , 
                  "FILE_NAME" VARCHAR(255) NOT NULL , 
                  "TITLE" VARCHAR(255) NOT NULL , 
                  "REGION_ID" VARCHAR(255) , 
                  "BRANCH_ID" VARCHAR(1000) , 
                  "PRODUCT_ID" VARCHAR(255) , 
                  "FAIR_ID" VARCHAR(1000) , 
                  "FORMAT" INTEGER , 
                  "HYPERLINK" VARCHAR(255) , 
                  "START_DATE" DATE NOT NULL , 
                  "END_DATE" DATE NOT NULL , 
                  "IMAGE" VARCHAR(255) , 
                  "IMAGE2" VARCHAR(255) , 
                  "LIFESPAN" VARCHAR(25) , 
                  "GB_ELEMENTARY_PG" VARCHAR(25) , 
                  "GB_MS_PG" VARCHAR(25) , 
                  "GB_BOGO_PG" VARCHAR(25) , 
                  "DESCRIPTION" VARCHAR(255) , 
                  "DESCRIPTION2" VARCHAR(255) , 
                  "PROGRAMS" VARCHAR(255) , 
                  "AD_PROMOTE" VARCHAR(255) , 
                  "VOLN_STU_CREW" VARCHAR(255) , 
                  "FORMS_TOOLS" VARCHAR(255) , 
                  "THEMES_DECOR" VARCHAR(255) , 
                  "CONTESTS_ACTIVITIES" VARCHAR(255) , 
                  "TYPE" VARCHAR(255) , 
                  "LANGUAGE" VARCHAR(255) , 
                  "GRADE" VARCHAR(255) , 
                  "STYLE" VARCHAR(255) , 
                  "COLLECTION_ID" VARCHAR(5) , 
                  "PRIORITY" SMALLINT WITH DEFAULT 9999 )   
                 IN "BKF8K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON COLUMN "BKFAIR01"."ASSET"."PRIORITY" IS 'The smallest value is the highest priority.';


-- DDL Statements for primary key on Table "BKFAIR01"."ASSET"

ALTER TABLE "BKFAIR01"."ASSET" 
        ADD CONSTRAINT "PK_ASSET_ID" PRIMARY KEY
                ("ASSET_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."MY_FAIR_SELECTION"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."MY_FAIR_SELECTION"  (
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "ASSET_ID" INTEGER NOT NULL , 
                  "CREATION_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP )   
                  IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for primary key on Table "BKFAIR01"."MY_FAIR_SELECTION"

ALTER TABLE "BKFAIR01"."MY_FAIR_SELECTION" 
        ADD CONSTRAINT "XPKC_SCHOOL_ASSET_MYFAIR" PRIMARY KEY
                ("SCHOOL_ID",
                 "ASSET_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."MAX_FAIR_HISTORY_CHLD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."MAX_FAIR_HISTORY_CHLD"  (
                  "PREVIOUS_FAIR_ID" BIGINT NOT NULL , 
                  "HISTORY_SEQUENCE_NO" INTEGER NOT NULL , 
                  "HISTORY_FAIR_DESCRIPTION" VARCHAR(30) , 
                  "HISTORY_FAIR_VALUE" DECIMAL(10,2) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP , 
                  "EARNINGS_TYPE" CHAR(1) NOT NULL )   
                  IN "BKF4K01D" INDEX IN "BKF4K01I"; 

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_CHLD"."EARNINGS_TYPE" IS 'S-in-school fair profit, O-Online fair profit, R-Reward Bonus';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_CHLD"."HISTORY_FAIR_DESCRIPTION" IS 'Holds the history fair description.';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_CHLD"."HISTORY_FAIR_VALUE" IS 'Holds the history fair value';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_CHLD"."HISTORY_SEQUENCE_NO" IS 'It''s part Primary Key columns';

COMMENT ON COLUMN "BKFAIR01"."MAX_FAIR_HISTORY_CHLD"."PREVIOUS_FAIR_ID" IS 'It''s part Primary Key columns';


-- DDL Statements for primary key on Table "BKFAIR01"."MAX_FAIR_HISTORY_CHLD"

ALTER TABLE "BKFAIR01"."MAX_FAIR_HISTORY_CHLD" 
        ADD CONSTRAINT "XPKC_MX_FR_HI_CHLD" PRIMARY KEY
                ("PREVIOUS_FAIR_ID",
                 "HISTORY_SEQUENCE_NO");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."TRANSACTION_OPEN_BALANCE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."TRANSACTION_OPEN_BALANCE"  (
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "OPENING_BALANCE_DATE" DATE NOT NULL , 
                  "OPENING_BALANCE_AMOUNT" DECIMAL(10,2) NOT NULL )   
                  IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."TRANSACTION_OPEN_BALANCE" IS 'Release 18, May 2011; Provides School Open Balance data. ';

COMMENT ON COLUMN "BKFAIR01"."TRANSACTION_OPEN_BALANCE"."OPENING_BALANCE_AMOUNT" IS 'The beginning Scholastic dollar balance for the transactions';

COMMENT ON COLUMN "BKFAIR01"."TRANSACTION_OPEN_BALANCE"."OPENING_BALANCE_DATE" IS 'The date of the opening balance amount';

COMMENT ON COLUMN "BKFAIR01"."TRANSACTION_OPEN_BALANCE"."SCHOOL_ID" IS 'The primary ID that represents a School';


-- DDL Statements for primary key on Table "BKFAIR01"."TRANSACTION_OPEN_BALANCE"

ALTER TABLE "BKFAIR01"."TRANSACTION_OPEN_BALANCE" 
        ADD CONSTRAINT "XPK_TRANSACOPENBAL_SCHOOL_ID" PRIMARY KEY
                ("SCHOOL_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."TRANSACTION_DETAILS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."TRANSACTION_DETAILS"  (
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "TRANSACTION_DATE" DATE NOT NULL , 
                  "TRANSACTION_DESCRIPTION" VARCHAR(41) NOT NULL , 
                  "ISSUED_AMOUNT" DECIMAL(10,2) NOT NULL , 
                  "REDEEMED_AMOUNT" DECIMAL(10,2) NOT NULL , 
                  "DAILY_BALANCED_AMOUNT" DECIMAL(10,2) NOT NULL )   
                  IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."TRANSACTION_DETAILS" IS 'Release 18, May 2011; Provides renewed daily detailed transaction records for a given School. ';

COMMENT ON COLUMN "BKFAIR01"."TRANSACTION_DETAILS"."DAILY_BALANCED_AMOUNT" IS 'The daily Scholastic dollar balance';

COMMENT ON COLUMN "BKFAIR01"."TRANSACTION_DETAILS"."ISSUED_AMOUNT" IS 'The Scholastic dollar value issued for the transaction';

COMMENT ON COLUMN "BKFAIR01"."TRANSACTION_DETAILS"."REDEEMED_AMOUNT" IS 'The Scholastic dollar value redeemed for the transaction';

COMMENT ON COLUMN "BKFAIR01"."TRANSACTION_DETAILS"."SCHOOL_ID" IS 'The primary ID that represents a School';

COMMENT ON COLUMN "BKFAIR01"."TRANSACTION_DETAILS"."TRANSACTION_DATE" IS 'The date that a transaction occurred';

COMMENT ON COLUMN "BKFAIR01"."TRANSACTION_DETAILS"."TRANSACTION_DESCRIPTION" IS 'The description of the transaction';






-- DDL Statements for indexes on Table "BKFAIR01"."TRANSACTION_DETAILS"

CREATE INDEX "BKFAIR01"."X_TRANSACDETAILS" ON "BKFAIR01"."TRANSACTION_DETAILS" 
                ("SCHOOL_ID" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."MAINT_BKFAIR_TASKS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."MAINT_BKFAIR_TASKS"  (
                  "TASK_ID" INTEGER NOT NULL , 
                  "CREATE_TIMESTAMP" TIMESTAMP NOT NULL , 
                  "TASK_NAME" VARCHAR(50) NOT NULL , 
                  "SCRIPT_NAME" VARCHAR(80) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."MAINT_BKFAIR_TASKS" IS 'Release 18, May 2011; Registers Tasks and the time at which they ran. ';

COMMENT ON COLUMN "BKFAIR01"."MAINT_BKFAIR_TASKS"."CREATE_TIMESTAMP" IS 'Timestamp at which the task has run.';

COMMENT ON COLUMN "BKFAIR01"."MAINT_BKFAIR_TASKS"."TASK_ID" IS 'Sequential number of the task.  Keep an automatic count as to how many times tasks have been running.';

COMMENT ON COLUMN "BKFAIR01"."MAINT_BKFAIR_TASKS"."TASK_NAME" IS 'Name of the task that has run; the writing of these names should be kept identical when run repetitevely to avoid creating an additional table.';






-- DDL Statements for indexes on Table "BKFAIR01"."MAINT_BKFAIR_TASKS"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_MAINTBKFAIRTASK" ON "BKFAIR01"."MAINT_BKFAIR_TASKS" 
                ("CREATE_TIMESTAMP" ASC,
                 "TASK_NAME" ASC)
                ALLOW REVERSE SCANS;




------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_SPECIAL_PROGRAMS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_SPECIAL_PROGRAMS"  (
                  "PRODUCT_ID" VARCHAR(2) NOT NULL , 
                  "SPECIAL_PROGRAM_INCLUDE" VARCHAR(90) , 
                  "SPECIAL_PROGRAM_INCLUDE_2" VARCHAR(90) , 
                  "SPECIAL_PROGRAM_INCLUDE_3" VARCHAR(90) , 
                  "SPECIAL_PROGRAM_INCLUDE_4" VARCHAR(90) , 
                  "SPECIAL_PROGRAM_INCLUDE_DESCRIPTION" VARCHAR(400) , 
                  "SPECIAL_PROGRAM_INCLUDE_2_DESCRIPTION" VARCHAR(400) , 
                  "SPECIAL_PROGRAM_INCLUDE_3_DESCRIPTION" VARCHAR(400) , 
                  "SPECIAL_PROGRAM_INCLUDE_4_DESCRIPTION" VARCHAR(400) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."LU_SPECIAL_PROGRAMS" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."LU_SPECIAL_PROGRAMS" IS 'Release 19, September 2011; Special_Programs features have been extended with description columns in Release-19.';


-- DDL Statements for primary key on Table "BKFAIR01"."LU_SPECIAL_PROGRAMS"

ALTER TABLE "BKFAIR01"."LU_SPECIAL_PROGRAMS" 
        ADD CONSTRAINT "XPK_LUSPECIALPROGRAMS__PRODID" PRIMARY KEY
                ("PRODUCT_ID");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."NEW_FAIRS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."NEW_FAIRS"  (
                  "ID" BIGINT NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"; 

COMMENT ON TABLE "BKFAIR01"."NEW_FAIRS" IS 'Release 19, October 2011; Stores the new Fair created via import file upon loading them into the table FAIR and a trigger.';

COMMENT ON COLUMN "BKFAIR01"."NEW_FAIRS"."ID" IS 'Tracks the new Fair ID.';






-- DDL Statements for indexes on Table "BKFAIR01"."NEW_FAIRS"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_NEW_FAIRS__ID" ON "BKFAIR01"."NEW_FAIRS" 
                ("ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."ONLINE_HOMEPAGE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."ONLINE_HOMEPAGE"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CONTACT_PERSON_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "CONTACT_EMAIL_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "CONTACT_PHONE_NUMBER_CKBOX" VARCHAR(1) WITH DEFAULT 'N' , 
                  "PAYMENT_CHECK_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "PAYMENT_CC_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "PAYMENT_CASH_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "VOLUNTEER_RECRUITMENT_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "BOOK_FAIR_GOAL_CKBOX" VARCHAR(1) WITH DEFAULT 'N' , 
                  "SPECIAL_PROGRAM_CKBOX" VARCHAR(1) WITH DEFAULT 'N' , 
                  "SPECIAL_PROGRAM_CKBOX2" VARCHAR(1) WITH DEFAULT 'N' , 
                  "SPECIAL_PROGRAM_CKBOX3" CHAR(1) WITH DEFAULT 'N' , 
                  "SPECIAL_PROGRAM_CKBOX4" CHAR(1) WITH DEFAULT 'N' , 
                  "OFE_FEATURE_CKBOX" CHAR(1) NOT NULL WITH DEFAULT 'N' , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "SELECTED_GOAL" VARCHAR(20) WITH DEFAULT 'NUMBEROFBOOKS' , 
                  "FAIR_START_DATE" DATE , 
                  "FAIR_END_DATE" DATE , 
                  "GOAL_AMOUNT" DECIMAL(8,2) WITH DEFAULT  , 
                  "ACTUAL_AMOUNT" DECIMAL(8,2) WITH DEFAULT  , 
                  "SCHOOL_STATE" VARCHAR(10) , 
                  "SCHOOL_POSTALCODE" VARCHAR(10) , 
                  "FAIR_LOCATION" VARCHAR(30) , 
                  "VOLUNTEER_HEADING" VARCHAR(30) , 
                  "WELCOME_HEADING" VARCHAR(30) , 
                  "CONTACT_PHONE_NUMBER" VARCHAR(40) , 
                  "SCHOOL_CITY" VARCHAR(50) , 
                  "CONTACT_FIRST_NAME" VARCHAR(70) , 
                  "CONTACT_LAST_NAME" VARCHAR(70) , 
                  "PRIMARY_EMAIL" VARCHAR(70) , 
                  "SECONDARY_EMAIL" VARCHAR(70) , 
                  "SECONDARY_EMAIL2" VARCHAR(70) , 
                  "SECONDARY_EMAIL3" VARCHAR(70) , 
                  "SCHOOL_NAME" VARCHAR(200) , 
                  "CONTACT_EMAIL" VARCHAR(200) , 
                  "SCHOOL_ADDRESS1" VARCHAR(255) , 
                  "VOLUNTEER_MESSAGE" VARCHAR(450) , 
                  "GOAL_PURPOSE" VARCHAR(500) , 
                  "WELCOME_MESSAGE" VARCHAR(1000) , 
                  "THANK_YOU_MESSAGE" VARCHAR(550) , 
                  "SPECIAL_PROGRAM_INCLUDE_DESCRIPTION" VARCHAR(400) , 
                  "SPECIAL_PROGRAM_INCLUDE_2_DESCRIPTION" VARCHAR(400) , 
                  "SPECIAL_PROGRAM_INCLUDE_3_DESCRIPTION" VARCHAR(400) , 
                  "SPECIAL_PROGRAM_INCLUDE_4_DESCRIPTION" VARCHAR(400) , 
                  "CHECK_DESCRIPTION" VARCHAR(50) WITH DEFAULT '(Payable to the school)' , 
                  "PRINCIPAL_DISPLAY_CKBOX" CHAR(1) WITH DEFAULT 'N' , 
                  "PRINCIPAL_HEADING" VARCHAR(35) , 
                  "PRINCIPAL_MESSAGE" VARCHAR(500) )   
                 IN "BKF8K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."ONLINE_HOMEPAGE" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."ONLINE_HOMEPAGE" IS 'Release 19, September 2011; Stores the PUBLISHED Online Homepage Fair information.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."ACTUAL_AMOUNT" IS 'The Actual amount.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."BOOK_FAIR_GOAL_CKBOX" IS 'Is it book fair Goal amout Y/N.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."CHECK_DESCRIPTION" IS 'Description field offered to the customer to provides details on the Check to be drawn.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."CONTACT_EMAIL" IS 'To store the contact person email.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."CONTACT_EMAIL_CKBOX" IS 'To store the contact person email check box value Y(yes) or N(No) .';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."CONTACT_FIRST_NAME" IS 'To store the contact person firstname .';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."CONTACT_LAST_NAME" IS 'To store the contact person lastname.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."CONTACT_PERSON_CKBOX" IS 'To store the contact person check box value Y(yes) or N(No) .';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."CONTACT_PHONE_NUMBER" IS 'To store the contact person phone number .';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."CONTACT_PHONE_NUMBER_CKBOX" IS 'To store the contact person phone number check box value Y(yes) or N(No) .';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."CREATE_DATE" IS 'The record created date.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."FAIR_END_DATE" IS 'To store the Physical fair End date.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."FAIR_ID" IS 'Fair_id and Its a primary key column.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."FAIR_LOCATION" IS 'To stores the Physical fair location.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."FAIR_START_DATE" IS 'To store Physical fair start date';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."GOAL_AMOUNT" IS 'Chairperson goad amounts.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."PAYMENT_CASH_CKBOX" IS ' To store the Payment Cash Y/N.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."PAYMENT_CC_CKBOX" IS 'To store the Payment type credit cards check box value Y(yes) or N(No) .';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."PAYMENT_CHECK_CKBOX" IS 'To store the Payment type check check box valueY(yes) or N(No) .';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."PRINCIPAL_DISPLAY_CKBOX" IS 'Release 21; allow (Y) or disallow (N) Principal Activity Heading and Message on the Homepage.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."PRINCIPAL_HEADING" IS 'Release 21; a gloabal Heading text for the Principal Activities.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."PRINCIPAL_MESSAGE" IS 'Release 21; a global Message text for the Principal Activities.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SCHOOL_ADDRESS1" IS 'To store the School address line1.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SCHOOL_CITY" IS 'To store the school city name.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SCHOOL_NAME" IS 'To store the School Name.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SCHOOL_POSTALCODE" IS 'To store the school zip code (postal code).';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SCHOOL_STATE" IS 'To store the school state name.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SELECTED_GOAL" IS 'Chairperson selected Goal amount e.g. NUMBEROFBOOKS/ DOLLARAMOUNT.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_CKBOX" IS 'Special progaram Y/N.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_INCLUDE_2_DESCRIPTION" IS 'Release 19, September 2011; Selected from LU_SPECIAL_PROGRAMS based on the PRODUCT_ID in FAIR table.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_INCLUDE_3_DESCRIPTION" IS 'Release 19, September 2011; Selected from LU_SPECIAL_PROGRAMS based on the PRODUCT_ID in FAIR table.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_INCLUDE_4_DESCRIPTION" IS 'Release 19, September 2011; Selected from LU_SPECIAL_PROGRAMS based on the PRODUCT_ID in FAIR table.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_INCLUDE_DESCRIPTION" IS 'Release 19, September 2011; Selected from LU_SPECIAL_PROGRAMS based on the PRODUCT_ID in FAIR table.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."THANK_YOU_MESSAGE" IS 'To store the chairperson thank you message subject.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."UPDATE_DATE" IS 'The record last modified date.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."VOLUNTEER_HEADING" IS 'To store the chairperson volunteer message heading.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."VOLUNTEER_MESSAGE" IS 'To store the chairperson volunter welcome message subject.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."VOLUNTEER_RECRUITMENT_CKBOX" IS 'To store the valunteer requirtment Y/N.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."WELCOME_HEADING" IS 'To store the chairperson welcome message heading.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_HOMEPAGE"."WELCOME_MESSAGE" IS 'To store the chairperson welcome message subject.';


-- DDL Statements for primary key on Table "BKFAIR01"."ONLINE_HOMEPAGE"

ALTER TABLE "BKFAIR01"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "XPK_ONLINEHOMEPAGE_FAIR_ID" PRIMARY KEY
                ("FAIR_ID");



-- DDL Statements for indexes on Table "BKFAIR01"."ONLINE_HOMEPAGE"

CREATE INDEX "BKFAIR01"."X2ONLINE_HOMEPAGE" ON "BKFAIR01"."ONLINE_HOMEPAGE" 
                ("FAIR_START_DATE" ASC,
                 "FAIR_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."ONLINE_HOMEPAGE"

CREATE INDEX "BKFAIR01"."X3ONLINE_HOMEPAGE" ON "BKFAIR01"."ONLINE_HOMEPAGE" 
                ("SCHOOL_STATE" ASC,
                 "FAIR_START_DATE" ASC,
                 "FAIR_ID" ASC,
                 "SCHOOL_CITY" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."ONLINE_HOMEPAGE"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_ONLINEHOMEPAGE__FAIRID_STARTDATE" ON "BKFAIR01"."ONLINE_HOMEPAGE" 
                ("FAIR_ID" ASC,
                 "FAIR_START_DATE" ASC,
                 "SCHOOL_STATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."ONLINE_HOMEPAGE"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_ONLINEHOMEPAGE_ENDATE" ON "BKFAIR01"."ONLINE_HOMEPAGE" 
                ("FAIR_ID" ASC,
                 "FAIR_END_DATE" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."HOMEPAGE_URL"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."HOMEPAGE_URL"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "FAIR_FINDER_CKBOX" CHAR(1) WITH DEFAULT 'Y' , 
                  "UPDATE_FLAG" CHAR(1) WITH DEFAULT 'N' , 
                  "URL_VISITED_COUNT" BIGINT WITH DEFAULT  , 
                  "PUBLISHED_DATE" TIMESTAMP , 
                  "PUBLISHED_STATUS" VARCHAR(25) NOT NULL , 
                  "URL_STATUS" VARCHAR(25) NOT NULL , 
                  "WEB_URL" VARCHAR(35) NOT NULL , 
                  "REPUBLISHED_DATE" TIMESTAMP , 
                  "STR_LATLON" VARCHAR(50) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."HOMEPAGE_URL" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."HOMEPAGE_URL" IS 'Release 19, September 2011; Stores the Online_homepage URL information.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."CREATE_DATE" IS 'The record created date.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."FAIR_FINDER_CKBOX" IS 'Release 19 on Sept. 2011; a value of Y offers the possibility for Parents to find a closest Fair in the State respective to the school.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."FAIR_ID" IS 'Fair_id and Its a primary key column and fair_id reference Fair.fair_id becuase we need to keep data on this table even the hompage table data was deleted.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."PUBLISHED_STATUS" IS 'To store page published status like (PUBLISHED,REMOVED,EXPIRED) and etc.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."REPUBLISHED_DATE" IS 'Released 19.5; Capture the timestamp when a URL is being republished.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."SCHOOL_ID" IS 'To store the School_id.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."UPDATE_DATE" IS 'The record last modified date.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."UPDATE_FLAG" IS 'Release 19 on Sept. 2011; Stores a value Y for a record that has been updated by the Chairperson but not yet published; this value is defaulted to N.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."URL_STATUS" IS 'To store the url status like (AVAILABLE,NOTAVAILABLE,RESERVED) and etc.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_URL"."WEB_URL" IS 'To store the chairperson homepage url name.';


-- DDL Statements for primary key on Table "BKFAIR01"."HOMEPAGE_URL"

ALTER TABLE "BKFAIR01"."HOMEPAGE_URL" 
        ADD CONSTRAINT "XPK_HOMEPAGE_URL__FAIR_ID" PRIMARY KEY
                ("FAIR_ID");



-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE INDEX "BKFAIR01"."X_HOMEPAGE_URL__PUBLISHED_AND_URL" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("PUBLISHED_STATUS" ASC,
                 "URL_STATUS" ASC)
                DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE INDEX "BKFAIR01"."X_HOMEPAGE_URL__REPUB_DATE" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("REPUBLISHED_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE INDEX "BKFAIR01"."X_HOMEPAGE_URL__SCHOOL_5_COLS" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("SCHOOL_ID" ASC,
                 "URL_STATUS" ASC,
                 "FAIR_ID" ASC,
                 "WEB_URL" ASC,
                 "UPDATE_DATE" ASC)
                DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE INDEX "BKFAIR01"."X_HOMEPAGE_URL__SCHOOL_ID" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("SCHOOL_ID" ASC,
                 "WEB_URL" ASC,
                 "URL_STATUS" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE INDEX "BKFAIR01"."X_HOMEPAGE_URL__SCHOOL_URL" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("SCHOOL_ID" ASC,
                 "URL_STATUS" ASC)
                DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE INDEX "BKFAIR01"."X_HOMEPAGE_URL__URL_6_COLS" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("WEB_URL" ASC,
                 "URL_STATUS" ASC,
                 "FAIR_ID" ASC,
                 "SCHOOL_ID" ASC,
                 "PUBLISHED_STATUS" ASC,
                 "UPDATE_DATE" ASC)
                DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE INDEX "BKFAIR01"."X_HOMEPAGEURL_URL_AND_STATUS" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("WEB_URL" ASC,
                 "URL_STATUS" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE INDEX "BKFAIR01"."X2HOMEPAGE_URL" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("FAIR_FINDER_CKBOX" ASC,
                 "PUBLISHED_STATUS" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE INDEX "BKFAIR01"."X4HOMEPAGE_URL" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("FAIR_FINDER_CKBOX" ASC,
                 "PUBLISHED_STATUS" ASC,
                 "URL_STATUS" ASC,
                 "FAIR_ID" ASC)
                PCTFREE 10 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_HOMEPAGE_URL__SCHOOL_ID_FAIR_ID" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("SCHOOL_ID" ASC,
                 "FAIR_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_URL"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_HOMEPAGEURL__PUBLISHED_URL_FINDER_FAIR_ID" ON "BKFAIR01"."HOMEPAGE_URL" 
                ("PUBLISHED_STATUS" ASC,
                 "FAIR_FINDER_CKBOX" ASC,
                 "URL_STATUS" ASC,
                 "FAIR_ID" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."ONLINE_SHOPPING"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."ONLINE_SHOPPING"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "OFE_START_DATE" DATE , 
                  "OFE_END_DATE" DATE , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATED_DATE" TIMESTAMP , 
                  "OFE_STATUS" VARCHAR(25) )   
                 IN "BKF8K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."ONLINE_SHOPPING" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."ONLINE_SHOPPING" IS 'Release 19, September 2011; Stores the dates for ONLINE_SHOPPING allowed only when an agreement was signed by the Chairperson.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_SHOPPING"."OFE_END_DATE" IS 'Stores for the Online shopping ending date, formerly called the Online Fair Extention.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_SHOPPING"."OFE_START_DATE" IS 'Stores for the Online shopping starting date, formerly called the Online Fair Extention.';

COMMENT ON COLUMN "BKFAIR01"."ONLINE_SHOPPING"."OFE_STATUS" IS 'Stores the status for the online_shopping for that fair_id which can be either: Active, Cancelled, Complete, Submitted.';


-- DDL Statements for primary key on Table "BKFAIR01"."ONLINE_SHOPPING"

ALTER TABLE "BKFAIR01"."ONLINE_SHOPPING" 
        ADD CONSTRAINT "XPK_ONLINESHOPPING__FAIR_ID" PRIMARY KEY
                ("FAIR_ID");



-- DDL Statements for indexes on Table "BKFAIR01"."ONLINE_SHOPPING"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_ONLINESHOPPING__OFE_ENDATE" ON "BKFAIR01"."ONLINE_SHOPPING" 
                ("FAIR_ID" ASC,
                 "OFE_END_DATE" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_HOMEPAGE_MESSAGES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_HOMEPAGE_MESSAGES"  (
                  "THANK_YOU_MESSAGE" VARCHAR(550) NOT NULL , 
                  "WELCOME_HEADING" VARCHAR(30) NOT NULL , 
                  "WELCOME_MESSAGE" VARCHAR(1000) NOT NULL , 
                  "VOLUNTEER_HEADING" VARCHAR(30) NOT NULL , 
                  "VOLUNTEER_MESSAGE" VARCHAR(450) , 
                  "GOAL_PURPOSE" VARCHAR(500) , 
                  "PRINCIPAL_HEADING" VARCHAR(35) , 
                  "PRINCIPAL_MESSAGE" VARCHAR(500) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."LU_HOMEPAGE_MESSAGES" IS 'Reviewed for Release_19 on Sept. 2011; Stores the fair Homepage personalised message information coming from business.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_MESSAGES"."PRINCIPAL_HEADING" IS 'Release 21; a default value for the online_homepage gloabal Heading text for the Principal Activities.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_MESSAGES"."PRINCIPAL_MESSAGE" IS 'Release 21; a default value for the online_homepage global Message text for the Principal Activities.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_MESSAGES"."THANK_YOU_MESSAGE" IS 'To store the chairperson thank you message subject.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_MESSAGES"."VOLUNTEER_HEADING" IS 'To store the chairperson volunteer message heading.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_MESSAGES"."VOLUNTEER_MESSAGE" IS 'To store the chairperson volunter welcome message subject.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_MESSAGES"."WELCOME_HEADING" IS 'To store the chairperson welcome message heading.';

COMMENT ON COLUMN "BKFAIR01"."LU_HOMEPAGE_MESSAGES"."WELCOME_MESSAGE" IS 'To store the chairperson welcome message subject.';





------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."ONLINE_HOMEPAGE"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."ONLINE_HOMEPAGE"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CONTACT_PERSON_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "CONTACT_EMAIL_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "CONTACT_PHONE_NUMBER_CKBOX" VARCHAR(1) WITH DEFAULT 'N' , 
                  "PAYMENT_CHECK_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "PAYMENT_CC_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "PAYMENT_CASH_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "VOLUNTEER_RECRUITMENT_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "BOOK_FAIR_GOAL_CKBOX" VARCHAR(1) WITH DEFAULT 'N' , 
                  "SPECIAL_PROGRAM_CKBOX" VARCHAR(1) WITH DEFAULT 'N' , 
                  "SPECIAL_PROGRAM_CKBOX2" VARCHAR(1) WITH DEFAULT 'N' , 
                  "SPECIAL_PROGRAM_CKBOX3" CHAR(1) WITH DEFAULT 'N' , 
                  "SPECIAL_PROGRAM_CKBOX4" CHAR(1) WITH DEFAULT 'N' , 
                  "OFE_FEATURE_CKBOX" CHAR(1) NOT NULL WITH DEFAULT 'N' , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "SELECTED_GOAL" VARCHAR(20) WITH DEFAULT 'NUMBEROFBOOKS' , 
                  "FAIR_START_DATE" DATE , 
                  "FAIR_END_DATE" DATE , 
                  "GOAL_AMOUNT" DECIMAL(8,2) WITH DEFAULT  , 
                  "ACTUAL_AMOUNT" DECIMAL(8,2) WITH DEFAULT  , 
                  "SCHOOL_STATE" VARCHAR(10) , 
                  "SCHOOL_POSTALCODE" VARCHAR(10) , 
                  "FAIR_LOCATION" VARCHAR(30) , 
                  "VOLUNTEER_HEADING" VARCHAR(30) , 
                  "WELCOME_HEADING" VARCHAR(30) , 
                  "CONTACT_PHONE_NUMBER" VARCHAR(40) , 
                  "SCHOOL_CITY" VARCHAR(50) , 
                  "CONTACT_FIRST_NAME" VARCHAR(70) , 
                  "CONTACT_LAST_NAME" VARCHAR(70) , 
                  "PRIMARY_EMAIL" VARCHAR(70) , 
                  "SECONDARY_EMAIL" VARCHAR(70) , 
                  "SECONDARY_EMAIL2" VARCHAR(70) , 
                  "SECONDARY_EMAIL3" VARCHAR(70) , 
                  "SCHOOL_NAME" VARCHAR(200) , 
                  "CONTACT_EMAIL" VARCHAR(200) , 
                  "SCHOOL_ADDRESS1" VARCHAR(255) , 
                  "VOLUNTEER_MESSAGE" VARCHAR(450) , 
                  "GOAL_PURPOSE" VARCHAR(500) , 
                  "WELCOME_MESSAGE" VARCHAR(1000) , 
                  "THANK_YOU_MESSAGE" VARCHAR(550) , 
                  "SPECIAL_PROGRAM_INCLUDE_DESCRIPTION" VARCHAR(400) , 
                  "SPECIAL_PROGRAM_INCLUDE_2_DESCRIPTION" VARCHAR(400) , 
                  "SPECIAL_PROGRAM_INCLUDE_3_DESCRIPTION" VARCHAR(400) , 
                  "SPECIAL_PROGRAM_INCLUDE_4_DESCRIPTION" VARCHAR(400) , 
                  "CHECK_DESCRIPTION" VARCHAR(50) WITH DEFAULT '(Payable to the school)' , 
                  "PRINCIPAL_DISPLAY_CKBOX" CHAR(1) WITH DEFAULT 'N' , 
                  "PRINCIPAL_HEADING" VARCHAR(35) , 
                  "PRINCIPAL_MESSAGE" VARCHAR(500) )   
                 IN "BKF8K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "UNPUBLISHED"."ONLINE_HOMEPAGE" PCTFREE 10;

COMMENT ON TABLE "UNPUBLISHED"."ONLINE_HOMEPAGE" IS 'Release 19, September 2011; Stores the UNPUBLISHED data for Online Homepage Fairinformation.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."ACTUAL_AMOUNT" IS 'The Actual amount.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."BOOK_FAIR_GOAL_CKBOX" IS 'Is it book fair Goal amout Y/N.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."CHECK_DESCRIPTION" IS 'Description field offered to the customer to provides details on the Check to be drawn.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."CONTACT_EMAIL" IS 'To store the contact person email.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."CONTACT_EMAIL_CKBOX" IS 'To store the contact person email check box value Y(yes) or N(No) .';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."CONTACT_FIRST_NAME" IS 'To store the contact person firstname .';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."CONTACT_LAST_NAME" IS 'To store the contact person lastname.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."CONTACT_PERSON_CKBOX" IS 'To store the contact person check box value Y(yes) or N(No) .';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."CONTACT_PHONE_NUMBER" IS 'To store the contact person phone number .';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."CONTACT_PHONE_NUMBER_CKBOX" IS 'To store the contact person phone number check box value Y(yes) or N(No) .';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."CREATE_DATE" IS 'The record created date.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."FAIR_END_DATE" IS 'To store the Physical fair End date.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."FAIR_ID" IS 'Fair_id and Its a primary key column.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."FAIR_LOCATION" IS 'To stores the Physical fair location.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."FAIR_START_DATE" IS 'To store Physical fair start date';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."GOAL_AMOUNT" IS 'Chairperson goad amounts.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."PAYMENT_CASH_CKBOX" IS ' To store the Payment Cash Y/N.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."PAYMENT_CC_CKBOX" IS 'To store the Payment type credit cards check box value Y(yes) or N(No) .';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."PAYMENT_CHECK_CKBOX" IS 'To store the Payment type check check box valueY(yes) or N(No) .';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."PRINCIPAL_DISPLAY_CKBOX" IS 'Release 21; allow (Y) or disallow (N) Principal Activity Heading and Message on the Homepage.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."PRINCIPAL_HEADING" IS 'Release 21; a gloabal Heading text for the Principal Activities.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."PRINCIPAL_MESSAGE" IS 'Release 21; a global Message text for the Principal Activities.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SCHOOL_ADDRESS1" IS 'To store the School address line1.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SCHOOL_CITY" IS 'To store the school city name.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SCHOOL_NAME" IS 'To store the School Name.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SCHOOL_POSTALCODE" IS 'To store the school zip code (postal code).';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SCHOOL_STATE" IS 'To store the school state name.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SELECTED_GOAL" IS 'Chairperson selected Goal amount e.g. NUMBEROFBOOKS/ DOLLARAMOUNT.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_CKBOX" IS 'Special progaram Y/N.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_INCLUDE_2_DESCRIPTION" IS 'Release 19, September 2011; Selected from LU_SPECIAL_PROGRAMS based on the PRODUCT_ID in FAIR table.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_INCLUDE_3_DESCRIPTION" IS 'Release 19, September 2011; Selected from LU_SPECIAL_PROGRAMS based on the PRODUCT_ID in FAIR table.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_INCLUDE_4_DESCRIPTION" IS 'Release 19, September 2011; Selected from LU_SPECIAL_PROGRAMS based on the PRODUCT_ID in FAIR table.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."SPECIAL_PROGRAM_INCLUDE_DESCRIPTION" IS 'Release 19, September 2011; Selected from LU_SPECIAL_PROGRAMS based on the PRODUCT_ID in FAIR table.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."THANK_YOU_MESSAGE" IS 'To store the chairperson thank you message subject.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."UPDATE_DATE" IS 'The record last modified date.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."VOLUNTEER_HEADING" IS 'To store the chairperson volunteer message heading.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."VOLUNTEER_MESSAGE" IS 'To store the chairperson volunter welcome message subject.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."VOLUNTEER_RECRUITMENT_CKBOX" IS 'To store the valunteer requirtment Y/N.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."WELCOME_HEADING" IS 'To store the chairperson welcome message heading.';

COMMENT ON COLUMN "UNPUBLISHED"."ONLINE_HOMEPAGE"."WELCOME_MESSAGE" IS 'To store the chairperson welcome message subject.';


-- DDL Statements for primary key on Table "UNPUBLISHED"."ONLINE_HOMEPAGE"

ALTER TABLE "UNPUBLISHED"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "XPK_UNPUB_ONLINEHOMEPAGE_FAIR_ID" PRIMARY KEY
                ("FAIR_ID");



-- DDL Statements for indexes on Table "UNPUBLISHED"."ONLINE_HOMEPAGE"

CREATE UNIQUE INDEX "UNPUBLISHED"."XUV_UNPUB_ONLINEHOMEPAGE_ENDATE" ON "UNPUBLISHED"."ONLINE_HOMEPAGE" 
                ("FAIR_ID" ASC,
                 "FAIR_END_DATE" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."HOMEPAGE_SCHEDULE"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."HOMEPAGE_SCHEDULE"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SCHEDULE_DATE" DATE NOT NULL , 
                  "SCHEDULE_HOURS" VARCHAR(30) , 
                  "SCHEDULE_DATE_CKBOX" VARCHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "START_TIME" TIME , 
                  "END_TIME" TIME )   
                 IN "BKF4K02D" INDEX IN "BKF4K02I"   ; 

ALTER TABLE "UNPUBLISHED"."HOMEPAGE_SCHEDULE" PCTFREE 10;

COMMENT ON TABLE "UNPUBLISHED"."HOMEPAGE_SCHEDULE" IS 'Stores the UNPUBLISHED schedule data for Online_homepage.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_SCHEDULE"."CREATE_DATE" IS 'The record created date.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_SCHEDULE"."END_TIME" IS 'Release 19.8; Replaces column data SCHEDULE_HOURS (varcbar), and tracks the global ending time of the fair schedule for a given day as a true time data.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_SCHEDULE"."FAIR_ID" IS 'It''s primary key and it''s foreign key references homepage.fair_id.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_SCHEDULE"."SCHEDULE_DATE" IS 'To store the Fair scheduled date.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_SCHEDULE"."SCHEDULE_DATE_CKBOX" IS 'To store the Fair scheduled Date Check box value Y(Yes) N(No).';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_SCHEDULE"."SCHEDULE_HOURS" IS 'To store the Fair scheduled Hours.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_SCHEDULE"."START_TIME" IS 'Release 19.8; Replaces column data SCHEDULE_HOURS (varchar), and tracks the global starting time of the fair schedule for a given day as a true time data.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_SCHEDULE"."UPDATE_DATE" IS 'The record last modified date.';


-- DDL Statements for primary key on Table "UNPUBLISHED"."HOMEPAGE_SCHEDULE"

ALTER TABLE "UNPUBLISHED"."HOMEPAGE_SCHEDULE" 
        ADD CONSTRAINT "XPK_UNPUB_HOMEPAGE_SCHDL_ID_DATE" PRIMARY KEY
                ("FAIR_ID",
                 "SCHEDULE_DATE");


------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."HOMEPAGE_EVENT"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."HOMEPAGE_EVENT"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SCHEDULE_DATE" DATE NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "EVENT_NAME" VARCHAR(50) , 
                  "EVENT_TIME" VARCHAR(20) , 
                  "EVENT_ID" BIGINT , 
                  "START_TIME" TIME WITH DEFAULT '8:00' , 
                  "END_TIME" TIME WITH DEFAULT '8:30' , 
                  "UPDATE_DATE" TIMESTAMP , 
                  "DISPLAY_SEQ_NO" SMALLINT , 
                  "PLANNER_CALENDAR_CKBOX" CHAR(1) WITH DEFAULT 'Y' , 
                  "COMPLETED_FLAG" CHAR(1) WITH DEFAULT 'N' )   
                 IN "BKF4K02D" INDEX IN "BKF4K02I"  ; 

ALTER TABLE "UNPUBLISHED"."HOMEPAGE_EVENT" PCTFREE 10;

COMMENT ON TABLE "UNPUBLISHED"."HOMEPAGE_EVENT" IS 'Stores the UNPUBLISHED Event data for Online_Homepage.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."COMPLETED_FLAG" IS 'Rel. 24.5: When set to Y, indicates that this auto-added task was completed; It is set to N when it is not.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."CREATE_DATE" IS 'The record created date.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."DISPLAY_SEQ_NO" IS 'Release 21, October 2012; Tracks the order sequence by which the events will be displayed.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."END_TIME" IS 'Release 19.8; Replaces column data EVENT_TIME (varcbar), and tracks the ending time of a given event as a true time data.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."EVENT_NAME" IS 'Release 19.8; This column no longer tracks a non-standard event name. it now tracks an event description.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."EVENT_TIME" IS 'To store the Fair Event time.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."FAIR_ID" IS 'To store the fair_id.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."PLANNER_CALENDAR_CKBOX" IS 'Rel. 24.5: Allow by default all homepage event records to be displayed except for event ID # 29';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."SCHEDULE_DATE" IS 'To store the fair schedule date.';

COMMENT ON COLUMN "UNPUBLISHED"."HOMEPAGE_EVENT"."START_TIME" IS 'Release 19.8; Replaces column data EVENT_TIME (varchar), and tracks the starting time of a given event as a true time data.';


-- DDL Statements for primary key on Table "UNPUBLISHED"."HOMEPAGE_EVENT"

ALTER TABLE "UNPUBLISHED"."HOMEPAGE_EVENT" 
        ADD CONSTRAINT "XPK_UNPUB_HOMEPAGE_EVENT__3COLS" PRIMARY KEY
                ("FAIR_ID",
                 "SCHEDULE_DATE",
                 "CREATE_DATE");


------------------------------------------------
-- DDL Statements for table "BKFAIR01"."WORKSHOP_REWARDS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."WORKSHOP_REWARDS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "REWARD_ID" BIGINT NOT NULL , 
                  "COMPLETE_FLAG" CHAR(1) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."WORKSHOP_REWARDS" IS 'Release 19, September 2011; Imported data on daily basis; the flag, when set to Y will force the display of specific workshops.';


-- DDL Statements for indexes on Table "BKFAIR01"."WORKSHOP_REWARDS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_WORKSHOP_REWARDS__FAIR_REWARD_IDS" ON "BKFAIR01"."WORKSHOP_REWARDS" 
                ("FAIR_ID" ASC,
                 "REWARD_ID" ASC)
                INCLUDE ("COMPLETE_FLAG" )
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."WORKSHOP_REWARDS"

ALTER TABLE "BKFAIR01"."WORKSHOP_REWARDS" 
        ADD CONSTRAINT "XPK_WORKSHOP_REWARDS__FAIR_REWARD_IDS" PRIMARY KEY
                ("FAIR_ID",
                 "REWARD_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."HOMEPAGE_CUSTOMER_EMAILS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."HOMEPAGE_CUSTOMER_EMAILS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "EMAIL" VARCHAR(200) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP NOT NULL WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."HOMEPAGE_CUSTOMER_EMAILS" IS 'Release 19, September 2011; The application captures the customer email as that customer tries to shop online as the homepage has expired.';






-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_CUSTOMER_EMAILS"

CREATE INDEX "BKFAIR01"."X_HOMEPAGECUSTOMEREMAILS__CREATE_DATE" ON "BKFAIR01"."HOMEPAGE_CUSTOMER_EMAILS" 
                ("CREATE_DATE" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."AUDIT_OFE"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."AUDIT_OFE"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "OLD_OFE_START_DATE" DATE , 
                  "OLD_OFE_END_DATE" DATE , 
                  "CREATE_TIMESTAMP" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP , 
                  "OPERATION" VARCHAR(20) , 
                  "OLD_OFE_STATUS" VARCHAR(25) )   
                IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."AUDIT_OFE" IS 'Release 19.5, March 2012; captures all modification to data occuring in table ONLINE_SHOPPING.';






-- DDL Statements for indexes on Table "BKFAIR01"."AUDIT_OFE"

CREATE INDEX "BKFAIR01"."X_AUDIT_OFE__FAIR_ID" ON "BKFAIR01"."AUDIT_OFE" 
                ("FAIR_ID" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."VOLUNTEER_FORMS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."VOLUNTEER_FORMS"  (
                  "ID" BIGINT NOT NULL GENERATED BY DEFAULT AS IDENTITY (  
                    START WITH +1000  
                    INCREMENT BY +1  
                    MINVALUE +1000  
                    MAXVALUE +9223372036854775807  
                    NO CYCLE  
                    NO CACHE  
                    ORDER ), 
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "HELP_AT_FAIR" CHAR(1) WITH DEFAULT 'N' , 
                  "QUESTION" CHAR(1) WITH DEFAULT 'N' , 
                  "CONTACT_ME_CKBOX" CHAR(1) WITH DEFAULT 'N' , 
                  "SUBMIT_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP , 
                  "CONTACT_METHOD" VARCHAR(2) WITH DEFAULT 'ET' , 
                  "OPTIN_SOURCE" VARCHAR(3) , 
                  "CONTACT_TIME" VARCHAR(10) , 
                  "FIRST_NAME" VARCHAR(25) , 
                  "LAST_NAME" VARCHAR(25) , 
                  "EMAIL" VARCHAR(75) , 
                  "PHONE" VARCHAR(40) , 
                  "UPDATE_TIMESTAMP" TIMESTAMP , 
                  "MESSAGE" VARCHAR(250) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."VOLUNTEER_FORMS" IS 'Release 19.7, May 2012; captures all Volunteer data registered on line(for the moment).';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."CONTACT_ME_CKBOX" IS 'Check box asking the Chairperson to call the Volunteer; Also know as ''Opt in''.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."CONTACT_METHOD" IS 'Method by which the Volunteer wants to be contacted: Email (EM), Phone (P), Either (ET).';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."CONTACT_TIME" IS 'Free formed text indicating the time at which the Volunteer wishes to be contacted.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."EMAIL" IS 'Volunteer email.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."FAIR_ID" IS 'Tracks the Fair_id the volunteer applied for.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."FIRST_NAME" IS 'Volunteer first name.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."HELP_AT_FAIR" IS 'Check box indicating that the volunteer registerd for helping at the fair (Y) or not (N).';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."ID" IS 'Record ID automatically generated at insertion time via an entity.  This column is mainly used towards data export to reporting to allow deltas.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."LAST_NAME" IS 'Volunteer last name.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."MESSAGE" IS 'Volunteer message to the Chairperson.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."OPTIN_SOURCE" IS 'Origin of the Volunteer Registration process: OnlineHomepage Form (OHF); at a later date, there is a possibility to merge the paper forms (PF).';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."PHONE" IS 'Volunteer phone.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."QUESTION" IS 'Check box letting the Chairperson know that the volunteer has a question to ask.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."SCHOOL_ID" IS 'Tracks the School_id the Fair_id is for.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."SUBMIT_DATE" IS 'Submission of the Volunteer On-Line registration for the fair.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_FORMS"."UPDATE_TIMESTAMP" IS 'Record update date (for future usage).';


-- DDL Statements for indexes on Table "BKFAIR01"."VOLUNTEER_FORMS"

CREATE INDEX "BKFAIR01"."X_VOLUNTEER_FORMS_FAIRID" ON "BKFAIR01"."VOLUNTEER_FORMS" 
                ("FAIR_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."VOLUNTEER_FORMS"

CREATE INDEX "BKFAIR01"."X_VOLUNTEER_FORMS_SUBMITCKBOX" ON "BKFAIR01"."VOLUNTEER_FORMS" 
                ("SUBMIT_DATE" ASC,
                 "CONTACT_ME_CKBOX" ASC,
                 "FAIR_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."VOLUNTEER_FORMS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_VOLUNTEER_FORMS__ID" ON "BKFAIR01"."VOLUNTEER_FORMS" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."VOLUNTEER_FORMS"

ALTER TABLE "BKFAIR01"."VOLUNTEER_FORMS" 
        ADD CONSTRAINT "XPK_VOLUNTEER_FORMS__ID" PRIMARY KEY
                ("ID");



ALTER TABLE "BKFAIR01"."VOLUNTEER_FORMS" ALTER COLUMN "ID" RESTART WITH 11618;

------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CHAIRPERSON_EVENTS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CHAIRPERSON_EVENTS"  (
                  "INT_EVENT_ID" INTEGER NOT NULL , 
                  "EVENT_TYPE" CHAR(2) , 
                  "STR_LATLON" VARCHAR(50) , 
                  "STR_SITE_NAME" VARCHAR(100) , 
                  "STR_ADDRESS1" VARCHAR(100) , 
                  "STR_ADDRESS2" VARCHAR(100) , 
                  "STR_CITY" VARCHAR(80) , 
                  "STR_ST_ABBREV" CHAR(2) , 
                  "STR_ZIP" CHAR(10) , 
                  "STR_PHONE" VARCHAR(50) , 
                  "STR_COUNTRY_CODE" CHAR(2) , 
                  "STR_DISDATES" VARCHAR(50) , 
                  "INT_SESSION_ID" INTEGER , 
                  "DT_S_DATE" CHAR(10) , 
                  "DT_E_DATE" CHAR(10) , 
                  "STR_TIME1" VARCHAR(250) , 
                  "STR_TIME2" VARCHAR(250) , 
                  "STR_TIME3" VARCHAR(250) , 
                  "STR_TIME4" VARCHAR(250) , 
                  "EVENT_FLYER" CHAR(1) , 
                  "REGISTRATION_OPEN" VARCHAR(6) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."CHAIRPERSON_EVENTS" IS 'Release 19.8; Reloaded on a daily basis via the shell script bkfair_load.sh; Collect all events occuring in the country such as warehouse sales etc; these data can be occasionally empty; was required to keep the column names as delivered.';


-- DDL Statements for indexes on Table "BKFAIR01"."CHAIRPERSON_EVENTS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_CHAIRPERSONEVENTS_ID" ON "BKFAIR01"."CHAIRPERSON_EVENTS" 
                ("INT_EVENT_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CHAIRPERSON_EVENTS"

ALTER TABLE "BKFAIR01"."CHAIRPERSON_EVENTS" 
        ADD CONSTRAINT "XPK_CHAIRPERSONEVENTS_ID" PRIMARY KEY
                ("INT_EVENT_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_ZIPCODES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_ZIPCODES"  (
                  "ZIPCODE" VARCHAR(10) NOT NULL , 
                  "STR_LATLON" VARCHAR(50) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."LU_ZIPCODES" IS 'Release 19.9; Keep track of all zipcodes used by the schools together with their latitude and longitude.';

COMMENT ON COLUMN "BKFAIR01"."LU_ZIPCODES"."STR_LATLON" IS 'Latitude and Longitude of the fair event separated by ^.';

COMMENT ON COLUMN "BKFAIR01"."LU_ZIPCODES"."ZIPCODE" IS 'Zipcode of the school.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_ZIPCODES"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_LUZIPCODES" ON "BKFAIR01"."LU_ZIPCODES" 
                ("ZIPCODE" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_ZIPCODES"

ALTER TABLE "BKFAIR01"."LU_ZIPCODES" 
        ADD CONSTRAINT "XPK_LUZIPCODES" PRIMARY KEY
                ("ZIPCODE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"  (
                  "ID" INTEGER NOT NULL , 
                  "SEQUENCE_NO" SMALLINT NOT NULL , 
                  "DESCRIPTION" VARCHAR(100) NOT NULL , 
                  "START_DATE" DATE NOT NULL , 
                  "END_DATE" DATE NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES" IS 'Release 21; lookup table that tracks all the activities that a School Principal can register for; this table data is coming via the loading script.';

COMMENT ON COLUMN "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"."DESCRIPTION" IS 'Description of the Principal activity';

COMMENT ON COLUMN "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"."END_DATE" IS 'Ending date for that activity set up by the business.';

COMMENT ON COLUMN "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"."ID" IS 'Unique value provided by Back End Business; Keeps track of the Principal Activity ID.';

COMMENT ON COLUMN "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"."SEQUENCE_NO" IS 'Principal Activity display sequence number.';

COMMENT ON COLUMN "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"."START_DATE" IS 'Starting date for that activity set up by the business.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_LU_PRINCIPAL_ACTIVITIES" ON "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"

ALTER TABLE "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES" 
        ADD CONSTRAINT "XPK_LU_PRINCIPAL_ACTIVITIES" PRIMARY KEY
                ("ID");




-- DDL Statements for indexes on Table "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_LU_PRINCIPAL_ACTIVITIES_3COLS" ON "BKFAIR01"."LU_PRINCIPAL_ACTIVITIES" 
                ("ID" ASC,
                 "SEQUENCE_NO" ASC,
                 "START_DATE" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "ACTIVITY_ID" INTEGER NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES" IS 'Release 21; keeps track of the activities that a School Principal will have registered for for a given fair.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES"."ACTIVITY_ID" IS 'Required; activity ID which can be found in Lookup table LU_PRINCIPAL_ACTIVITIES.';

COMMENT ON COLUMN "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES"."FAIR_ID" IS 'Required; Keeps track of the Fair ID.';


-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES"

CREATE INDEX "BKFAIR01"."X_HP_PRINCIPAL_ACTIVITIES_DATE" ON "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES" 
                ("CREATE_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_HP_PRINCIPAL_ACTIVITIES" ON "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES" 
                ("FAIR_ID" ASC,
                 "ACTIVITY_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES"

ALTER TABLE "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES" 
        ADD CONSTRAINT "XPK_HP_PRINCIPAL_ACTIVITIES" PRIMARY KEY
                ("FAIR_ID",
                 "ACTIVITY_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING"  (
                  "ID" BIGINT NOT NULL GENERATED BY DEFAULT AS IDENTITY (  
                    START WITH +1000  
                    INCREMENT BY +1  
                    MINVALUE +1  
                    MAXVALUE +9223372036854775807  
                    NO CYCLE  
                    CACHE 200  
                    ORDER ), 
                  "FAIR_ID" BIGINT NOT NULL , 
                  "VOLUNTEER_ID" BIGINT NOT NULL , 
                  "LOGIN_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING" IS 'Release 21; Tracks all volunteers logging into the Chairperson toolkit.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING"."FAIR_ID" IS 'Tracks the fair_id to which the Volunteer is logging.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING"."ID" IS 'Generated ID for Primary Key and to allow for delta data export.';

COMMENT ON COLUMN "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING"."VOLUNTEER_ID" IS 'Tracks the Volunteer ID.';


-- DDL Statements for indexes on Table "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING"

CREATE INDEX "BKFAIR01"."X_VOLOGINTRACKING_DATE" ON "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING" 
                ("LOGIN_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_VOLUNTEERLOGINTRACKING" ON "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING"

ALTER TABLE "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING" 
        ADD CONSTRAINT "XPK_VOLUNTEERLOGINTRACKING" PRIMARY KEY
                ("ID");



ALTER TABLE "BKFAIR01"."VOLUNTEER_LOGIN_TRACKING" ALTER COLUMN "ID" RESTART WITH 97799;

------------------------------------------------
-- DDL Statements for table "BKFAIR01"."ZIPCODE_DATA_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."ZIPCODE_DATA_LOAD"  (
                  "CITY" VARCHAR(30) , 
                  "STATE" VARCHAR(2) , 
                  "ZIPCODE" VARCHAR(5) , 
                  "AREA_CODE" VARCHAR(3) , 
                  "COUNTY_FIPS" VARCHAR(5) , 
                  "COUNTY_NAME" VARCHAR(30) , 
                  "PREFERRED" CHAR(1) , 
                  "TIME_ZONE" VARCHAR(5) , 
                  "DST" CHAR(1) , 
                  "LATITUDE" VARCHAR(7) , 
                  "LONGITUDE" VARCHAR(9) , 
                  "MSA" VARCHAR(4) , 
                  "PMSA" VARCHAR(4) , 
                  "CITY_NAME_ABBV" VARCHAR(13) , 
                  "MARKET_AREA" VARCHAR(3) , 
                  "ZIPCODE_TYPE" CHAR(1) , 
                  "CBSA" VARCHAR(5) , 
                  "DIVISION" VARCHAR(5) , 
                  "POPULATION" VARCHAR(12) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."ZIPCODE_DATA_LOAD" IS 'Loaded from a Vendor data file zipinfo.com to access city latitude and longitude data.';






-- DDL Statements for indexes on Table "BKFAIR01"."ZIPCODE_DATA_LOAD"

CREATE INDEX "BKFAIR01"."X_ZIPCODE_DATA_LOAD" ON "BKFAIR01"."ZIPCODE_DATA_LOAD" 
                ("ZIPCODE" ASC,
                 "PREFERRED" DESC,
                 "STATE" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SCHOOL_TAX_SELECTION"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SCHOOL_TAX_SELECTION"  (
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "TAX_SELECTION" CHAR(1) NOT NULL , 
                  "UPDATE_DATE" DATE NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."SCHOOL_TAX_SELECTION" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."SCHOOL_TAX_SELECTION" IS 'Release 22; Tracks the School Tax Selection; Data loaded daily from CIS import using file PROD_SCHOOL_TAX_SELECTION_CPW which originates from .';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL_TAX_SELECTION"."SCHOOL_ID" IS 'Comes from CIS; Unique value tracking the School ID';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL_TAX_SELECTION"."TAX_SELECTION" IS 'Comes from CIS SBF_TX_CRSP.SBF_TX_CMP; Tax Selection being either B (Book Fairs) or C (Customer)';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL_TAX_SELECTION"."UPDATE_DATE" IS 'Comes from CIS; Tracks the date the record has been updated (by CIS)';


-- DDL Statements for indexes on Table "BKFAIR01"."SCHOOL_TAX_SELECTION"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SCHOOL_TAX_SELECTION" ON "BKFAIR01"."SCHOOL_TAX_SELECTION" 
                ("SCHOOL_ID" ASC)
                INCLUDE ("TAX_SELECTION" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SCHOOL_TAX_SELECTION"

ALTER TABLE "BKFAIR01"."SCHOOL_TAX_SELECTION" 
        ADD CONSTRAINT "XPKC_SCHOOL_TAX_SELECTION" PRIMARY KEY
                ("SCHOOL_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SCHOOL_TAX_STATUS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SCHOOL_TAX_STATUS"  (
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "TAX_STATUS" VARCHAR(4) NOT NULL , 
                  "EXPIRATION_DATE" DATE , 
                  "INVALID_REASON" VARCHAR(100) , 
                  "UPDATE_DATE" DATE NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."SCHOOL_TAX_STATUS" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."SCHOOL_TAX_STATUS" IS 'Release 22; Tracks the School Type based on its school_id; Data loaded daily from CRM import using file PROD_SCHOOL_TAX_STATUS_CPW.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL_TAX_STATUS"."EXPIRATION_DATE" IS 'Tracks the expiration date of a certificate.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL_TAX_STATUS"."INVALID_REASON" IS 'Tracks the Invalid Reason for a School Tax Certification';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL_TAX_STATUS"."SCHOOL_ID" IS 'Unique value tracking the School ID.';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL_TAX_STATUS"."TAX_STATUS" IS 'Tracks the School Tax Status; EP-Expired, EX-Expiring, IV-Invalid, NF-NotOnFile, NT-NonTaxable, OF-OnFile, OS-OtherStates, PR-PendingReview';

COMMENT ON COLUMN "BKFAIR01"."SCHOOL_TAX_STATUS"."UPDATE_DATE" IS 'Tracks the date the record has been updated (by CRM).';


-- DDL Statements for indexes on Table "BKFAIR01"."SCHOOL_TAX_STATUS"

CREATE INDEX "BKFAIR01"."X_SCHOOL_TAX_STATUS__UPDTDATE" ON "BKFAIR01"."SCHOOL_TAX_STATUS" 
                ("UPDATE_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."SCHOOL_TAX_STATUS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SCHOOL_TAX_STATUS" ON "BKFAIR01"."SCHOOL_TAX_STATUS" 
                ("SCHOOL_ID" ASC)
                INCLUDE ("TAX_STATUS" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SCHOOL_TAX_STATUS"

ALTER TABLE "BKFAIR01"."SCHOOL_TAX_STATUS" 
        ADD CONSTRAINT "XPKC_SCHOOL_TAX_STATUS" PRIMARY KEY
                ("SCHOOL_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."TAX_SCHOOL_TYPES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."TAX_SCHOOL_TYPES"  (
                  "STATE" VARCHAR(6) NOT NULL , 
                  "SCHOOL_TYPE" VARCHAR(4) NOT NULL , 
                  "STATE_URL" VARCHAR(150) , 
                  "UPDATE_DATE" DATE NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."TAX_SCHOOL_TYPES" IS 'Release 22; Tracks the School type and its tax url based on the State where the School is based; Data loaded daily from CRM import using file PROD_TAX_SCHOOL_TYPES_CPW.';

COMMENT ON COLUMN "BKFAIR01"."TAX_SCHOOL_TYPES"."SCHOOL_TYPE" IS 'Tracks the School Type.';

COMMENT ON COLUMN "BKFAIR01"."TAX_SCHOOL_TYPES"."STATE" IS '2-character State value';

COMMENT ON COLUMN "BKFAIR01"."TAX_SCHOOL_TYPES"."STATE_URL" IS 'Tracks the State URL where the tax document can be accessed.';

COMMENT ON COLUMN "BKFAIR01"."TAX_SCHOOL_TYPES"."UPDATE_DATE" IS 'Tracks the date the record has been updated (by CRM).';


-- DDL Statements for indexes on Table "BKFAIR01"."TAX_SCHOOL_TYPES"

CREATE INDEX "BKFAIR01"."X_SCHOOLTAXTYPE__DATE" ON "BKFAIR01"."TAX_SCHOOL_TYPES" 
                ("UPDATE_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."TAX_SCHOOL_TYPES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_TAXSCHOOLTYPES" ON "BKFAIR01"."TAX_SCHOOL_TYPES" 
                ("STATE" ASC,
                 "SCHOOL_TYPE" ASC)
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."TAX_SCHOOL_TYPES"

ALTER TABLE "BKFAIR01"."TAX_SCHOOL_TYPES" 
        ADD CONSTRAINT "XPKC_TAXSCHOOLTYPES" PRIMARY KEY
                ("STATE",
                 "SCHOOL_TYPE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."STATE_TAX_CHOICES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."STATE_TAX_CHOICES"  (
                  "STATE" VARCHAR(6) NOT NULL , 
                  "ACTIVATED" CHAR(1) NOT NULL , 
                  "AGENCY_URL" VARCHAR(150) , 
                  "UPDATE_DATE" DATE NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."STATE_TAX_CHOICES" IS 'Release 22; Tracks the States for which the Tax function is to be implemented; Data loaded daily from CRM import using file PROD_STATE_TAX_CHOICE_CPW.';

COMMENT ON COLUMN "BKFAIR01"."STATE_TAX_CHOICES"."ACTIVATED" IS 'Indicates if a State becomes part (Y) of the State Tax functionality implementation or not (N).';

COMMENT ON COLUMN "BKFAIR01"."STATE_TAX_CHOICES"."AGENCY_URL" IS 'Tracks the Agency URL where the tax document can be accessed.';

COMMENT ON COLUMN "BKFAIR01"."STATE_TAX_CHOICES"."STATE" IS '2-character State value';

COMMENT ON COLUMN "BKFAIR01"."STATE_TAX_CHOICES"."UPDATE_DATE" IS 'Tracks the date the record has been updated (by CRM).';


-- DDL Statements for indexes on Table "BKFAIR01"."STATE_TAX_CHOICES"

CREATE INDEX "BKFAIR01"."X_STATETAXCHOICE__DATE" ON "BKFAIR01"."STATE_TAX_CHOICES" 
                ("UPDATE_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."STATE_TAX_CHOICES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_STATETAXCHOICE" ON "BKFAIR01"."STATE_TAX_CHOICES" 
                ("STATE" ASC)
                INCLUDE ("ACTIVATED" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."STATE_TAX_CHOICES"

ALTER TABLE "BKFAIR01"."STATE_TAX_CHOICES" 
        ADD CONSTRAINT "XPKC_STATETAXCHOICE" PRIMARY KEY
                ("STATE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_COA_TAXES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_COA_TAXES"  (
                  "COA_TAX_FAIR_TYPE" CHAR(1) NOT NULL , 
                  "COA_TAX_SELECTION" CHAR(1) NOT NULL , 
                  "COA_TAX_START_DATE" DATE NOT NULL , 
                  "COA_TAX_END_DATE" DATE NOT NULL , 
                  "COA_TAX_FILE_NAME" VARCHAR(50) NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."LU_COA_TAXES" IS 'Release 22; New reference table to track the COA tax related data; Data used dynamically to select the include file only; Data loaded daily from CRM import using file PROD_COA_TAX_CPW.';

COMMENT ON COLUMN "BKFAIR01"."LU_COA_TAXES"."COA_TAX_END_DATE" IS 'Tracks the ending date of the COA.';

COMMENT ON COLUMN "BKFAIR01"."LU_COA_TAXES"."COA_TAX_FAIR_TYPE" IS '1-character value tracking the type of fair.';

COMMENT ON COLUMN "BKFAIR01"."LU_COA_TAXES"."COA_TAX_FILE_NAME" IS 'Tracks the file name of the COA for a given fair type';

COMMENT ON COLUMN "BKFAIR01"."LU_COA_TAXES"."COA_TAX_SELECTION" IS 'Indicates the type of selection None (N), Book_Fairs (B), Customer (C).';

COMMENT ON COLUMN "BKFAIR01"."LU_COA_TAXES"."COA_TAX_START_DATE" IS 'Tracks the starting date of the COA.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_COA_TAXES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_LU_COA_TAXES" ON "BKFAIR01"."LU_COA_TAXES" 
                ("COA_TAX_FAIR_TYPE" ASC,
                 "COA_TAX_SELECTION" ASC,
                 "COA_TAX_START_DATE" ASC)
                INCLUDE ("COA_TAX_FILE_NAME" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_COA_TAXES"

ALTER TABLE "BKFAIR01"."LU_COA_TAXES" 
        ADD CONSTRAINT "XPKC_LU_COA_TAXES" PRIMARY KEY
                ("COA_TAX_FAIR_TYPE",
                 "COA_TAX_SELECTION",
                 "COA_TAX_START_DATE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "EMAIL_TYPE" VARCHAR(10) NOT NULL , 
                  "EMAIL_SEND_DATE" TIMESTAMP NOT NULL WITH DEFAULT  , 
                  "EMAIL" VARCHAR(75) NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX" IS 'Release 22; Tracks the email types sent to a given school just once; The application will be populated that table with specific types such as E22,E23 each time such email is sent.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"."EMAIL" IS 'Tracks the email of the Chairperson to whom it was sent.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"."EMAIL_SEND_DATE" IS 'Timestamp at which the record is entered corresponding to the email sent date. This column can be understood as a create_date.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"."EMAIL_TYPE" IS 'Tracks specific email_type that need to be sent just once to a school.';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"."FAIR_ID" IS 'Tracks the Fair ID';

COMMENT ON COLUMN "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"."SCHOOL_ID" IS 'Tracks the School ID';


-- DDL Statements for indexes on Table "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"

CREATE INDEX "BKFAIR01"."X_CHAIRPERSONSCHOOLEMAIL_DATE" ON "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX" 
                ("EMAIL_SEND_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_CHAIRPERSONEMAILSCHOOLTAX_3COL" ON "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX" 
                ("FAIR_ID" ASC,
                 "SCHOOL_ID" ASC,
                 "EMAIL_TYPE" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX"

ALTER TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_SCHOOL_TAX" 
        ADD CONSTRAINT "XPK_CHAIRPERSONEMAILSCHOOLTAX_3COL" PRIMARY KEY
                ("FAIR_ID",
                 "SCHOOL_ID",
                 "EMAIL_TYPE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_FAIR_TYPE_PRODUCT_ID"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_FAIR_TYPE_PRODUCT_ID"  (
                  "PRODUCT_ID" VARCHAR(2) NOT NULL , 
                  "FAIR_TYPE" VARCHAR(1) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 


-- DDL Statements for indexes on Table "BKFAIR01"."LU_FAIR_TYPE_PRODUCT_ID"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_LU_FAIRTYPEPRODUCTID" ON "BKFAIR01"."LU_FAIR_TYPE_PRODUCT_ID" 
                ("PRODUCT_ID" ASC,
                 "FAIR_TYPE" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_FAIR_TYPE_PRODUCT_ID"

ALTER TABLE "BKFAIR01"."LU_FAIR_TYPE_PRODUCT_ID" 
        ADD CONSTRAINT "XPK_LU_FAIRTYPEPRODUCTID" PRIMARY KEY
                ("PRODUCT_ID",
                 "FAIR_TYPE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FSRPERSON"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FSRPERSON"  (
                  "ID" BIGINT NOT NULL , 
                  "FSR_FIRST_NAME" VARCHAR(40) , 
                  "FSR_LAST_NAME" VARCHAR(40) , 
                  "FSR_PHONE" VARCHAR(40) , 
                  "FSR_EMAIL" VARCHAR(75) , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."FSRPERSON" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."FSRPERSON" IS 'Recreated for Release 23; Stores the FSR Person information which comes from CRM Real Time Messages.';

COMMENT ON COLUMN "BKFAIR01"."FSRPERSON"."FSR_EMAIL" IS 'Stores the FSR person email.';

COMMENT ON COLUMN "BKFAIR01"."FSRPERSON"."FSR_FIRST_NAME" IS 'Stores the FSR person first name.';

COMMENT ON COLUMN "BKFAIR01"."FSRPERSON"."FSR_LAST_NAME" IS 'Stores the FSR person last name.';

COMMENT ON COLUMN "BKFAIR01"."FSRPERSON"."FSR_PHONE" IS 'Stores the FSR person phone number.';

COMMENT ON COLUMN "BKFAIR01"."FSRPERSON"."ID" IS 'Primay key column coming from CRM via messages.';


-- DDL Statements for indexes on Table "BKFAIR01"."FSRPERSON"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FSRPERSN_ID" ON "BKFAIR01"."FSRPERSON" 
                ("ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FSRPERSON"

ALTER TABLE "BKFAIR01"."FSRPERSON" 
        ADD CONSTRAINT "XPKC_FSRPERSN_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR"  (
                  "ID" BIGINT NOT NULL , 
                  "SCHOOL_ID" BIGINT NOT NULL , 
                  "SALESPERSON_ID" BIGINT NOT NULL , 
                  "DELIVERY_DATE" DATE , 
                  "PICKUP_DATE" DATE , 
                  "REGION_ID" VARCHAR(2) NOT NULL WITH DEFAULT 'XX' , 
                  "FAIR_TYPE" VARCHAR(1) , 
                  "PRODUCT_ID" VARCHAR(2) , 
                  "BRANCH_ID" INTEGER , 
                  "FSRPERSON_ID" BIGINT , 
                  "FAIR_STATUS" CHAR(1) , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP , 
                  "OFE_SALES_AMOUNT" DECIMAL(10,2) , 
                  "FAIR_ENROLLMENT" DECIMAL(10,0) , 
                  "FAIR_CLASS" CHAR(1) , 
                  "CHANGED_PRODUCT" CHAR(1) WITH DEFAULT 'N' )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."FAIR" PCTFREE 15;

COMMENT ON TABLE "BKFAIR01"."FAIR" IS 'Stores the Bookfair information coming from CRM real time messages; Reduced data structure from original single fair table from Summer 2013.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."BRANCH_ID" IS 'To store Branch_ID Categories & Files will display based on FAIR_ID, PRODUCT_ID, REGION_ID, BRANCH_ID and Availability dates.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."CHANGED_PRODUCT" IS 'Release 24; When set to -Y-, allows the user to be displayed an overlay warning them of the product ID modification for that fair.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."CREATE_DATE" IS 'Stores the timestamp when this record was created.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."DELIVERY_DATE" IS 'Stores the fair effective starting date.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."FAIR_CLASS" IS 'Release 24: Tracks the Class of the Fair; S: For Profit; B: BOGO.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."FAIR_ENROLLMENT" IS 'Release 24; Tracks the Enrollment of that fair which numeric data will be used in calculation in a later release; the column is populated by force at the application level.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."FAIR_STATUS" IS 'Stores the fair status as of Summer 2013.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."FAIR_TYPE" IS 'Fair Types are C (core), B (bogo), P (premium), X (custom).';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."FSRPERSON_ID" IS 'Stores the Field Salesperson Representative ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."ID" IS 'Primary Key column; tracks the unique fair ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."OFE_SALES_AMOUNT" IS 'Release 23; Tracls the dollar amount for sales accumulated via OFE setup.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."PICKUP_DATE" IS 'Stores the fair effective end date.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."PRODUCT_ID" IS 'Product_ids are SC, SZ (core), SG (bogo), SE, ST (premium), L1, L2, L3 (custom),SJ (middle school core), J2 (middle school premium), TT (Sample account elementary school), TM (Sample account middle school).';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."SALESPERSON_ID" IS 'Stores the Sales person ID which comes from CRM from Summer 2013.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."SCHOOL_ID" IS 'Stores the school ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR"."UPDATE_DATE" IS 'Stores the timestamp when this record is updated.';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."IDX_DLVRYDATE" ON "BKFAIR01"."FAIR" 
                ("DELIVERY_DATE" DESC,
                 "ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."IDX_PICKUPDATE" ON "BKFAIR01"."FAIR" 
                ("PICKUP_DATE" DESC,
                 "ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."X2_FAIR" ON "BKFAIR01"."FAIR" 
                ("ID" ASC,
                 "REGION_ID" ASC)
                DISALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."X2FAIR" ON "BKFAIR01"."FAIR" 
                ("PRODUCT_ID" ASC,
                 "ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."X3_FAIR_ID" ON "BKFAIR01"."FAIR" 
                ("ID" ASC,
                 "DELIVERY_DATE" ASC,
                 "PRODUCT_ID" ASC)
                PCTFREE 15 ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."XFK_FAIR_ISSID" ON "BKFAIR01"."FAIR" 
                ("ID" ASC,
                 "SCHOOL_ID" ASC,
                 "SALESPERSON_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."XFK_FAIR_SALSPN_ID" ON "BKFAIR01"."FAIR" 
                ("SALESPERSON_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."XFK_FAIR_SCHOOL_ID" ON "BKFAIR01"."FAIR" 
                ("SCHOOL_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."XFK_FAIR_SDDATE" ON "BKFAIR01"."FAIR" 
                ("SCHOOL_ID" ASC,
                 "DELIVERY_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE INDEX "BKFAIR01"."XFK_FAIR_SPDATE" ON "BKFAIR01"."FAIR" 
                ("SCHOOL_ID" ASC,
                 "PICKUP_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FAIR_ID" ON "BKFAIR01"."FAIR" 
                ("ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR"

ALTER TABLE "BKFAIR01"."FAIR" 
        ADD CONSTRAINT "XPKC_FAIR_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_CIS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_CIS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "GRAY_STATE" VARCHAR(1) NOT NULL WITH DEFAULT 'N' , 
                  "PAYMENT_RCVD" VARCHAR(1) , 
                  "TRK_DELIVERY_DATE" DATE , 
                  "TRK_PICKUP_DATE" DATE , 
                  "PREFAIR_DESCRIPTION" VARCHAR(30) , 
                  "PREFAIR_VALUE" DECIMAL(10,2) , 
                  "ATTENDED_WORKSHOP_DESCRIPTION" VARCHAR(35) , 
                  "ATTENDED_WORKSHOP_VALUE" DECIMAL(10,2) NOT NULL WITH DEFAULT 0.00 , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP , 
                  "OFE_SALES" CHAR(1) WITH DEFAULT 'N' , 
                  "SECOND_BOGO_BOOKING" CHAR(1) , 
                  "OFE_SALES_DATE" TIMESTAMP , 
                  "TRK_DELIVERY_COMPLETE" CHAR(1) WITH DEFAULT 'N' , 
                  "TRK_PICKUP_COMPLETE" CHAR(1) WITH DEFAULT 'N' )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."FAIR_CIS" PCTFREE 15;

COMMENT ON TABLE "BKFAIR01"."FAIR_CIS" IS 'Stores the Bookfair information coming from CIS via flat files and loaded daily; split from the original single fair table from Summer 2013.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_CIS"."FAIR_ID" IS 'Primary Key column; tracks the unique fair ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_CIS"."GRAY_STATE" IS 'The new business rules for the message that will display when TAX_RATE and GRAY_STATE= Y on the web site.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_CIS"."OFE_SALES" IS 'When set to N and fair dates changed, the ofe dates are re-computed; Rel 23.0.1: Added default value of N towards integration to OFE into Real Time Messaging; data now comes from Real Time Messaging.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_CIS"."OFE_SALES_DATE" IS 'Release 24: tracks the timestamp at which the column ofe_sales has been updated via Optimmum Returns(OR).';

COMMENT ON COLUMN "BKFAIR01"."FAIR_CIS"."PAYMENT_RCVD" IS 'To store whether Book Fair IT received the Payment Y means Yes / N means No.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_CIS"."TRK_DELIVERY_COMPLETE" IS 'Rel. 24.5: Defaulted to -N- when the record is created, it is updated to -Y- when the Truck_Delivery event will have been completed.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_CIS"."TRK_PICKUP_COMPLETE" IS 'Rel. 24.5: Defaulted to -N- when the record is created, it is updated to -Y- when the Truck_Pickup event will have been completed.';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_CIS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FAIRCIS_FAIRID" ON "BKFAIR01"."FAIR_CIS" 
                ("FAIR_ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_CIS"

ALTER TABLE "BKFAIR01"."FAIR_CIS" 
        ADD CONSTRAINT "XPKC_FAIRCIS_FAIRID" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CANCELLED_FAIRS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CANCELLED_FAIRS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."CANCELLED_FAIRS" IS 'Release 23 Real time summer 2013; Stores all fair IDs that have been cancelled by Chairperson/Business.';

COMMENT ON COLUMN "BKFAIR01"."CANCELLED_FAIRS"."CREATE_DATE" IS 'Tracks the timestamp when the fair got cancelled and all its data removed.';

COMMENT ON COLUMN "BKFAIR01"."CANCELLED_FAIRS"."FAIR_ID" IS 'Tracks the fair ID.';






-- DDL Statements for indexes on Table "BKFAIR01"."CANCELLED_FAIRS"

CREATE INDEX "BKFAIR01"."X_CANCELLEDFAIRS_FAIR_ID" ON "BKFAIR01"."CANCELLED_FAIRS" 
                ("FAIR_ID" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES"  (
                  "ERROR_ID" VARCHAR(6) NOT NULL , 
                  "ROOT" VARCHAR(25) NOT NULL , 
                  "DESCRIPTION" VARCHAR(50) NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"; 

COMMENT ON TABLE "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES" IS 'Release 23 Real Time; Reference table which stores all the possible failure codes which could be issued from processing bkfair CRM incoming nessages.';

COMMENT ON COLUMN "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES"."DESCRIPTION" IS 'Stores the description of the email; for the condition, refer to the document BF_CPT_Funtional spec, Section of Incoming Message Failures.';

COMMENT ON COLUMN "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES"."ERROR_ID" IS 'Primary Key column; tracks the unique Error ID for CRM incoming message errors.';

COMMENT ON COLUMN "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES"."ROOT" IS 'Refers to the CRM XML Codem message root section such as Fair data, Person data, Email datacoming into the message. etc.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_LUCRMINERRORCODES" ON "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES" 
                ("ERROR_ID" ASC)
                INCLUDE ("ROOT" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES"

ALTER TABLE "BKFAIR01"."LU_CRM_INCOMING_ERROR_CODES" 
        ADD CONSTRAINT "XPKC_LUCRMINERRORCODES" PRIMARY KEY
                ("ERROR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES"  (
                  "ERROR_ID" VARCHAR(3) NOT NULL , 
                  "ROOT" VARCHAR(25) NOT NULL , 
                  "DESCRIPTION" VARCHAR(50) NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

COMMENT ON TABLE "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES" IS 'Release 23 Real Time; Reference table which stores all the possible failure codes which could be issued from processing bkfair Toolkit outgoing nessages.';

COMMENT ON COLUMN "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES"."DESCRIPTION" IS 'Stores the description of the email; for the condition, refer to the document BF_CPT_Funtional spec, Section of Outgoing Message Failures.';

COMMENT ON COLUMN "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES"."ERROR_ID" IS 'Primary Key column; tracks the unique Error ID for bkfair toolkit outgoing message errors.';

COMMENT ON COLUMN "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES"."ROOT" IS 'Refers to the Toolkit Bkfair XML Codem message root section such as, COA email etc.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_LUBKFAIROUTERRORCODES" ON "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES" 
                ("ERROR_ID" ASC)
                INCLUDE ("ROOT" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES"

ALTER TABLE "BKFAIR01"."LU_BKFAIR_OUTGOING_ERROR_CODES" 
        ADD CONSTRAINT "XPKC_LUBKFAIROUTERRORCODES" PRIMARY KEY
                ("ERROR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"  (
                  "ID" BIGINT NOT NULL GENERATED BY DEFAULT AS IDENTITY (  
                    START WITH +1000  
                    INCREMENT BY +1  
                    MINVALUE +1  
                    MAXVALUE +9223372036854775807  
                    NO CYCLE  
                    CACHE 50  
                    ORDER ), 
                  "ROOT" VARCHAR(25) , 
                  "CORP_ID" VARCHAR(75) , 
                  "TRANSACTION_DATE" TIMESTAMP , 
                  "ERROR_ID" VARCHAR(140) , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES" IS 'Release 23 Real Time; Stores all Fair related CRM incoming message errors captured by the application.';

COMMENT ON COLUMN "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"."CORP_ID" IS 'Refers to the CRM XML linguo Primary key for a given message, e.g. fair_id, salesperson_id etc';

COMMENT ON COLUMN "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"."CREATE_DATE" IS 'Stores the creation date of the record.';

COMMENT ON COLUMN "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"."ERROR_ID" IS 'Stores comma separated Error IDs for a given CORP_ID produced by the application based onon the XML message error data type following a set of rules.';

COMMENT ON COLUMN "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"."ID" IS 'Primary Key column; generated sequential id (identity key).';

COMMENT ON COLUMN "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"."ROOT" IS 'Refers to the CRM XML Codem message root section such as COA email etc.';

COMMENT ON COLUMN "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"."TRANSACTION_DATE" IS 'Stores the XML transaction date from message.';


-- DDL Statements for indexes on Table "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_CRMINMSG_FAILURES" ON "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES" 
                ("ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES"

ALTER TABLE "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES" 
        ADD CONSTRAINT "XPKC_CRMINMSG_FAILURES" PRIMARY KEY
                ("ID");



ALTER TABLE "BKFAIR01"."CRM_INCOMING_MESSAGE_FAILURES" ALTER COLUMN "ID" RESTART WITH 9499;

------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_MESSAGE_RETRY_COUNTS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_MESSAGE_RETRY_COUNTS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "COUNT" SMALLINT , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."FAIR_MESSAGE_RETRY_COUNTS" IS 'tracks the number of times a fair message run thru an error; There is a maximum of 3 retries for submitting a message to the process; When failing a third time the record for that fair is deleted.';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_MESSAGE_RETRY_COUNTS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_FAIRMSGRETRYCOUNTS" ON "BKFAIR01"."FAIR_MESSAGE_RETRY_COUNTS" 
                ("FAIR_ID" ASC)
                INCLUDE ("COUNT" )
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_MESSAGE_RETRY_COUNTS"

ALTER TABLE "BKFAIR01"."FAIR_MESSAGE_RETRY_COUNTS" 
        ADD CONSTRAINT "XPK_FAIRMSGRETRYCOUNTS" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_LOAD_FEEDBACK"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_LOAD_FEEDBACK"  (
                  "ID" BIGINT NOT NULL GENERATED BY DEFAULT AS IDENTITY (  
                    START WITH +1000  
                    INCREMENT BY +1  
                    MINVALUE +1  
                    MAXVALUE +9223372036854775807  
                    NO CYCLE  
                    CACHE 200  
                    ORDER ), 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "TABLE_NAME" VARCHAR(50) , 
                  "INSERTION_COUNT" INTEGER , 
                  "UPDATE_COUNT" INTEGER , 
                  "MISSING_PARENT_COUNT" INTEGER , 
                  "PROCEDURE_NAME" VARCHAR(50) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."FAIR_LOAD_FEEDBACK" IS 'Release 23: provides data feedback following a data load from CIS files. It was necessary to capture missing parent records due to the introduction of Real Time Messaging in which FAIR.ID may have been rejected.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_LOAD_FEEDBACK"."CREATE_DATE" IS 'Track the timestamp for the record creation.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_LOAD_FEEDBACK"."ID" IS 'Identity column automatically generated.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_LOAD_FEEDBACK"."INSERTION_COUNT" IS 'Tracks the number of rows being inserted for the table_name.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_LOAD_FEEDBACK"."MISSING_PARENT_COUNT" IS 'Tracks the number of rows that were skipped due to a Missing Parent record which could be either from FAIR or SCHOOL tables.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_LOAD_FEEDBACK"."TABLE_NAME" IS 'Track the table name loaded via CIS file and potentially could be not fully populated.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_LOAD_FEEDBACK"."UPDATE_COUNT" IS 'Tracks the number of rows being updated for the table_name.';






-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_LOAD_FEEDBACK"

CREATE INDEX "BKFAIR01"."X_FAIRLOADFEEDBACK_DATE" ON "BKFAIR01"."FAIR_LOAD_FEEDBACK" 
                ("CREATE_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_LOAD_FEEDBACK"

CREATE INDEX "BKFAIR01"."X_FAIRLOADFEEDBACK_TABLE" ON "BKFAIR01"."FAIR_LOAD_FEEDBACK" 
                ("TABLE_NAME" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_LOAD_FEEDBACK"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FAIRLOADFEEDBACK_ID" ON "BKFAIR01"."FAIR_LOAD_FEEDBACK" 
                ("ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
ALTER TABLE "BKFAIR01"."FAIR_LOAD_FEEDBACK" ALTER COLUMN "ID" RESTART WITH 4399;

------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_DATE_CHANGED"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_DATE_CHANGED"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "DATE_CHANGED" CHAR(1) NOT NULL WITH DEFAULT 'N' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."FAIR_DATE_CHANGED" IS 'Release 23.5; Populated each time by the Toolkit application when either one or both of the fair dates has been changed. it sets the column date_changed to Y.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_DATE_CHANGED"."CREATE_DATE" IS 'Tracks the creation date of the record.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_DATE_CHANGED"."DATE_CHANGED" IS 'Indicates by 1- Y that a fair date has changed; 2- by N when the daily loading script has reset the flag.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_DATE_CHANGED"."FAIR_ID" IS 'Primary Key column; tracks the unique fair ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_DATE_CHANGED"."UPDATE_DATE" IS 'Tracks the updated date when the loading script reset the flag.';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_DATE_CHANGED"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FAIRDATECHANGED" ON "BKFAIR01"."FAIR_DATE_CHANGED" 
                ("FAIR_ID" ASC)
                INCLUDE ("DATE_CHANGED" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_DATE_CHANGED"

ALTER TABLE "BKFAIR01"."FAIR_DATE_CHANGED" 
        ADD CONSTRAINT "XPKC_FAIRDATECHANGED" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."SALESPERSON"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."SALESPERSON"  (
                  "ID" BIGINT NOT NULL , 
                  "FIRST_NAME" VARCHAR(40) NOT NULL , 
                  "LAST_NAME" VARCHAR(40) NOT NULL , 
                  "EMAIL" VARCHAR(75) , 
                  "PHONE_NUMBER" VARCHAR(40) , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."SALESPERSON" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."SALESPERSON" IS 'Recreated for Release 23; Stores the salesperson information which comes from CRM Real Time Messages.';

COMMENT ON COLUMN "BKFAIR01"."SALESPERSON"."EMAIL" IS 'Stores the sales person email.';

COMMENT ON COLUMN "BKFAIR01"."SALESPERSON"."FIRST_NAME" IS 'Stores the sales person first name.';

COMMENT ON COLUMN "BKFAIR01"."SALESPERSON"."ID" IS 'Primay key column coming from CRM via messages.';

COMMENT ON COLUMN "BKFAIR01"."SALESPERSON"."LAST_NAME" IS 'Stores the sales person last name.';

COMMENT ON COLUMN "BKFAIR01"."SALESPERSON"."PHONE_NUMBER" IS 'Stores the sales person phone number.';


-- DDL Statements for indexes on Table "BKFAIR01"."SALESPERSON"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_SALESPERSN_ID" ON "BKFAIR01"."SALESPERSON" 
                ("ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."SALESPERSON"

ALTER TABLE "BKFAIR01"."SALESPERSON" 
        ADD CONSTRAINT "XPKC_SALESPERSN_ID" PRIMARY KEY
                ("ID");






------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."PLANNER_GOALS"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."PLANNER_GOALS"  (
                  "PERCENTAGE_1" INTEGER NOT NULL WITH DEFAULT 5 , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "PERCENTAGE_2" INTEGER , 
                  "PERCENTAGE_3" INTEGER , 
                  "PUBLISH_DATE" TIMESTAMP WITH DEFAULT '1900-01-01-01.00.00.000000' )   
                 IN "BKF4K02D" INDEX IN "BKF4K02I"  ; 

ALTER TABLE "UNPUBLISHED"."PLANNER_GOALS" PCTFREE 10;

COMMENT ON TABLE "UNPUBLISHED"."PLANNER_GOALS" IS 'Planner Release 24: Keeps track of the different defaulted goal values set up by the Administrator.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_GOALS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_GOALS"."PERCENTAGE_1" IS 'First Percentage value which must be populated.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_GOALS"."PERCENTAGE_2" IS 'Second Percentage value which is nullable.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_GOALS"."PERCENTAGE_3" IS 'Third Percentage value which is nullable.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_GOALS"."PUBLISH_DATE" IS 'Tracks the timestamp when the data was published to the schema bkfair01.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_GOALS"."UPDATE_DATE" IS 'Tracks the update date of that record.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PLANNER_GOALS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PLANNER_GOALS"  (
                  "PERCENTAGE_1" INTEGER NOT NULL WITH DEFAULT 5 , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "PERCENTAGE_2" INTEGER , 
                  "PERCENTAGE_3" INTEGER , 
                  "PUBLISH_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."PLANNER_GOALS" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."PLANNER_GOALS" IS 'Planner Release 24: Keeps track of the different defaulted goal values set up by the Administrator.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_GOALS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_GOALS"."PERCENTAGE_1" IS 'First Percentage value which must be populated.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_GOALS"."PERCENTAGE_2" IS 'Second Percentage value which is nullable.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_GOALS"."PERCENTAGE_3" IS 'Third Percentage value which is nullable.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_GOALS"."PUBLISH_DATE" IS 'Tracks the timestamp when the data was published to the schema bkfair01.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_GOALS"."UPDATE_DATE" IS 'Tracks the update date of that record.';





------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."PLANNER_CATEGORIES"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."PLANNER_CATEGORIES"  (
                  "ID" INTEGER NOT NULL , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "DESCRIPTION" VARCHAR(260) NOT NULL , 
                  "SEQUENCE" INTEGER NOT NULL , 
                  "ACTIVE" CHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "ACTIVE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "INACTIVE_DATE" TIMESTAMP )   
                 IN "BKF4K02D" INDEX IN "BKF4K02I"  ; 

ALTER TABLE "UNPUBLISHED"."PLANNER_CATEGORIES" PCTFREE 20;

COMMENT ON TABLE "UNPUBLISHED"."PLANNER_CATEGORIES" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Categories which will be used in turn by the Chairperson when creating her detailed schedules.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_CATEGORIES"."ACTIVE" IS 'When set to Y, the Category is immediately displayed to the Chairperson; and is immediately removed when set to N unless that Category had already been selected.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_CATEGORIES"."ACTIVE_DATE" IS 'Tracks the activation date of that category.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_CATEGORIES"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_CATEGORIES"."DESCRIPTION" IS 'Describes the category.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_CATEGORIES"."ID" IS 'Primary Key column; tracks the unique Category ID.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_CATEGORIES"."INACTIVE_DATE" IS 'Tracks the inactivation date of that category.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_CATEGORIES"."SEQUENCE" IS 'Tracks the sequence of the Category which will be used to display the Categories by that number.  Its uniqueness is enforced at the application level.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_CATEGORIES"."TITLE" IS 'Tracks the displayed category name.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_CATEGORIES"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_CATEGORIES"

CREATE UNIQUE INDEX "UNPUBLISHED"."XPK_PLANNERCATEG_ID" ON "UNPUBLISHED"."PLANNER_CATEGORIES" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "UNPUBLISHED"."PLANNER_CATEGORIES"

ALTER TABLE "UNPUBLISHED"."PLANNER_CATEGORIES" 
        ADD CONSTRAINT "XPK_PLANNERCATEG" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PLANNER_CATEGORIES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PLANNER_CATEGORIES"  (
                  "ID" INTEGER NOT NULL , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "DESCRIPTION" VARCHAR(260) NOT NULL , 
                  "SEQUENCE" INTEGER NOT NULL , 
                  "ACTIVE" CHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "ACTIVE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "INACTIVE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."PLANNER_CATEGORIES" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."PLANNER_CATEGORIES" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Categories which will be used in turn by the Chairperson when creating her detailed schedules.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_CATEGORIES"."ACTIVE" IS 'When set to Y, the Category is immediately displayed to the Chairperson; and is immediately removed when set to N unless that Category had already been selected.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_CATEGORIES"."ACTIVE_DATE" IS 'Tracks the activation date of that category.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_CATEGORIES"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_CATEGORIES"."DESCRIPTION" IS 'Describes the category.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_CATEGORIES"."ID" IS 'Primary Key column; tracks the unique Category ID.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_CATEGORIES"."INACTIVE_DATE" IS 'Tracks the inactivation date of that category.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_CATEGORIES"."SEQUENCE" IS 'Tracks the sequence of the Category which will be used to display the Categories by that number.  Its uniqueness is enforced at the application level.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_CATEGORIES"."TITLE" IS 'Tracks the displayed category name.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_CATEGORIES"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_CATEGORIES"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_PLANNERCATEG_ID" ON "BKFAIR01"."PLANNER_CATEGORIES" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PLANNER_CATEGORIES"

ALTER TABLE "BKFAIR01"."PLANNER_CATEGORIES" 
        ADD CONSTRAINT "XPK_PLANNERCATEG" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."PLANNER_PROGRAMS"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."PLANNER_PROGRAMS"  (
                  "ID" INTEGER NOT NULL , 
                  "CATEGORY_ID" INTEGER NOT NULL , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "ACTIVE" CHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "ACTIVE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "DESCRIPTION" VARCHAR(260) NOT NULL , 
                  "FILE_NAME" VARCHAR(255) , 
                  "INACTIVE_DATE" TIMESTAMP )   
                 IN "BKF4K02D" INDEX IN "BKF4K02I"  ; 

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAMS" PCTFREE 20;

COMMENT ON TABLE "UNPUBLISHED"."PLANNER_PROGRAMS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Programs under a given Category which will be used in turn by the Chairperson when creating her detailed schedules.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."ACTIVE" IS 'When set to Y, the Program is immediately displayed to the Chairperson; When set to N, it is immediately removed unless that Program had already been used by that Chairperson.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."ACTIVE_DATE" IS 'Tracks the activation date of that Program record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."CATEGORY_ID" IS 'Foreign key tracking the category ID representing the parent record of the program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."CREATE_DATE" IS 'Tracks the creation date of the Program record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."DESCRIPTION" IS 'Describes the Program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."FILE_NAME" IS 'Tracks the File Name existing in Teamsite of the uploaded image of the corresponding program. Its data length is to match the asset.file_name length for data consistency.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."ID" IS 'Primary Key column; tracks the unique Program ID.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."INACTIVE_DATE" IS 'Tracks the inactivation date of that Program record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."TITLE" IS 'Tracks the displayed Program name.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAMS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_PROGRAMS"

CREATE INDEX "UNPUBLISHED"."XFK_PLANRPRGM_CATEGID" ON "UNPUBLISHED"."PLANNER_PROGRAMS" 
                ("CATEGORY_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_PROGRAMS"

CREATE UNIQUE INDEX "UNPUBLISHED"."XPK_PLANNERPROGRAM_ID" ON "UNPUBLISHED"."PLANNER_PROGRAMS" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "UNPUBLISHED"."PLANNER_PROGRAMS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAMS" 
        ADD CONSTRAINT "XPK_PLANNERPROGRAM_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PLANNER_PROGRAMS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PLANNER_PROGRAMS"  (
                  "ID" INTEGER NOT NULL , 
                  "CATEGORY_ID" INTEGER NOT NULL , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "ACTIVE" CHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "ACTIVE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "DESCRIPTION" VARCHAR(260) NOT NULL , 
                  "FILE_NAME" VARCHAR(255) , 
                  "INACTIVE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAMS" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."PLANNER_PROGRAMS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Programs under a given Category which will be used in turn by the Chairperson when creating her detailed schedules.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."ACTIVE" IS 'When set to Y, the Program is immediately displayed to the Chairperson; When set to N, it is immediately removed unless that Program had already been used by that Chairperson.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."ACTIVE_DATE" IS 'Tracks the activation date of that Program record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."CATEGORY_ID" IS 'Foreign key tracking the category ID representing the parent record of the program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."CREATE_DATE" IS 'Tracks the creation date of the Program record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."DESCRIPTION" IS 'Describes the Program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."FILE_NAME" IS 'Tracks the File Name existing in Teamsite of the uploaded image of the corresponding program. Its data length is to match the asset.file_name length for data consistency.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."ID" IS 'Primary Key column; tracks the unique Program ID.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."INACTIVE_DATE" IS 'Tracks the inactivation date of that Program record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."TITLE" IS 'Tracks the displayed Program name.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAMS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_PROGRAMS"

CREATE INDEX "BKFAIR01"."XFK_PLANRPRGM_CATEGID" ON "BKFAIR01"."PLANNER_PROGRAMS" 
                ("CATEGORY_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_PROGRAMS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_PLANNERPROGRAM_ID" ON "BKFAIR01"."PLANNER_PROGRAMS" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PLANNER_PROGRAMS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAMS" 
        ADD CONSTRAINT "XPK_PLANNERPROGRAM_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"  (
                  "ID" INTEGER NOT NULL , 
                  "PROGRAM_ID" INTEGER NOT NULL , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "DESCRIPTION" VARCHAR(460) NOT NULL , 
                  "ACTIVE" CHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "ACTIVE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "NO_OF_DAYS" INTEGER , 
                  "DATE_FLAG" CHAR(1) , 
                  "INACTIVE_DATE" TIMESTAMP , 
                  "PARTIAL_EDIT_CKBOX" CHAR(1) WITH DEFAULT 'N' )   
                 IN "BKF4K02D" INDEX IN "BKF4K02I"  ; 

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_TASKS" PCTFREE 20;

COMMENT ON TABLE "UNPUBLISHED"."PLANNER_PROGRAM_TASKS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Tasks linked to a given Program which will be used in turn by the Chairperson when creating her detailed schedules.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."ACTIVE" IS 'When set to Y, the Task is immediately displayed to the Chairperson; When set to N is immediately removed from being offered as a choice to the Chairperson unless that Task had already been used by the Chairperson.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."ACTIVE_DATE" IS 'Tracks the activation date of that Program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."DATE_FLAG" IS 'When populated with the value S, it indicates that the display in the planner must take the Fair Start-Date as its origin; When populated with E, the Fair End-Date becomes the origin.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."DESCRIPTION" IS 'Describes the Task.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."ID" IS 'Primary Key column; tracks the unique Task ID created via the sequence SEQ_EVENT_TASK.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."INACTIVE_DATE" IS 'Tracks the deactivation date of that Program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."NO_OF_DAYS" IS 'Provides the number of days for being displayed on the planner before or after whichever selected date [ Fair-start-date:S, fair-end-date:E ] via the column date_flag.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."PARTIAL_EDIT_CKBOX" IS 'Rel. 24.5: Allow edition on the overlay.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."PROGRAM_ID" IS 'Foreign key tracking the Program ID representing the parent record of the Task.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."TITLE" IS 'Tracks the displayed Task title.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"

CREATE INDEX "UNPUBLISHED"."XFK_PLANNERPRGMTASK_PRGMID" ON "UNPUBLISHED"."PLANNER_PROGRAM_TASKS" 
                ("PROGRAM_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"

CREATE UNIQUE INDEX "UNPUBLISHED"."XPK_PLANNERPRGMTASK_ID" ON "UNPUBLISHED"."PLANNER_PROGRAM_TASKS" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_TASKS" 
        ADD CONSTRAINT "XPK_PLANNERPRGMTASK_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PLANNER_PROGRAM_TASKS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PLANNER_PROGRAM_TASKS"  (
                  "ID" INTEGER NOT NULL , 
                  "PROGRAM_ID" INTEGER NOT NULL , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "DESCRIPTION" VARCHAR(460) NOT NULL , 
                  "ACTIVE" CHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "ACTIVE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "NO_OF_DAYS" INTEGER , 
                  "DATE_FLAG" CHAR(1) , 
                  "INACTIVE_DATE" TIMESTAMP , 
                  "PARTIAL_EDIT_CKBOX" CHAR(1) WITH DEFAULT 'N' )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_TASKS" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."PLANNER_PROGRAM_TASKS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Tasks linked to a given Program which will be used in turn by the Chairperson when creating her detailed schedules.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."ACTIVE" IS 'When set to Y, the Task is immediately displayed to the Chairperson; When set to N is immediately removed from being offered as a choice to the Chairperson unless that Task had already been used by the Chairperson.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."ACTIVE_DATE" IS 'Tracks the activation date of that Program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."DATE_FLAG" IS 'When populated with the value S, it indicates that the display in the planner must take the Fair Start-Date as its origin; When populated with E, the Fair End-Date becomes the origin.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."DESCRIPTION" IS 'Describes the Task.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."ID" IS 'Primary Key column; tracks the unique Task ID created via the sequence SEQ_EVENT_TASK.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."INACTIVE_DATE" IS 'Tracks the deactivation date of that Program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."NO_OF_DAYS" IS 'Provides the number of days for being displayed on the planner before or after whichever selected date [ Fair-start-date:S, fair-end-date:E ] via the column date_flag.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."PARTIAL_EDIT_CKBOX" IS 'Rel. 24.5: Allow edition on the overlay.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."PROGRAM_ID" IS 'Foreign key tracking the Program ID representing the parent record of the Task.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."TITLE" IS 'Tracks the displayed Task title.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_TASKS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_PROGRAM_TASKS"

CREATE INDEX "BKFAIR01"."X_PLANNERPRGMTASK_PRGMID" ON "BKFAIR01"."PLANNER_PROGRAM_TASKS" 
                ("PROGRAM_ID" ASC,
                 "ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_PROGRAM_TASKS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_PLANNERPRGMTASK_ID" ON "BKFAIR01"."PLANNER_PROGRAM_TASKS" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PLANNER_PROGRAM_TASKS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_TASKS" 
        ADD CONSTRAINT "XPK_PLANNERPRGMTASK_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS"  (
                  "GROUP_ID" INTEGER NOT NULL , 
                  "PRODUCT_LISTING" VARCHAR(150) NOT NULL , 
                  "GROUP_NAME" VARCHAR(50) NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS" IS 'Planner Release 24: As part of the Administrator Tool, this Reference table tracks all the Product Groupings that will be part of any created Program record.';

COMMENT ON COLUMN "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS"."GROUP_ID" IS 'Primary Key column; tracks the unique Product Grouping ID.';

COMMENT ON COLUMN "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS"."GROUP_NAME" IS 'Tracks the Label of the Product Grouping as it displays on the Planner.';

COMMENT ON COLUMN "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS"."PRODUCT_LISTING" IS 'List of Products assigned to that Group and engineered by Business.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_LUPRGMPRODUCTGRP_ID" ON "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS" 
                ("GROUP_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS"

ALTER TABLE "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS" 
        ADD CONSTRAINT "XPK_LUPRGMPRODUCTGRP_ID" PRIMARY KEY
                ("GROUP_ID");



------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"  (
                  "PROGRAM_ID" INTEGER NOT NULL , 
                  "GROUP_ID" INTEGER NOT NULL , 
                  "CATEGORY_ID" INTEGER NOT NULL , 
                  "SEQUENCE" INTEGER NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "PRODUCT_LISTING" VARCHAR(150) , 
                  "RECOMMENDED_FLAG" CHAR(1) , 
                  "DISPLAY_SCHEDULER_FLAG" CHAR(1) , 
                  "NO_OF_VOLUNTEERS" VARCHAR(5) , 
                  "DURATION" VARCHAR(25) , 
                  "PARTICIPANTS" INTEGER , 
                  "BOOKS_PER_PARTICIPANT" INTEGER , 
                  "BENEFITS_STATEMENT" VARCHAR(210) )   
              IN "BKF4K02D" INDEX IN "BKF4K02I"  ; 

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" PCTFREE 20;

COMMENT ON TABLE "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Product Groupings used under a given Program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."BENEFITS_STATEMENT" IS 'Rel. 24.5: Provides information to the User.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."BOOKS_PER_PARTICIPANT" IS 'Rel. 24.5: Tracks the number of books per participant.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."CATEGORY_ID" IS 'Tracks the category ID; necessary to have as data to follow a business rule which requires uniqueness of [group, sequence] across all programs but not across categories.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."DISPLAY_SCHEDULER_FLAG" IS 'When checked (Y), indicates that the choice should be displayed on the Scheduler.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."DURATION" IS 'Tracks the duration of that Program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."GROUP_ID" IS 'Tracks the Group ID.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."NO_OF_VOLUNTEERS" IS 'Tracks the number of Volunteers.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."PARTICIPANTS" IS 'Rel. 24.5: Tracks the number of participants to that program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."PRODUCT_LISTING" IS 'Tracks the list of the Product IDs selected by the Administrator for that Group ID for that Program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."PROGRAM_ID" IS 'Tracks the Program ID representing the parent record of the Program Product Grouping.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."RECOMMENDED_FLAG" IS 'When selected (Y), indicates that the product grouping as designed by the Administrator is recommended.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."SEQUENCE" IS 'Tracks the sequence.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"

CREATE UNIQUE INDEX "UNPUBLISHED"."XPK_PLANPRGMPRODUCT_2COLS" ON "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" 
                ("PROGRAM_ID" ASC,
                 "GROUP_ID" ASC)
                INCLUDE ("CATEGORY_ID" )
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "XPK_PLANPRGMPRODUCT_2COLS" PRIMARY KEY
                ("PROGRAM_ID",
                 "GROUP_ID");




-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"

CREATE UNIQUE INDEX "UNPUBLISHED"."XUV_PLANPRGMPRODUCT_GRPSEQ" ON "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" 
                ("CATEGORY_ID" ASC,
                 "GROUP_ID" ASC,
                 "SEQUENCE" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"  (
                  "PROGRAM_ID" INTEGER NOT NULL , 
                  "GROUP_ID" INTEGER NOT NULL , 
                  "CATEGORY_ID" INTEGER NOT NULL , 
                  "SEQUENCE" INTEGER NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "PRODUCT_LISTING" VARCHAR(150) , 
                  "RECOMMENDED_FLAG" CHAR(1) , 
                  "DISPLAY_SCHEDULER_FLAG" CHAR(1) , 
                  "NO_OF_VOLUNTEERS" VARCHAR(5) , 
                  "DURATION" VARCHAR(25) , 
                  "PARTICIPANTS" INTEGER , 
                  "BOOKS_PER_PARTICIPANT" INTEGER , 
                  "BENEFITS_STATEMENT" VARCHAR(210) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Product Groupings used under a given Program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."BENEFITS_STATEMENT" IS 'Rel. 24.5: Provides information to the User.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."BOOKS_PER_PARTICIPANT" IS 'Rel. 24.5: Tracks the number of books per participant.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."CATEGORY_ID" IS 'Tracks the category ID; necessary to have as data to follow a business rule which requires uniqueness of [group, sequence] across all programs but not across categories.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."DISPLAY_SCHEDULER_FLAG" IS 'When checked (Y), indicates that the choice should be displayed on the Scheduler.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."DURATION" IS 'Tracks the duration of that Program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."GROUP_ID" IS 'Tracks the Group ID.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."NO_OF_VOLUNTEERS" IS 'Tracks the number of Volunteers.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."PARTICIPANTS" IS 'Rel. 24.5: Tracks the number of paricipants to that program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."PRODUCT_LISTING" IS 'Tracks the list of the Product IDs selected by the Administrator for that Group ID for that Program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."PROGRAM_ID" IS 'Tracks the Program ID representing the parent record of the Program Product Grouping.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."RECOMMENDED_FLAG" IS 'When selected (Y), indicates that the product grouping as designed by the Administrator is recommended.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."SEQUENCE" IS 'Tracks the sequence.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_PLANPRGMPRODUCT_2COLS" ON "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" 
                ("PROGRAM_ID" ASC,
                 "GROUP_ID" ASC)
                INCLUDE ("CATEGORY_ID" )
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "XPK_PLANPRGMPRODUCT_2COLS" PRIMARY KEY
                ("PROGRAM_ID",
                 "GROUP_ID");




-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_PLANPRGMPRODUCT_GRPSEQ" ON "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" 
                ("CATEGORY_ID" ASC,
                 "GROUP_ID" ASC,
                 "SEQUENCE" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_PRODUCT_GROUPINGS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_PRODUCT_GROUPINGS"  (
                  "GROUP_ID" INTEGER NOT NULL , 
                  "PRODUCT_LISTING" VARCHAR(150) NOT NULL , 
                  "GROUP_NAME" VARCHAR(50) NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."LU_PRODUCT_GROUPINGS" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."LU_PRODUCT_GROUPINGS" IS 'Planner Release 24: As part of the Administrator Tool, this Reference table tracks all the PROGRAMLESS Product Groupings.';

COMMENT ON COLUMN "BKFAIR01"."LU_PRODUCT_GROUPINGS"."GROUP_ID" IS 'Primary Key column; tracks the unique Product Grouping ID to be used with Tasks Without Programs.';

COMMENT ON COLUMN "BKFAIR01"."LU_PRODUCT_GROUPINGS"."GROUP_NAME" IS 'Tracks the Label of the Programless Product Grouping as it displays on the Planner.';

COMMENT ON COLUMN "BKFAIR01"."LU_PRODUCT_GROUPINGS"."PRODUCT_LISTING" IS 'List of Products assigned to that Group and engineered by Business.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_PRODUCT_GROUPINGS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_LUPRODUCTGRP_ID" ON "BKFAIR01"."LU_PRODUCT_GROUPINGS" 
                ("GROUP_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_PRODUCT_GROUPINGS"

ALTER TABLE "BKFAIR01"."LU_PRODUCT_GROUPINGS" 
        ADD CONSTRAINT "XPK_LUPRODUCTGRP_ID" PRIMARY KEY
                ("GROUP_ID");



------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."PLANNER_TASKS"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."PLANNER_TASKS"  (
                  "ID" INTEGER NOT NULL , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "DESCRIPTION" VARCHAR(460) NOT NULL , 
                  "ACTIVE" CHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "ACTIVE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "NO_OF_DAYS" INTEGER , 
                  "DATE_FLAG" CHAR(1) , 
                  "INACTIVE_DATE" TIMESTAMP , 
                  "PARTIAL_EDIT_CKBOX" CHAR(1) WITH DEFAULT 'N' )   
                 IN "BKF4K02D" INDEX IN "BKF4K02I"  ; 

ALTER TABLE "UNPUBLISHED"."PLANNER_TASKS" PCTFREE 20;

COMMENT ON TABLE "UNPUBLISHED"."PLANNER_TASKS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Tasks WITHOUT PROGRAMS which will be used in turn by the Chairperson when creating her detailed schedules.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."ACTIVE" IS 'When set to Y, the Task is immediately displayed to the Chairperson; When set to N is immediately removed from being offered as a choice to the Chairperson unless that Task had already been used by the Chairperson.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."ACTIVE_DATE" IS 'Tracks the activation date of that Program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."DATE_FLAG" IS 'When populated with the value S, it indicates that the display in the planner must take the Fair Start-Date as its origin; When populated with E, the Fair End-Date becomes the origin.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."DESCRIPTION" IS 'Describes the Task.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."ID" IS 'Primary Key column; tracks the unique Task ID created via the sequence SEQ_EVENT_TASK.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."INACTIVE_DATE" IS 'Tracks the deactivation date of that Program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."NO_OF_DAYS" IS 'Provides the number of days for being displayed on the planner before or after whichever selected date [ Fair-start-date:S, fair-end-date:E ] via the column date_flag.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."PARTIAL_EDIT_CKBOX" IS 'Rel. 24.5: Allow edition on the overlay.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."TITLE" IS 'Tracks the displayed Task title.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASKS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_TASKS"

CREATE UNIQUE INDEX "UNPUBLISHED"."XPK_PLANNERTASK_ID" ON "UNPUBLISHED"."PLANNER_TASKS" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "UNPUBLISHED"."PLANNER_TASKS"

ALTER TABLE "UNPUBLISHED"."PLANNER_TASKS" 
        ADD CONSTRAINT "XPK_PLANNERTASK_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PLANNER_TASKS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PLANNER_TASKS"  (
                  "ID" INTEGER NOT NULL , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "DESCRIPTION" VARCHAR(460) NOT NULL , 
                  "ACTIVE" CHAR(1) WITH DEFAULT 'Y' , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "ACTIVE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "NO_OF_DAYS" INTEGER , 
                  "DATE_FLAG" CHAR(1) , 
                  "INACTIVE_DATE" TIMESTAMP , 
                  "PARTIAL_EDIT_CKBOX" CHAR(1) WITH DEFAULT 'N' )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I" ; 

ALTER TABLE "BKFAIR01"."PLANNER_TASKS" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."PLANNER_TASKS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Tasks WITHOUT PROGRAMS which will be used in turn by the Chairperson when creating her detailed schedules.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."ACTIVE" IS 'When set to Y, the Task is immediately displayed to the Chairperson; When set to N is immediately removed from being offered as a choice to the Chairperson unless that Task had already been used by the Chairperson.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."ACTIVE_DATE" IS 'Tracks the activation date of that Program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."DATE_FLAG" IS 'When populated with the value S, it indicates that the display in the planner must take the Fair Start-Date as its origin; When populated with E, the Fair End-Date becomes the origin.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."DESCRIPTION" IS 'Describes the Task.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."ID" IS 'Primary Key column; tracks the unique Task ID created via the sequence SEQ_EVENT_TASK.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."INACTIVE_DATE" IS 'Tracks the deactivation date of that Program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."NO_OF_DAYS" IS 'Provides the number of days for being displayed on the planner before or after whichever selected date [ Fair-start-date:S, fair-end-date:E ] via the column date_flag.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."PARTIAL_EDIT_CKBOX" IS 'Rel. 24.5: Allow edition on the overlay.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."TITLE" IS 'Tracks the displayed Task title.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASKS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_TASKS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_PLANNERTASK_ID" ON "BKFAIR01"."PLANNER_TASKS" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PLANNER_TASKS"

ALTER TABLE "BKFAIR01"."PLANNER_TASKS" 
        ADD CONSTRAINT "XPK_PLANNERTASK_ID" PRIMARY KEY
                ("ID");




-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_TASKS"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_PLANNERTASK_ACTIVEID" ON "BKFAIR01"."PLANNER_TASKS" 
                ("ID" ASC,
                 "ACTIVE" ASC)
                ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"  (
                  "TASK_ID" INTEGER NOT NULL , 
                  "GROUP_ID" INTEGER NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "PRODUCT_LISTING" VARCHAR(150) )   
                 IN "BKF4K02D" INDEX IN "BKF4K02I"  ; 

ALTER TABLE "UNPUBLISHED"."PLANNER_TASK_PRODUCTS" PCTFREE 20;

COMMENT ON TABLE "UNPUBLISHED"."PLANNER_TASK_PRODUCTS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Product Groupings for TASKS WITHOUT PROGRAMS.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"."GROUP_ID" IS 'Tracks the Grouping ID unique to the affilitated Task ID; the combination of task ID and Group ID is unique.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"."PRODUCT_LISTING" IS 'Tracks the list of the Product IDs selected by the Administrator for that Group ID for that Task Without Program.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"."TASK_ID" IS 'Tracks the Task ID WITHOUT PROGRAM representing the parent record of the Task Product Grouping.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"

CREATE UNIQUE INDEX "UNPUBLISHED"."XPK_PLANTASKPRODUCT" ON "UNPUBLISHED"."PLANNER_TASK_PRODUCTS" 
                ("TASK_ID" ASC,
                 "GROUP_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"

ALTER TABLE "UNPUBLISHED"."PLANNER_TASK_PRODUCTS" 
        ADD CONSTRAINT "XPK_PLANTASKPRODUCT" PRIMARY KEY
                ("TASK_ID",
                 "GROUP_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PLANNER_TASK_PRODUCTS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PLANNER_TASK_PRODUCTS"  (
                  "TASK_ID" INTEGER NOT NULL , 
                  "GROUP_ID" INTEGER NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "PRODUCT_LISTING" VARCHAR(150) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."PLANNER_TASK_PRODUCTS" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."PLANNER_TASK_PRODUCTS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Product Groupings for TASKS WITHOUT PROGRAMS.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_PRODUCTS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_PRODUCTS"."GROUP_ID" IS 'Tracks the Grouping ID unique to the affilitated Task ID; the combination of task ID and Group ID is unique.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_PRODUCTS"."PRODUCT_LISTING" IS 'Tracks the list of the Product IDs selected by the Administrator for that Group ID for that Task Without Program.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_PRODUCTS"."TASK_ID" IS 'Tracks the Task ID WITHOUT PROGRAM representing the parent record of the Task Product Grouping.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_PRODUCTS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_TASK_PRODUCTS"

CREATE INDEX "BKFAIR01"."X_PLANTASKPRODUCT_LISTING" ON "BKFAIR01"."PLANNER_TASK_PRODUCTS" 
                ("TASK_ID" ASC,
                 "PRODUCT_LISTING" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_TASK_PRODUCTS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_PLANTASKPRODUCT" ON "BKFAIR01"."PLANNER_TASK_PRODUCTS" 
                ("TASK_ID" ASC,
                 "GROUP_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PLANNER_TASK_PRODUCTS"

ALTER TABLE "BKFAIR01"."PLANNER_TASK_PRODUCTS" 
        ADD CONSTRAINT "XPK_PLANTASKPRODUCT" PRIMARY KEY
                ("TASK_ID",
                 "GROUP_ID");



------------------------------------------------
-- DDL Statements for table "UNPUBLISHED"."PLANNER_TASK_HEADINGS"
------------------------------------------------
 

CREATE TABLE "UNPUBLISHED"."PLANNER_TASK_HEADINGS"  (
                  "ID" INTEGER NOT NULL , 
                  "SEQUENCE" INTEGER NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "DATE_FLAG" CHAR(1) , 
                  "FROM_DAYS" INTEGER , 
                  "TO_DAYS" INTEGER )   
                 IN "BKF4K02D" INDEX IN "BKF4K02I"  ; 

ALTER TABLE "UNPUBLISHED"."PLANNER_TASK_HEADINGS" PCTFREE 20;

COMMENT ON TABLE "UNPUBLISHED"."PLANNER_TASK_HEADINGS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Tasks HEADINGS to separate section in  quick report displayed to the Administrator of Categories and Programs listing.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_HEADINGS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_HEADINGS"."DATE_FLAG" IS 'When populated with the value S, it indicates that the display in the planner must take the Fair Start-Date as its origin; When populated with E, the Fair End-Date becomes the origin.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_HEADINGS"."FROM_DAYS" IS 'Tracks the number of daya before or after either the fair starting-date or the fair end-date whichever is selected via the column Date_flag.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_HEADINGS"."ID" IS 'Primary Key column; tracks the unique Task Heading ID.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_HEADINGS"."SEQUENCE" IS 'Tracks the sequence by which the Headings will displayed into the quick report to the Administrator';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_HEADINGS"."TITLE" IS 'Tracks the displayed Task Heading.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_HEADINGS"."TO_DAYS" IS 'Tracks the number of daya before or after either the fair starting-date or the fair end-date whichever is selected via the column Date_flag.';

COMMENT ON COLUMN "UNPUBLISHED"."PLANNER_TASK_HEADINGS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "UNPUBLISHED"."PLANNER_TASK_HEADINGS"

CREATE UNIQUE INDEX "UNPUBLISHED"."XPK_PLANNERTASKHEAD_ID" ON "UNPUBLISHED"."PLANNER_TASK_HEADINGS" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "UNPUBLISHED"."PLANNER_TASK_HEADINGS"

ALTER TABLE "UNPUBLISHED"."PLANNER_TASK_HEADINGS" 
        ADD CONSTRAINT "XPK_PLANNERTASKHEAD_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."PLANNER_TASK_HEADINGS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."PLANNER_TASK_HEADINGS"  (
                  "ID" INTEGER NOT NULL , 
                  "SEQUENCE" INTEGER NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "TITLE" VARCHAR(50) NOT NULL , 
                  "DATE_FLAG" CHAR(1) , 
                  "FROM_DAYS" INTEGER , 
                  "TO_DAYS" INTEGER )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."PLANNER_TASK_HEADINGS" PCTFREE 20;

COMMENT ON TABLE "BKFAIR01"."PLANNER_TASK_HEADINGS" IS 'Planner Release 24: As part of the Administrator Tool, this table tracks all Tasks HEADINGS to separate section in  quick report displayed to the Administrator of Categories and Programs listing.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_HEADINGS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_HEADINGS"."DATE_FLAG" IS 'When populated with the value S, it indicates that the display in the planner must take the Fair Start-Date as its origin; When populated with E, the Fair End-Date becomes the origin.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_HEADINGS"."FROM_DAYS" IS 'Tracks the number of daya before or after either the fair starting-date or the fair end-date whichever is selected via the column Date_flag.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_HEADINGS"."ID" IS 'Primary Key column; tracks the unique Task Heading ID.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_HEADINGS"."SEQUENCE" IS 'Tracks the sequence by which the Headings will displayed into the quick report to the Administrator';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_HEADINGS"."TITLE" IS 'Tracks the displayed Task Heading.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_HEADINGS"."TO_DAYS" IS 'Tracks the number of daya before or after either the fair starting-date or the fair end-date whichever is selected via the column Date_flag.';

COMMENT ON COLUMN "BKFAIR01"."PLANNER_TASK_HEADINGS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "BKFAIR01"."PLANNER_TASK_HEADINGS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_PLANNERTASKHEAD_ID" ON "BKFAIR01"."PLANNER_TASK_HEADINGS" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."PLANNER_TASK_HEADINGS"

ALTER TABLE "BKFAIR01"."PLANNER_TASK_HEADINGS" 
        ADD CONSTRAINT "XPK_PLANNERTASKHEAD_ID" PRIMARY KEY
                ("ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_PLANNER_STATUS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_PLANNER_STATUS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "STATUS" VARCHAR(15) NOT NULL )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_STATUS" PCTFREE 2;

COMMENT ON TABLE "BKFAIR01"."FAIR_PLANNER_STATUS" IS 'Planner Release 24: On the application side, this table tracks the landing page status for a given fair using the page label used in the wire frame:LAN100 for first timers; LAN200 for WelcomeBack; PRO100 for SAVE AND CONTINUE.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_STATUS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_STATUS"."FAIR_ID" IS 'Tracks the Fair ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_STATUS"."STATUS" IS 'Tracks the Planner page landing where a Customer last saved her data entry point for a given fair ID; LAN100 for first timers; LAN200 for WelcomeBack; PRO100 for SAVE AND CONTINUE.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_STATUS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_PLANNER_STATUS"

CREATE INDEX "BKFAIR01"."X_FAIRPLANSTATUS_UPDTDATE" ON "BKFAIR01"."FAIR_PLANNER_STATUS" 
                ("UPDATE_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_PLANNER_STATUS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_FAIRPLANSTATUS" ON "BKFAIR01"."FAIR_PLANNER_STATUS" 
                ("FAIR_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_PLANNER_STATUS"

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_STATUS" 
        ADD CONSTRAINT "XPK_FAIRPLANSTATUS" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_PLANNER_GOALS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_PLANNER_GOALS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "PERCENTAGE_FLAG" VARCHAR(15) , 
                  "PERCENTAGE_1" INTEGER , 
                  "PERCENTAGE_2" INTEGER , 
                  "PERCENTAGE_3" INTEGER , 
                  "WHERE_INITIAL_GOAL_SETUP" VARCHAR(10) , 
                  "WHERE_CURRENT_GOAL_SETUP" VARCHAR(10) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_GOALS" PCTFREE 5;

COMMENT ON TABLE "BKFAIR01"."FAIR_PLANNER_GOALS" IS 'Planner Release 24: Tracks the sales goals setup by the Chairperson using as a template value choices set up by the Planner Administrator.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_GOALS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_GOALS"."FAIR_ID" IS 'Tracks the Fair ID for which goals are being saved.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_GOALS"."PERCENTAGE_1" IS 'Tracks the First percentage value (out of 4) of the Percentage Goal hoped to be reached for a given fair based on its Product ID and chosen by the Chairperson if and only if it was set up originally by the Planner Administrator.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_GOALS"."PERCENTAGE_2" IS 'Tracks the Second percentage value (out of 4) of the Percentage Goal hoped to be reached for a given fair based on its Product ID and chosen by the Chairperson if and only if it was set up originally by the Planner Administrator.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_GOALS"."PERCENTAGE_3" IS 'Tracks the Third percentage value (out of 4) of the Percentage Goal hoped to be reached for a given fair based on its Product ID and chosen by the Chairperson if and only if it was set up originally by the Planner Administrator.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_GOALS"."PERCENTAGE_FLAG" IS 'Tracks the one of 5 values indicating whether the goal is a - percentage_1... - user_entered or null.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_GOALS"."UPDATE_DATE" IS 'Tracks the update date of that record.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_GOALS"."WHERE_CURRENT_GOAL_SETUP" IS 'Data entry Page-source of the goal setup for subsequent goal data modification(s).';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_GOALS"."WHERE_INITIAL_GOAL_SETUP" IS 'Data entry Page-Source of the first Goal setup; either HOMEPAGE or PLANNER; this piece of data will never be modified by the code during any subsequent record modification(s).';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_PLANNER_GOALS"

CREATE INDEX "BKFAIR01"."X_FAIRPLANNERGOAL_UPDTDATE" ON "BKFAIR01"."FAIR_PLANNER_GOALS" 
                ("UPDATE_DATE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_PLANNER_GOALS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_FAIRPLANNERGOAL" ON "BKFAIR01"."FAIR_PLANNER_GOALS" 
                ("FAIR_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_PLANNER_GOALS"

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_GOALS" 
        ADD CONSTRAINT "XPK_FAIRPLANNERGOAL" PRIMARY KEY
                ("FAIR_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_PLANNER_PROGRAMS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_PLANNER_PROGRAMS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CATEGORY_ID" INTEGER NOT NULL , 
                  "PROGRAM_ID" INTEGER NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "RECOMMENDED" CHAR(1) , 
                  "USER_SELECTION" CHAR(1) , 
                  "PARTICIPANTS" INTEGER , 
                  "BOOKS_PER_PARTICIPANT" INTEGER )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_PROGRAMS" PCTFREE 5;

COMMENT ON TABLE "BKFAIR01"."FAIR_PLANNER_PROGRAMS" IS 'Planner Release 24: Tracks as many programs set originally up by the Planner Administrator for any given Fair; Each Fair Program records the Administation recommendation and whether or not the Chairperson selected it.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_PROGRAMS"."BOOKS_PER_PARTICIPANT" IS 'Rel. 24.5: Tracks the number of books per participant.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_PROGRAMS"."CATEGORY_ID" IS 'Tracks the Planner Category ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_PROGRAMS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_PROGRAMS"."FAIR_ID" IS 'Tracks the Fair ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_PROGRAMS"."PARTICIPANTS" IS 'Rel. 24.5: Tracks the number of paricipants to that program.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_PROGRAMS"."PROGRAM_ID" IS 'Tracks the Planner Program ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_PROGRAMS"."RECOMMENDED" IS 'When Y, indicates that the Planner Administrator had recommended that program and N when it was not; That value is for report purposes.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_PROGRAMS"."UPDATE_DATE" IS 'Tracks the update date of that record.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_PROGRAMS"."USER_SELECTION" IS 'When Y, indicates that the Chairperson had selected that program.';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_PLANNER_PROGRAMS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_FAIRPLANNERPRGRM" ON "BKFAIR01"."FAIR_PLANNER_PROGRAMS" 
                ("FAIR_ID" ASC,
                 "CATEGORY_ID" ASC,
                 "PROGRAM_ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_PLANNER_PROGRAMS"

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_PROGRAMS" 
        ADD CONSTRAINT "XPK_FAIRPLANPRGRM" PRIMARY KEY
                ("FAIR_ID",
                 "CATEGORY_ID",
                 "PROGRAM_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_PLANNER_EVENTS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_PLANNER_EVENTS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "TASK_ID" INTEGER NOT NULL , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "UPDATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "COMPLETED_FLAG" CHAR(1) WITH DEFAULT 'N' , 
                  "ALL_DAY_FLAG" CHAR(1) WITH DEFAULT 'N' , 
                  "MOVABLE_EVENT" CHAR(1) WITH DEFAULT 'Y' , 
                  "TASK_TYPE" VARCHAR(5) , 
                  "START_DATE" DATE , 
                  "END_DATE" DATE , 
                  "START_TIME" TIME , 
                  "END_TIME" TIME , 
                  "COLOR_CODE" VARCHAR(1) , 
                  "LOCATION" VARCHAR(30) , 
                  "TITLE" VARCHAR(50) , 
                  "DESCRIPTION" VARCHAR(460) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_EVENTS" PCTFREE 15;

COMMENT ON TABLE "BKFAIR01"."FAIR_PLANNER_EVENTS" IS 'Planner Release 24: Tracks all Planner Event Tasks once the Chairperson Saved and Continued her program creation for a given fair.  This data can then be edited by the Chairperson.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."ALL_DAY_FLAG" IS 'When set to Y, indicates that the Event will last the whole day.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."COLOR_CODE" IS 'Tracks the color of the message to be displayed to the Chairperson: will be in red when no tasks will have been set.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."COMPLETED_FLAG" IS 'When set to Y, indicates that task was completed; It is set to N when it is not.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."DESCRIPTION" IS 'Tracks the Description of the Fair Planned Event.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."END_DATE" IS 'Tracks the Ending Day of the Fair Planned Event.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."END_TIME" IS 'Tracks the Ending Time of the Fair Planned Event.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."FAIR_ID" IS 'Tracks the Fair ID.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."LOCATION" IS 'Tracks the Location of the Fair Planned Event.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."MOVABLE_EVENT" IS 'Indicates if an event can be moved from one date to another (post release 24).';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."START_DATE" IS 'Tracks the Starting Day of the Fair Planned Event.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."START_TIME" IS 'Tracks the Starting Time of the Fair Planned Event.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."TASK_ID" IS 'Tracks all the Published Task IDs pushed by the Chairperson when she clicks on Save and Continue; also tracks the User Defined Tasks added by the Chairperson using the sequence SEQ_EVENT_TASK as well.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."TASK_TYPE" IS 'As of Release 24.5: Possible values: PTASK; TTASK; UTASK; STASK; DTASK; DEL; SCPIC; SCDEL; DIOFE; CCSSU; One can find their description in the table bkfair01.lu_planner_task_types (not used in any way by the application).';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."TITLE" IS 'Tracks the Title of the Fair Planned Event.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_EVENTS"."UPDATE_DATE" IS 'Tracks the update date of that record.';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_PLANNER_EVENTS"

CREATE INDEX "BKFAIR01"."X_FAIRPLANEVENT_TASKTYPE" ON "BKFAIR01"."FAIR_PLANNER_EVENTS" 
                ("FAIR_ID" ASC,
                 "TASK_TYPE" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_PLANNER_EVENTS"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FAIRPLANEVENT" ON "BKFAIR01"."FAIR_PLANNER_EVENTS" 
                ("FAIR_ID" ASC,
                 "TASK_ID" ASC)
                INCLUDE ("TASK_TYPE" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_PLANNER_EVENTS"

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_EVENTS" 
        ADD CONSTRAINT "XPKC_FAIRPLANEVENT" PRIMARY KEY
                ("FAIR_ID",
                 "TASK_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."BOOK_FAIR_DATA_LOAD"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."BOOK_FAIR_DATA_LOAD"  (
                  "FAIR_ID" VARCHAR(25) , 
                  "TAX_DETAIL_TAX_RATE" VARCHAR(9) , 
                  "TAX_DETAIL_EXEMPT_SALESALLOWED" VARCHAR(1) , 
                  "TAX_DETAIL_CUSTOMER_MAY_PAYTAX" VARCHAR(1) , 
                  "FAIR_FINANCIAL_INFO_SCDBOOKING" VARCHAR(1) , 
                  "FAIR_FINANCIAL_INFO_INCR_REVEN" VARCHAR(11) , 
                  "FAIR_GRAY_STATE" VARCHAR(1) , 
                  "PAYMENT_RCVD" VARCHAR(1) , 
                  "ORIGINAL_TRK_DELIVERY_DATE" DATE , 
                  "TRK_DELIVERY_DATE" DATE GENERATED ALWAYS AS ( CASE WHEN ORIGINAL_TRK_DELIVERY_DATE = '1901-01-01' THEN NULL ELSE ORIGINAL_TRK_DELIVERY_DATE END ) , 
                  "ORIGINAL_TRK_PICKUP_DATE" DATE , 
                  "TRK_PICKUP_DATE" DATE GENERATED ALWAYS AS ( CASE WHEN ORIGINAL_TRK_PICKUP_DATE = '1901-01-01' THEN NULL ELSE ORIGINAL_TRK_PICKUP_DATE END ) , 
                  "BOGO_BONUS" VARCHAR(1) , 
                  "PREFAIR_DESCRIPTION" VARCHAR(30) , 
                  "PREFAIR_VALUE" VARCHAR(20) , 
                  "ATTENDED_WORKSHOP_DESCRIPTION" VARCHAR(35) , 
                  "ATTENDED_WORKSHOP_VALUE" VARCHAR(20) , 
                  "SECOND_BOGO_BOOKING" CHAR(1) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON COLUMN "BKFAIR01"."BOOK_FAIR_DATA_LOAD"."FAIR_GRAY_STATE" IS 'The new business rules for the message that will display when TAX_RATE and GRAY_STATE= Y on the web site.';

COMMENT ON COLUMN "BKFAIR01"."BOOK_FAIR_DATA_LOAD"."ORIGINAL_TRK_DELIVERY_DATE" IS 'Original date delivered by CIS as the old format of 00000000 has been replaced by 19010101 to be able to replace that value with a null value and make the data import more adapted to database work.';

COMMENT ON COLUMN "BKFAIR01"."BOOK_FAIR_DATA_LOAD"."ORIGINAL_TRK_PICKUP_DATE" IS 'Original date delivered by CIS as the old format of 00000000 has been replaced by 19010101 to be able to replace that value with a null value and make the data import more adapted to database work.';

COMMENT ON COLUMN "BKFAIR01"."BOOK_FAIR_DATA_LOAD"."SECOND_BOGO_BOOKING" IS 'Release 23.5 BTS; Y ? Customer is eligible for additional 2nd fair BOGO bonus; N ? Customer is not eligible for bonus.';

COMMENT ON COLUMN "BKFAIR01"."BOOK_FAIR_DATA_LOAD"."TRK_DELIVERY_DATE" IS 'Date column being evaluated against 19010101 which value will be replaced by a null value.';

COMMENT ON COLUMN "BKFAIR01"."BOOK_FAIR_DATA_LOAD"."TRK_PICKUP_DATE" IS 'Date column being evaluated against 19010101 which value will be replaced by a null value.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."BOOK_FAIR_ERROR_DATA"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."BOOK_FAIR_ERROR_DATA"  (
                  "FAIR_ID" VARCHAR(25) , 
                  "TAX_DETAIL_TAX_RATE" VARCHAR(9) , 
                  "TAX_DETAIL_EXEMPT_SALESALLOWED" VARCHAR(1) , 
                  "TAX_DETAIL_CUSTOMER_MAY_PAYTAX" VARCHAR(1) , 
                  "FAIR_FINANCIAL_INFO_SCDBOOKING" VARCHAR(1) , 
                  "FAIR_FINANCIAL_INFO_INCR_REVEN" VARCHAR(11) , 
                  "FAIR_GRAY_STATE" VARCHAR(1) , 
                  "PAYMENT_RCVD" VARCHAR(1) , 
                  "ATTENDED_WORKSHOP_DESCRIPTION" VARCHAR(35) , 
                  "ATTENDED_WORKSHOP_VALUE" VARCHAR(20) , 
                  "MISSING_FAIR_RECORD" CHAR(1) WITH DEFAULT 'N' , 
                  "SECOND_BOGO_BOOKING" CHAR(1) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON COLUMN "BKFAIR01"."BOOK_FAIR_ERROR_DATA"."FAIR_GRAY_STATE" IS 'The new business rules for the message that will display when TAX_RATE and GRAY_STATE= Y on the web site.';

COMMENT ON COLUMN "BKFAIR01"."BOOK_FAIR_ERROR_DATA"."MISSING_FAIR_RECORD" IS 'Indicates by Y that a FAIR_CIS will not have a corresponding parent record in the table FAIR.';

COMMENT ON COLUMN "BKFAIR01"."BOOK_FAIR_ERROR_DATA"."SECOND_BOGO_BOOKING" IS 'Release 23.5 BTS; Y ? Customer is eligible for additional 2nd fair BOGO bonus; N ? Customer is not eligible for bonus.';





------------------------------------------------
-- DDL Statements for table "BKFAIR01"."REL_24_KEN_FAIR_CLASS_ENROLLMENT_DATA"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."REL_24_KEN_FAIR_CLASS_ENROLLMENT_DATA"  (
                  "FAIR_ID" BIGINT , 
                  "FAIR_CLASS" CHAR(1) , 
                  "FAIR_ENROLLMENT" DECIMAL(10,0) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 






-- DDL Statements for indexes on Table "BKFAIR01"."REL_24_KEN_FAIR_CLASS_ENROLLMENT_DATA"

CREATE INDEX "BKFAIR01"."XUV_REL24KENFAIRCLASSENROLL" ON "BKFAIR01"."REL_24_KEN_FAIR_CLASS_ENROLLMENT_DATA" 
                ("FAIR_ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIRDATE_LOGIN_TRACKING"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIRDATE_LOGIN_TRACKING"  (
                  "ID" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (  
                    START WITH +100  
                    INCREMENT BY +1  
                    MINVALUE +100  
                    MAXVALUE +2147483647  
                    NO CYCLE  
                    CACHE 500  
                    ORDER ), 
                  "FAIR_ID" BIGINT NOT NULL , 
                  "CHAIRPERSON_ID" BIGINT NOT NULL , 
                  "LOGIN_TYPE" VARCHAR(20) NOT NULL , 
                  "LOGIN_DATE" TIMESTAMP WITH DEFAULT  )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 


-- DDL Statements for indexes on Table "BKFAIR01"."FAIRDATE_LOGIN_TRACKING"

CREATE INDEX "BKFAIR01"."XFK_FAIRDATELT_CI" ON "BKFAIR01"."FAIRDATE_LOGIN_TRACKING" 
                ("CHAIRPERSON_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIRDATE_LOGIN_TRACKING"

CREATE INDEX "BKFAIR01"."XFK_FAIRDATELT_FI" ON "BKFAIR01"."FAIRDATE_LOGIN_TRACKING" 
                ("FAIR_ID" ASC)
                ALLOW REVERSE SCANS;

-- DDL Statements for indexes on Table "BKFAIR01"."FAIRDATE_LOGIN_TRACKING"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_FAIRDATELT_ID" ON "BKFAIR01"."FAIRDATE_LOGIN_TRACKING" 
                ("ID" ASC)
                INCLUDE ("LOGIN_DATE" )
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIRDATE_LOGIN_TRACKING"

ALTER TABLE "BKFAIR01"."FAIRDATE_LOGIN_TRACKING" 
        ADD CONSTRAINT "XPKC_FAIRDATELT_ID" PRIMARY KEY
                ("ID");



ALTER TABLE "BKFAIR01"."FAIRDATE_LOGIN_TRACKING" ALTER COLUMN "ID" RESTART WITH 11548599;

------------------------------------------------
-- DDL Statements for table "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"  (
                  "FAIR_ID" BIGINT NOT NULL , 
                  "EVENT_ID" BIGINT NOT NULL , 
                  "WORKSHOP_PERSON_ID" INTEGER NOT NULL , 
                  "REGISTRATION_STATUS" VARCHAR(10) , 
                  "FIRST_NAME" VARCHAR(50) , 
                  "LAST_NAME" VARCHAR(50) , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT  , 
                  "COMPLETED_FLAG" CHAR(1) WITH DEFAULT 'N' , 
                  "UPDATE_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_WORKSHOPS" PCTFREE 5;

COMMENT ON TABLE "BKFAIR01"."FAIR_PLANNER_WORKSHOPS" IS 'Planner Calendar Release 24.5: tracks the different workshops (events) that multiple fair Folks will attend.  Such workshop can be either -Live- -Change-Cancel- tracked in column: Registration_status.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"."COMPLETED_FLAG" IS 'When set to Y, indicates that this workshop was completed; It is set to N when it is not.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"."CREATE_DATE" IS 'Tracks the creation date of that record.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"."EVENT_ID" IS 'Tracks the Event ID to occur for a given Fair and Person';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"."FAIR_ID" IS 'Tracks the Fair ID for which Workshop(s)/Event(s) will occur.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"."FIRST_NAME" IS 'Tracks the First-Name of a Person attending a given Fair Workshop/Event.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"."LAST_NAME" IS 'Tracks the Last-Name of a Person attending a given Fair Workshop/Event.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"."REGISTRATION_STATUS" IS 'Tracks the status for that workshop/Event for a given fair and Person.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"."UPDATE_DATE" IS 'Tracks the update date of that record.';

COMMENT ON COLUMN "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"."WORKSHOP_PERSON_ID" IS 'Tracks the Florida Workshop Person ID for a given event.';


-- DDL Statements for indexes on Table "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_FAIRPLANWORKSHOP" ON "BKFAIR01"."FAIR_PLANNER_WORKSHOPS" 
                ("FAIR_ID" ASC,
                 "EVENT_ID" ASC,
                 "WORKSHOP_PERSON_ID" ASC)
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_WORKSHOPS" 
        ADD CONSTRAINT "XPK_FAIRPLANWORKSHOP" PRIMARY KEY
                ("FAIR_ID",
                 "EVENT_ID",
                 "WORKSHOP_PERSON_ID");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."LU_PLANNER_TASK_TYPES"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."LU_PLANNER_TASK_TYPES"  (
                  "TASK_TYPE" VARCHAR(5) NOT NULL , 
                  "DESCRIPTION" VARCHAR(50) )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

COMMENT ON TABLE "BKFAIR01"."LU_PLANNER_TASK_TYPES" IS 'Release 24.5: Reference table created to keep track of all values which can populate bkfair01.fair_planner_events.task_type. This table is strictly for information only and is not used by Toolkit.';

COMMENT ON COLUMN "BKFAIR01"."LU_PLANNER_TASK_TYPES"."DESCRIPTION" IS 'Provides a description for the task_type which is only an abbreviation.';

COMMENT ON COLUMN "BKFAIR01"."LU_PLANNER_TASK_TYPES"."TASK_TYPE" IS 'Tracks the Fair ID for which Workshop(s)/Event(s) will occur.';


-- DDL Statements for indexes on Table "BKFAIR01"."LU_PLANNER_TASK_TYPES"

CREATE UNIQUE INDEX "BKFAIR01"."XPKC_LU_PLAN_TSK_TYPE" ON "BKFAIR01"."LU_PLANNER_TASK_TYPES" 
                ("TASK_TYPE" ASC)
                CLUSTER ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."LU_PLANNER_TASK_TYPES"

ALTER TABLE "BKFAIR01"."LU_PLANNER_TASK_TYPES" 
        ADD CONSTRAINT "XPKC_LU_PLAN_TSK_TYPE" PRIMARY KEY
                ("TASK_TYPE");



------------------------------------------------
-- DDL Statements for table "BKFAIR01"."RECIPIENT_POSITION"
------------------------------------------------
 

CREATE TABLE "BKFAIR01"."RECIPIENT_POSITION"  (
                  "ID" INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (  
                    START WITH +100  
                    INCREMENT BY +1  
                    MINVALUE +100  
                    MAXVALUE +2147483647  
                    NO CYCLE  
                    CACHE 50  
                    ORDER ), 
                  "FAIR_ID" BIGINT NOT NULL , 
                  "RECIPIENT_FIRST_NAME" VARCHAR(25) , 
                  "RECIPIENT_LAST_NAME" VARCHAR(25) , 
                  "RECIPIENT_EMAIL" VARCHAR(50) , 
                  "RECIPIENT_POSITION_ID" BIGINT , 
                  "CREATE_DATE" TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP , 
                  "COA_TYPE" VARCHAR(4) WITH DEFAULT 'POST' , 
                  "POSTCOA_FWD_DATE" TIMESTAMP , 
                  "PRECOA_FWD_DATE" TIMESTAMP )   
                 IN "BKF4K01D" INDEX IN "BKF4K01I"  ; 

ALTER TABLE "BKFAIR01"."RECIPIENT_POSITION" PCTFREE 10;

COMMENT ON TABLE "BKFAIR01"."RECIPIENT_POSITION" IS 'Stores Fair COA Recipient data and together with its COA type indicating if and when the COA was sent before or after agreeing to still an additional Recipient.';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."COA_TYPE" IS 'Rel. 25 (BTS 2014-2015): Indicates if a COA was forwarded to an additional Recipient prior to agreeing -PRE-; Or indicates by -POST- (default value) when sent following sending of COA.';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."CREATE_DATE" IS 'Tracks the original creation timestamp of the record.';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."FAIR_ID" IS 'Tracks the Fair_id which COA has been agreed.';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."ID" IS 'Rel. 25(BTS 2014-2015): Tracks record ID and becomes the PK to allow delta data export to Reporting.  There can be multiple records for a single Fair.';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."POSTCOA_FWD_DATE" IS 'Rel.25: Tracks Timestamp of POST COA sending.';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."PRECOA_FWD_DATE" IS 'Rel.25: Tracks Timestamp of PRE COA sending to Additional Recipient.';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."RECIPIENT_EMAIL" IS 'Tracks the Recipient email address';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."RECIPIENT_FIRST_NAME" IS 'Tracks the Recipient First Name.';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."RECIPIENT_LAST_NAME" IS 'Tracks the Recipient Last Name';

COMMENT ON COLUMN "BKFAIR01"."RECIPIENT_POSITION"."RECIPIENT_POSITION_ID" IS 'Tracks the Recipient Position_id which references the parent table LU_RECIPIENT_POSITION (recipient_position_id) (Position held by the Recipient such as Assistant Principal etc.';


-- DDL Statements for indexes on Table "BKFAIR01"."RECIPIENT_POSITION"

CREATE UNIQUE INDEX "BKFAIR01"."XPK_RECIPIENT_POS_ID" ON "BKFAIR01"."RECIPIENT_POSITION" 
                ("ID" ASC)
                ALLOW REVERSE SCANS;
-- DDL Statements for primary key on Table "BKFAIR01"."RECIPIENT_POSITION"

ALTER TABLE "BKFAIR01"."RECIPIENT_POSITION" 
        ADD CONSTRAINT "XPK_RECIPIENT_POS_ID" PRIMARY KEY
                ("ID");




-- DDL Statements for indexes on Table "BKFAIR01"."RECIPIENT_POSITION"

CREATE UNIQUE INDEX "BKFAIR01"."XUV_RECIPNT_POS_FAIRID" ON "BKFAIR01"."RECIPIENT_POSITION" 
                ("FAIR_ID" ASC,
                 "COA_TYPE" ASC)
                ALLOW REVERSE SCANS;
ALTER TABLE "BKFAIR01"."RECIPIENT_POSITION" ALTER COLUMN "ID" RESTART WITH 609599;

-- DDL Statements for foreign keys on Table "BKFAIR01"."FAIR_CPLIST"

ALTER TABLE "BKFAIR01"."FAIR_CPLIST" 
        ADD CONSTRAINT "XFK_FAIR_CPLST_CID" FOREIGN KEY
                ("CHAIRPERSON_ID")
        REFERENCES "BKFAIR01"."CHAIRPERSON"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."FAIR_CPLIST" 
        ADD CONSTRAINT "XFK_FAIR_CPLST_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."ADDRESS_BOOK"

ALTER TABLE "BKFAIR01"."ADDRESS_BOOK" 
        ADD CONSTRAINT "XFK_ADDRSBOOK_SID" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."ADDRESS_BOOK"

ALTER TABLE "BKFAIR01"."ADDRESS_BOOK" 
        ADD CONSTRAINT "CK_ADDRESS_BOOK_DF" CHECK 
                (DELETED_FLAG IN ('Y', 'N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."MYTEAM"

ALTER TABLE "BKFAIR01"."MYTEAM" 
        ADD CONSTRAINT "FK_MYTEAM_CONTACT_ID" FOREIGN KEY
                ("CONTACT_ID")
        REFERENCES "BKFAIR01"."ADDRESS_BOOK"
                ("ID")
        ON DELETE CASCADE
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."MYTEAM" 
        ADD CONSTRAINT "XFK_MYTEAM_FAIR_ID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."DISTRIBUTION_LIST"

ALTER TABLE "BKFAIR01"."DISTRIBUTION_LIST" 
        ADD CONSTRAINT "XFK_DISBTLIST_SID" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS"

ALTER TABLE "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS" 
        ADD CONSTRAINT "FK_DISTROLISTC_CID" FOREIGN KEY
                ("CONTACT_ID")
        REFERENCES "BKFAIR01"."ADDRESS_BOOK"
                ("ID")
        ON DELETE CASCADE
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."DISTRIBUTION_LIST_CONTACTS" 
        ADD CONSTRAINT "XFK_DISTLISTC_DI" FOREIGN KEY
                ("DISTRIBUTION_ID")
        REFERENCES "BKFAIR01"."DISTRIBUTION_LIST"
                ("ID")
        ON DELETE CASCADE
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."DISTRIBUTION_LIST_EMAILS"

ALTER TABLE "BKFAIR01"."DISTRIBUTION_LIST_EMAILS" 
        ADD CONSTRAINT "XFK_DISTLISTEM_DI" FOREIGN KEY
                ("DISTRIBUTION_ID")
        REFERENCES "BKFAIR01"."DISTRIBUTION_LIST"
                ("ID")
        ON DELETE CASCADE
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."FILES"

ALTER TABLE "BKFAIR01"."FILES" 
        ADD CONSTRAINT "XFK_FILES_SCHOOLID" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PREVIOUS_FAIRS"

ALTER TABLE "BKFAIR01"."PREVIOUS_FAIRS" 
        ADD CONSTRAINT "XFK_PREVIOUS_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."PREVIOUS_FAIRS" 
        ADD CONSTRAINT "XFK_PREVIOUS_FSID" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."PREVIOUS_FAIRS" 
        ADD CONSTRAINT "XFK_PREVIOUS_PFID" FOREIGN KEY
                ("PREVIOUS_FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."CASH_AND_CHECKS_SALE"

ALTER TABLE "BKFAIR01"."CASH_AND_CHECKS_SALE" 
        ADD CONSTRAINT "XFK_CASH_CHK_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."FINANCIAL_SECTION_STATUS"

ALTER TABLE "BKFAIR01"."FINANCIAL_SECTION_STATUS" 
        ADD CONSTRAINT "XFK_FINCL_SEC_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FINANCIAL_SECTION_STATUS"

ALTER TABLE "BKFAIR01"."FINANCIAL_SECTION_STATUS" 
        ADD CONSTRAINT "CK_FINCL_SEC_CURR" CHECK 
                (CURRENT_SECTION IN ('Sales','Profit', 'Rewards','Complete','Confirm'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FINANCIAL_SECTION_STATUS"

ALTER TABLE "BKFAIR01"."FINANCIAL_SECTION_STATUS" 
        ADD CONSTRAINT "CK_FINCL_SEC_INSC" CHECK 
                (INVOICE_SCENARIO IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."CC_SALES"

ALTER TABLE "BKFAIR01"."CC_SALES" 
        ADD CONSTRAINT "XFK_CC_SALES_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PO_CONTACT"

ALTER TABLE "BKFAIR01"."PO_CONTACT" 
        ADD CONSTRAINT "XFK_PO_CONTACT_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PO_SALES"

ALTER TABLE "BKFAIR01"."PO_SALES" 
        ADD CONSTRAINT "XFK_PO_SALES_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."SALES_TAX_COLLECTED"

ALTER TABLE "BKFAIR01"."SALES_TAX_COLLECTED" 
        ADD CONSTRAINT "XFK_SALES_TXC_FI" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS"

ALTER TABLE "BKFAIR01"."SALES_TAX_EXEMPT_TRANSACTIONS" 
        ADD CONSTRAINT "XFK_SALESTAX_E_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."SALES_TAX_EXEMPT_STATUS"

ALTER TABLE "BKFAIR01"."SALES_TAX_EXEMPT_STATUS" 
        ADD CONSTRAINT "XFK_SALES_TXE_FI" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS"

ALTER TABLE "BKFAIR01"."SALES_TAX_EXEMPT_DETAILS" 
        ADD CONSTRAINT "XFK_SALESTAX_E_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."SALES_TAX_REMITTANCE"

ALTER TABLE "BKFAIR01"."SALES_TAX_REMITTANCE" 
        ADD CONSTRAINT "XFK_CASH_CHK_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."SALES_SUMMARY"

ALTER TABLE "BKFAIR01"."SALES_SUMMARY" 
        ADD CONSTRAINT "XFK_SALES_SUM_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."FAIR_FINANCIAL_INFO"

ALTER TABLE "BKFAIR01"."FAIR_FINANCIAL_INFO" 
        ADD CONSTRAINT "XFK_FAIR_FNCL_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_FINANCIAL_INFO"

ALTER TABLE "BKFAIR01"."FAIR_FINANCIAL_INFO" 
        ADD CONSTRAINT "CK_FAIR_FNCL_SB" CHECK 
                (SECOND_BOOKING IN (1, 0))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."TAX_DETAIL"

ALTER TABLE "BKFAIR01"."TAX_DETAIL" 
        ADD CONSTRAINT "XFK_TAX_DETAI_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."TAX_DETAIL"

ALTER TABLE "BKFAIR01"."TAX_DETAIL" 
        ADD CONSTRAINT "CK_TAX_DETAIL_CMPT" CHECK 
                (CUSTOMER_MAY_PAY_TAX IN (1, 0))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."TAX_DETAIL"

ALTER TABLE "BKFAIR01"."TAX_DETAIL" 
        ADD CONSTRAINT "CK_TAX_DETAIL_ESA" CHECK 
                (EXEMPT_SALES_ALLOWED IN (1, 0))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PROFIT_TAKEN"

ALTER TABLE "BKFAIR01"."PROFIT_TAKEN" 
        ADD CONSTRAINT "XFK_PROFIT_TAK_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PROFIT_SELECTION"

ALTER TABLE "BKFAIR01"."PROFIT_SELECTION" 
        ADD CONSTRAINT "XFK_PROFIT_SEL_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PROFIT_SELECTION"

ALTER TABLE "BKFAIR01"."PROFIT_SELECTION" 
        ADD CONSTRAINT "CK_PROFIT_SELE_PSO" CHECK 
                (PROFIT_SELECTION_OPTION IN ('BOOKS', 'IRC', 'CASH', 'SPLIT'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PROFIT_TO_SPLIT"

ALTER TABLE "BKFAIR01"."PROFIT_TO_SPLIT" 
        ADD CONSTRAINT "XFK_PROFIT_TO_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PROFIT_TO_SPLIT"

ALTER TABLE "BKFAIR01"."PROFIT_TO_SPLIT" 
        ADD CONSTRAINT "CK_PROFIT_TO_SP_IL" CHECK 
                (IND_LOCK IN ('B', 'I', 'C', 'U'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PROFIT_SUMMARY"

ALTER TABLE "BKFAIR01"."PROFIT_SUMMARY" 
        ADD CONSTRAINT "XFK_PROFIT_SU_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."FINANCIAL_SAVED_URLS"

ALTER TABLE "BKFAIR01"."FINANCIAL_SAVED_URLS" 
        ADD CONSTRAINT "XFK_FINCL_SAV_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."CASH_AND_CHECKS_SALE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "XFK_SALES_CONS_FID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_SALES_CONS_SCBE" CHECK 
                (SC_CONTACT_BY_EMAIL IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_SALES_CONS_SCBP" CHECK 
                (SC_CONTACT_BY_PHONE IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_SALES_CONS_SCBR" CHECK 
                (SC_CONTACT_BY_EITHER IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_SALES_CONS_SCI" CHECK 
                (SC_CHAIPERSON_INFORMATION IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_SALES_CONS_SFD" CHECK 
                (SC_FAIR_DATES IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_SALES_CONS_SO" CHECK 
                (SC_OTHER IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_SALES_CONS_SSBF" CHECK 
                (SC_SCHEDULE_BOOK_FAIR IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_SALES_CONS_STA" CHECK 
                (SC_TOOLKIT_ASSISTANCE IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."SALES_CONSULTANT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_SALES_TYPE" CHECK 
                (SALES_TYPE in ('FSR','SC',''))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
        ADD CONSTRAINT "XFK_NOT_CORRECT_FI" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_NOT_CORRECT_NCB" CHECK 
                (NC_CONTACT_BY_EMAIL IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_NOT_CORRECT_NCE" CHECK 
                (NC_CONTACT_BY_EITHER IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_NOT_CORRECT_NCI" CHECK 
                (NC_CHAIRPERSON_INFORMATION IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_NOT_CORRECT_NCP" CHECK 
                (NC_CONTACT_BY_PHONE IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_NOT_CORRECT_NFD" CHECK 
                (NC_FAIR_DATES IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_NOT_CORRECT_NO" CHECK 
                (NC_OTHER IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS"

ALTER TABLE "BKFAIR01"."NOT_CORRECT_EMAIL_FORMS" 
        ADD CONSTRAINT "CK_NOT_CORRECT_NSI" CHECK 
                (NC_SCHOOL_INFORMATION IN ('Y','N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."CUSTOMER_REWARDS_PARENT"

ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_PARENT" 
        ADD CONSTRAINT "XFK_CUSTOMER_RP_RR" FOREIGN KEY
                ("REGION_ID")
        REFERENCES "BKFAIR01"."REGION"
                ("REGION_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."CUSTOMER_REWARDS_PARENT"

ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_PARENT" 
        ADD CONSTRAINT "CK_CUSTOMER_RP_RT" CHECK 
                (RESPONSE_TYPE IN ('Y', 'N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."CUSTOMER_REWARDS_CHILD"

ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_CHILD" 
        ADD CONSTRAINT "CK_CUSTOMER_RC_CRT" CHECK 
                (CHILD_RESPONSE_TYPE IN ('Y', 'N','D','C','T',''))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"

ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE" 
        ADD CONSTRAINT "CK_CUSTOMER_RP_CRT" CHECK 
                (CHILD_RESPONSE_TYPE IN ('Y', 'N','D','C','T',''))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"

ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE" 
        ADD CONSTRAINT "CK_CUSTOMER_RP_RRT" CHECK 
                (RESPONSE_TYPE IN ('Y', 'N',''))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE"

ALTER TABLE "BKFAIR01"."CUSTOMER_REWARDS_RESPONSE" 
        ADD CONSTRAINT "CK_CUSTOMER_RP_RV" CHECK 
                (RESPONSE_VALUE IN ('checked',''))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"

ALTER TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION" 
        ADD CONSTRAINT "XFK_CHAIRPERN_EIFD" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"

ALTER TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION" 
        ADD CONSTRAINT "CK_CHAIRPERSN_ED" CHECK 
                (EMAIL_DELIVERED IN ('Y', 'N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION"

ALTER TABLE "BKFAIR01"."CHAIRPERSON_EMAIL_INVITATION" 
        ADD CONSTRAINT "CK_CHAIRPERSN_EECT" CHECK 
                (EMAIL_CLICK_THROUGH IN ('Y', 'N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."HOMEPAGE_SCHEDULE"

ALTER TABLE "BKFAIR01"."HOMEPAGE_SCHEDULE" 
        ADD CONSTRAINT "FK_HOMEPAGE_SCHEDULE__FAIR_ID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."ONLINE_HOMEPAGE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."HOMEPAGE_EVENT"

ALTER TABLE "BKFAIR01"."HOMEPAGE_EVENT" 
        ADD CONSTRAINT "XFK_FAIR_HMPG_EVFS" FOREIGN KEY
                ("FAIR_ID",
                 "SCHEDULE_DATE")
        REFERENCES "BKFAIR01"."HOMEPAGE_SCHEDULE"
                ("FAIR_ID",
                 "SCHEDULE_DATE")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."HOMEPAGE_EVENT"

ALTER TABLE "BKFAIR01"."HOMEPAGE_EVENT" 
        ADD CONSTRAINT "CK_HP_EVENT_CALCKBOX" CHECK 
                ( PLANNER_CALENDAR_CKBOX IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."HOMEPAGE_EVENT"

ALTER TABLE "BKFAIR01"."HOMEPAGE_EVENT" 
        ADD CONSTRAINT "CK_HP_EVENT_COMPLTECKBOX" CHECK 
                ( COMPLETED_FLAG IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."MYOB"

ALTER TABLE "BKFAIR01"."MYOB" 
        ADD CONSTRAINT "XFK_MYOB_SCHOOL_ID" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."MAX_FAIR_HISTORY_PRT"

ALTER TABLE "BKFAIR01"."MAX_FAIR_HISTORY_PRT" 
        ADD CONSTRAINT "XFK_MAX_FR_HI_PRT" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."MAX_FAIR_HISTORY_PRT"

ALTER TABLE "BKFAIR01"."MAX_FAIR_HISTORY_PRT" 
        ADD CONSTRAINT "CK_FAIR_HIST_TYP" CHECK 
                (HISTORY_FAIR_TYPE IN ('C', 'B', 'P', 'X','E','R','U','G',''))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PROFIT_BALANCE"

ALTER TABLE "BKFAIR01"."PROFIT_BALANCE" 
        ADD CONSTRAINT "XFK_PROFIT_BAL_SID" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PROFIT_BALANCE"

ALTER TABLE "BKFAIR01"."PROFIT_BALANCE" 
        ADD CONSTRAINT "CK_PROFIT_BALAN_VT" CHECK 
                (VOUCHER_TYPE IN ('P', 'R', 'O',''))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."MY_FAIR_SELECTION"

ALTER TABLE "BKFAIR01"."MY_FAIR_SELECTION" 
        ADD CONSTRAINT "XFK_ASSET_MYFAIR" FOREIGN KEY
                ("ASSET_ID")
        REFERENCES "BKFAIR01"."ASSET"
                ("ASSET_ID")
        ON DELETE CASCADE
        ON UPDATE NO ACTION
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."TRANSACTION_OPEN_BALANCE"

ALTER TABLE "BKFAIR01"."TRANSACTION_OPEN_BALANCE" 
        ADD CONSTRAINT "FK_TRANSACOPENBAL_SCHOOL_ID" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE CASCADE
        ON UPDATE NO ACTION
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."TRANSACTION_DETAILS"

ALTER TABLE "BKFAIR01"."TRANSACTION_DETAILS" 
        ADD CONSTRAINT "FK_TRANSACDETAIL_SCHOOL_ID" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE CASCADE
        ON UPDATE NO ACTION
        ENFORCED
        ENABLE QUERY OPTIMIZATION;



-- DDL Statements for foreign keys on Table "BKFAIR01"."ONLINE_HOMEPAGE"

ALTER TABLE "BKFAIR01"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "FK_ONLINEHOMEPAGE_FAIR_ID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."ONLINE_HOMEPAGE"

ALTER TABLE "BKFAIR01"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "CK_ONLINEHOMEPAGE_PAYCASH" CHECK 
                ( PAYMENT_CASH_CKBOX IN ('Y','N') )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."ONLINE_HOMEPAGE"

ALTER TABLE "BKFAIR01"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "CK_ONLINEHOMEPAGE_PAYCC" CHECK 
                (PAYMENT_CC_CKBOX IN ( 'Y', 'N' ))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."ONLINE_HOMEPAGE"

ALTER TABLE "BKFAIR01"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "CK_ONLINEHOMEPAGE_PAYCHK" CHECK 
                (PAYMENT_CHECK_CKBOX IN ('Y', 'N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."HOMEPAGE_URL"

ALTER TABLE "BKFAIR01"."HOMEPAGE_URL" 
        ADD CONSTRAINT "FK_HOMEPAGE_URL__FAIR_ID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."HOMEPAGE_URL"

ALTER TABLE "BKFAIR01"."HOMEPAGE_URL" 
        ADD CONSTRAINT "CK_HOMEPAGE_URL__FINDER" CHECK 
                ( FAIR_FINDER_CKBOX IN ('N', 'Y') )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."HOMEPAGE_URL"

ALTER TABLE "BKFAIR01"."HOMEPAGE_URL" 
        ADD CONSTRAINT "CK_HOMEPAGE_URL__PUBLISHED" CHECK 
                ( PUBLISHED_STATUS IN ('PUBLISHED', 'EXPIRED') )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."HOMEPAGE_URL"

ALTER TABLE "BKFAIR01"."HOMEPAGE_URL" 
        ADD CONSTRAINT "CK_HOMEPAGE_URL__UPDATEFLAG" CHECK 
                ( UPDATE_FLAG IN ('N', 'Y') )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."HOMEPAGE_URL"

ALTER TABLE "BKFAIR01"."HOMEPAGE_URL" 
        ADD CONSTRAINT "CK_HOMEPAGE_URL__URL" CHECK 
                ( URL_STATUS IN ('AVAILABLE', 'NOTAVAILABLE', 'RESERVED') )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."ONLINE_SHOPPING"

ALTER TABLE "BKFAIR01"."ONLINE_SHOPPING" 
        ADD CONSTRAINT "FK_ONLINESHOPPING__FAIR_ID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."ONLINE_SHOPPING"

ALTER TABLE "BKFAIR01"."ONLINE_SHOPPING" 
        ADD CONSTRAINT "CK_ONLINESHOPPING__OFE_STATUS" CHECK 
                ( OFE_STATUS IN ('Submitted', 'Cancelled', 'Active', 'Complete', 'Removed'
) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "UNPUBLISHED"."ONLINE_HOMEPAGE"

ALTER TABLE "UNPUBLISHED"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "FK_UNPUB_ONLINEHOMEPAGE_FAIR_ID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."ONLINE_HOMEPAGE"

ALTER TABLE "UNPUBLISHED"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "CK_UNPUB_ONLINEHOMEPAGE_PAYCASH" CHECK 
                ( PAYMENT_CASH_CKBOX IN ('Y','N') )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."ONLINE_HOMEPAGE"

ALTER TABLE "UNPUBLISHED"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "CK_UNPUB_ONLINEHOMEPAGE_PAYCC" CHECK 
                (PAYMENT_CC_CKBOX IN ( 'Y', 'N' ))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."ONLINE_HOMEPAGE"

ALTER TABLE "UNPUBLISHED"."ONLINE_HOMEPAGE" 
        ADD CONSTRAINT "CK_UNPUB_ONLINEHOMEPAGE_PAYCHK" CHECK 
                (PAYMENT_CHECK_CKBOX IN ('Y', 'N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "UNPUBLISHED"."HOMEPAGE_SCHEDULE"

ALTER TABLE "UNPUBLISHED"."HOMEPAGE_SCHEDULE" 
        ADD CONSTRAINT "FK_UNPUB_HOMEPAGE_SCHDL__FAIR_ID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "UNPUBLISHED"."ONLINE_HOMEPAGE"
                ("FAIR_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "UNPUBLISHED"."HOMEPAGE_EVENT"

ALTER TABLE "UNPUBLISHED"."HOMEPAGE_EVENT" 
        ADD CONSTRAINT "FK_UNPUB_HOMEPAGE_EVENT__FAIR_ID" FOREIGN KEY
                ("FAIR_ID",
                 "SCHEDULE_DATE")
        REFERENCES "UNPUBLISHED"."HOMEPAGE_SCHEDULE"
                ("FAIR_ID",
                 "SCHEDULE_DATE")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."HOMEPAGE_EVENT"

ALTER TABLE "UNPUBLISHED"."HOMEPAGE_EVENT" 
        ADD CONSTRAINT "CK_HP_EVENT_CALCKBOX" CHECK 
                ( PLANNER_CALENDAR_CKBOX IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."HOMEPAGE_EVENT"

ALTER TABLE "UNPUBLISHED"."HOMEPAGE_EVENT" 
        ADD CONSTRAINT "CK_HP_EVENT_COMPLTECKBOX" CHECK 
                ( COMPLETED_FLAG IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."WORKSHOP_REWARDS"

ALTER TABLE "BKFAIR01"."WORKSHOP_REWARDS" 
        ADD CONSTRAINT "CK_WORKSHOPREWARDS_COMPLETEFLAG" CHECK 
                (COMPLETE_FLAG IN ('Y', 'N'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."VOLUNTEER_FORMS"

ALTER TABLE "BKFAIR01"."VOLUNTEER_FORMS" 
        ADD CONSTRAINT "CK_VOLUNTEER_FORMS__CONTACTMTHD" CHECK 
                (CONTACT_METHOD IN ('P', 'EM', 'ET'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES"

ALTER TABLE "BKFAIR01"."HOMEPAGE_PRINCIPAL_ACTIVITIES" 
        ADD CONSTRAINT "FK_HP_PRINCIPAL_ACTIVITIES_FAIRID" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SCHOOL_TAX_SELECTION"

ALTER TABLE "BKFAIR01"."SCHOOL_TAX_SELECTION" 
        ADD CONSTRAINT "CK_STATE_TAX_SELECTION" CHECK 
                (TAX_SELECTION IN ( 'B', 'C' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."SCHOOL_TAX_STATUS"

ALTER TABLE "BKFAIR01"."SCHOOL_TAX_STATUS" 
        ADD CONSTRAINT "CK_STATE_TAX_STATUS" CHECK 
                (TAX_STATUS IN ( 'EP', 'EX', 'IV', 'NF', 'NT', 'OF', 'OS', 'PR' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."STATE_TAX_CHOICES"

ALTER TABLE "BKFAIR01"."STATE_TAX_CHOICES" 
        ADD CONSTRAINT "CK_STATE_TAX_CHOICES__ACT" CHECK 
                (ACTIVATED IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."LU_COA_TAXES"

ALTER TABLE "BKFAIR01"."LU_COA_TAXES" 
        ADD CONSTRAINT "CK_LU_COA_TAXES__SELECTION" CHECK 
                (COA_TAX_SELECTION IN ( 'B', 'C', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."FAIR"

ALTER TABLE "BKFAIR01"."FAIR" 
        ADD CONSTRAINT "FK_FAIR_SALSPN_ID" FOREIGN KEY
                ("SALESPERSON_ID")
        REFERENCES "BKFAIR01"."SALESPERSON"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."FAIR" 
        ADD CONSTRAINT "XFK_FAIR_REGION_ID" FOREIGN KEY
                ("REGION_ID")
        REFERENCES "BKFAIR01"."REGION"
                ("REGION_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."FAIR" 
        ADD CONSTRAINT "XFK_FAIR_SCHOOL_ID" FOREIGN KEY
                ("SCHOOL_ID")
        REFERENCES "BKFAIR01"."SCHOOL"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR"

ALTER TABLE "BKFAIR01"."FAIR" 
        ADD CONSTRAINT "CK_FAIR__CLASS" CHECK 
                ( FAIR_CLASS IN ( 'S', 'B' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_CIS"

ALTER TABLE "BKFAIR01"."FAIR_CIS" 
        ADD CONSTRAINT "CK_FAIR_GRAY_STATE" CHECK 
                ( GRAY_STATE IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_CIS"

ALTER TABLE "BKFAIR01"."FAIR_CIS" 
        ADD CONSTRAINT "CK_FAIRCIS_PAYMT_RCVD" CHECK 
                ( PAYMENT_RCVD IN ( 'Y', 'N','') )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_CIS"

ALTER TABLE "BKFAIR01"."FAIR_CIS" 
        ADD CONSTRAINT "CK_FAIRCIS_TRUCKDELVCOMP" CHECK 
                (TRK_DELIVERY_COMPLETE IN ('N', 'Y'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_CIS"

ALTER TABLE "BKFAIR01"."FAIR_CIS" 
        ADD CONSTRAINT "CK_FAIRCIS_TRUCKPICKCOMP" CHECK 
                (TRK_PICKUP_COMPLETE IN ('N', 'Y'))
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_DATE_CHANGED"

ALTER TABLE "BKFAIR01"."FAIR_DATE_CHANGED" 
        ADD CONSTRAINT "CK_FAIRDATECHANGED" CHECK 
                (DATE_CHANGED IN ('N', 'Y') )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;


-- DDL Statements for check constraints on Table "UNPUBLISHED"."PLANNER_CATEGORIES"

ALTER TABLE "UNPUBLISHED"."PLANNER_CATEGORIES" 
        ADD CONSTRAINT "CK_PLANNER_CATEGS_ACTIVE" CHECK 
                ( ACTIVE IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PLANNER_CATEGORIES"

ALTER TABLE "BKFAIR01"."PLANNER_CATEGORIES" 
        ADD CONSTRAINT "CK_PLANNER_CATEGS_ACTIVE" CHECK 
                ( ACTIVE IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "UNPUBLISHED"."PLANNER_PROGRAMS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAMS" 
        ADD CONSTRAINT "XFK_PLANRPRGM_CATEGID" FOREIGN KEY
                ("CATEGORY_ID")
        REFERENCES "UNPUBLISHED"."PLANNER_CATEGORIES"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."PLANNER_PROGRAMS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAMS" 
        ADD CONSTRAINT "CK_PLANNER_PROGRAMS_ACTIVE" CHECK 
                ( ACTIVE IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PLANNER_PROGRAMS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAMS" 
        ADD CONSTRAINT "XFK_PLANRPRGM_CATEGID" FOREIGN KEY
                ("CATEGORY_ID")
        REFERENCES "BKFAIR01"."PLANNER_CATEGORIES"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PLANNER_PROGRAMS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAMS" 
        ADD CONSTRAINT "CK_PLANNER_PROGRAMS_ACTIVE" CHECK 
                ( ACTIVE IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_TASKS" 
        ADD CONSTRAINT "XFK_PLANNERPRGMTASK_PRGMID" FOREIGN KEY
                ("PROGRAM_ID")
        REFERENCES "UNPUBLISHED"."PLANNER_PROGRAMS"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_TASKS" 
        ADD CONSTRAINT "CK_PLANPRGRMTASKS_ACTIVE" CHECK 
                ( ACTIVE IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."PLANNER_PROGRAM_TASKS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_TASKS" 
        ADD CONSTRAINT "CK_PLANPRGRMTASKS_DAYFLAG" CHECK 
                ( DATE_FLAG IN ( 'S', 'E' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PLANNER_PROGRAM_TASKS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_TASKS" 
        ADD CONSTRAINT "XFK_PLANNERPRGMTASK_PRGMID" FOREIGN KEY
                ("PROGRAM_ID")
        REFERENCES "BKFAIR01"."PLANNER_PROGRAMS"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PLANNER_PROGRAM_TASKS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_TASKS" 
        ADD CONSTRAINT "CK_PLANPRGRMTASKS_ACTIVE" CHECK 
                ( ACTIVE IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PLANNER_PROGRAM_TASKS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_TASKS" 
        ADD CONSTRAINT "CK_PLANPRGRMTASKS_DAYFLAG" CHECK 
                ( DATE_FLAG IN ( 'S', 'E' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANPRGMPRODUCT_CATEGID" FOREIGN KEY
                ("CATEGORY_ID")
        REFERENCES "UNPUBLISHED"."PLANNER_CATEGORIES"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANPRGMPRODUCT_GRPID" FOREIGN KEY
                ("GROUP_ID")
        REFERENCES "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS"
                ("GROUP_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANPRGMPRODUCT_PRGMID" FOREIGN KEY
                ("PROGRAM_ID")
        REFERENCES "UNPUBLISHED"."PLANNER_PROGRAMS"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "CK_PLANPRGMPRODUCT_RECMD" CHECK 
                ( RECOMMENDED_FLAG IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS"

ALTER TABLE "UNPUBLISHED"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "CK_PLANPRGMPRODUCT_SCHFLG" CHECK 
                ( DISPLAY_SCHEDULER_FLAG IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANPRGMPRODUCT_CATEGID" FOREIGN KEY
                ("CATEGORY_ID")
        REFERENCES "BKFAIR01"."PLANNER_CATEGORIES"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANPRGMPRODUCT_GRPID" FOREIGN KEY
                ("GROUP_ID")
        REFERENCES "BKFAIR01"."LU_PROGRAM_PRODUCT_GROUPINGS"
                ("GROUP_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANPRGMPRODUCT_PRGMID" FOREIGN KEY
                ("PROGRAM_ID")
        REFERENCES "BKFAIR01"."PLANNER_PROGRAMS"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "CK_PLANPRGMPRODUCT_RECMD" CHECK 
                ( RECOMMENDED_FLAG IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS"

ALTER TABLE "BKFAIR01"."PLANNER_PROGRAM_PRODUCTS" 
        ADD CONSTRAINT "CK_PLANPRGMPRODUCT_SCHFLG" CHECK 
                ( DISPLAY_SCHEDULER_FLAG IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."PLANNER_TASKS"

ALTER TABLE "UNPUBLISHED"."PLANNER_TASKS" 
        ADD CONSTRAINT "CK_PLANNERTASKS_ACTIVE" CHECK 
                ( ACTIVE IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."PLANNER_TASKS"

ALTER TABLE "UNPUBLISHED"."PLANNER_TASKS" 
        ADD CONSTRAINT "CK_PLANNERTASKS_DAYFLAG" CHECK 
                ( DATE_FLAG IN ( 'S', 'E' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PLANNER_TASKS"

ALTER TABLE "BKFAIR01"."PLANNER_TASKS" 
        ADD CONSTRAINT "CK_PLANNERTASKS_ACTIVE" CHECK 
                ( ACTIVE IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PLANNER_TASKS"

ALTER TABLE "BKFAIR01"."PLANNER_TASKS" 
        ADD CONSTRAINT "CK_PLANNERTASKS_DAYFLAG" CHECK 
                ( DATE_FLAG IN ( 'S', 'E' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "UNPUBLISHED"."PLANNER_TASK_PRODUCTS"

ALTER TABLE "UNPUBLISHED"."PLANNER_TASK_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANTASKPROD_GRPID" FOREIGN KEY
                ("GROUP_ID")
        REFERENCES "BKFAIR01"."LU_PRODUCT_GROUPINGS"
                ("GROUP_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "UNPUBLISHED"."PLANNER_TASK_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANTASKPROD_TSKID" FOREIGN KEY
                ("TASK_ID")
        REFERENCES "UNPUBLISHED"."PLANNER_TASKS"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."PLANNER_TASK_PRODUCTS"

ALTER TABLE "BKFAIR01"."PLANNER_TASK_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANTASKPROD_GRPID" FOREIGN KEY
                ("GROUP_ID")
        REFERENCES "BKFAIR01"."LU_PRODUCT_GROUPINGS"
                ("GROUP_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."PLANNER_TASK_PRODUCTS" 
        ADD CONSTRAINT "FK_PLANTASKPROD_TSKID" FOREIGN KEY
                ("TASK_ID")
        REFERENCES "BKFAIR01"."PLANNER_TASKS"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "UNPUBLISHED"."PLANNER_TASK_HEADINGS"

ALTER TABLE "UNPUBLISHED"."PLANNER_TASK_HEADINGS" 
        ADD CONSTRAINT "CK_PLANNERTASKHEAD_DATEFLAG" CHECK 
                ( DATE_FLAG IN ( 'S', 'E' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."PLANNER_TASK_HEADINGS"

ALTER TABLE "BKFAIR01"."PLANNER_TASK_HEADINGS" 
        ADD CONSTRAINT "CK_PLANNERTASKHEAD_DATEFLAG" CHECK 
                ( DATE_FLAG IN ( 'S', 'E' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_PLANNER_STATUS"

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_STATUS" 
        ADD CONSTRAINT "CK_FAIRPLANSTATUS" CHECK 
                ( STATUS IN ( 'CAL400', 'GOALA', 'GOALB', 'LAN100', 'LAN200','PRO100' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_PLANNER_GOALS"

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_GOALS" 
        ADD CONSTRAINT "CK_FAIRPLANGOALS_FLAG" CHECK 
                ( PERCENTAGE_FLAG IN ( 'EYO', 'GOAL1', 'GOAL2', 'GOAL3' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."FAIRDATE_LOGIN_TRACKING"

ALTER TABLE "BKFAIR01"."FAIRDATE_LOGIN_TRACKING" 
        ADD CONSTRAINT "XFK_FAIRDATELT_CI" FOREIGN KEY
                ("CHAIRPERSON_ID")
        REFERENCES "BKFAIR01"."CHAIRPERSON"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

ALTER TABLE "BKFAIR01"."FAIRDATE_LOGIN_TRACKING" 
        ADD CONSTRAINT "XFK_FAIRDATELT_FI" FOREIGN KEY
                ("FAIR_ID")
        REFERENCES "BKFAIR01"."FAIR"
                ("ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_WORKSHOPS" 
        ADD CONSTRAINT "CK_FAIRPLNWRKSHP_COMPLETE" CHECK 
                ( COMPLETED_FLAG IN ( 'Y', 'N' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."FAIR_PLANNER_WORKSHOPS"

ALTER TABLE "BKFAIR01"."FAIR_PLANNER_WORKSHOPS" 
        ADD CONSTRAINT "CK_FAIRPLNWRKSHP_STAT" CHECK 
                ( REGISTRATION_STATUS IN ( 'Live', 'Cancel' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for foreign keys on Table "BKFAIR01"."RECIPIENT_POSITION"

ALTER TABLE "BKFAIR01"."RECIPIENT_POSITION" 
        ADD CONSTRAINT "FK_RECIPNT_POS_POSID" FOREIGN KEY
                ("RECIPIENT_POSITION_ID")
        REFERENCES "BKFAIR01"."LU_RECIPIENT_POSITION"
                ("RECIPIENT_POSITION_ID")
        ON DELETE RESTRICT
        ON UPDATE RESTRICT
        ENFORCED
        ENABLE QUERY OPTIMIZATION;

-- DDL Statements for check constraints on Table "BKFAIR01"."RECIPIENT_POSITION"

ALTER TABLE "BKFAIR01"."RECIPIENT_POSITION" 
        ADD CONSTRAINT "CK_RECIPIENT_POS_COA" CHECK 
                ( COA_TYPE IN ( 'POST', 'PRE' ) )
        ENFORCED
        ENABLE QUERY OPTIMIZATION;






---------------------------------
-- DDL statements for User Defined Functions
---------------------------------


CREATE FUNCTION "VENKAPER"."VP_F1" 
                ( 
                  P1   CHARACTER(10)
                )
 
                RETURNS  CHARACTER(10)
                SPECIFIC VP_F1 
                EXTERNAL NAME 'vp_f1()' 
                LANGUAGE JAVA     
                PARAMETER STYLE JAVA 
                VARIANT 
                FENCED THREADSAFE 
                NOT NULL CALL 
                NO SQL 
                EXTERNAL ACTION 
                NO SCRATCHPAD 
                NO FINAL CALL 
                ALLOW PARALLEL 
                NO DBINFO;

SET CURRENT SCHEMA = "RCHOU   ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","RCHOU";
CREATE FUNCTION EXPLAIN_GET_MSGS( EXPLAIN_REQUESTER VARCHAR(128),
                                  EXPLAIN_TIME      TIMESTAMP,
                                  SOURCE_NAME       VARCHAR(128),
                                  SOURCE_SCHEMA     VARCHAR(128),
                                  SOURCE_VERSION    VARCHAR(64),
                                  EXPLAIN_LEVEL     CHAR(1),
                                  STMTNO            INTEGER,
                                  SECTNO            INTEGER,
                                  INLOCALE          VARCHAR(33) )
  RETURNS TABLE ( EXPLAIN_REQUESTER VARCHAR(128),
                  EXPLAIN_TIME      TIMESTAMP,
                  SOURCE_NAME       VARCHAR(128),
                  SOURCE_SCHEMA     VARCHAR(128),
                  SOURCE_VERSION    VARCHAR(64),
                  EXPLAIN_LEVEL     CHAR(1),
                  STMTNO            INTEGER,
                  SECTNO            INTEGER,
                  DIAGNOSTIC_ID     INTEGER,
                  LOCALE            VARCHAR(33),
                  MSG               VARCHAR(4096) )
  SPECIFIC EXPLAIN_GET_MSGS
  LANGUAGE SQL
  DETERMINISTIC
  NO EXTERNAL ACTION
  READS SQL DATA
  RETURN SELECT A.A_EXPLAIN_REQUESTER,
                A.A_EXPLAIN_TIME,
                A.A_SOURCE_NAME,
                A.A_SOURCE_SCHEMA,
                A.A_SOURCE_VERSION,
                A.A_EXPLAIN_LEVEL,
                A.A_STMTNO,
                A.A_SECTNO,
                A.A_DIAGNOSTIC_ID,
                F.LOCALE,
                F.MSG
         FROM EXPLAIN_DIAGNOSTIC A( A_EXPLAIN_REQUESTER,
                                    A_EXPLAIN_TIME,
                                    A_SOURCE_NAME,
                                    A_SOURCE_SCHEMA,
                                    A_SOURCE_VERSION,
                                    A_EXPLAIN_LEVEL,
                                    A_STMTNO,
                                    A_SECTNO,
                                    A_DIAGNOSTIC_ID,
                                    A_CODE ),
              TABLE( SYSPROC.EXPLAIN_GET_MSG2(
                       INLOCALE,
                       A.A_CODE,
                       ( SELECT TOKEN FROM EXPLAIN_DIAGNOSTIC_DATA B
                         WHERE A.A_EXPLAIN_REQUESTER = B.EXPLAIN_REQUESTER
                           AND A.A_EXPLAIN_TIME      = B.EXPLAIN_TIME
                           AND A.A_SOURCE_NAME       = B.SOURCE_NAME
                           AND A.A_SOURCE_SCHEMA     = B.SOURCE_SCHEMA
                           AND A.A_SOURCE_VERSION    = B.SOURCE_VERSION
                           AND A.A_EXPLAIN_LEVEL     = B.EXPLAIN_LEVEL
                           AND A.A_STMTNO            = B.STMTNO
                           AND A.A_SECTNO            = B.SECTNO
                           AND A.A_DIAGNOSTIC_ID     = B.DIAGNOSTIC_ID
                           AND B.ORDINAL=1 ),
                       ( SELECT TOKEN FROM EXPLAIN_DIAGNOSTIC_DATA B
                         WHERE A.A_EXPLAIN_REQUESTER = B.EXPLAIN_REQUESTER
                           AND A.A_EXPLAIN_TIME      = B.EXPLAIN_TIME
                           AND A.A_SOURCE_NAME       = B.SOURCE_NAME
                           AND A.A_SOURCE_SCHEMA     = B.SOURCE_SCHEMA

                           AND A.A_SOURCE_VERSION    = B.SOURCE_VERSION
                           AND A.A_EXPLAIN_LEVEL     = B.EXPLAIN_LEVEL
                           AND A.A_STMTNO            = B.STMTNO
                           AND A.A_SECTNO            = B.SECTNO
                           AND A.A_DIAGNOSTIC_ID     = B.DIAGNOSTIC_ID
                           AND B.ORDINAL=2 ),
                       ( SELECT TOKEN FROM EXPLAIN_DIAGNOSTIC_DATA B
                         WHERE A.A_EXPLAIN_REQUESTER = B.EXPLAIN_REQUESTER
                           AND A.A_EXPLAIN_TIME      = B.EXPLAIN_TIME
                           AND A.A_SOURCE_NAME       = B.SOURCE_NAME
                           AND A.A_SOURCE_SCHEMA     = B.SOURCE_SCHEMA
                           AND A.A_SOURCE_VERSION    = B.SOURCE_VERSION
                           AND A.A_EXPLAIN_LEVEL     = B.EXPLAIN_LEVEL
                           AND A.A_STMTNO            = B.STMTNO
                           AND A.A_SECTNO            = B.SECTNO
                           AND A.A_DIAGNOSTIC_ID     = B.DIAGNOSTIC_ID
                           AND B.ORDINAL=3 ) ) ) F
         WHERE ( EXPLAIN_REQUESTER IS NULL OR
                 EXPLAIN_REQUESTER = A.A_EXPLAIN_REQUESTER )
           AND ( EXPLAIN_TIME      IS NULL OR
                 EXPLAIN_TIME      = A.A_EXPLAIN_TIME      )
           AND ( SOURCE_NAME       IS NULL OR
                 SOURCE_NAME       = A.A_SOURCE_NAME       )
           AND ( SOURCE_SCHEMA     IS NULL OR
                 SOURCE_SCHEMA     = A.A_SOURCE_SCHEMA     )
           AND ( SOURCE_VERSION    IS NULL OR
                 SOURCE_VERSION    = A.A_SOURCE_VERSION    )
           AND ( EXPLAIN_LEVEL     IS NULL OR
                 EXPLAIN_LEVEL     = A.A_EXPLAIN_LEVEL     )
           AND ( STMTNO            IS NULL OR
                 STMTNO            = A.A_STMTNO            )
           AND ( SECTNO            IS NULL OR
                 SECTNO            = A.A_SECTNO            );

SET CURRENT SCHEMA = "BKFRPRD ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE FUNCTION EXPLAIN_GET_MSGS( EXPLAIN_REQUESTER VARCHAR(128),
                                  EXPLAIN_TIME      TIMESTAMP,
                                  SOURCE_NAME       VARCHAR(128),
                                  SOURCE_SCHEMA     VARCHAR(128),
                                  SOURCE_VERSION    VARCHAR(64),
                                  EXPLAIN_LEVEL     CHAR(1),
                                  STMTNO            INTEGER,
                                  SECTNO            INTEGER,
                                  INLOCALE          VARCHAR(33) )
  RETURNS TABLE ( EXPLAIN_REQUESTER VARCHAR(128),
                  EXPLAIN_TIME      TIMESTAMP,
                  SOURCE_NAME       VARCHAR(128),
                  SOURCE_SCHEMA     VARCHAR(128),
                  SOURCE_VERSION    VARCHAR(64),
                  EXPLAIN_LEVEL     CHAR(1),
                  STMTNO            INTEGER,
                  SECTNO            INTEGER,
                  DIAGNOSTIC_ID     INTEGER,
                  LOCALE            VARCHAR(33),
                  MSG               VARCHAR(4096) )
  SPECIFIC EXPLAIN_GET_MSGS
  LANGUAGE SQL
  DETERMINISTIC
  NO EXTERNAL ACTION
  READS SQL DATA
  RETURN SELECT A.A_EXPLAIN_REQUESTER,
                A.A_EXPLAIN_TIME,
                A.A_SOURCE_NAME,
                A.A_SOURCE_SCHEMA,
                A.A_SOURCE_VERSION,
                A.A_EXPLAIN_LEVEL,
                A.A_STMTNO,
                A.A_SECTNO,
                A.A_DIAGNOSTIC_ID,
                F.LOCALE,
                F.MSG
         FROM EXPLAIN_DIAGNOSTIC A( A_EXPLAIN_REQUESTER,
                                    A_EXPLAIN_TIME,
                                    A_SOURCE_NAME,
                                    A_SOURCE_SCHEMA,
                                    A_SOURCE_VERSION,
                                    A_EXPLAIN_LEVEL,
                                    A_STMTNO,
                                    A_SECTNO,
                                    A_DIAGNOSTIC_ID,
                                    A_CODE ),
              TABLE( SYSPROC.EXPLAIN_GET_MSG2(
                       INLOCALE,
                       A.A_CODE,
                       ( SELECT TOKEN FROM EXPLAIN_DIAGNOSTIC_DATA B
                         WHERE A.A_EXPLAIN_REQUESTER = B.EXPLAIN_REQUESTER
                           AND A.A_EXPLAIN_TIME      = B.EXPLAIN_TIME
                           AND A.A_SOURCE_NAME       = B.SOURCE_NAME
                           AND A.A_SOURCE_SCHEMA     = B.SOURCE_SCHEMA
                           AND A.A_SOURCE_VERSION    = B.SOURCE_VERSION
                           AND A.A_EXPLAIN_LEVEL     = B.EXPLAIN_LEVEL
                           AND A.A_STMTNO            = B.STMTNO
                           AND A.A_SECTNO            = B.SECTNO
                           AND A.A_DIAGNOSTIC_ID     = B.DIAGNOSTIC_ID
                           AND B.ORDINAL=1 ),
                       ( SELECT TOKEN FROM EXPLAIN_DIAGNOSTIC_DATA B
                         WHERE A.A_EXPLAIN_REQUESTER = B.EXPLAIN_REQUESTER
                           AND A.A_EXPLAIN_TIME      = B.EXPLAIN_TIME
                           AND A.A_SOURCE_NAME       = B.SOURCE_NAME
                           AND A.A_SOURCE_SCHEMA     = B.SOURCE_SCHEMA

                           AND A.A_SOURCE_VERSION    = B.SOURCE_VERSION
                           AND A.A_EXPLAIN_LEVEL     = B.EXPLAIN_LEVEL
                           AND A.A_STMTNO            = B.STMTNO
                           AND A.A_SECTNO            = B.SECTNO
                           AND A.A_DIAGNOSTIC_ID     = B.DIAGNOSTIC_ID
                           AND B.ORDINAL=2 ),
                       ( SELECT TOKEN FROM EXPLAIN_DIAGNOSTIC_DATA B
                         WHERE A.A_EXPLAIN_REQUESTER = B.EXPLAIN_REQUESTER
                           AND A.A_EXPLAIN_TIME      = B.EXPLAIN_TIME
                           AND A.A_SOURCE_NAME       = B.SOURCE_NAME
                           AND A.A_SOURCE_SCHEMA     = B.SOURCE_SCHEMA
                           AND A.A_SOURCE_VERSION    = B.SOURCE_VERSION
                           AND A.A_EXPLAIN_LEVEL     = B.EXPLAIN_LEVEL
                           AND A.A_STMTNO            = B.STMTNO
                           AND A.A_SECTNO            = B.SECTNO
                           AND A.A_DIAGNOSTIC_ID     = B.DIAGNOSTIC_ID
                           AND B.ORDINAL=3 ) ) ) F
         WHERE ( EXPLAIN_REQUESTER IS NULL OR
                 EXPLAIN_REQUESTER = A.A_EXPLAIN_REQUESTER )
           AND ( EXPLAIN_TIME      IS NULL OR
                 EXPLAIN_TIME      = A.A_EXPLAIN_TIME      )
           AND ( SOURCE_NAME       IS NULL OR
                 SOURCE_NAME       = A.A_SOURCE_NAME       )
           AND ( SOURCE_SCHEMA     IS NULL OR
                 SOURCE_SCHEMA     = A.A_SOURCE_SCHEMA     )
           AND ( SOURCE_VERSION    IS NULL OR
                 SOURCE_VERSION    = A.A_SOURCE_VERSION    )
           AND ( EXPLAIN_LEVEL     IS NULL OR
                 EXPLAIN_LEVEL     = A.A_EXPLAIN_LEVEL     )
           AND ( STMTNO            IS NULL OR
                 STMTNO            = A.A_STMTNO            )
           AND ( SECTNO            IS NULL OR
                 SECTNO            = A.A_SECTNO            );

SET CURRENT SCHEMA = "IGNMON1 ";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","IGNMON1";
CREATE FUNCTION EXPLAIN_GET_MSGS( EXPLAIN_REQUESTER VARCHAR(128),
                                  EXPLAIN_TIME      TIMESTAMP,
                                  SOURCE_NAME       VARCHAR(128),
                                  SOURCE_SCHEMA     VARCHAR(128),
                                  SOURCE_VERSION    VARCHAR(64),
                                  EXPLAIN_LEVEL     CHAR(1),
                                  STMTNO            INTEGER,
                                  SECTNO            INTEGER,
                                  INLOCALE          VARCHAR(33) )
  RETURNS TABLE ( EXPLAIN_REQUESTER VARCHAR(128),
                  EXPLAIN_TIME      TIMESTAMP,
                  SOURCE_NAME       VARCHAR(128),
                  SOURCE_SCHEMA     VARCHAR(128),
                  SOURCE_VERSION    VARCHAR(64),
                  EXPLAIN_LEVEL     CHAR(1),
                  STMTNO            INTEGER,
                  SECTNO            INTEGER,
                  DIAGNOSTIC_ID     INTEGER,
                  LOCALE            VARCHAR(33),
                  MSG               VARCHAR(4096) )
  SPECIFIC EXPLAIN_GET_MSGS
  LANGUAGE SQL
  DETERMINISTIC
  NO EXTERNAL ACTION
  READS SQL DATA
  RETURN SELECT A.A_EXPLAIN_REQUESTER,
                A.A_EXPLAIN_TIME,
                A.A_SOURCE_NAME,
                A.A_SOURCE_SCHEMA,
                A.A_SOURCE_VERSION,
                A.A_EXPLAIN_LEVEL,
                A.A_STMTNO,
                A.A_SECTNO,
                A.A_DIAGNOSTIC_ID,
                F.LOCALE,
                F.MSG
         FROM EXPLAIN_DIAGNOSTIC A( A_EXPLAIN_REQUESTER,
                                    A_EXPLAIN_TIME,
                                    A_SOURCE_NAME,
                                    A_SOURCE_SCHEMA,
                                    A_SOURCE_VERSION,
                                    A_EXPLAIN_LEVEL,
                                    A_STMTNO,
                                    A_SECTNO,
                                    A_DIAGNOSTIC_ID,
                                    A_CODE ),
              TABLE( SYSPROC.EXPLAIN_GET_MSG2(
                       INLOCALE,
                       A.A_CODE,
                       ( SELECT TOKEN FROM EXPLAIN_DIAGNOSTIC_DATA B
                         WHERE A.A_EXPLAIN_REQUESTER = B.EXPLAIN_REQUESTER
                           AND A.A_EXPLAIN_TIME      = B.EXPLAIN_TIME
                           AND A.A_SOURCE_NAME       = B.SOURCE_NAME
                           AND A.A_SOURCE_SCHEMA     = B.SOURCE_SCHEMA
                           AND A.A_SOURCE_VERSION    = B.SOURCE_VERSION
                           AND A.A_EXPLAIN_LEVEL     = B.EXPLAIN_LEVEL
                           AND A.A_STMTNO            = B.STMTNO
                           AND A.A_SECTNO            = B.SECTNO
                           AND A.A_DIAGNOSTIC_ID     = B.DIAGNOSTIC_ID
                           AND B.ORDINAL=1 ),
                       ( SELECT TOKEN FROM EXPLAIN_DIAGNOSTIC_DATA B
                         WHERE A.A_EXPLAIN_REQUESTER = B.EXPLAIN_REQUESTER
                           AND A.A_EXPLAIN_TIME      = B.EXPLAIN_TIME
                           AND A.A_SOURCE_NAME       = B.SOURCE_NAME
                           AND A.A_SOURCE_SCHEMA     = B.SOURCE_SCHEMA

                           AND A.A_SOURCE_VERSION    = B.SOURCE_VERSION
                           AND A.A_EXPLAIN_LEVEL     = B.EXPLAIN_LEVEL
                           AND A.A_STMTNO            = B.STMTNO
                           AND A.A_SECTNO            = B.SECTNO
                           AND A.A_DIAGNOSTIC_ID     = B.DIAGNOSTIC_ID
                           AND B.ORDINAL=2 ),
                       ( SELECT TOKEN FROM EXPLAIN_DIAGNOSTIC_DATA B
                         WHERE A.A_EXPLAIN_REQUESTER = B.EXPLAIN_REQUESTER
                           AND A.A_EXPLAIN_TIME      = B.EXPLAIN_TIME
                           AND A.A_SOURCE_NAME       = B.SOURCE_NAME
                           AND A.A_SOURCE_SCHEMA     = B.SOURCE_SCHEMA
                           AND A.A_SOURCE_VERSION    = B.SOURCE_VERSION
                           AND A.A_EXPLAIN_LEVEL     = B.EXPLAIN_LEVEL
                           AND A.A_STMTNO            = B.STMTNO
                           AND A.A_SECTNO            = B.SECTNO
                           AND A.A_DIAGNOSTIC_ID     = B.DIAGNOSTIC_ID
                           AND B.ORDINAL=3 ) ) ) F
         WHERE ( EXPLAIN_REQUESTER IS NULL OR
                 EXPLAIN_REQUESTER = A.A_EXPLAIN_REQUESTER )
           AND ( EXPLAIN_TIME      IS NULL OR
                 EXPLAIN_TIME      = A.A_EXPLAIN_TIME      )
           AND ( SOURCE_NAME       IS NULL OR
                 SOURCE_NAME       = A.A_SOURCE_NAME       )
           AND ( SOURCE_SCHEMA     IS NULL OR
                 SOURCE_SCHEMA     = A.A_SOURCE_SCHEMA     )
           AND ( SOURCE_VERSION    IS NULL OR
                 SOURCE_VERSION    = A.A_SOURCE_VERSION    )
           AND ( EXPLAIN_LEVEL     IS NULL OR
                 EXPLAIN_LEVEL     = A.A_EXPLAIN_LEVEL     )
           AND ( STMTNO            IS NULL OR
                 STMTNO            = A.A_STMTNO            )
           AND ( SECTNO            IS NULL OR
                 SECTNO            = A.A_SECTNO            );

----------------------------

-- DDL Statements for Views

----------------------------
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","COOL01";
CREATE VIEW bkfair01.v_cpt_school (school_id, school_name, address1, address2,
city, state, postalcode ) AS SELECT id, name, address1, address2, city,
state, postalcode FROM school;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","COOL01";
CREATE VIEW v_cpt_upload (upload_id, school_id, upload_date, file_ext, category_name)
AS SELECT id, school_id, creation_date, file_extension, category_name FROM
BKFAIR01.files;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","BKFAIR01";
CREATE VIEW BKFAIR01.V_CPT_FAIR_STATUS (FAIR_ID, LOGIN_STATUS, STATUS_DATE)
AS SELECT fair_id, info_status,info_status_date FROM BKFAIR01.fair_cplist;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","BKFRPRD";
CREATE VIEW BKFAIR01.V_REWARDS_EXPORT_RPT (FAIR_ID, REWARD_ID, REWARD_DESCRIPTION_SEQUENCE,
REWARD_SUB_DESCRIPTIO_SEQUENCE, REWARD_SUB_RESPONSE_TYPE, REWARD_SUB_DESCRIPTIO_RESPONSE,
REWARD_VOUCHER_VALUE) AS SELECT rtrim(COALESCE(CHAR(CRR.FAIR_ID),'')),
rtrim(COALESCE(CHAR(CRR.REWARD_ID),'')), rtrim(COALESCE(CHAR(CRR.SEQUENCE_NO),'')),
rtrim(COALESCE(CHAR(CRR.CHILD_SEQUENCE_NO),'')), rtrim(COALESCE(CRR.CHILD_RESPONSE_TYPE,'')),
rtrim(COALESCE(CHAR(CRR.CHILD_RESPONSE_VALUE),'')), rtrim(COALESCE(CHAR(CRR.VOUCHER_VALUE,'.'),''))
FROM BKFAIR01.FINANCIAL_SECTION_STATUS F , BKFAIR01.CUSTOMER_REWARDS_RESPONSE
CRR WHERE F.FAIR_ID =CRR.FAIR_ID AND F.CURRENT_SECTION = 'Complete' AND
F.UPDATE_DATE >= (CURRENT TIMESTAMP - 7 DAYS);

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","BKFRPRD";
CREATE VIEW BKFAIR01.V_CPT_CHAIRPERSON (FAIR_ID, CHAIRPERSON_ID, CHAIRPERSON_FIRST_NAME,
CHAIRPERSON_LAST_NAME, CHAIPERSON_EMAIL, CHAIRPERSON_PHONE, COA_STATUS,
COA_STATUS_DATE ) AS  SELECT FLIST.FAIR_ID, C.ID, C.FIRST_NAME, C.LAST_NAME,
C.EMAIL, C.PHONE_NUMBER, FLIST.COA_STATUS, FLIST.COA_STATUS_DATE FROM BKFAIR01.FAIR_CPLIST
FLIST , BKFAIR01.CHAIRPERSON C WHERE FLIST.CHAIRPERSON_ID = C.ID;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_ofe_export_rpt2 ( fair_id, ofe_start_date, ofe_end_date,
ofe_status ) AS SELECT fair_id, ofe_start_date, ofe_end_date, ofe_status
FROM bkfair01.online_shopping;


COMMENT ON TABLE "BKFAIR01"."V_OFE_EXPORT_RPT2" IS 'Release 18, May 2011; View created towards exporting specific data in original format to database RPORTING.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_lu_homepage_event_rpt ( event_id, event_product_id,
event_start_date, event_end_date, event_name ) AS SELECT event_id, event_product_id,
event_start_date, event_end_date, event_name FROM bkfair01.lu_homepage_event;


COMMENT ON TABLE "BKFAIR01"."V_LU_HOMEPAGE_EVENT_RPT" IS 'Per QC 1291, August 2012; view created towards full export of lookup table LU_HOMEPAGE_EVENT data to reporting.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_homepage_principal_activity_rpt ( fair_id, activity_id
) AS SELECT fair_id, activity_id FROM bkfair01.homepage_principal_activities
hpa;


COMMENT ON TABLE "BKFAIR01"."V_HOMEPAGE_PRINCIPAL_ACTIVITY_RPT" IS 'Created for Bkfair Release 21, Fall 2012; Used to export all Principal Activity data to the Reporting database by Reporting script bookfairs_reporting_script.sh.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_lu_principal_activities_rpt ( id, description ) AS
SELECT id, description FROM bkfair01.lu_principal_activities;


COMMENT ON TABLE "BKFAIR01"."V_LU_PRINCIPAL_ACTIVITIES_RPT" IS 'Bkfair Release 21, Fall 2012; Used to export all Lookup Principal Activity data to the Reporting database by Reporting script bookfairs_reporting_script.sh.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_volunteer_login ( id, volunteer_id, fair_id, login_date
) AS SELECT id, volunteer_id, fair_id, login_date FROM bkfair01.volunteer_login_tracking;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_chairperson_spsregistrations_rpt ( chairperson_id,
sps_id, sps_registration_date ) AS SELECT id, spsid, spsid_registration_date
FROM bkfair01.chairperson WHERE spsid_registration_date IS NOT NULL;


COMMENT ON TABLE "BKFAIR01"."V_CHAIRPERSON_SPSREGISTRATIONS_RPT" IS 'New view requested by Business for Reporting purposes in June 2013; Collects the SPS Registration date by Chairpersons for them to get a new SPS_ID. The column names are self explanatory. ';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_volunteer_spsregistrations_rpt ( contact_id, school_id,
sps_id, sps_registration_date ) AS SELECT id, school_id, spsid, spsid_registration_date
FROM bkfair01.address_book WHERE spsid_registration_date IS NOT NULL;


COMMENT ON TABLE "BKFAIR01"."V_VOLUNTEER_SPSREGISTRATIONS_RPT" IS 'New view requested by Business for Reporting purposes in June 2013; Collects the SPS Registration date by Volunteers (Address_book records) for them to get a new SPS_ID. The column names are self explanatory. ';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_new_published_online_homepage_record ( fair_id, contact_person_ckbox,
contact_email_ckbox, contact_phone_number_ckbox, payment_check_ckbox, payment_cc_ckbox,
payment_cash_ckbox, volunteer_recruitment_ckbox, book_fair_goal_ckbox,
special_program_ckbox, special_program_ckbox2, special_program_ckbox3,
special_program_ckbox4, ofe_feature_ckbox, create_date, update_date, selected_goal,
fair_start_date, fair_end_date, goal_amount, actual_amount, school_state,
school_postalcode, fair_location, contact_phone_number, school_city, contact_first_name,
contact_last_name, school_name, contact_email, school_address1 ) AS WITH
fair_data ( fair_id, school_id, fair_start_date, fair_end_date, product_id
) AS ( SELECT id,   school_id, delivery_date, pickup_date, product_id FROM
bkfair01.fair f), default_values ( contact_person_ckbox, contact_email_ckbox,
contact_phone_number_ckbox, payment_check_ckbox, payment_cc_ckbox, payment_cash_ckbox,
volunteer_recruitment_ckbox, book_fair_goal_ckbox, special_program_ckbox,
special_program_ckbox2, special_program_ckbox3, special_program_ckbox4,
ofe_feature_ckbox, create_date, update_date, selected_goal, goal_amount,
actual_amount, fair_location ) AS ( SELECT 'Y', 'Y', 'N', 'Y', 'Y', 'Y',
'Y', 'N', 'N', 'N', 'N', 'N', 'N', CURRENT TIMESTAMP, CURRENT TIMESTAMP,
'NUMBEROFBOOKS', 0, 0, NULLIF('', '') FROM SYSIBM.SYSDUMMY1 ), school_data
( fair_id, school_state, school_postalcode, school_city, school_name, school_address1
) AS ( SELECT f1.fair_id, s.state, s.postalcode, s.city, s.name, s.address1
FROM fair_data       f1, bkfair01.school s WHERE f1.school_id = s.id ),
chairperson_data ( fair_id, contact_phone_number, contact_first_name, contact_last_name,
contact_email ) AS ( SELECT f3.fair_id, cp.phone_number, cp.first_name,
cp.last_name, cp.email FROM fair_data f3 LEFT OUTER JOIN bkfair01.fair_cplist
fcp ON f3.fair_id = fcp.fair_id FULL OUTER JOIN bkfair01.chairperson cp
ON fcp.chairperson_id = cp.id ) SELECT fair_data.fair_id, default_values.contact_person_ckbox,
default_values.contact_email_ckbox, default_values.contact_phone_number_ckbox,
default_values.payment_check_ckbox, default_values.payment_cc_ckbox, default_values.payment_cash_ckbox,
default_values.volunteer_recruitment_ckbox, default_values.book_fair_goal_ckbox,
default_values.special_program_ckbox, default_values.special_program_ckbox2,
default_values.special_program_ckbox3, default_values.special_program_ckbox4,
default_values.ofe_feature_ckbox, default_values.create_date, default_values.update_date,
default_values.selected_goal, fair_data.fair_start_date, fair_data.fair_end_date,
default_values.goal_amount, default_values.actual_amount, school_data.school_state,
school_data.school_postalcode, default_values.fair_location, chairperson_data.contact_phone_number,
school_data.school_city, chairperson_data.contact_first_name, chairperson_data.contact_last_name,
school_data.school_name, chairperson_data.contact_email, school_data.school_address1
FROM fair_data, default_values, school_data, chairperson_data WHERE fair_data.fair_id
= school_data.fair_id AND fair_data.fair_id = chairperson_data.fair_id;


COMMENT ON TABLE "BKFAIR01"."V_NEW_PUBLISHED_ONLINE_HOMEPAGE_RECORD" IS 'Bkfair release 19; View specifically design to create ONLINE_HOMEPAGE records via a trigger. This view is not used by the online application as such.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_chairperson_email_invitation_rpt ( fair_id, region_id,
chairperson_email, email_type, email_click_through, email_delivered, email_sent_date
) AS SELECT f.id, f.region_id, c.chairperson_email, c.email_type, c.email_click_through,
c.email_delivered, c.email_sent_date FROM bkfair01.fair f, bkfair01.chairperson_email_invitation
c WHERE f.id = c.fair_id UNION SELECT f.id, f.region_id, c.email, c.email_type,
'N', 'N', c.email_send_date FROM bkfair01.fair f, bkfair01.chairperson_email_school_tax
c WHERE f.id = c.fair_id;


COMMENT ON TABLE "BKFAIR01"."V_CHAIRPERSON_EMAIL_INVITATION_RPT" IS 'Modified for Release 22; For reporting purposes, this view now combines all email types and new ones sent only once to Schools and recorded in table CHAIRPERSON_EMAIL_SCHOOL_TAX.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_cpt_event ( fair_id, region_id, product_id, school_id,
event_id, event_name, event_date, event_hours, starting_time, ending_time,
event_name_feed ) AS SELECT DISTINCT f.id, f.region_id, f.product_id, f.school_id,
he.event_id, he.event_name, he.schedule_date, he.event_time, he.start_time,
he.end_time, lhe.event_name FROM bkfair01.homepage_event  he JOIN bkfair01.fair
         f ON he.fair_id = f.id LEFT OUTER JOIN bkfair01.lu_homepage_event
lhe ON he.event_id = lhe.event_id;


COMMENT ON TABLE "BKFAIR01"."V_CPT_EVENT" IS 'Modified in Release 19.8; View created essentially for Reporting export purposes; added the starting_time and ending_time, columns newly created in HOMEPAGE_EVENT.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_cpt_financial_form_rpt ( fair_id, region_id, total_fair_sales,
total_collected, total_book_profit_taken, total_book_profit_balance, total_irc_vochure,
total_cash_profit, total_cash_and_check_collected, total_cc_collected,
total_po_collected, total_book_bonus_taken, total_book_bonus_balance, start_date,
submission_date, current_section, invoice_scenario  ) AS SELECT f.id, f.region_id,
ss.total_fair_sales, ss.total_collected, ps.total_book_profit_taken, ps.total_book_profit_bal,
ps.total_irc_vochure, ps.total_cash_profit, ccs.total_amount, cc.total_amount,
po.total_po_collected, ps.total_book_bonus_taken, ps.total_book_bonus_bal,
fss.started_on, ( CASE WHEN fss.current_section = 'Complete' THEN fss.update_date
END ), fss.current_section, fss.invoice_scenario FROM bkfair01.fair f JOIN
bkfair01.cash_and_checks_sale      ccs ON ccs.fair_id = f.id LEFT OUTER
JOIN bkfair01.financial_section_status fss ON ccs.fair_id = fss.fair_id
LEFT OUTER JOIN bkfair01.sales_summary  ss ON ccs.fair_id = ss.fair_id
LEFT OUTER JOIN bkfair01.profit_summary ps ON ccs.fair_id = ps.fair_id
LEFT OUTER JOIN bkfair01.cc_sales       cc ON ccs.fair_id = cc.fair_id
LEFT OUTER JOIN bkfair01.po_sales       po ON ccs.fair_id = po.fair_id
GROUP BY f.id, region_id, total_fair_sales, total_collected, total_book_profit_taken,
ps.total_book_profit_bal, total_irc_vochure, total_cash_profit, ccs.total_amount,
cc.total_amount, total_po_collected, total_book_bonus_taken, ps.total_book_bonus_bal,
fss.started_on, fss.update_date, current_section, invoice_scenario;


COMMENT ON TABLE "BKFAIR01"."V_CPT_FINANCIAL_FORM_RPT" IS 'Bkfair release 19, Nov. 2011; Replaces View v_cpt_fin_firm; Captures Financial Info needed for reporting purposes; Used by Reporting script bookfairs_reporting_script.sh.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_export_sales_consultant_rpt ( sc_toolkit_assistance,
sc_schedule_book_fair, sc_book_fair_month, sc_book_fair_year, sc_chaiperson_information,
sc_fair_dates, sc_other, sc_first_name, sc_last_name, sc_email, sc_phone,
sc_time_to_call, sc_contact_by_email, sc_contact_by_phone, sc_contact_by_either,
sc_comments, sc_submit_date_time, fair_id, school_name, school_address,
school_address_line2, school_city, school_state, school_zip, school_account_no,
fair_start_date, fair_end_date, chairperson_first_name, chairperson_last_name,
chairperson_email, chairperson_phone, sales_type ) AS SELECT RTRIM (COALESCE
(scef.sc_toolkit_assistance, '')), RTRIM (COALESCE (scef.sc_schedule_book_fair,
'')), RTRIM (COALESCE (scef.sc_book_fair_month, '')), RTRIM (COALESCE (CHAR
(scef.sc_book_fair_year), '')), RTRIM (COALESCE (scef.sc_chaiperson_information,
'')), RTRIM (COALESCE (scef.sc_fair_dates, '')), RTRIM (COALESCE (scef.sc_other,
'')), RTRIM (COALESCE (scef.sc_first_name, '')), RTRIM (COALESCE (scef.sc_last_name,
'')), RTRIM (COALESCE (scef.sc_email, '')), RTRIM (COALESCE (scef.sc_phone_number,
'')), RTRIM (COALESCE (scef.sc_time_to_call, '')), RTRIM (COALESCE (scef.sc_contact_by_email,
'')), RTRIM (COALESCE (scef.sc_contact_by_phone, '')), RTRIM (COALESCE
(scef.sc_contact_by_either, '')), RTRIM (COALESCE (scef.sc_comments, '')),
RTRIM (CHAR (YEAR (scef.sc_submit_date_time))) || (CASE WHEN LENGTH (RTRIM
(CHAR (MONTH (scef.sc_submit_date_time)))) = 2 THEN LTRIM (RTRIM (CHAR
(MONTH (scef.sc_submit_date_time)))) ELSE '0' || LTRIM (RTRIM (CHAR (MONTH
(scef.sc_submit_date_time)))) END) || (CASE WHEN LENGTH (RTRIM (CHAR (DAY
(scef.sc_submit_date_time)))) = 2 THEN LTRIM (RTRIM (CHAR (DAY (scef.sc_submit_date_time))))
ELSE '0' || LTRIM (RTRIM (CHAR (DAY (scef.sc_submit_date_time)))) END)
|| (CASE WHEN LENGTH (RTRIM (CHAR (HOUR (scef.sc_submit_date_time)))) =
2 THEN LTRIM (RTRIM (CHAR (HOUR (scef.sc_submit_date_time)))) ELSE '0'
|| LTRIM (RTRIM (CHAR (HOUR (scef.sc_submit_date_time)))) END) || (CASE
WHEN LENGTH (RTRIM (CHAR (MINUTE (scef.sc_submit_date_time)))) = 2 THEN
LTRIM (RTRIM (CHAR (MINUTE (scef.sc_submit_date_time)))) ELSE '0' || LTRIM
(RTRIM (CHAR (MINUTE (scef.sc_submit_date_time)))) END), RTRIM (CHAR (scef.fair_id)),
RTRIM (COALESCE (s.name, '')), RTRIM (COALESCE (s.address1, '')), RTRIM
(COALESCE (s.address2, '')), RTRIM (COALESCE (s.city, '')), RTRIM (COALESCE
(s.state, '')), RTRIM (COALESCE (s.postalcode, '')), RTRIM (COALESCE (RTRIM
(CHAR (s.id)), '')), SUBSTR (CHAR (f.delivery_date), 1, 4) || SUBSTR (CHAR
(f.delivery_date), 6, 2) || SUBSTR (CHAR (f.delivery_date), 9, 2), SUBSTR
(CHAR (f.pickup_date), 1, 4) || SUBSTR (CHAR (f.pickup_date), 6, 2) ||
SUBSTR (CHAR (f.pickup_date), 9, 2), RTRIM (COALESCE (c.first_name, '')),
RTRIM (COALESCE (c.last_name, '')), RTRIM (COALESCE (c.email, '')), RTRIM
(COALESCE (c.phone_number, '')), RTRIM (coalesce (scef.sales_type, ''))
FROM bkfair01.sales_consultant_email_forms scef JOIN bkfair01.fair f ON
scef.fair_id = f.id LEFT OUTER JOIN bkfair01.school s ON f.school_id =
s.id LEFT OUTER JOIN bkfair01.fair_cplist flist ON scef.fair_id = flist.fair_id
LEFT OUTER JOIN bkfair01.chairperson c ON flist.chairperson_id = c.id WHERE
DATE (scef.sc_submit_date_time) BETWEEN DATE ( CURRENT TIMESTAMP - 7 DAYS)
AND DATE ( CURRENT TIMESTAMP - 1 DAYS);

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE view bkfair01.v_irc_voucher_profit_rpt ( fair_id, irc_voucher_profit_multiplier,
irc_voucher_profit ) AS SELECT ss.fair_id, t.irc_voucher_profit_multiplier,
(CASE WHEN f.fair_type = 'U' THEN '0' ELSE COALESCE (CHAR (irc.irc_voucher_profit),
'0') END) FROM bkfair01.fair f JOIN bkfair01.sales_summary ss ON f.id =
ss.fair_id LEFT OUTER JOIN bkfair01.fair_financial_info info ON ss.fair_id
= info.fair_id AND info.incresing_revenue_prior != 0.00 LEFT OUTER JOIN
bkfair01.irc_percentage_increase_rule irc ON ((( ss.total_fair_sales -
info.incresing_revenue_prior ) / info.incresing_revenue_prior ) * 100 )
>= irc.percentage_increase_from AND ((( ss.total_fair_sales - info.incresing_revenue_prior
) / info.incresing_revenue_prior ) * 100 ) < irc.percentage_increase_to
AND irc.percentage_start_date <= f.delivery_date AND irc.percentage_end_date
>= f.delivery_date LEFT OUTER JOIN bkfair01.irc_threshold_bonus_rules t
ON ss.total_fair_sales >= t.total_fair_sales_from AND ss.total_fair_sales
< t.total_fair_sales_to AND t.multiplier_start_date <= f.delivery_date
AND t.multiplier_end_date >= f.delivery_date;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_online_homepage_features_rpt ( fair_id, region_id,
display_volunteer, display_goal, display_special1, display_special2, display_special3,
display_special4, book, dollar, principal_activity_display ) AS SELECT
f.id, f.region_id, oh.volunteer_recruitment_ckbox, oh.book_fair_goal_ckbox,
oh.special_program_ckbox, oh.special_program_ckbox2, oh.special_program_ckbox3,
oh.special_program_ckbox4, ( CASE WHEN oh.book_fair_goal_ckbox = 'Y' AND
selected_goal = 'NUMBEROFBOOKS' THEN 'Y' ELSE 'N' END ), ( CASE WHEN oh.book_fair_goal_ckbox
= 'Y' AND oh.selected_goal = 'NUMBEROFDOLLARS' THEN 'Y' ELSE 'N' END ),
oh.principal_display_ckbox FROM bkfair01.online_homepage  oh, bkfair01.fair
 f WHERE oh.fair_id = f.id;


COMMENT ON TABLE "BKFAIR01"."V_ONLINE_HOMEPAGE_FEATURES_RPT" IS 'Updated in Bkfair release 21, Fall 2012; Used by Reporting script bookfairs_reporting_script.sh.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_online_homepage_published_rpt ( region_id, fair_id,
homepage_published_date, homepage_status, web_url, fair_finder_ckbox, republished_date
) AS SELECT f.region_id, hu.fair_id, hu.published_date, hu.published_status,
hu.web_url, hu.fair_finder_ckbox, hu.republished_date FROM bkfair01.fair
f, bkfair01.homepage_url hu WHERE f.id = hu.fair_id;


COMMENT ON TABLE "BKFAIR01"."V_ONLINE_HOMEPAGE_PUBLISHED_RPT" IS 'Bkfair release 19, Nov. 2011; Replaces View v_homepage_published_rpt; Captures indiscriminately all HOMEPAGE_URL records and needs to be reviewed in next release; Used by Reporting script bookfairs_reporting_script.sh.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_ff_export_rpt ( fair_id,                    total_collected,
             tax_exempt_sales, taxable_sales_less_tax,     sales_tax_due,
               sales_tax_collected, tax_paid_by,                total_fair_sales,
            tax_exempt_id, total_cashcheck_collected,  total_cc_collected,
          total_po_collected, resale_certificate,         bonus_books_taken_from_fair,
 bonus_book_credit, books_taken_from_fair,      book_profit_credit,   
       max_bonus_incentive, cash_profit,                ccsales_num_of_transactions,
 invoice_date, fair_invoice_id,            po_invoice_id,             
  tax_invoice_id, po_bill_to_contact,         po_bill_to_office,      
     po_bill_to_address, po_bill_to_city,            po_bill_to_state,
            po_bill_to_zip, po_1_number,                po_1_amount,  
               po_2_number, po_2_amount,                po_3_number,  
               po_3_amount, po_4_number,                po_4_amount,  
               po_5_number, po_5_amount,                po_6_number,  
               po_6_amount, po_7_number,                po_7_amount,  
               po_8_number, po_8_amount,                po_9_number,  
               po_9_amount, po_10_number,               po_10_amount, 
               po_11_number, po_11_amount,               po_12_number,
                po_12_amount, initial_tax_rate,           threshold_multiplier,
        irc_voucher_profit, total_redeemed_gc,          books_from_fair_prev_bal,
    max_bonus, bogo_bonus,                 po_1_type,                 
  po_2_type, po_3_type,                  po_4_type,                   
po_5_type, po_6_type,                  po_7_type,                    po_8_type,
po_9_type,                  po_10_type,                   po_11_type, po_12_type,
                second_bogo_bonus ) AS SELECT DISTINCT RTRIM (COALESCE
(CHAR (f.fair_id), '')), RTRIM (COALESCE (CHAR (ss.total_collected, '.'),
'')), RTRIM (COALESCE (CHAR (ss.tax_exempt_sales, '.'), '')), RTRIM (COALESCE
(CHAR (ss.taxable_sales_less_tax, '.'), '')), RTRIM (COALESCE (CHAR (ss.sales_tax_due,
'.'), '')), RTRIM (COALESCE (CHAR (ss.sales_tax, '.'), '')), RTRIM (COALESCE
( (CASE WHEN str.remittance_by_scholastic = 0 THEN 'C' ELSE 'B' END ),
'')), RTRIM (COALESCE (CHAR (ss.total_fair_sales, '.'), '')), RTRIM (COALESCE
( (CASE WHEN (   stet.tax_exempt_id != '' OR stet.tax_exempt_id IS NOT
NULL) THEN stet.tax_exempt_id ELSE sted.tax_exempt_id END), '')), RTRIM
(COALESCE (CHAR (ccs.total_amount, '.'), '')), RTRIM (COALESCE (CHAR (ccsales.total_amount,
'.'), '')), RTRIM (COALESCE (CHAR (posales.total_po_collected, '.'), '')),
RTRIM (COALESCE ( (CASE WHEN stes.tax_status IN ('AllResale', 'ResaleAndExempted')
THEN 'Y' ELSE 'N' END), '')), RTRIM (COALESCE (CHAR (profits.total_book_bonus_taken,
'.'), '')), RTRIM (COALESCE (CHAR (profits.total_book_bonus_bal, '.'),
'')), RTRIM (COALESCE (CHAR (profits.total_book_profit_taken, '.'), '')),
RTRIM (COALESCE (CHAR (pts.total_amount_books, '.'), '')), RTRIM (COALESCE
(CHAR (profits.max_bonus_incentive, '.'), '')), RTRIM (COALESCE (CHAR (profits.total_cash_profit,
'.'), '')), RTRIM (COALESCE (CHAR (ccsales.num_of_transactions), '')),
RTRIM (CHAR (YEAR (f.update_date)))  || (CASE WHEN LENGTH (RTRIM (CHAR
(MONTH (f.update_date)))) = 1 THEN '0' || RTRIM (CHAR (MONTH (f.update_date)))
ELSE RTRIM (CHAR (MONTH (f.update_date))) END)  || (CASE WHEN LENGTH (RTRIM
(CHAR (DAY (f.update_date)))) = 1 THEN '0' || RTRIM (CHAR (DAY (f.update_date)))
ELSE RTRIM (CHAR (day (f.update_date))) END), 'W' || RTRIM (CHAR (f.fair_id))
|| 'BF', (CASE WHEN posales.total_po_collected > 0.00 THEN 'W' || RTRIM
(CHAR (f.fair_id)) || 'PO' ELSE '' END), (CASE WHEN ((ss.sales_tax > 0.00
OR ss.sales_tax_due > 0.00) AND (str.remittance_by_scholastic = 0)) THEN
'W' || RTRIM (CHAR (f.fair_id)) || 'TX' ELSE '' END), RTRIM (COALESCE (pocontact.bill_to_name,
'')), RTRIM (COALESCE (pocontact.district_name, '')), RTRIM (COALESCE (pocontact.address,
'')), RTRIM (COALESCE (pocontact.city, '')), RTRIM (COALESCE (pocontact.state,
'')), RTRIM (COALESCE (pocontact.zipcode, '')), RTRIM (COALESCE (posales.po_1_number,
'')), RTRIM (COALESCE (CHAR (posales.po_1_amount, '.'), '')), RTRIM (COALESCE
(posales.po_2_number, '')), RTRIM (COALESCE (CHAR (posales.po_2_amount,
'.'), '')), RTRIM (COALESCE (posales.po_3_number, '')), RTRIM (COALESCE
(CHAR (posales.po_3_amount, '.'), '')), RTRIM (COALESCE (posales.po_4_number,
'')), RTRIM (COALESCE (CHAR (posales.po_4_amount, '.'), '')), RTRIM (COALESCE
(posales.po_5_number, '')), RTRIM (COALESCE (CHAR (posales.po_5_amount,
'.'), '')), RTRIM (COALESCE (posales.po_6_number, '')), RTRIM (COALESCE
(CHAR (posales.po_6_amount, '.'), '')), RTRIM (COALESCE (posales.po_7_number,
'')), RTRIM (COALESCE (CHAR (posales.po_7_amount, '.'), '')), RTRIM (COALESCE
(posales.po_8_number, '')), RTRIM (COALESCE (CHAR (posales.po_8_amount,
'.'), '')), RTRIM (COALESCE (posales.po_9_number, '')), RTRIM (COALESCE
(CHAR (posales.po_9_amount, '.'), '')), RTRIM (COALESCE (posales.po_10_number,
'')), RTRIM (COALESCE (CHAR (posales.po_10_amount, '.'), '')), RTRIM (COALESCE
(posales.po_11_number, '')), RTRIM (COALESCE (CHAR (posales.po_11_amount,
'.'), '')), RTRIM (COALESCE (posales.po_12_number, '')), RTRIM (COALESCE
(CHAR (posales.po_12_amount, '.'), '')), RTRIM (COALESCE (CHAR (taxdetail.initial_tax_rate),
'')), RTRIM (COALESCE (CHAR (vivp.irc_voucher_profit_multiplier), '')),
RTRIM (COALESCE (CHAR (vivp.irc_voucher_profit), '')), '', RTRIM (CHAR
( COALESCE (DECIMAL (pt.pre_book_profit_taken, 9, 2), 0000000.00))), RTRIM
(CHAR ( COALESCE (DECIMAL (SUM (profits.max_bonus), 9, 2), 0000000.00))),
RTRIM (CHAR ( COALESCE (DECIMAL (SUM (profits.bogo_bonus), 10, 2), 0000000.00))),
COALESCE (po_1_type, ''), COALESCE (po_2_type, ''), COALESCE (po_3_type,
''), COALESCE (po_4_type, ''), COALESCE (po_5_type, ''), COALESCE (po_6_type,
''), COALESCE (po_7_type, ''), COALESCE (po_8_type, ''), COALESCE (po_9_type,
''), COALESCE (po_10_type, ''), COALESCE (po_11_type, ''), COALESCE (po_12_type,
''), RTRIM (CHAR ( COALESCE (DECIMAL (SUM (profits.second_bogo_bonus),
10, 2), 0000000.00))) FROM bkfair01.financial_section_status f LEFT OUTER
JOIN bkfair01.sales_summary ss ON f.fair_id = ss.fair_id LEFT OUTER JOIN
bkfair01.sales_tax_remittance str ON f.fair_id = str.fair_id LEFT OUTER
JOIN bkfair01.sales_tax_exempt_transactions stet ON f.fair_id = stet.fair_id
LEFT OUTER JOIN bkfair01.sales_tax_exempt_details sted ON f.fair_id = sted.fair_id
LEFT OUTER JOIN bkfair01.cash_and_checks_sale ccs ON f.fair_id = ccs.fair_id
LEFT OUTER JOIN bkfair01.po_sales posales ON f.fair_id = posales.fair_id
LEFT OUTER JOIN bkfair01.sales_tax_exempt_status stes ON f.fair_id = stes.fair_id
LEFT OUTER JOIN bkfair01.profit_summary profits ON f.fair_id = profits.fair_id
LEFT OUTER JOIN bkfair01.profit_to_split pts ON f.fair_id = pts.fair_id
LEFT OUTER JOIN bkfair01.cc_sales ccsales ON f.fair_id = ccsales.fair_id
LEFT OUTER JOIN bkfair01.po_contact pocontact ON f.fair_id = pocontact.fair_id
LEFT OUTER JOIN bkfair01.tax_detail taxdetail ON f.fair_id = taxdetail.fair_id
LEFT OUTER JOIN bkfair01.v_irc_voucher_profit_rpt vivp ON f.fair_id = vivp.fair_id
LEFT OUTER JOIN bkfair01.profit_taken pt ON f.fair_id = pt.fair_id WHERE
f.current_section = 'Complete' AND f.update_date >= (CURRENT TIMESTAMP
- 7 DAYS) GROUP BY f.fair_id,             irc_voucher_profit,         
 irc_voucher_profit_multiplier, f.update_date,         initial_tax_rate,
            po_12_amount, num_of_transactions,   total_cash_profit,   
        max_bonus_incentive, total_amount_books,    total_book_profit_taken,
     total_book_bonus_bal, total_book_bonus_taken,tax_status,         
         total_fair_sales, taxable_sales_less_tax,tax_exempt_sales,   
         taxable_sales_less_tax, total_collected,       po_1_number,  
               po_1_amount, po_2_number,           po_2_amount,       
          po_3_number, po_3_amount,           po_4_number,            
     po_4_amount, po_5_number,           po_5_amount,                 
po_6_number, po_6_amount,           po_7_number,                  po_7_amount,
po_8_number,           po_8_amount,                  po_9_number, po_9_amount,
          po_10_number,                 po_10_amount, po_11_number,   
      po_11_amount,                 po_12_number, zipcode,            
  state,                        city, address,               district_name,
               bill_to_name, sales_tax,             sales_tax_due,    
           remittance_by_scholastic, total_po_collected,    ccs.total_amount,
            ccsales.total_amount, sted.tax_exempt_id,    stet.tax_exempt_id,
          pt.pre_book_profit_taken, profits.max_bonus,     profits.bogo_bonus,
          po_1_type, po_2_type,             po_3_type,                
   po_4_type, po_5_type,             po_6_type,                    po_7_type,
po_8_type,             po_9_type,                    po_10_type, po_11_type,
           po_12_type,                   profits.second_bogo_bonus UNION
SELECT DISTINCT RTRIM (COALESCE (CHAR (f.fair_id), '')), RTRIM (COALESCE
(CHAR (ss.total_collected, '.'), '')), RTRIM (COALESCE (CHAR (ss.tax_exempt_sales,
'.'), '')), RTRIM (COALESCE (CHAR (ss.taxable_sales_less_tax, '.'), '')),
RTRIM (COALESCE (CHAR (ss.sales_tax_due, '.'), '')), RTRIM (COALESCE (CHAR
(ss.sales_tax, '.'), '')), RTRIM (COALESCE ( (CASE WHEN str.remittance_by_scholastic
= 0 THEN 'C' ELSE 'B' END), '')), RTRIM (COALESCE (CHAR (ss.total_fair_sales,
'.'), '')), RTRIM (COALESCE ( (CASE WHEN (   stet.tax_exempt_id != '' OR
stet.tax_exempt_id IS NOT NULL) THEN stet.tax_exempt_id ELSE sted.tax_exempt_id
END), '')), RTRIM (COALESCE (CHAR (ccs.total_amount, '.'), '')), RTRIM
(COALESCE (CHAR (ccsales.total_amount, '.'), '')), RTRIM (COALESCE (CHAR
(posales.total_po_collected, '.'), '')), RTRIM (COALESCE ( (CASE WHEN stes.tax_status
IN ('AllResale', 'ResaleAndExempted') THEN 'Y' ELSE 'N' END), '')), RTRIM
(COALESCE (CHAR (profits.total_book_bonus_taken, '.'), '')), RTRIM (COALESCE
(CHAR (profits.total_book_bonus_bal, '.'), '')), RTRIM (COALESCE (CHAR
(profits.total_book_profit_taken, '.'), '')), RTRIM (COALESCE (CHAR (pts.total_amount_books,
'.'), '')), RTRIM (COALESCE (CHAR (profits.max_bonus_incentive, '.'), '')),
RTRIM (COALESCE (CHAR (profits.total_cash_profit, '.'), '')), RTRIM (COALESCE
(CHAR (ccsales.num_of_transactions), '')), RTRIM (CHAR (YEAR (f.update_date)))
 || (CASE WHEN LENGTH (RTRIM (CHAR (MONTH (f.update_date)))) = 1 THEN '0'
|| RTRIM (CHAR (MONTH (f.update_date))) ELSE RTRIM (CHAR (MONTH (f.update_date)))
END) || (CASE  WHEN LENGTH (RTRIM (CHAR (DAY (f.update_date)))) = 1 THEN
'0' || RTRIM (CHAR (DAY (f.update_date))) ELSE RTRIM (CHAR (DAY (f.update_date)))
END), 'W' || RTRIM (CHAR (f.fair_id)) || 'BF', (CASE WHEN posales.total_po_collected
> 0.00 THEN 'W' || RTRIM (CHAR (f.fair_id)) || 'PO' ELSE '' END), (CASE
WHEN ((ss.sales_tax > 0.00 OR ss.sales_tax_due > 0.00) AND (str.remittance_by_scholastic
= 0)) THEN 'W' || RTRIM (CHAR (f.fair_id)) || 'TX' ELSE '' END), RTRIM
(COALESCE (pocontact.bill_to_name, '')), RTRIM (COALESCE (pocontact.district_name,
'')), RTRIM (COALESCE (pocontact.address, '')), RTRIM (COALESCE (pocontact.city,
'')), RTRIM (COALESCE (pocontact.state, '')), RTRIM (COALESCE (pocontact.zipcode,
'')), RTRIM (COALESCE (posales.po_1_number, '')), RTRIM (COALESCE (CHAR
(posales.po_1_amount, '.'), '')), RTRIM (COALESCE (posales.po_2_number,
'')), RTRIM (COALESCE (CHAR (posales.po_2_amount, '.'), '')), RTRIM (COALESCE
(posales.po_3_number, '')), RTRIM (COALESCE (CHAR (posales.po_3_amount,
'.'), '')), RTRIM (COALESCE (posales.po_4_number, '')), RTRIM (COALESCE
(CHAR (posales.po_4_amount, '.'), '')), RTRIM (COALESCE (posales.po_5_number,
'')), RTRIM (COALESCE (CHAR (posales.po_5_amount, '.'), '')), RTRIM (COALESCE
(posales.po_6_number, '')), RTRIM (COALESCE (CHAR (posales.po_6_amount,
'.'), '')), RTRIM (COALESCE (posales.po_7_number, '')), RTRIM (COALESCE
(CHAR (posales.po_7_amount, '.'), '')), RTRIM (COALESCE (posales.po_8_number,
'')), RTRIM (COALESCE (CHAR (posales.po_8_amount, '.'), '')), RTRIM (COALESCE
(posales.po_9_number, '')), RTRIM (COALESCE (CHAR (posales.po_9_amount,
'.'), '')), RTRIM (COALESCE (posales.po_10_number, '')), RTRIM (COALESCE
(CHAR (posales.po_10_amount, '.'), '')), RTRIM (COALESCE (posales.po_11_number,
'')), RTRIM (COALESCE (CHAR (posales.po_11_amount, '.'), '')), RTRIM (COALESCE
(posales.po_12_number, '')), RTRIM (COALESCE (CHAR (posales.po_12_amount,
'.'), '')), RTRIM (COALESCE (CHAR (taxdetail.initial_tax_rate), '')), RTRIM
(COALESCE (CHAR (vivp.irc_voucher_profit_multiplier), '')), RTRIM (COALESCE
(CHAR (vivp.irc_voucher_profit), '')), '', RTRIM (CHAR ( COALESCE (DECIMAL
(pt.pre_book_profit_taken, 9, 2), 0000000.00))), RTRIM (CHAR ( COALESCE
(DECIMAL (SUM (profits.max_bonus), 9, 2), 0000000.00))), RTRIM (CHAR (
COALESCE (DECIMAL (SUM (profits.bogo_bonus), 10, 2), 0000000.00))), COALESCE
(po_1_type, ''), COALESCE (po_2_type, ''), COALESCE (po_3_type, ''), COALESCE
(po_4_type, ''), COALESCE (po_5_type, ''), COALESCE (po_6_type, ''), COALESCE
(po_7_type, ''), COALESCE (po_8_type, ''), COALESCE (po_9_type, ''), COALESCE
(po_10_type, ''), COALESCE (po_11_type, ''), COALESCE (po_12_type, ''),
RTRIM (CHAR ( COALESCE (DECIMAL (SUM (profits.second_bogo_bonus), 10, 2),
0000000.00))) FROM bkfair01.financial_section_status f LEFT OUTER JOIN
bkfair01.sales_summary ss ON f.fair_id = ss.fair_id LEFT OUTER JOIN bkfair01.sales_tax_remittance
str ON f.fair_id = str.fair_id LEFT OUTER JOIN bkfair01.sales_tax_exempt_transactions
stet ON f.fair_id = stet.fair_id LEFT OUTER JOIN bkfair01.sales_tax_exempt_details
sted ON f.fair_id = sted.fair_id LEFT OUTER JOIN bkfair01.cash_and_checks_sale
ccs ON f.fair_id = ccs.fair_id LEFT OUTER JOIN bkfair01.po_sales posales
ON f.fair_id = posales.fair_id LEFT OUTER JOIN bkfair01.sales_tax_exempt_status
stes ON f.fair_id = stes.fair_id LEFT OUTER JOIN bkfair01.profit_summary
profits ON f.fair_id = profits.fair_id LEFT OUTER JOIN bkfair01.profit_to_split
pts ON f.fair_id = pts.fair_id LEFT OUTER JOIN bkfair01.cc_sales ccsales
ON f.fair_id = ccsales.fair_id LEFT OUTER JOIN bkfair01.po_contact pocontact
ON f.fair_id = pocontact.fair_id LEFT OUTER JOIN bkfair01.tax_detail taxdetail
ON f.fair_id = taxdetail.fair_id LEFT OUTER JOIN bkfair01.v_irc_voucher_profit_rpt
vivp ON f.fair_id = vivp.fair_id LEFT OUTER JOIN bkfair01.profit_taken
pt ON f.fair_id = pt.fair_id WHERE f.current_section = 'Complete' AND f.update_date
>= (CURRENT TIMESTAMP - 7 DAYS) GROUP BY f.fair_id,              irc_voucher_profit,
          irc_voucher_profit_multiplier, f.update_date,          initial_tax_rate,
            po_12_amount, num_of_transactions,    total_cash_profit,  
         max_bonus_incentive, total_amount_books,     total_book_profit_taken,
     total_book_bonus_bal, total_book_bonus_taken, tax_status,        
          total_fair_sales, taxable_sales_less_tax, tax_exempt_sales, 
           taxable_sales_less_tax, total_collected,        po_1_number,
                 po_1_amount, po_2_number,            po_2_amount,    
             po_3_number, po_3_amount,            po_4_number,        
         po_4_amount, po_5_number,            po_5_amount,            
     po_6_number, po_6_amount,            po_7_number,                
 po_7_amount, po_8_number,            po_8_amount,                  po_9_number,
po_9_amount,            po_10_number,                 po_10_amount, po_11_number,
          po_11_amount,                 po_12_number, zipcode,        
       state,                        city, address,                district_name,
               bill_to_name, sales_tax,              sales_tax_due,   
            remittance_by_scholastic, total_po_collected,     ccs.total_amount,
            ccsales.total_amount, sted.tax_exempt_id,     stet.tax_exempt_id,
          pt.pre_book_profit_taken, profits.max_bonus,      profits.bogo_bonus,
          po_1_type, po_2_type,              po_3_type,               
    po_4_type, po_5_type,              po_6_type,                    po_7_type,
po_8_type,              po_9_type,                    po_10_type, po_11_type,
            po_12_type,                   profits.second_bogo_bonus;


COMMENT ON TABLE "BKFAIR01"."V_FF_EXPORT_RPT" IS 'Used by 2 Kornshell scripts (...secondary...); Rel-22, replaced tax_paid_by with B instead of S.; Rel-23.5, BTS 2013-2014, add handling of second_bogo_bonus_percentage.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_export_not_correct_rpt ( nc_chairperson_information,
nc_school_information, nc_fair_dates, nc_other, nc_first_name, nc_last_name,
nc_email, nc_phone, nc_time_to_call, nc_contact_by_email, nc_contact_by_phone,
nc_contact_by_either, nc_comments, cs_submit_date_time, fair_id, school_name,
school_address, school_address_line2, school_city, school_state, school_zip,
school_account_no, fair_start_date, fair_end_date, chairperson_first_name,
chairperson_last_name, chairperson_email, chairperson_phone, nc_school_address1,
nc_school_address2, nc_school_city, nc_school_state, nc_school_zip, nc_fair_start_date,
nc_fair_end_date, nc_not_scheduled, region_id, salesperson_first_name,
salesperson_last_name ) AS SELECT RTRIM (COALESCE (ncef.nc_chairperson_information,
'')), RTRIM (COALESCE (ncef.nc_school_information, '')), RTRIM (COALESCE
(ncef.nc_fair_dates, '')), RTRIM (COALESCE (ncef.nc_other, '')), RTRIM
(COALESCE (ncef.nc_first_name, '')), RTRIM (COALESCE (ncef.nc_last_name,
'')), RTRIM (COALESCE (ncef.nc_email, '')), RTRIM (COALESCE (ncef.nc_phone,
'')), RTRIM (COALESCE (ncef.nc_time_to_call, '')), RTRIM (COALESCE (ncef.nc_contact_by_email,
'')), RTRIM (COALESCE (ncef.nc_contact_by_phone, '')), RTRIM (COALESCE
(ncef.nc_contact_by_either, '')), RTRIM (COALESCE (ncef.nc_comments, '')),
RTRIM (CHAR (YEAR (ncef.nc_submit_date_time))) || (CASE WHEN LENGTH (RTRIM
(CHAR (MONTH (ncef.nc_submit_date_time)))) = 2 THEN LTRIM (RTRIM (CHAR
(MONTH (ncef.nc_submit_date_time)))) ELSE '0' || LTRIM (RTRIM (CHAR (MONTH
(ncef.nc_submit_date_time)))) END ) || (CASE WHEN LENGTH (RTRIM (CHAR (DAY
(ncef.nc_submit_date_time)))) = 2 THEN LTRIM (RTRIM (CHAR (DAY (ncef.nc_submit_date_time))))
ELSE '0' || LTRIM (RTRIM (CHAR (DAY (ncef.nc_submit_date_time)))) END )
|| (CASE WHEN LENGTH (RTRIM (CHAR (HOUR (ncef.nc_submit_date_time)))) =
2 THEN LTRIM (RTRIM (CHAR (HOUR (ncef.nc_submit_date_time)))) ELSE '0'
|| LTRIM (RTRIM (CHAR (HOUR (ncef.nc_submit_date_time)))) END ) || (CASE
WHEN LENGTH (RTRIM (CHAR (MINUTE (ncef.nc_submit_date_time)))) = 2 THEN
LTRIM (RTRIM (CHAR (MINUTE (ncef.nc_submit_date_time)))) ELSE '0' || LTRIM
(RTRIM (CHAR (MINUTE (ncef.nc_submit_date_time)))) END ), RTRIM (CHAR (ncef.fair_id)),
RTRIM (COALESCE (s.name, '')), RTRIM (COALESCE (s.address1, '')), RTRIM
(COALESCE (s.address2, '')), RTRIM (COALESCE (s.city, '')), RTRIM (COALESCE
(s.state, '')), RTRIM (COALESCE (s.postalcode, '')), RTRIM (COALESCE (rtrim
(char (s.id)), '')), SUBSTR (CHAR (f.delivery_date), 1, 4) || SUBSTR (CHAR
(f.delivery_date), 6, 2) || SUBSTR (CHAR (f.delivery_date), 9, 2), SUBSTR
(CHAR (f.pickup_date), 1, 4) || SUBSTR (CHAR (f.pickup_date), 6, 2) ||
SUBSTR (CHAR (f.pickup_date), 9, 2), RTRIM (COALESCE (c.first_name, '')),
RTRIM (COALESCE (c.last_name, '')), RTRIM (COALESCE (c.email, '')), RTRIM
(COALESCE (c.phone_number, '')), RTRIM (COALESCE (ncef.nc_school_address1,
'')), RTRIM (COALESCE (ncef.nc_school_address2, '')), RTRIM (COALESCE (ncef.nc_school_city,
'')), RTRIM (COALESCE (ncef.nc_school_state, '')), RTRIM (COALESCE (ncef.nc_school_zip,
'')), RTRIM (CHAR (YEAR (ncef.nc_fair_start_date))) || (CASE WHEN LENGTH
(RTRIM (CHAR (MONTH (ncef.nc_fair_start_date)))) = 2 THEN LTRIM (RTRIM
(CHAR (MONTH (ncef.nc_fair_start_date)))) ELSE '0' || LTRIM (RTRIM (CHAR
(MONTH (ncef.nc_fair_start_date)))) END ) || (CASE WHEN LENGTH (RTRIM (CHAR
(DAY (ncef.nc_fair_start_date)))) = 2 THEN LTRIM (RTRIM (CHAR (DAY (ncef.nc_fair_start_date))))
ELSE '0' || LTRIM (RTRIM (CHAR (DAY (ncef.nc_fair_start_date)))) END ),
RTRIM (CHAR (YEAR (ncef.nc_fair_end_date))) || (CASE WHEN LENGTH (RTRIM
(CHAR (MONTH (ncef.nc_fair_end_date)))) = 2 THEN LTRIM (RTRIM (CHAR (MONTH
(ncef.nc_fair_end_date)))) ELSE '0' || LTRIM (RTRIM (CHAR (MONTH (ncef.nc_fair_end_date))))
END ) || (CASE WHEN LENGTH (RTRIM (CHAR (DAY (ncef.nc_fair_end_date))))
= 2 THEN LTRIM (RTRIM (CHAR (DAY (ncef.nc_fair_end_date)))) ELSE '0' ||
LTRIM (RTRIM (CHAR (DAY (ncef.nc_fair_end_date)))) END ), RTRIM (COALESCE
(ncef.nc_not_scheduled, '')), RTRIM (COALESCE (CHAR (f.region_id), '')),
RTRIM (COALESCE (sp.first_name, '')), RTRIM (COALESCE (sp.last_name, ''))
FROM bkfair01.not_correct_email_forms ncef JOIN bkfair01.fair f ON ncef.fair_id
= f.id LEFT OUTER JOIN bkfair01.school s ON f.school_id = s.id LEFT OUTER
JOIN bkfair01.fair_cplist flist ON flist.fair_id = ncef.fair_id LEFT OUTER
JOIN bkfair01.chairperson c ON flist.chairperson_id = c.id LEFT OUTER JOIN
bkfair01.salesperson sp ON f.salesperson_id = sp.id WHERE DATE (ncef.nc_submit_date_time)
BETWEEN DATE ( CURRENT TIMESTAMP - 7 DAYS ) AND DATE ( CURRENT TIMESTAMP
- 1 DAYS );

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_cpt_salesperson ( salesperson_id, salesperson_first_name,
salesperson_last_name, salesperson_email, salesperson_phone ) AS SELECT
id, first_name, last_name, email, phone_number FROM bkfair01.salesperson
s;

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_ems_fairs ( fair_id, fair_status, fair_start_date,
fair_end_date, create_date ) AS SELECT id, fair_status, delivery_date,
pickup_date, create_date FROM bkfair01.fair WHERE DATE(create_date) BETWEEN
( CURRENT DATE - 4 DAYS ) AND ( CURRENT DATE - 1 DAYS ) UNION SELECT fair_id,
'1', '1900-01-01', '1900-01-01', create_date FROM bkfair01.cancelled_fairs
WHERE DATE(create_date) BETWEEN ( CURRENT DATE - 4 DAYS ) AND ( CURRENT
DATE - 1 DAYS );


COMMENT ON TABLE "BKFAIR01"."V_EMS_FAIRS" IS 'Sept. 23, 2013:EMS (eBook Management System) related view: this view allows for fair data daily capture from 1:00 AM to 2:00 AM created yesterday towards a new bookfair functionality for eBook gift coupon; fair_status = 1 indicates a cancelled fair.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_school_tax_selection_export ( school_id, tax_selection,
update_date ) AS SELECT DISTINCT sts.school_id, sts.tax_selection, sts.update_date
FROM bkfair01.school_tax_selection  sts LEFT OUTER JOIN bkfair01.fair 
  f ON sts.school_id = f.school_id WHERE sts.update_date > ( CURRENT DATE
- 7 DAYS ) AND f.product_id NOT IN ( 'TT', 'TM', 'BM', 'BE', 'BP', 'TB',
'BB', 'ME', 'MM', 'MB' );


COMMENT ON TABLE "BKFAIR01"."V_SCHOOL_TAX_SELECTION_EXPORT" IS 'Release 22; View to collect Tax School Selection delta data updated within the past 7 days towards export to CRM. Release 24: Add 3 new products to filter out: ME, MM, MB';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_ofe_export_rpt ( fair_id, ofe_start_date, ofe_end_date,
ofe_status, create_date, updated_date ) AS SELECT RTRIM ( CHAR ( fair_id
) ), REPLACE ( CHAR ( ofe_start_date ), '-', '' ), REPLACE ( CHAR ( ofe_end_date
), '-', '' ), ( CASE ofe_status WHEN 'Submitted' THEN 'S' WHEN 'Active'
THEN 'A' WHEN 'Cancelled' THEN 'X' WHEN 'Complete' THEN 'C' WHEN 'Removed'
THEN 'R' END ), REPLACE ( CHAR ( DATE ( create_date ) ), '-', '' ), REPLACE
( CHAR ( DATE ( updated_date ) ), '-', '' ) FROM bkfair01.online_shopping;


COMMENT ON TABLE "BKFAIR01"."V_OFE_EXPORT_RPT" IS 'Bkfair release 19, Nov. 2011; Reviewed in removing a few columns; Used by 2 scripts (...secondary and daily ofe); Release 24: Add status of -Removed-.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_volunteer_rpt ( id, fair_id, school_id, volunteer_first_name,
volunteer_last_name, volunteer_email, volunteer_phone, volunteer_contact_time,
volunteer_contact_method, volunteer_help_at_fair, volunteer_question, contact_me_ckbox,
volunteer_message, volunteer_submit_date, volunteer_optin_source, update_timestamp
) AS SELECT v.id, v.fair_id, v.school_id, v.first_name, v.last_name, v.email,
v.phone, v.contact_time, CASE v.contact_method WHEN 'P' THEN 'Phone' WHEN
'EM' THEN 'Email' WHEN 'ET' THEN 'Either' END CASE, v.help_at_fair, v.question,
v.contact_me_ckbox, v.message, v.submit_date, v.optin_source, v.update_timestamp
FROM bkfair01.volunteer_forms  v, bkfair01.fair  f WHERE v.submit_date
> ( CURRENT TIMESTAMP - 7 DAYS ) AND v.fair_id = f.id AND f.product_id
NOT IN ( 'BE', 'BM', 'BP', 'TM', 'TT', 'TB', 'BB', 'ME', 'MM', 'MB' );


COMMENT ON TABLE "BKFAIR01"."V_VOLUNTEER_RPT" IS 'Rel 19.7, May 2012; Reporting View towards data export to the Reporting database. The exporting script will select the last 7 days of data insertion as only data inserts are done at this time. Rel 23-24: Prevents Test Fairs from being exported.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_planner_goals_rpt ( fair_id, region_id, product_id,
school_id, prior_revenue, where_initial_goal_setup, percentage_flag, where_current_goal_setup,
display_goal, update_date ) AS SELECT f.id, f.region_id, f.product_id,
f.school_id, ff.incresing_revenue_prior, pg.where_initial_goal_setup, pg.percentage_flag,
pg.where_current_goal_setup, oh.book_fair_goal_ckbox, pg.update_date FROM
bkfair01.fair_planner_goals  pg, bkfair01.fair f, bkfair01.fair_financial_info
ff, bkfair01.online_homepage     oh WHERE pg.fair_id = f.id AND f.product_id
NOT IN ( 'BE', 'BM', 'BP', 'TM', 'TT', 'TB', 'BB', 'ME', 'MM', 'MB' ) AND
pg.fair_id = ff.fair_id AND pg.fair_id = oh.fair_id;


COMMENT ON TABLE "BKFAIR01"."V_PLANNER_GOALS_RPT" IS 'Release 24, Jan. 2014; Collect all planner goals data regarding mainly initial and current goal setup as well as its being displayed or not.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_planner_status_rpt ( fair_id, planner_status, update_date
) AS SELECT ps.fair_id, ps.status, ps.update_date FROM bkfair01.fair_planner_status
  ps, bkfair01.fair   f WHERE ps.fair_id = f.id AND f.product_id NOT IN
( 'BE', 'BM', 'BP', 'TM', 'TT', 'TB', 'BB', 'ME', 'MM', 'MB' );


COMMENT ON TABLE "BKFAIR01"."V_PLANNER_STATUS_RPT" IS 'Release 24, Jan. 2014; Collect all Fair Planner Program records as long as the fair.pickup_date is current or set in the future.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_planner_programs_rpt ( fair_id, region_id, product_id,
school_id, program_id ) AS SELECT f.id, f.region_id, f.product_id, f.school_id,
pp.program_id FROM bkfair01.fair_planner_programs  pp, bkfair01.fair  f
WHERE pp.user_selection = 'Y' AND pp.fair_id = f.id AND f.product_id NOT
IN ( 'BE', 'BM', 'BP', 'TM', 'TT', 'TB', 'BB', 'ME', 'MM', 'MB' );

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_lu_planner_programs_rpt ( program_id, program_title
) AS SELECT id, title FROM bkfair01.planner_programs;


COMMENT ON TABLE "BKFAIR01"."V_LU_PLANNER_PROGRAMS_RPT" IS 'Release 24, Jan. 2014; Collect all Planner Program Reference records';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_planner_user_added_tasks_rpt ( fair_id, region_id,
product_id, school_id, task_id, task_title, task_start_date ) AS SELECT
f.id, f.region_id, f.product_id, f.school_id, pe.task_id, pe.title, pe.start_date
FROM bkfair01.fair f, bkfair01.fair_planner_events  pe WHERE pe.task_type
= 'UTASK' AND pe.fair_id = f.id AND f.product_id NOT IN ( 'BE', 'BM', 'BP',
'TM', 'TT', 'TB', 'BB', 'ME', 'MM', 'MB' );


COMMENT ON TABLE "BKFAIR01"."V_PLANNER_USER_ADDED_TASKS_RPT" IS 'Release 24, Jan. 2014; Collect all Fair Planner Task records as long as the fair.pickup_date is not older than 30 days ago and the task type is equal to UTASK.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_cpt_fair ( fair_id, school_id, reg_id, salesperson_id,
fair_start_date, fair_end_date, product_id, branch_id ) AS SELECT f.id,
f.school_id, f.region_id, f.salesperson_id, f.delivery_date, f.pickup_date,
f.product_id, f.branch_id FROM bkfair01.fair f;


COMMENT ON TABLE "BKFAIR01"."V_CPT_FAIR" IS 'Modified in Release 24: Added the column Branch_id. Import fair Data.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_cpt_login( id, cp_id, fair_id, login_date, login_type
) AS SELECT id, chairperson_id, fair_id, login_date, login_type FROM fairdate_login_tracking
WHERE DATE(login_date) > ( CURRENT DATE - 7 DAYS );

SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_precoa_forward_rpt( id, fair_id, region_id, delivery_date,
precoa_fwd_date ) AS SELECT rp.id, f.id, f.region_id, f.delivery_date,
rp.precoa_fwd_date FROM bkfair01.recipient_position rp, bkfair01.fair f
WHERE DATE(rp.precoa_fwd_date) > ( CURRENT DATE - 7 DAYS ) AND rp.coa_type
= 'PRE' AND rp.fair_id = f.id AND f.product_id NOT IN ( 'BB', 'BE', 'BM',
'BP', 'MB', 'ME', 'MM', 'TB', 'TM', 'TT' );


COMMENT ON TABLE "BKFAIR01"."V_PRECOA_FORWARD_RPT" IS 'Bkfair release 25(BTS 2014-2015); Provides for PRE-COA records created within the past 7 days.';

COMMENT ON COLUMN "BKFAIR01"."V_PRECOA_FORWARD_RPT"."ID" IS 'Unique Identity generated ID coming from the table RECIPIENT_POSITION; It tracks the record number and is used solely to allow delta data export.';
SET CURRENT SCHEMA = "BKFAIR01";
SET CURRENT PATH = "SYSIBM","SYSFUN","SYSPROC","SYSIBMADM","BKFRPRD";
CREATE VIEW bkfair01.v_postcoa_forward_rpt( id, fair_id, region_id, delivery_date,
postcoa_fwd_date ) AS SELECT rp.id, f.id, f.region_id, f.delivery_date,
rp.postcoa_fwd_date FROM bkfair01.recipient_position rp, bkfair01.fair
f WHERE DATE(rp.postcoa_fwd_date) > ( CURRENT DATE - 7 DAYS ) AND rp.coa_type
= 'POST' AND rp.fair_id = f.id AND f.product_id NOT IN ( 'BB', 'BE', 'BM',
'BP', 'MB', 'ME', 'MM', 'TB', 'TM', 'TT' );


COMMENT ON TABLE "BKFAIR01"."V_POSTCOA_FORWARD_RPT" IS 'Bkfair release 25(BTS 2014-2015); Provides for POST-COA records created within the past 7 days.';

COMMENT ON COLUMN "BKFAIR01"."V_POSTCOA_FORWARD_RPT"."ID" IS 'Unique Identity generated ID coming from the table RECIPIENT_POSITION; It tracks the record number and is used solely to allow delta data export.';



