 ##########################################################################
# Author.....: SAMPATH MURUGESAN
# Name.......: New_Tab_access.sh
# Description:
# Created....: 2014-06-04
# Updated....: 2014-07-03 by Sampath Murgesan
# Updated....:
# Notes......: This Script checks for new table access for roles
# Usage......: sh [script] [DATBASE] [Schema] [New Table] 
# ...........:
##########################################################################

LOGPATH="/backups/logs"



  if [ $# -ne 3 ]; then
     echo "Script usage: sh [script] [Database] [Schema] [New Table]  "
        exit 1
         fi

         echo `date`
         db2 connect to $1
     echo "*** The following ROLE is for Read-Only access ***"
     echo "ENTER THE ROLE and press [ENTER]:"
     db2 -x "select rolename from syscat.roles where rolename not in ('SYSDEBUG','SYSTS_ADM','SYSTS_MGR','SYSTS_USR')" > $LOGPATH/temp_PREVIL_TABVIEW.log
      cat $LOGPATH/temp_PREVIL_TABVIEW.log
        read ROLE
         db2 "GRANT SELECT ON TABLE  $2.$3 TO $ROLE" > $LOGPATH/PREVIL_TABVIEW.log
         #db2 "select 'GRANT SELECT ON TABLE  UNPUBLISHED.$2 TO prod_supp_user" > PREVIL_TABVIEW.log
     echo "*** The following ROLE is for Read-Write access ***"
     echo "ENTER THE ROLE and press [ENTER]:"
     db2 -x "select rolename from syscat.roles where rolename not in ('SYSDEBUG','SYSTS_ADM','SYSTS_MGR','SYSTS_USR')" > $LOGPATH/temp_PREVIL_TABVIEW.log
      cat $LOGPATH/temp_PREVIL_TABVIEW.log
        read ROLE
          db2 "GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE  $2.$3 TO $ROLE" >> $LOGPATH/PREVIL_TABVIEW.log
          # db2 "GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE  BKFAIR01.$2 TO app_cpt_user" > PREVIL_TABVIEW.log

          MESG="PLEASE CHECK THE LOG FILE PREVIL_TABVIEW.log"
          echo $MESG

